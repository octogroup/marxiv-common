/**
 * The context contains _everything_ necessary to display a page
 */
import { HastElement, } from '@octogroup/hast-typescript';

import { MarXivSitemap, MarXivSitemapMinimum, MarXivAssets, Asset } from './sitemap';
import { PostedContentType, LicenseOption, ModerationStatus, FileType, } from './common';
import { SubmenuType, UserMenuType } from './menus';
import { WorkData, WorkTypeBadge, OctoWorkTypeAliases } from './works';
import { GenericContributor, Institution, Relationship, Citation, ConferencePaper, Database, CrossrefDate, Publisher, PeerReview, } from './submitForms';
import { SupportedLanguage, TranslatableMetadata } from './languages';

/**
 * Provide types for our VDOMData (context)
 */
export interface PageMetadataMinimum {
   title: string
   localURL: string
   depth: 'primary' | 'secondary' | 'tertiary'
}

/**
 * @param localURL In order to make the SPA more like an old-school webserver, we'll mint canonical URLs for our "pages".
 * @param noGutter If true, does not apply a gutter around the Body.
 * @param homepageHeader If true, uses the homepage header over the standard header.
 * @param noBots If true, tells search engines not to crawl/index the page
 * @param useWorkHead If true, provides Highwire Metatags for Google Scholar
 */
export interface PageMetadata extends PageMetadataMinimum {
   description: string
   bodyContent: HastElement
   submenuToDisplay: SubmenuType
   noGutter?: boolean
   homepageHeader?: boolean
   useWorkHead?: boolean
   urlParameters?: string // e.g. ?page=2
   noBots?: boolean
   isReactPage?: boolean
}

// Include Highwire Metatags for Google Scholar on Work pages
export interface UseWorkHead {
   useWorkHead: true
}
export type WorkMetadata = PageMetadata & UseWorkHead

// Active Work Data
export interface ActiveWorkData {
   // Database ID used to make DOIs and canonical URLs
   workID: string
   // OCTO Work Type tell us which submission-form the work uses, and which badge to display to the user on the work-page
   octoWorkType?: OctoWorkTypeAliases
   badgeType?: WorkTypeBadge
   // Moderation status tells us badge data and notices to display on edit forms
   moderationStatus?: ModerationStatus
   moderationMessage?: string
   // Crossref data
   postedContentType?: PostedContentType // Required for Crossref, if Posted Content
   // Basic information
   title?: string // Required for Crossref
   language?: string
   peerReviewed?: boolean
   subtitle?: string
   publisherDOI?: string
   // Original publication date
   originalPublicationDate?: CrossrefDate
   // Publisher
   publisher?: Publisher
   // License & copyright
   license?: LicenseOption
   copyrightHolder?: string
   // Description
   description?: string
   // Conference Paper
   conferencePaper?: ConferencePaper
   // Database
   database?: Database
   // Dataset
   parentDatabaseDOI?: string
   // Peer Review
   peerReview?: PeerReview
   // Contribututors
   contributors?: GenericContributor[]
   submittedBy?: string
   // Institutions
   institutions?: Institution[]
   // Relationships between works
   relationships?: Relationship[]
   // Citations
   citations?: Citation[]
   // MarXiv DOI
   marxivDOI?: string
   // File data
   fileType?: FileType
   fileSize?: string
   downloadLink?: string
   // VoR Cost
   vorCost?: string
   // Stats
   downloads?: number
   // If withdrawn
   withdrawnDate?: string
   withdrawnReason?: string
}

// export interface Pagination {
//    paginationData: PaginationData
//    paginationCBs: {
//       nextPageCB: (event: Event) => void,
//       previousPageCB: (event: Event) => void,
//    }
// }

/**
 * Define our user data for the context
 */
export type UserStatus = 'anonymous' | 'user' | 'admin' | 'org';

export interface UserType<Type> {
   userType: Type
}

/**
 * @param surName Crossref only requires a Surname, but we'll require both for OCTO user accounts.
 */
interface UserData {
   // Required fields
   userName: string
   emailAddress: string
   givenName: string
   surname: string
   // Alert the user if their account isn't verified
   emailVerified: boolean
   // Optional fields
   organization?: string
   jobFocus?: string[]
   orcid?: string
   country?: string
   newsletter?: boolean
}

export interface AnonymousUserData extends UserType<'anonymous'> {
}

export interface BasicUserData extends UserType<'user'>, UserData {
}

export interface AdminUserData extends UserType<'admin'>, UserData {
   emailVerified: true
}

export interface OrgUserData extends UserType<'org'>, UserData {
   emailVerified: true
}

export type GenericUserData = AnonymousUserData | BasicUserData | AdminUserData | OrgUserData;

export type GenericAuthenticatedUserData = BasicUserData | AdminUserData | OrgUserData;

// Add-on the sitemap, assets, and companion functions
interface _MinimalSitemap {
   minimalSitemap: MarXivSitemapMinimum
}
interface StaticAssets {
   assets: MarXivAssets
}
interface CurrentPage {
   currentPage: PageMetadataMinimum
}
interface CompanionFunctions {
   linkToAsset: (currentPage: PageMetadataMinimum) => (asset: Asset) => string
}

export const companionFns: CompanionFunctions = {
   linkToAsset: (currentPage: PageMetadataMinimum) => (asset: Asset) => {
      if (currentPage.depth === 'tertiary') {
         const prefix = '/../..';
         return prefix + asset.location;
      }
      else if (currentPage.depth === 'secondary') {
         const prefix = '/..';
         return prefix + asset.location;
      }
      else {
         // Default to primary
         return asset.location
      }
   }
}

// We need to pass around a sitemap, so we know the URL to apply for a give page
export interface MarXivDataWithoutSitemap extends StaticAssets, _MinimalSitemap, CurrentPage, CompanionFunctions {
   // To upload files, we need the file stream (or file location)
   // We also need the DOI (or some other identifier unique & known for each Work) to use as the container (folder) name
   // We also need a static Share Name (e.g. works, maybe?)
   // We should probably standardize file names to make retrieval easy.
   // To download files, we'll need to know the Share Name, DOI/directory name, and File Name. 
   userData: GenericUserData
   gtag: any
   requestedLanguage: SupportedLanguage // This needs to be set from the backend.
   // Work listing
   works: WorkData[]
   // Active work data (i.e. the work being submitted, moderated, or edited)
   workData: ActiveWorkData
   // The User's works for dashboards
   userWorks: ActiveWorkData[]
   // Menus
   userMenuToDisplay: UserMenuType
}
export interface MarXivDataWithoutTranslatables extends MarXivDataWithoutSitemap {
   sitemap: MarXivSitemap
}

export interface MarXivData extends MarXivDataWithoutTranslatables, TranslatableMetadata {}
