/**
 * Full Submission Guidelines
 */

import { HastElement, make, } from '@octogroup/hast-typescript';
import { MarXivDataWithoutSitemap } from './context';
import { makeCollapsibleBlock, } from './common';

/**
 * Import the dictionary
 * @param titleAndIntro Should be wrapped in a div
 * @param commonQuestions Should be wrapped in a div
 */
import { SubmissionGuidelinesContent } from './translatables';

export const makeSubmissionGuidelines = (context: MarXivDataWithoutSitemap) => (d: SubmissionGuidelinesContent): HastElement => make.element({tag: 'div', properties: { className: 'guidelines' }, children: [
   d.titleAndIntro,
   makeCollapsibleBlock(d.beforeSubmit),
   makeCollapsibleBlock(d.transferredCopyright),
   d.commonQuestions,
   makeCollapsibleBlock(d.post),
   makeCollapsibleBlock(d.license),
   makeCollapsibleBlock(d.preprint),
   makeCollapsibleBlock(d.cta),
   makeCollapsibleBlock(d.publish)
]})