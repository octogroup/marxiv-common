/**
 * Footer elements for all pages
 */

import { HastElement, make, } from '@octogroup/hast-typescript';

/**
 * Import local interfaces
 */
import { FooterInformation, Translatables } from './translatables';
import { MarXivDataWithoutSitemap } from './context';

/**
 * Common footer elements
 */
const makeMarXivLogo = (context: MarXivDataWithoutSitemap): HastElement => make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.marxivVert), className: 'marxivLogo' }, children: []});

const makeOctoLogo = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'a', properties: { href: d.commonReplacements.octoWebsite }, children: [
   make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.octoClearOrange), className: 'octoLogo' }, children: []})
]})

const makeDesktopFooter = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'desktopInfo flex all-child-span2 all-child-first' }, children: [
   makeOctoLogo(context)(d)
]})

const makeMobileFooter = (context: MarXivDataWithoutSitemap) => (footerInfo: FooterInformation): HastElement => make.element({ tag: 'div', properties: { className: 'mobileInfo flex all-parent-row center' }, children: [
   makeMarXivLogo(context),
   footerInfo.mobileFooterProduced
]})

/**
 * Social media icons
 */
const makeFacebookLogo = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { id: 'facebook', className: 'flex all-child-span1 all-child-first' }, children: [
   make.element({ tag: 'a', properties: { href: d.commonReplacements.octoFacebookLink }, children: [
      make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.facebookSquare), className: 'facebookLogo' }, children: []})
   ]})
]})

const makeTwitterLogo = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: {id: 'twitter', className: 'flex all-child-span1 all-child-second'}, children: [
   make.element({ tag: 'a', properties: { href: d.commonReplacements.octoTwiterLink }, children: [
      make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.twitterSquare), className: 'twitterLogo' }, children: []})
   ]})
]})

const makeSocialLinks = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { id: 'social', className: 'flex phone-parent-column all-parent-row all-child-span1 all-child-third center' }, children: [
   makeFacebookLogo(context)(d),
   makeTwitterLogo(context)(d)
]})

/**
 * Privacy Policy and License
 */
const makeTerms = (footerInfo: FooterInformation): HastElement => make.element({ tag: 'div', properties: { className: 'terms flex all-parent-column all-child-span3 all-child-second' }, children: [
   footerInfo.licenseText,
   footerInfo.privacyText
]})

/**
 * Assemble the completed generic footer
 */
const makeWrappedDesktopFooter = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'footerItems flex all-parent-row' }, children: [
   makeTerms(d.footerText),
   makeDesktopFooter(context)(d),
   makeSocialLinks(context)(d)
]})

export const makeFooter = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { id: 'footer' }, children: [
   makeMobileFooter(context)(d.footerText),
   makeWrappedDesktopFooter(context)(d)
]})

/**
 * Assemble the completed Homepage footer
 * @param homepageText Should be a few <p>s wrapped in a <div>
 */
const aboveFooterHome = (context: MarXivDataWithoutSitemap) => (footerInfoHomepage: FooterInformation): HastElement => make.element({ tag: 'div', properties: { id: 'homeFooterItems', className: 'flex all-parent-column tablet-portrait-parent-row' }, children: [
   make.element({ tag: 'div', properties: { className: 'logoWrapper flex all-child-span1 center' }, children: [
      make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.marxivLines), className: 'marxivLogoLines' }, children: []})
   ]}),
   make.element({ tag: 'div', properties: { className: 'homeFooter textWrapper teaser flex all-child-span5' }, children: [
      make.element({ tag: 'h3', properties: { className: 'homeFooter title' }, children: [
         make.text(footerInfoHomepage.homepageTitle)
      ]}),
      make.element({ tag: 'div', properties: { className: 'homeFooter text' }, children: [
         footerInfoHomepage.homepageText
      ]})
   ]})
]})

export const makeFooterHome = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { id: 'homeFooter' }, children: [
   aboveFooterHome(context)(d.footerText),
   makeMobileFooter(context)(d.footerText),
   makeWrappedDesktopFooter(context)(d)
]})