import { HastElement, } from '@octogroup/hast-typescript';
import { CollapsibleBlock, SimpleList, } from './common';
import { NameStyle, ContribRole, SharingLicenseOption, PolicyWorkType, } from './common';

// Common replacements
export interface CommonReplacements {
   costSavings: string
   downloadCount: string
   octo: string
   octoWebsite: string
   facebook: string
   octoFacebookLink: string
   twitter: string
   octoTwiterLink: string
   crossref: string
   crossrefWebsite: string
   slackLink: string
   // Form replacements
   page: string
   yes: string
   no: string
   reject: string
   approve: string
   cancel: string
   reset: string
   submit: string
   edit: string
   notSet: string
   selectAndDrag: string // Select and drag an item to reorder the list.
   optionalInformation: string
   months: {
      jan: string
      feb: string
      mar: string
      apr: string
      may: string
      jun: string
      jul: string
      aug: string
      sep: string
      oct: string
      nov: string
      dec: string
   }
}

export interface MainMenuText {
   home: string
   why: string
   papers: string
   orgs: string
   submit: string
   team: string
   ambassadors: string
   selfArchivingPolicies: string
   submissionGuidelines: string
   codeOfConduct: string
}

export interface UserMenuText {
   join: string
   login: string
   logout: string
   adminDashboard: string
   userDashboard: string
   orgDashboard: string
   account: string
}

export interface FooterInformation {
   mobileFooterProduced: HastElement
   licenseText: HastElement
   privacyText: HastElement
   homepageTitle: string
   homepageText: HastElement
}

// Header text
export interface BelowHeaderIcon {
   title: string // Why MarXiv?
   desktopSubtitle: string // How we increase impact
}
export interface HeaderText {
   why: BelowHeaderIcon
   submit: BelowHeaderIcon
   find: BelowHeaderIcon
   subtitle: string // The free repository for ocean and marine-climate science
   producedBy: string // Produced by OCTO
   subtitleProduced: string // The free repository for ocean and marine-climate science. Produced by OCTO.
}

export interface HomepageText {
   shareButtonTitle: string
   shareButtonSubtitle: string
   visitButtonTitle: string
   visitButtonSubtitle: string
   learnButtonTitle: string
   learnButtonSubtitle: string
   statSavedUpper: string
   statSavedLower: string
   statDownloadsUpper: string
   statDownloadsLower: string
   statsTitle: string
   statsSubtitle: string
   desktopSubmitTitle: string
   desktopSubmitSubtitle: string
   uploadText: string
   metadataText: string
   licenseText: string
   doneText: string
   submitText: string
   visitTitle: string
   visitSubtitle: string
   testamonialsTitle: string
   testamonial1Name: string
   testamonial1Text: string
   testamonial2Name: string
   testamonial2Text: string
   testamonial3Name: string
   testamonial3Text: string
}

// Team pages
export interface TeamMember {
   name: string
   title: string
}
export interface Ambassador {
   name: string
   institution: string
   country: string
   email: string
   twitter?: string
}
export interface AmbassadorEmeritus {
   name: string
   institution: string
}
// Static pages
export interface StaticPagesData {
   // Why MarXiv?
   whyTitle: string
   whyBody: HastElement
   // Team
   teamTitle: string
   teamContent: HastElement
   teamMarXivTitle: string
   teamMarXiv: TeamMember[]
   teamAdvisoryTitle: string
   teamAdvisorySubtitle: string
   teamAdvisory: TeamMember[]
   // Ambassadors
   ambassadorTitle: string
   ambassadorP1: string
   ambassadorP2: string
   ambassador2019Title: string
   ambassadors2019: Ambassador[]
   ambassador2018Title: string
   ambassador2018Subtitle: string
   ambassadorsEmeritus: AmbassadorEmeritus[]
   // Code of Conduct
   codeTitle: string
   codePledgeTitle: string
   codePledgeContent: HastElement
   codeStandardsTitle: string
   codeStandardsList1: SimpleList
   codeStandardsList2: SimpleList
   codeEnforcementTitle: string
   codeEnforcementContent: HastElement
   codeAttributionTitle: string
   codeAttributionContent: HastElement
   // Privacy Policy
   privacyTitle: string
   privacyContent: HastElement
   // MarXiv for Organizations
   orgsTitle: string
   orgsContent: HastElement
   // Types of Works Accepted (metadata link from homepage)
   worksTitle: string
   worksContent: HastElement
   // License Options
   licenseOptions : {
      title: string
      description: HastElement
      learnMore: string
      softwareTitle: string
      descriptions: {
         cc0: HastElement
         afl: HastElement
         ccBy: HastElement
         ccBySa: HastElement
         ccByNd: HastElement
         ccByNc: HastElement
         ccByNcSa: HastElement
         ccByNcNd: HastElement
         gpl: HastElement
         mit: HastElement
         ecl: HastElement
      }
   }
   // Done page with otter GIF
   doneGiphy: string
   // Common Confirmation Strings
   confirmSocialSharing: HastElement
   confirmSignature: HastElement
   // Confirmation page for new submissions
   confirmNewTitle: string
   confirmNewText: HastElement
   // Confirmation page for editing an approved submission
   confirmUpdatedTitle: string
   // Confirmation page for edits to rejected works
   confirmResubmitTitle: string
   confirmResubmitText: HastElement
   // Confirmation page for new user accounts
   confirmNewAccountTitle: string
   confirmNewAccountText: HastElement
   // Confirmation page for verifying an email address
   confirmVerifyEmailTitle: string
   confirmVerifyEmailText: HastElement
   // Error pages
   errors: {
      notFound: {
         title: string
         description: HastElement
      }
      accessDenied: {
         title: string
         description: HastElement
      }
      general: {
         title: string
         description: HastElement
      }
   }
}

export interface WorkListingText {
   workListingWelcome: string
   workListingSubtitle: string
   workListingSearch: string
   workListingSearchWorks: string
   workListingRecent: string
}

export interface SubmissionGuidelinesContent {
   titleAndIntro: HastElement
   beforeSubmit: CollapsibleBlock
   transferredCopyright: CollapsibleBlock
   commonQuestions: HastElement
   post: CollapsibleBlock
   license: CollapsibleBlock
   preprint: CollapsibleBlock
   cta: CollapsibleBlock
   publish: CollapsibleBlock
}

// Self-archiving policies
export interface Policy {
   forWorkType: PolicyWorkType,
   policy: HastElement,
   allowedLicenses: SharingLicenseOption[],
   source: string,
   coverPage?: string,
}

export interface Publisher {
   name: string,
   defaultPolicyPreprint: Policy,
   defaultPolicyPostprint: Policy,
}

export interface Journal {
   title: string,
   publisher: Publisher,
   overridingPreprintPolicy?: Policy,
   overridingPostprintPolicy?: Policy,
}

export interface SelfArchivingPoliciesPublishers {
   aaas: Publisher,
   ams: Publisher,
   brill: Publisher,
   cambridge: Publisher,
   cell: Publisher,
   csiro: Publisher,
   elsevier: Publisher,
   gsa: Publisher,
   ir: Publisher,
   nas: Publisher,
   nature: Publisher,
   nrc: Publisher,
   oxford: Publisher,
   royalSociety: Publisher,
   sage: Publisher,
   springer: Publisher,
   tandf: Publisher,
   tos: Publisher,
   wiley: Publisher,
}

export interface SelfArchivingPoliciesText {
   // Common strings
   source: string
   preprints: string
   postprints: string
   // Make cover pages
   coverpageDescriptionPreprint: string
   coverpageDescriptionPostprint: string
   // Make licenses
   licenseDescriptionAnyPreprint: string
   licenseDescriptionAnyPostprint: string
   licenseDescriptionNonePreprint: string
   licenseDescriptionNonePostprint: string
   licenseDescriptionLimitedPreprint: string
   licenseDescriptionLimitedPostprint: string
   // Main body content
   titleAndIntro: HastElement[]
   journals: Journal[]
   defaultPublisherPoliciesTitle: string
   journalPoliciesTitle: string
   bookChaptersPolicies: HastElement[]
}

// Works
export interface StaticWorkText {
   anonymousContrib: string
   doi: string
   pending: string
   vor: string
   downloads: string
   download: string
   openPDF: string
   peerReviewStatement: string
   description: string
   references: string
   relatedMaterials: string
   datasetCollection: string
   reasonTitle: string
   badges: {
      preprint: string
      postprint: string
      report: string
      thesis: string
      dissertation: string
      database: string
      dataset: string
      letter: string
      workingPaper: string
      peerReview: string
      journalArticle: string
      paper: string
      poster: string
      conferenceProceedings: string
      conferencePaper: string
      presentation: string
      supplementalInfo: string
      other: string
      // For OCTO Work Types
      reportPublished: string
      reportUnpublished: string
      copyrightedPaper: string
      oaPaper: string
   }
   licensesFull: {
      cc0: string
      afl: string
      ccby: string
      ccbysa: string
      ccbynd: string
      ccbync: string
      ccbyncsa: string
      ccbyncnd: string
      gpl: string
      mit: string
      ecl: string
      none: string
   }
   licensesAbbreviated: {
      cc0: string
      afl: string
      ccby: string
      ccbysa: string
      ccbynd: string
      ccbync: string
      ccbyncsa: string
      ccbyncnd: string
      gpl: string
      mit: string
      ecl: string
      none: string
   }
   licensesLinks: {
      cc0: string
      afl: string
      ccby: string
      ccbysa: string
      ccbynd: string
      ccbync: string
      ccbyncsa: string
      ccbyncnd: string
      gpl: string
      mit: string
      ecl: string
   }
   relationships: {
      relatedMaterial: string
      reviewOf: string
      commentOn: string
      replyTo: string
      derivedFrom: string
      basedOn: string
      basisFor: string
      preprintOf: string
      manuscriptOf: string
      translationOf: string
      replaces: string
      replacedBy: string
      sameAs: string
   }
   peerReview: {
      prePublication: string
      postPublication: string
      typeOfReview: string
      recommendation: string
      competingInterests: string
      type: {
         refereeReport: string
         editorReport: string
         authorComment: string
         communityComment: string
         aggregateReport: string
         recommendation: string
      }
      recommendationType: {
         majorRevision: string
         minorRevision: string
         reject: string
         resubmit: string
         accept: string
         acceptReservations: string
      }
   }
   moderation: {
      pending: string
      approved: string
      rejected: string
      withdrawn: string
   }
   userDashboardTitle: string
}

export interface DynamicWorkText {
   copyright: {
      none: HastElement
      licensed: HastElement
   }
   tombstoneNotice: HastElement
}

export interface WorkText {
   static: StaticWorkText
   dynamic: DynamicWorkText
}

// Share / Submit Guide
export interface SubmitGuideText {
   // Splash page
   spashDescription: string
   splashLogin: string
   splashRegister: string
   // Common elements
   shareSomethingElse: string
   shareTitle: string
   shareSubtitle: string
   // Published or Not
   share1OptionUnpublished: string
   share1OptionPublished: string
   // Options 2
   share2OptionReport: string
   share2OptionThesis: string
   share2OptionConference: string
   share2OptionDataset: string
   // Options 3
   share3OptionPoster: string
   share3OptionLetter: string
   share3OptionPaper: string
   share3OptionReview: string
   share3OptionReturn: string
   // OA or Paywalled paper
   shareOA: string
   sharePaywalled: string
   // Published Paper options
   sharePublishedOptions: HastElement
   // Report options
   shareReportOptions: HastElement
   shareReportUnpublished: string
   shareReportPublished: string
   // Conference options
   shareConferenceOptions: HastElement
   shareConferenceUnpublished: string
   shareConferencePublished: string
   // Dataset and Database options
   shareDatabaseOptions: HastElement
   shareDatabase: string
   shareDataset: string
}

// Forms
export interface JobOptions {
   researcher: string
   conservationist: string
   undergrad: string
   grad: string
   student: string
   comms: string
   teaching: string
   naturalRes: string
   projectManager: string
   policyAnalysis: string
   consultant: string
   policy: string
   fisher: string
   regulator: string
   recFisher: string
   citSci: string
   dataScience: string
   chem: string
   atmos: string
   fisheries: string
   biology: string
   psychology: string
   political: string
   econ: string
   lobby: string
   otherSocialSci: string
   otherNatSci: string
   otherInter: string
   otherNonSci: string
   journalist: string
   charity: string
   military: string
   extraction: string
}

export interface RegistrationFormText {
   // Note that we won't translate the Country or Timezone drop-downs
   username: string
   emailAddress: string
   confirmEmailAddress: string
   password: string
   confirmPassword: string
   givenName: string
   surName: string
   organization: string
   jobFocus: string
   orcid: string
   country: string
   timezone: string
   newsletter: string
   terms: HastElement
   // Defaults
   usernameDefault: string
   emailAddressDefault: string
   passwordDefault: string
   givenNameDefault: string
   surNameDefault: string
   organizationDefault: string
   orcidDefault: string
}



export interface SubmissionFormText {
   topDisclaimer: HastElement
   titles: {
      preprint: string
      postprint: string
      openAccess: string
      unCopyrighted: string
      unpublishedReport: string
      publishedReport: string
      thesis: string
      database: string
      dataset: string
      peerReview: string
      workingPaper: string
      letter: string
      posterPresentationSI: string
      publishedConference: string
   }
   files: {
      // generic: {
      //    formTitle: string
      //    dragAndDrop: string
      //    or: string
      //    chooseFile: string
      //    miniDescription: string
      // }
      pdf: {
         formTitle: string
         dragAndDrop: string
         or: string
         choosePDF: string
         miniDescription: string
      }
      uploadingFile: string
      uploadingError: string
      cancelUpload: string
      removeFile: string
      // dataset: {
      //    formTitle: string
      //    dragAndDrop: string
      //    chooseCompressedArchive: string
      //    miniDescription: string
      // }
      editDescription: string
   }
   basic: {
      formTitle: string
      formTitleDataset: string
      formTitleDatabase: string
      formMinidescription: string
      title: {
         fieldTitle: string
         default: string
         defaultDataset: string
         defaultDatabase: string
      }
      publisherDOI: {
         fieldTitle: string
         default: string
      }
      type: {
         fieldTitle: string
         preprint: string
         workingPaper: string
         letter: string
         thesis: string
         report: string
         other: string
      }
      peerReview: {
         fieldTitle: string
         yes: string
         no: string
      }
      originalPublicationDate: {
         fieldTitle: string
         year: {
            fieldTitle: string
            default: string
         }
         month: {
            fieldTitle: string
            default: string
         }
         day: {
            fieldTitle: string
            default: string
         }
      }
      description: {
         fieldTitle: string
         default: string
         defaultDatabase: string
         defaultDataset: string
      }
      // database: {
      //    createdDate: string
      //    updatedDate: string
      // }
      optional: {
         fieldTitle: string
         miniDescription: string
         language: {
            fieldTitle: string
         }
         subtitle: {
            fieldTitle: string
            default: string
         }
      }
   }
   contributors: {
      formTitle: string
      description: HastElement
      previewDefault: string
      contributor: {
         formTitle: string
         add: string
         affiliation: {
            fieldTitle: string
            default: string
         }
         suffix: {
            fieldTitle: string
            default: string
         }
         nameStyle: {
            fieldTitle: string
            default: NameStyle
            western: string
            eastern: string
            islensk: string
            givenOnly: string

         }
         role: {
            fieldTitle: string
            default: ContribRole
            author: string
            editor: string
            chair: string
            reviewer: string
            reviewerAst: string
            statsReviewer: string
            externalReviewer: string
            reader: string
            translator: string
         }
         order: string
         firstAuthor: string
         additionalAuthor: string
      }
      organization: {
         isOrg: string
         fieldTitle: string
         default: string
      }
   }
   // publisher: {
   //    formTitle: string
   //    description: string
   //    descriptionDatabase: string
   //    name: {
   //       formTitle: string
   //       default: string
   //    }
   //    location: {
   //       formTitle: string
   //       default: string
   //    }
   // }
   // citations: {
   //    formTitle: string
   //    description: HastElement
   //    previewDefault: string
   //    citation: {
   //       formTitle: string
   //       add: string
   //       doi: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       isbn: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       issn: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       articleTitle: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       journalTitle: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       surname: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       volume: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       issue: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       page: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       year: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       edition: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       component: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       seriesTitle: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       volumeTitle: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       unstructured: {
   //          fieldTitle: string
   //          default: string
   //       }
   //    }
   // }
   // institutions: {
   //    formTitle: string
   //    description: HastElement
   //    previewDefault: string
   //    institution: {
   //       formTitle: string
   //       add: string
   //       name: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       acronym: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       location: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       department: {
   //          fieldTitle: string
   //          default: string
   //       }
   //    }
   // }
   relationships: {
      formTitle: string
      description: HastElement
      previewDefault: string
      relationship: {
         formTitle: string
         add: string
         doi: {
            fieldTitle: string
            default: string
         }
         typeOfRelationship: {
            fieldTitle: string
            relatedMaterial: string
            reviewOf: string
            commentOn: string
            replyTo: string
            derivedFrom: string
            basedOn: string
            basisFor: string
            preprintOf: string
            manuscriptOf: string
            translationOf: string
            replacedBy: string
            replaces: string
            sameAs: string
         }
      }
   }
   license: {
      formTitle: string
      description: HastElement
      descriptionPostprint: HastElement
      license: {
         formTitle: string
         cc0: string
         afl: string
         ccBy: string
         ccBySa: string
         ccByNd: string
         ccByNc: string
         ccByNcSa: string
         ccByNcNd: string
         gpl: string
         mit: string
         ecl: string
         none: string
      }
      noLicense: string // Since you are not granting others a license to this work, please enter the copyright holder below. This should not be your publisher.
      copyrightHolder: {
         fieldTitle: string
         default: string
      }
   }
   support: {
      formTitle: string
      paypalLink: string
      p1: string
      button: string
      p2: string
   }
   // peerReview: {
   //    formTitle: string
   //    description: string
   //    prePost: {
   //       fieldTitle: string
   //       prePublication: string
   //       postPublication: string
   //    }
   //    doiOfReviewedWork: {
   //       fieldTitle: string
   //       default: string
   //    }
   //    type: {
   //       fieldTitle: string
   //       refereeReport: string
   //       editorReport: string
   //       authorComment: string
   //       communityComment: string
   //       aggregateReport: string
   //       recommendation: string
   //    }
   //    recommendation: {
   //       fieldTitle: string
   //       majorRevision: string
   //       minorRevision: string
   //       reject: string
   //       resubmit: string
   //       accept: string
   //       acceptReservations: string
   //    }
   //    competingInterests: {
   //       fieldTitle: string
   //       default: string
   //    }
   // }
   // databaseDOI: {
   //    formTitle: string
   //    description: HastElement
   //    databaseDOI: {
   //       fieldTitle: string
   //       default: string
   //    }
   // }
   // conferenceEvent: {
   //    formTitle: string
   //    description: string
   //    conferenceName: {
   //       fieldTitle: string
   //       default: string
   //    }
   //    theme: {
   //       fieldTitle: string
   //       default: string
   //    }
   //    acronym: {
   //       fieldTitle: string
   //       default: string
   //    }
   //    number: {
   //       fieldTitle: string
   //       default: string
   //    }
   //    sponsor: {
   //       fieldTitle: string
   //       default: string
   //    }
   //    location: {
   //       fieldTitle: string
   //       default: string
   //    }
   //    dates: {
   //       formTitle: string
   //       startYear: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       startMonth: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       startDay: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       endYear: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       endMonth: {
   //          fieldTitle: string
   //          default: string
   //       }
   //       endDay: {
   //          fieldTitle: string
   //          default: string
   //       }
   //    }
   // }
   // conferencePaper: {
   //    formTitle: string
   //    description: string
   //    type: {
   //       fieldTitle: string
   //       fullText: string
   //       abstract: string
   //    }
   //    firstPage: {
   //       fieldTitle: string
   //       default: string
   //    }
   //    lastPage: {
   //       fieldTitle: string
   //       default: string
   //    }
   // }
   assignDOI: {
      formTitle: string
      description: HastElement
      marxivDOI: {
         fieldTitle: string
         default: string
      }
      doiReplacement: string
      marxivPrefix: string
   }
}

export interface EditFormText {
   titles: {
      preprint: string
      postprint: string
      openAccess: string
      unCopyrighted: string
      unpublishedReport: string
      publishedReport: string
      thesis: string
      database: string
      dataset: string
      peerReview: string
      workingPaper: string
      letter: string
      posterPresentationSI: string
      publishedConference: string
   }
   rejectedNote: HastElement
   editNote: HastElement
}

export interface ModerateFormText {
   titles: {
      preprint: string
      postprint: string
      openAccess: string
      unCopyrighted: string
      unpublishedReport: string
      publishedReport: string
      thesis: string
      database: string
      dataset: string
      peerReview: string
      workingPaper: string
      letter: string
      posterPresentationSI: string
      publishedConference: string
   }
   messageToSubmitter: {
      formTitle: string
      description: HastElement
      message: {
         fieldTitle: string
         default: string
      }
   }
   moderateExtras: {
      formTitle: string
      miniDescription: string
   }
   octoWorkType: {
      fieldTitle: string
      badgeTypeFieldTitle: string
      // Options are the same as badges
   }
   vorCost: {
      fieldTitle: string
      default: string
   }
}

export interface FormMessageText {
   errors: {
      usernameOrEmail: string
      usernameTaken: string
      emailInUse: string
      emailAddress: string
      emailAddressNoMatch: string
      passwordNoMatch: string
      legacyDOI: string
      relationshipType: string
      contribsRequired: string
      doi: string
      passwordShort: string
      passwordLong: string
      passwordInvalid: string
      surname: string
      givenName: string
      newsletter: string
      title: string
      description: string
      year: string
      month: string
      day: string
      date: string
      orcid: string
      license: string
      copyright: string
      generalInvlidShortText: string
      fileTypePDF: string
      fileTypeTarball: string
      fileTypeEither: string
      fileMissing: string
      // File upload errors
      fileInvalid: string
      fileUploadError: string
      // Moderation Form
      generalRequired: string
   }
   confirmations: {
      // File upload
      fileSaved: string
   }
   loading: {
      loading: string
      form: string
      dashboard: string
      page: string
      users: string
      works: string
   }
}

export interface FormText {
   join: {
      formTitle: string
      description: string
      jobOptions: JobOptions
      fields: RegistrationFormText
   }
   login: {
      formTitle: string
      usernameOrEmail: string
      usernameOrEmailDefault: string
   }
   logout: {
      formTitle: string
   }
   resetPassword: {
      formTitle: string
      description: string
   }
   userAccount: {
      title: string
      jobFocus: string
      editButton: string
      editTitle: string
   }
   grecaptcha: {
      terms: HastElement
      errorMessage: string
      isBotMessage: string
   }
   dashboards: {
      applyFilters: string
      removeFilters: string
      loadMoreWorks: string
      loadMoreUsers: string
      userFilters: string
      workFilters: string
      emailVerificationStatus: string
      workID: string
      moderationStatus: string
      postedContentType: string
      marxivDOI: string
      newsletter: string
      hasContributorSurname: string
      submittedBy: string
      displayWorks: string
      displayUsers: string
   }
   // Form Messages
   messages: FormMessageText
   // Submission forms
   submit: SubmissionFormText
   edit: EditFormText
   moderate: ModerateFormText
}

export interface HeadData {
   title: string
   description: string
}

export interface TranslatablePageMetadata {
   homepage: HeadData
   whyMarXiv: HeadData
   team: HeadData
   ambassadors: HeadData
   workListing: HeadData
   forOrgs: HeadData
   submit: HeadData
   selfArchivingPolicies: HeadData
   submissionGuidelines: HeadData
   // Google Search
   googleSearch: HeadData
   // Minor pages
   codeOfConduct: HeadData
   privacyPolicy: HeadData
   celebrate: HeadData
   licenses: HeadData
   typesOfWork: HeadData
   // User menu
   join: HeadData
   login: HeadData
   resetPassword: HeadData
   logout: HeadData
   // User pages
   user: {
      dashboard: HeadData
      account: HeadData
      editAccount: HeadData
   }
   // General confirmation pages
   confirm: {
      newSubmission: HeadData
      updatedWork: HeadData
      resubmit: HeadData
      newAccount: HeadData
      verifyEmail: HeadData
   }
   // Upload works
   upload: {
      preprint: HeadData
      postprint: HeadData
      openAccess: HeadData
      unCopyrighted: HeadData
      unpublishedReport: HeadData
      thesis: HeadData
      database: HeadData
      dataset: HeadData
      posterPresentationSI: HeadData
      letter: HeadData
      workingPaper: HeadData
      peerReview: HeadData
      publishedReport: HeadData
      publishedConferencePaper: HeadData
   }
   // Edit works
   edit: {
      preprint: HeadData
      postprint: HeadData
      openAccess: HeadData
      unCopyrighted: HeadData
      unpublishedReport: HeadData
      thesis: HeadData
      database: HeadData
      dataset: HeadData
      posterPresentationSI: HeadData
      letter: HeadData
      workingPaper: HeadData
      peerReview: HeadData
      publishedReport: HeadData
      publishedConferencePaper: HeadData
   }
   // Moderate works
   moderate: {
      preprint: HeadData
      postprint: HeadData
      openAccess: HeadData
      unCopyrighted: HeadData
      unpublishedReport: HeadData
      thesis: HeadData
      database: HeadData
      dataset: HeadData
      posterPresentationSI: HeadData
      letter: HeadData
      workingPaper: HeadData
      peerReview: HeadData
      publishedReport: HeadData
      publishedConferencePaper: HeadData
   }
   // Error pages
   error: {
      notFound: HeadData
      accessDenied: HeadData
      uhoh: HeadData
   }
   // Admin Dashboard
   admin: HeadData
   adminEditAccount: HeadData
}

// Combine all our translation data for a specific language implementation
export interface Translatables {
   commonReplacements: CommonReplacements
   mainMenuText: MainMenuText
   userMenuText: UserMenuText
   footerText: FooterInformation
   headerText: HeaderText
   pages: {
      homepage: HomepageText
      staticPages: StaticPagesData
      workListing: WorkListingText
      submissionGuidelines: SubmissionGuidelinesContent
      selfArchivingPolicies: SelfArchivingPoliciesText
      submitGuide: SubmitGuideText
   }
   pageMetadata: TranslatablePageMetadata
   works: WorkText
   forms: FormText
}