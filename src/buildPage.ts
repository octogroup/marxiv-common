/**
 * Build VDOM for the requested page
 */
import { HastBody, HastElement, HastHead, make, HastRoot, makeHastRoot, } from '@octogroup/hast-typescript';
import { MarXivDataWithoutTranslatables, PageMetadata, WorkMetadata, } from './context';
import { makeHead, } from './head';
import { makeHeader, makeHeaderHome, } from './header';
import { makeFooter, makeFooterHome } from './footer';
import { Translatables } from './translatables';
import { SupportedLanguage } from './languages';

/**
 * Make page data
 */
export const makeHastBody = (content: HastElement[]): HastBody => ({
   type: 'element',
   tagName: 'body',
   children: content
})

export const applyGutter = (bodyContent: HastElement): HastElement => make.element({ tag: 'div', properties: { className: 'gutter' }, children: [
   bodyContent
]})

/**
 * Make a page from PageMetadata
 */
export const makePage = (context: MarXivDataWithoutTranslatables) => (d: Translatables) => (pageMetadata: PageMetadata): { head: HastHead, body: HastBody } => {
   // Make the HastBody
   const makeBody = (context: MarXivDataWithoutTranslatables) => (d: Translatables) => (pageMetadata: PageMetadata): HastBody => {
      if (pageMetadata.noGutter) {
         if (pageMetadata.homepageHeader) {
            // Use a homepage header
            return makeHastBody([
               makeHeaderHome(context)(d),
               pageMetadata.bodyContent,
               makeFooterHome(context)(d),
            ])
         }
         else {
            // Use a normal header
            return makeHastBody([
               makeHeader(context)(d)(pageMetadata.submenuToDisplay),
               pageMetadata.bodyContent,
               makeFooter(context)(d),
            ])
         }
      }
      else {
         // Apply the gutter
         if (pageMetadata.homepageHeader) {
            // Use a homepage header
            return makeHastBody([
               makeHeaderHome(context)(d),
               applyGutter(pageMetadata.bodyContent),
               makeFooterHome(context)(d),
            ])
         }
         else {
            // Use a normal header
            return makeHastBody([
               makeHeader(context)(d)(pageMetadata.submenuToDisplay),
               applyGutter(pageMetadata.bodyContent),
               makeFooter(context)(d),
            ])
         }
      }
   }
   // Return the [ HastHead, HastBody ]
   return {
      head: makeHead(context)(pageMetadata),
      body: makeBody(context)(d)(pageMetadata),
   }
}

/**
 * Buid an HTMLDoc
 */
export const buildHastRoot = (pageContent: { head: HastHead, body: HastBody }) => (lang: SupportedLanguage): HastRoot => makeHastRoot(pageContent)(lang);

/**
 * Make Work page (or return a 404 if it does not exist)
 */
export const makeWorkPageFromSitemap = (context: MarXivDataWithoutTranslatables) => (d: Translatables) => (reqPath: string): { head: HastHead, body: HastBody } => {
   // Loop over the workMetadata and match on local paths
   let pageFound: WorkMetadata[] = new Array;
   for (let i = 0; i < context.sitemap.works.works.length; i++) {
      let work = context.sitemap.works.works[i];
      if (work.localURL === reqPath) {
         pageFound.push(work);
      }
   }
   // If we didn't find the page, throw an error
   if (pageFound.length === 0) {
      throw new Error('Requested Work page does not exist.')
   }
   else if (pageFound.length === 1) {
      return makePage(context)(d)(pageFound[0]);
   }
   else {
      throw new Error('Requested path matched on more than one Work localURL.')
   }
}

export const makePageFromSitemap = (reqPath: string) => (context: MarXivDataWithoutTranslatables) => (d: Translatables): { head: HastHead, body: HastBody } => {
   switch (reqPath) {
      // Homepage
      case context.sitemap.homepage.localURL:
         return makePage(context)(d)(context.sitemap.homepage);
      // Work Listing
      case context.sitemap.works.workListing.localURL:
         return makePage(context)(d)(context.sitemap.works.workListing);
      // Why MarXiv
      case context.sitemap.why.whyMarXiv.localURL:
         return makePage(context)(d)(context.sitemap.why.whyMarXiv);
      // MarXiv Team
      case context.sitemap.why.team.localURL:
         return makePage(context)(d)(context.sitemap.why.team);
      // MarXiv Ambassadors
      case context.sitemap.why.ambassadors.localURL:
         return makePage(context)(d)(context.sitemap.why.ambassadors);
      // MarXiv for Orgs
      case context.sitemap.forOrgs.localURL:
         return makePage(context)(d)(context.sitemap.forOrgs);
      // Privacy Policy
      case context.sitemap.privacyPolicy.localURL:
         return makePage(context)(d)(context.sitemap.privacyPolicy);
      // Celebrate
      case context.sitemap.celebrate.localURL:
         return makePage(context)(d)(context.sitemap.celebrate);
      // License Options
      case context.sitemap.licenses.localURL:
         return makePage(context)(d)(context.sitemap.licenses);
      // Work Metadata (types of works accepted)
      case context.sitemap.worksAccepted.localURL:
         return makePage(context)(d)(context.sitemap.worksAccepted);
      // Code of Conduct
      case context.sitemap.share.codeOfConduct.localURL:
         return makePage(context)(d)(context.sitemap.share.codeOfConduct);
      // Self-archiving Policies
      case context.sitemap.share.selfArchivingPolicies.localURL:
         return makePage(context)(d)(context.sitemap.share.selfArchivingPolicies);
      // Submission Guidelines
      case context.sitemap.share.submissionGuidelines.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuidelines);
      // Join
      case context.sitemap.join.localURL:
         return makePage(context)(d)(context.sitemap.join);
      // Login
      case context.sitemap.login.localURL:
         return makePage(context)(d)(context.sitemap.login);
      // Logout
      case context.sitemap.logout.localURL:
         return makePage(context)(d)(context.sitemap.logout);
      // Reset password
      case context.sitemap.resetPassword.localURL:
         return makePage(context)(d)(context.sitemap.resetPassword);
      // User dashboard
      case context.sitemap.user.dashboard.localURL:
         return makePage(context)(d)(context.sitemap.user.dashboard);
      // User account
      case context.sitemap.user.account.localURL:
         return makePage(context)(d)(context.sitemap.user.account);
      // Edit user account
      case context.sitemap.user.editAccount.localURL:
         return makePage(context)(d)(context.sitemap.user.editAccount);
      // Confirm New Submission
      case context.sitemap.confirm.newSubmission.localURL:
         return makePage(context)(d)(context.sitemap.confirm.newSubmission);
      // Confirm Updated Work
      case context.sitemap.confirm.updatedWork.localURL:
         return makePage(context)(d)(context.sitemap.confirm.updatedWork);
      // Confirm Resubmit
      case context.sitemap.confirm.resubmit.localURL:
         return makePage(context)(d)(context.sitemap.confirm.resubmit);
      // Confirm New Account
      case context.sitemap.confirm.newAccount.localURL:
         return makePage(context)(d)(context.sitemap.confirm.newAccount);
      // Confirm Verify Email
      case context.sitemap.confirm.verifyEmail.localURL:
         return makePage(context)(d)(context.sitemap.confirm.verifyEmail);
      // Share Splash PAge
      case context.sitemap.share.splash.localURL:
         return makePage(context)(d)(context.sitemap.share.splash);
      // Submission Guide - Page 1
      case context.sitemap.share.submissionGuide1.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuide1);
      // Submission Guide - Published Options
      case context.sitemap.share.submissionGuidePublishedOptions.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuidePublishedOptions);
      // Submission Guide - Published & Copyrighted Options
      case context.sitemap.share.submissionGuidePublishedCopyrighted.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuidePublishedCopyrighted);
      // Submission Guide - Page 2
      case context.sitemap.share.submissionGuide2.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuide2);
      // Submission Guide - Report Uptions
      case context.sitemap.share.submissionGuideReportOptions.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuideReportOptions);
      // Submission Guide - Conference Options
      case context.sitemap.share.submissionGuideConferenceOptions.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuideConferenceOptions);
      // Submission Guide - Dataset & Database Options
      // case context.sitemap.share.submissionGuideDatasetOptions.localURL:
      //    return makePage(context)(d)(context.sitemap.share.submissionGuideDatasetOptions);
      // Submission Guide - Page 3
      case context.sitemap.share.submissionGuide3.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuide3);
      // ------------------------
      // Upload Works - Preprint
      case context.sitemap.submit.preprint.localURL:
         return makePage(context)(d)(context.sitemap.submit.preprint);
      // Upload Works - Postprint
      case context.sitemap.submit.postprint.localURL:
         return makePage(context)(d)(context.sitemap.submit.postprint);
      // Upload Works - Open Access
      case context.sitemap.submit.openAccess.localURL:
         return makePage(context)(d)(context.sitemap.submit.openAccess);
      // Upload Works - Un-Copyrighted
      case context.sitemap.submit.unCopyrighted.localURL:
         return makePage(context)(d)(context.sitemap.submit.unCopyrighted);
      // Upload Works - Unpublished Report
      case context.sitemap.submit.unpublishedReport.localURL:
         return makePage(context)(d)(context.sitemap.submit.unpublishedReport);
      // Upload Works - Thesis
      case context.sitemap.submit.thesis.localURL:
         return makePage(context)(d)(context.sitemap.submit.thesis);
      // Upload Works - Database
      // case context.sitemap.submit.database.localURL:
      //    return makePage(context)(d)(context.sitemap.submit.database);
      // Upload Works - Dataset
      // case context.sitemap.submit.dataset.localURL:
      //    return makePage(context)(d)(context.sitemap.submit.dataset);
      // Upload Works - Poster, etc.
      case context.sitemap.submit.posterPresentationSI.localURL:
         return makePage(context)(d)(context.sitemap.submit.posterPresentationSI);
      // Upload Works - Letter
      case context.sitemap.submit.letter.localURL:
         return makePage(context)(d)(context.sitemap.submit.letter);
      // Upload Works - Working Paper
      case context.sitemap.submit.workingPaper.localURL:
         return makePage(context)(d)(context.sitemap.submit.workingPaper);
      // Upload Works - Peer Review
      // case context.sitemap.submit.peerReview.localURL:
      //    return makePage(context)(d)(context.sitemap.submit.peerReview);
      // Upload Works - Published Report
      // case context.sitemap.submit.publishedReport.localURL:
      //    return makePage(context)(d)(context.sitemap.submit.publishedReport);
      // Upload Works - Published Conference Paper
      // case context.sitemap.submit.publishedConferencePaper.localURL:
      //    return makePage(context)(d)(context.sitemap.submit.publishedConferencePaper);
      // --------------
      // Edit Works - Preprint
      case context.sitemap.edit.preprint.localURL:
         return makePage(context)(d)(context.sitemap.edit.preprint);
      // Edit Works - Postprint
      case context.sitemap.edit.postprint.localURL:
         return makePage(context)(d)(context.sitemap.edit.postprint);
      // Edit Works - Open Access
      case context.sitemap.edit.openAccess.localURL:
         return makePage(context)(d)(context.sitemap.edit.openAccess);
      // Edit Works - Un-Copyrighted
      case context.sitemap.edit.unCopyrighted.localURL:
         return makePage(context)(d)(context.sitemap.edit.unCopyrighted);
      // Edit Works - Unpublished Report
      case context.sitemap.edit.unpublishedReport.localURL:
         return makePage(context)(d)(context.sitemap.edit.unpublishedReport);
      // Edit Works - Thesis
      case context.sitemap.edit.thesis.localURL:
         return makePage(context)(d)(context.sitemap.edit.thesis);
      // Edit Works - Database
      // case context.sitemap.edit.database.localURL:
      //    return makePage(context)(d)(context.sitemap.edit.database);
      // // Edit Works - Dataset
      // case context.sitemap.edit.dataset.localURL:
      //    return makePage(context)(d)(context.sitemap.edit.dataset);
      // Edit Works - Poster, etc.
      case context.sitemap.edit.posterPresentationSI.localURL:
         return makePage(context)(d)(context.sitemap.edit.posterPresentationSI);
      // Edit Works - Letter
      case context.sitemap.edit.letter.localURL:
         return makePage(context)(d)(context.sitemap.edit.letter);
      // Edit Works - Working Paper
      case context.sitemap.edit.workingPaper.localURL:
         return makePage(context)(d)(context.sitemap.edit.workingPaper);
      // Edit Works - Peer Review
      // case context.sitemap.edit.peerReview.localURL:
      //    return makePage(context)(d)(context.sitemap.edit.peerReview);
      // Edit Works - Published Report
      // case context.sitemap.edit.publishedReport.localURL:
      //    return makePage(context)(d)(context.sitemap.edit.publishedReport);
      // Edit Works - Published Conference Paper
      // case context.sitemap.edit.publishedConferencePaper.localURL:
      //    return makePage(context)(d)(context.sitemap.edit.publishedConferencePaper);
      // --------------
      // Moderate Works - Preprint
      case context.sitemap.moderate.preprint.localURL:
         return makePage(context)(d)(context.sitemap.moderate.preprint);
      // Moderate Works - Postprint
      case context.sitemap.moderate.postprint.localURL:
         return makePage(context)(d)(context.sitemap.moderate.postprint);
      // Moderate Works - Open Access
      case context.sitemap.moderate.openAccess.localURL:
         return makePage(context)(d)(context.sitemap.moderate.openAccess);
      // Moderate Works - Un-Copyrighted
      case context.sitemap.moderate.unCopyrighted.localURL:
         return makePage(context)(d)(context.sitemap.moderate.unCopyrighted);
      // Moderate Works - Unpublished Report
      case context.sitemap.moderate.unpublishedReport.localURL:
         return makePage(context)(d)(context.sitemap.moderate.unpublishedReport);
      // Moderate Works - Thesis
      case context.sitemap.moderate.thesis.localURL:
         return makePage(context)(d)(context.sitemap.moderate.thesis);
      // Moderate Works - Database
      // case context.sitemap.moderate.database.localURL:
      //    return makePage(context)(d)(context.sitemap.moderate.database);
      // Moderate Works - Dataset
      // case context.sitemap.moderate.dataset.localURL:
      //    return makePage(context)(d)(context.sitemap.moderate.dataset);
      // Moderate Works - Poster, etc.
      case context.sitemap.moderate.posterPresentationSI.localURL:
         return makePage(context)(d)(context.sitemap.moderate.posterPresentationSI);
      // Moderate Works - Letter
      case context.sitemap.moderate.letter.localURL:
         return makePage(context)(d)(context.sitemap.moderate.letter);
      // Moderate Works - Working Paper
      case context.sitemap.moderate.workingPaper.localURL:
         return makePage(context)(d)(context.sitemap.moderate.workingPaper);
      // Moderate Works - Peer Review
      // case context.sitemap.moderate.peerReview.localURL:
      //    return makePage(context)(d)(context.sitemap.moderate.peerReview);
      // Moderate Works - Published Report
      // case context.sitemap.moderate.publishedReport.localURL:
      //    return makePage(context)(d)(context.sitemap.moderate.publishedReport);
      // Moderate Works - Published Conference Paper
      // case context.sitemap.moderate.publishedConferencePaper.localURL:
      //    return makePage(context)(d)(context.sitemap.moderate.publishedConferencePaper);
      // The requested page is either an individual Work, or the page does not exist.
      default:
         return makeWorkPageFromSitemap(context)(d)(reqPath);
   }
}
