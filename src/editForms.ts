/**
 * Work Edit Forms
 */
import { HastElement, make, } from '@octogroup/hast-typescript';
import { MarXivDataWithoutSitemap } from './context';
import { FormMessageText } from './translatables';

/**
 * Embed the React form
 */
export const makeReactEditForm = (d: FormMessageText) => (context: MarXivDataWithoutSitemap): HastElement => make.element({ tag: 'div', properties: { id: 'reactEditForm' }, children: [
   make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.reactEditFormBundle), type: 'module', async: 'async' }, children:[] }),
   make.element({ tag: 'h2', children: [
      make.text(d.loading.loading + ' ' + d.loading.form + '...')
   ]})
]});

/**
 * Edit Description
 */
// export const makeEditDescription = (context: MarXivDataWithoutSitemap) => (d: EditFormText): HastElement => {
//    if (context.workData.moderationStatus === 'rejected' && context.workData.moderationMessage) {
//       return d.rejectedNote
//    }
//    else {
//       return d.editNote
//    }
// }

/**
 * New PDF Upload
 */
// export const makeUploadFilePDFNewVersionContent = (d: SubmissionFormText): HastElement => make.element({ tag: 'div', properties: { className: 'field' }, children: [
//    // Description
//    make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.files.editDescription)
//       ]})
//    ]}),
//    // File upload
//    make.element({ tag: 'div', properties: { className: 'center' }, children: [
//       make.text(d.files.pdf.dragAndDrop)
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'separator'}, children: [
//       make.element({ tag: 'div', children: [
//          make.text(d.files.pdf.or)
//       ]}),
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'button blue choose' }, children: [
//       make.text(d.files.pdf.choosePDF)
//    ]})
// ]})
// export const makeUploadFilePDFNewVersion = (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.files.pdf.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.files.editDescription)
//    ]}),
//    content: makeUploadFilePDFNewVersionContent(d),
//    id: randomizeID('fileUpload'),
//    collapsible: 'maxFile',
// })

/**
 * New Dataset File Upload
 */
// export const makeUploadFileDatasetNewVersionContent = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => make.element({ tag: 'div', properties: { className: 'field' }, children: [
//    // Description
//    make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.files.editDescription)
//       ]})
//    ]}),
//    // File upload
//    make.element({ tag: 'div', properties: { className: 'center' }, children: [
//       make.text(d.files.dataset.dragAndDrop)
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'separator'}, children: [
//       make.element({ tag: 'div', children: [
//          make.text(d.files.generic.or)
//       ]}),
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'button blue choose' }, children: [
//       make.text(d.files.dataset.chooseCompressedArchive)
//    ]})
// ]});
// export const makeUploadFileDatasetNewVersion = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.files.dataset.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.files.editDescription)
//    ]}),
//    content: makeUploadFileDatasetNewVersionContent(context)(d),
//    id: randomizeID('fileUpload'),
//    collapsible: 'maxFile',
// })

/**
 * New Generic File Upload
 */
// export const makeUploadFileGenericNewVersionContent = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => make.element({ tag: 'div', properties: { className: 'field' }, children: [
//    // Description
//    make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.files.editDescription)
//       ]})
//    ]}),
//    // File upload
//    make.element({ tag: 'div', properties: { className: 'center' }, children: [
//       make.text(d.files.generic.dragAndDrop)
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'separator'}, children: [
//       make.element({ tag: 'div', children: [
//          make.text(d.files.generic.or)
//       ]}),
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'button blue choose' }, children: [
//       make.text(d.files.generic.chooseFile)
//    ]})
// ]});
// export const makeUploadFileGenericNewVersion = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.files.generic.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.files.editDescription)
//    ]}),
//    content: makeUploadFileGenericNewVersionContent(context)(d),
//    id: randomizeID('fileUpload'),
//    collapsible: 'maxFile',
// })

/**
 * Preprint Edit Form
 */
// export const makeEditFormPreprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.preprint)('editPreprint')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File New Version
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Open Access paper Edit
 */
// export const makeEditFormOA = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.openAccess)('editOA')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File New Version
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Postprint Edit
 */
// export const makeEditFormPostprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.postprint)('editPostprint')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information minus Peer Review (since it's a postprint, it's been peer-reviewed already)
//    makeBasicInformationPostedContentNoPeerReview(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePostprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Un-copyrighted paper edit
 */
// export const makeEditFormUncopyrighted = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.unCopyrighted)('editUncopyrighted')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicenseCopyrighted(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Unpublished Report edit
 */
// export const makeEditFormReportUnpublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.unpublishedReport)('editReportUnpublished')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Thesis / Dissertation edit
 */
// export const makeEditFormThesis = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.thesis)('editThesis')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Poster, Presentation, or Supplementary Information edit
 */
// export const makeEditFormPoster = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.posterPresentationSI)('editPoster')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Letter edit
 */
// export const makeEditFormLetter = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.letter)('editLetter')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Working Paper edit
 */
// export const makeEditFormWorkingPaper = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.workingPaper)('editWorkingPaper')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Published Report edit
 */
// export const makeEditFormReportPublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.publishedReport)('editReportPublished')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information minus Publisher's DOI and Peer-Review question
//    makeBasicInformationNoDOIorPeerReview(context)(d),
//    // Publisher
//    makePublisher(context)(d.forms.submit),
//    // Supporting Institutions
//    makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    makeCitations(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Assign DOI
//    customizeDOI(context)(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Published Conference Paper edit
 */
// export const makeEditFormConferencePaperPublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.publishedConference)('editConferencePaperPublished')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // BBasic Information minus Publisher's DOI and Peer-Review question
//    makeBasicInformationNoDOIorPeerReview(context)(d),
//    // Conference Paper Info
//    makeConferencePaper(context)(d.forms.submit),
//    // Conference Event Info
//    makeConferenceEvent(context)(d.forms.submit),
//    // Supporting Institutions
//    makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    makeCitations(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Assign DOI
//    customizeDOI(context)(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Peer Review edit
 */
// export const makeEditFormPeerReview = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.peerReview)('editPeerReview')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Upload PDF File
//    makeUploadFilePDFNewVersion(d.forms.submit),
//    // Basic Information minus Publisher's DOI and Peer-Review question
//    makeBasicInformationNoDOIorPeerReview(context)(d),
//    // Peer Review Information
//    makePeerReviewInformation(context)(d),
//    // Supporting Institutions
//    makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Hide relationships, since this will be handled automatically
//    // License
//    makeLicensePreprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Database, a collection for Datasets edit
 */
// export const makeEditFormDatabase = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.database)('editDatabase')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Basic Information for a Database
//    makeBasicInformationDatabase(context)(d.forms.submit),
//    // Publisher
//    makePublisherDatabase(context)(d.forms.submit),
//    // Supporting Institutions
//    makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    makeCitations(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Dataset, a child of a Database edit
 */
// export const makeEditFormDataset = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.edit.titles.dataset)('editDataset')([
//    // Description
//    makeEditDescription(context)(d.forms.edit),
//    // Database DOI
//    makeDatabaseDOIForm(context)(d.forms.submit),
//    // Dataset file upload
//    makeUploadFileDatasetNewVersion(context)(d.forms.submit),
//    // Basic Information for a Dataset
//    makeBasicInformationDataset(context)(d.forms.submit),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Form end
//    makeFormEnd(d),
// ]);
