/**
 * This file contains factories (makeSomething functions) that are common to pretty much every page
 */
import { HastElement, make, HastNode, } from '@octogroup/hast-typescript';
import { PageMetadataMinimum } from './context';
import { Translatables, CommonReplacements } from './translatables';
import { SupportedLanguage } from './languages';

/**
 * Define common types
 */
export type NameStyle = 'western' | 'eastern' | 'islensk' | 'givenOnly';

export type ContribRole = 'author' | 'editor' | 'chair' | 'reviewer' | 'reviewAssistant' | 'statsReviewer' | 'reviewerExternal' | 'reader' | 'translator';

export type ContribSequence = 'first' | 'additional';

export type WorkRelationship = 'relatedMaterial' | 'reviewOf' | 'commentOn' | 'replyTo' | 'derivedFrom' | 'basedOn' | 'basisFor' | 'preprintOf' | 'manuscriptOf' | 'translationOf' | 'replacedBy' | 'replaces' | 'sameAs';

export type PeerReviewType = 'refereeReport' | 'editorReport' | 'authorComment' | 'communityComment' | 'aggregateReport' | 'recommendation';

export type PeerReviewRecommendation = 'majorRevision' | 'minorRevision' | 'reject' | 'resubmit' | 'accept' | 'acceptReservations';

export type PeerReviewConducted = 'prePublication' | 'postPublication';

export type ModerationStatus = 'pending' | 'approved' | 'rejected' | 'withdrawn';

export type PolicyWorkType = 'preprint' | 'postprint';

export type LicenseOption = 'cc0' | 'afl' | 'ccBy' | 'ccBySa' | 'ccByNd' | 'ccByNc' | 'ccByNcSa' | 'ccByNcNd' | 'gpl' | 'mit' | 'ecl' | 'noLicense';

export type SharingLicenseOption = 'cc0' | 'afl' | 'ccBy' | 'ccBySa' | 'ccByNd' | 'ccByNc' | 'ccByNcSa' | 'ccByNcNd' | 'gpl' | 'mit' | 'ecl' | 'noLicense' | 'all' | 'noSharing';

export type PostedContentType = 'preprint' | 'workingPaper' | 'letter' | 'dissertation' | 'report' | 'other';

export type FileType = 'PDF' | 'ZIP' | 'TAR';

/**
 * Collapsible blocks
 */
export type Collapsible = 'maxFile' | 'maxBasic' | 'maxContribs' | 'maxCitations' | 'maxInstitutions' | 'maxRelationships' | 'maxLicense' | 'maxPublisher' | 'maxDatabaseDOI' | 'maxConferenceEvent' | 'maxConferencePaper' | 'maxPeerReview' | 'maxModeration' | 'maxBeforeSubmit' | 'maxTransferredCopyright' | 'maxPost' | 'maxLicenseOptions' | 'maxWhatPreprint' | 'maxCTA' | 'maxPublish' | 'maxMainMenu';
export interface CollapsibleBlock {
   title: string
   miniDescription: HastElement
   content: HastElement
   id: string
   collapsible: Collapsible
}

export const makeCollapsibleBlockMin = (b: CollapsibleBlock) => make.element({ tag: 'div', properties: {className: 'form wrapper min',  id: b.id + 'Min'},  children: [
   make.element({ tag: 'div', properties: { className: 'collapseBar flex all-parent-row', id: b.id + 'MinCollapseBar' }, children: [
      make.element({ tag: 'div', properties: { className: 'title flex all-child-span1' }, children: [
         make.text(b.title)
      ]}),
      make.element({ tag: 'div', properties: {className: 'icon center' }, children: [
         make.element({ tag: 'img', properties: { src: '/images/icons/plus-square', className: 'right maximize' }, children: []})
      ]})
   ]}),
   make.element({ tag: 'div', properties: { className: 'miniDescription' }, children: [
      b.miniDescription
   ]})
]})

export const makeCollapsibleBlockMax = (b: CollapsibleBlock) => make.element({ tag: 'div', properties: {className: 'form wrapper max', id: b.id + 'Max'}, children: [
   make.element({ tag: 'div', properties: { className: 'collapseBar flex all-parent-row', id: b.id + 'MaxCollapseBar' }, children: [
      make.element({ tag: 'div', properties: { className: 'title flex all-child-span1' }, children: [
         make.text(b.title)
      ]}),
      make.element({ tag: 'div', properties: {className: 'icon center' }, children: [
         make.element({ tag: 'img', properties: { src: '/images/icons/minus-square', className: 'right minimize' }, children: []})
      ]})
   ]}),
   make.element({ tag: 'div', properties: { className: 'content' }, children: [
      b.content
   ]})
]})

// Export a general builder
export const makeCollapsibleBlock = (b: CollapsibleBlock): HastElement => make.element({ tag: 'div', properties: { id: b.id }, children: [
   makeCollapsibleBlockMin(b),
   makeCollapsibleBlockMax(b)
]})

/**
 * Simple lists with titles
 * @param listTitle is wrapped in a <p>
 * @param listContent Each node in the array is wrapped in a <li>
 */
export interface SimpleList {
   listContent: Array<HastNode>
   listTitle?: HastNode
}

export const makeBulletList = (simpleList: SimpleList) => {
   let listElementsArray: HastElement[] = new Array;
   for (let i = 0; i < simpleList.listContent.length; i++) {
      let item = simpleList.listContent[i];
      if (item.type === 'element' || item.type === 'text') {
         listElementsArray.push(make.element({ tag: 'li', children: [item]}))
      }
      else {
         throw new Error('Invalid listContent item supplied to make a bullet list.')
      }
   }
   if (simpleList.listTitle) {
      return make.element({ tag: 'div', children: [
         make.element({ tag: 'p', children: [
            simpleList.listTitle
         ]}),
         make.element({ tag: 'ul', children: listElementsArray })
      ]})
   }
   else {
      return make.element({ tag: 'ul', children: listElementsArray })
   }
}

export const makeOrderedList = (simpleList: SimpleList) => {
   let listElementsArray: HastElement[] = new Array;
   for (let i = 0; i < simpleList.listContent.length; i++) {
      let item = simpleList.listContent[i];
      if (item.type === 'element' || item.type === 'text') {
         listElementsArray.push(make.element({ tag: 'li', children: [item]}))
      }
      else {
         throw new Error('Invalid listContent item supplied to make a bullet list.')
      }
   }
   if (simpleList.listTitle) {
      return make.element({ tag: 'div', children: [
         make.element({ tag: 'p', children: [
            simpleList.listTitle
         ]}),
         make.element({ tag: 'ol', children: listElementsArray })
      ]})
   }
   else {
      return make.element({ tag: 'ol', children: listElementsArray })
   }
}



/**
 * Make DOI links
 */
export const makeDOILinkElement = (doi: string): HastElement => {
   const link = 'https://doi.org/' + doi;
   return make.element({ tag: 'a', properties: { href: link }, children: [
      make.text(doi)
   ]})
} 

/**
 * Make general forms
 */
export const makeForm = (title: string) => (content: HastElement[]): HastElement => make.element({ tag: 'div', properties: { className: 'form wrapper' }, children: [
   make.element({ tag: 'div', properties: { className: 'title flex all-child-span1'}, children: [
      make.text(title)
   ]}),
   make.element({ tag: 'div', properties: { className: 'content' }, children: content })
]});

export const makeFormPage = (title: string)  => (id: string) => (content: HastElement[]): HastElement => make.element({ tag: 'form', properties: { id: id }, children: [
   make.element({ tag: 'h1', properties: { className: 'title'}, children: [
      make.text(title)
   ]}),
   make.element({ tag: 'div', properties: { className: 'content' }, children: content })
]});

/**
 * Make Collapsible Forms
 */
export const makeFormCollapsibleMin = (b: CollapsibleBlock): HastElement => make.element({ tag: 'div', properties: { className: 'form wrapper min' }, children: [
   make.element({ tag: 'div', properties: { className: 'collapseBar flex all-parent-row', id: b.id + 'Min' }, children: [
      make.element({ tag: 'div', properties: { className: 'title flex all-child-span1' }, children: [
         make.text(b.title)
      ]}),
      make.element({ tag: 'div', properties: { className: 'icon center'}, children: [
         make.element({ tag: 'img', properties: { src: '/images/icons/plus-square.svg', className: 'right maximize' }, children: []})
      ]}),
   ]}),
   make.element({ tag: 'div', properties: { className: 'miniDescription' }, children: [
      b.miniDescription
   ]}),
]});

export const makeFormCollapsibleMax = (b: CollapsibleBlock): HastElement => make.element({ tag: 'div', properties: { className: 'form wrapper max' }, children: [
   make.element({ tag: 'div', properties: { className: 'collapseBar flex all-parent-row', id: b.id + 'Max' }, children: [
      make.element({ tag: 'div', properties: { className: 'title flex all-child-span1' }, children: [
         make.text(b.title)
      ]}),
      make.element({ tag: 'div', properties: { className: 'icon center'}, children: [
         make.element({ tag: 'img', properties: { src: '/images/icons/minus-square.svg', className: 'right minimize' }, children: []})
      ]}),
   ]}),
   make.element({ tag: 'div', properties: { className: 'content' }, children: [
      b.content
   ]}),
]});

// Export a general builder
export const makeCollapsibleForm = (b: CollapsibleBlock): HastElement => make.element({ tag: 'div', properties: { id: b.id }, children: [
   makeFormCollapsibleMax(b),
   makeFormCollapsibleMin(b)
]});

/**
 * Form-end elements
 */
export const makeSubmitButton = (translatables: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'flex all-child-span1 center' }, children: [
   make.element({ tag: 'button', properties: { type: 'submit', className: 'button submit flex all-child-span1' }, children: [
      make.text(translatables.commonReplacements.submit)
   ]})
]});

/**
 * Reset buttons reset forms back to their initial (empty) states
 */
export const makeResetButton = (translatables: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'flex all-child-span1 center' }, children: [
   make.element({ tag: 'button', properties: { type: 'reset', className: 'button cancel' }, children: [
      make.text(translatables.commonReplacements.reset)
   ]})
]});

/**
 * Cancel button should (probably) take the user back to the last page they were on
 */
export const makeCancelButton = (translatables: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'flex all-child-span1 center' }, children: [
   make.element({ tag: 'button', properties: { type: 'reset', className: 'button cancel' }, children: [
      make.text(translatables.commonReplacements.cancel)
   ]})
]});

/**
 * Edit Button
 */
export const makeEditButton = (translatables: Translatables) => (location: string): HastElement => make.element({ tag: 'div', properties: { className: 'flex all-child-span1 center' }, children: [
   make.element({ tag: 'a', properties: { href: location, className: 'button blue' }, children: [
      make.text(translatables.commonReplacements.edit)
   ]})
]});

export const makeFormEnd = (translatables: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'form end flex all-parent-row' }, children: [
   makeCancelButton(translatables),
   makeSubmitButton(translatables)
]});

/**
 * Make a single radio-button input
 * 
 * @param label The user-facing label.
 * @param id HTML ID. DO NOT DUPLICATE IDs! (HTML IDs are unique, per the spec)
 * @param name Static name used to group radio selection elements together, such that only 1 element in the named group can be selected at at time.
 * @param value HTML form value upon submit.
 */
export interface RadioInput {
   label: string
   id: string
   name: string
   value: string
   selected?: boolean
}
export const makeRadioInput = (options: RadioInput): HastElement => {
   if (options.selected) {
      return make.element({ tag: 'div', properties: { className: 'radio' }, children: [
         make.element({ tag: 'input', properties: { type: 'radio', id: options.id, name: options.name, value: options.value, checked: options.selected }, children: [] }),
         make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
            make.text(options.label)
         ]})
      ]});
   }
   else {
      return make.element({ tag: 'div', properties: { className: 'radio' }, children: [
         make.element({ tag: 'input', properties: { type: 'radio', id: options.id, name: options.name, value: options.value }, children: [] }),
         make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
            make.text(options.label)
         ]})
      ]});
   }
}

/**
 * Make drop-down lists
 * 
 * @param text The user-facing label.
 * @param value HTML form value upon submit.
 * @param selected If true, displays the value as selected by default.
 */
export interface SelectOption {
   label: string
   value: string
   selected?: boolean
}

export const makeInnerOptions = (options: SelectOption[]): HastElement[] => {
   let elements: HastElement[] = new Array;
   for (let i = 0; i < options.length; i++) {
      if ( options[i].selected ) {
         elements.push( make.element({ tag: 'option', properties: { value: options[i].value, selected: true }, children: [
            make.text(options[i].label)
         ]}) )
      }
      else {
         elements.push( make.element({ tag: 'option', properties: { value: options[i].value }, children: [
            make.text(options[i].label)
         ]}) )
      }
   }
  return elements;
}
/**
 * Factories for drop-down lists
 * 
 * @param label The user-facing label.
 * @param id HTML ID. DO NOT DUPLICATE IDs! (HTML IDs are unique, per the spec)
 * @param value HTML form value upon submit.
 * @param required Should this be a required form element?
 * @param multi True allows the user to select multiple options, false allows only one (1) option.
 */
export interface SelectList {
   label: string
   id: string
   required: boolean
   multi: boolean
   options: SelectOption[]
   selected?: string | string[]
}

export const makeSelectList = (options: SelectList): HastElement => {
   // Separate single from multiple inputs
   if (options.multi) {
      // Separate required inputs
      if (options.required) {
         // Multiple, required
         return make.element({ tag: 'div', properties: { className: 'field selectListMulti flex all-parent-row center' }, children: [
            make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
               make.text(options.label)
            ]}),
            make.element({ tag: 'select', properties: { id: options.id, multiple: true, className: 'required' }, children: makeInnerOptions(options.options) })
         ]});
      }
      else {
         // Multiple, not required
         return make.element({ tag: 'div', properties: { className: 'field selectListMulti flex all-parent-row center' }, children: [
            make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
               make.text(options.label)
            ]}),
            make.element({ tag: 'select', properties: { id: options.id, multiple: true }, children: makeInnerOptions(options.options) })
         ]});
      }
   }
   else {
      // Separate required inputs
      if (options.required) {
         // Single, required
         return make.element({ tag: 'div', properties: { className: 'field selectList flex all-parent-row center' }, children: [
            make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
               make.text(options.label)
            ]}),
            make.element({ tag: 'select', properties: { id: options.id, className: 'required' }, children: makeInnerOptions(options.options) })
         ]});
      }
      else {
         // Single, not required
         return make.element({ tag: 'div', properties: { className: 'field selectList flex all-parent-row center' }, children: [
            make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
               make.text(options.label)
            ]}),
            make.element({ tag: 'select', properties: { id: options.id }, children: makeInnerOptions(options.options) })
         ]});
      }
   }
}

export interface SingleSelectList extends SelectList {
   multi: false
   selected: string
}
export const makeSingleSelectListWithDefault = (options: SingleSelectList): HastElement => {
   // Modify the selectOptions to add the selected attribute.
   let selectedOptions: SelectOption[] = new Array;
   for (let i = 0; i < options.options.length; i++) {
      let opt = options.options[i];
      if (opt.value === options.selected) {
         selectedOptions.push({
            ...opt,
            selected: true
         })
      }
      else {
         selectedOptions.push(opt)
      }
   }
   // Separate required inputs
   if (options.required) {
      return make.element({ tag: 'div', properties: { className: 'field selectList flex all-parent-row center' }, children: [
         make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
            make.text(options.label)
         ]}),
         make.element({ tag: 'select', properties: { id: options.id, className: 'required' }, children: makeInnerOptions(selectedOptions) })
      ]});
   }
   else {
      return make.element({ tag: 'div', properties: { className: 'field selectList flex all-parent-row center' }, children: [
         make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
            make.text(options.label)
         ]}),
         make.element({ tag: 'select', properties: { id: options.id }, children: makeInnerOptions(selectedOptions) })
      ]});
   }
}

/**
 * Make text input
 * 
 * @param label The user-facing label.
 * @param placeholder the user-facing placeholder text.
 * @param id Used for both HTML ID and Name properties. DO NOT DUPLICATE IDs! (HTML IDs are unique, per the spec)
 * @param required Should this be a required form element?
 * @param short Should this be a short-text input (true) or long-text input (false).
 * @param value If provided, the initial value of the input field.
 */
export interface TextInput {
   label: string
   placeholder: string
   id: string
   required: boolean
   short: boolean
   value?: string
}

export const makeTextInput = (options: TextInput) => {
   // Separate between long and short
   if (options.short) {
      // Separate required inputs
      if (options.required) {
         // Short, required
         if (options.value) {
            return make.element({ tag: 'div', properties: { className: 'field input shortText flex all-parent-row center' }, children: [
               make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
                  make.text(options.label)
               ]}),
               make.element({ tag: 'input', properties: { type: 'text', id: options.id, name: options.id, placeholder: options.placeholder, value: options.value, className: 'required' }, children: [] })
            ]});
         }
         else {
            return make.element({ tag: 'div', properties: { className: 'field input shortText flex all-parent-row center' }, children: [
               make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
                  make.text(options.label)
               ]}),
               make.element({ tag: 'input', properties: { type: 'text', id: options.id, name: options.id, placeholder: options.placeholder, className: 'required' }, children: [] })
            ]});
         }
      }
      else {
         // short, not required
         if (options.value) {
            return make.element({ tag: 'div', properties: { className: 'field input shortText flex all-parent-row center' }, children: [
               make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
                  make.text(options.label)
               ]}),
               make.element({ tag: 'input', properties: { type: 'text', id: options.id, name: options.id, placeholder: options.placeholder, value: options.value }, children: [] })
            ]});
         }
         else {
            return make.element({ tag: 'div', properties: { className: 'field input shortText flex all-parent-row center' }, children: [
               make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
                  make.text(options.label)
               ]}),
               make.element({ tag: 'input', properties: { type: 'text', id: options.id, name: options.id, placeholder: options.placeholder }, children: [] })
            ]});
         }
      }
   }
   else {
      // Separate required inputs
      if (options.required) {
         // Long, required
         if (options.value) {
            return make.element({ tag: 'div', properties: { className: 'field input longText flex all-parent-column' }, children: [
               make.element({ tag: 'label', properties: { htmlFor: options.id, className: 'flex all-child-span1' }, children: [
                  make.text(options.label)
               ]}),
               make.element({ tag: 'textarea', properties: { id: options.id, name: options.id, placeholder: options.placeholder, value: options.value, className: 'flex all-child-span100 required' }, children: [] })
            ]});
         }
         else {
            return make.element({ tag: 'div', properties: { className: 'field input longText flex all-parent-column' }, children: [
               make.element({ tag: 'label', properties: { htmlFor: options.id, className: 'flex all-child-span1' }, children: [
                  make.text(options.label)
               ]}),
               make.element({ tag: 'textarea', properties: { id: options.id, name: options.id, placeholder: options.placeholder, className: 'flex all-child-span100 required' }, children: [] })
            ]});
         }
      }
      else {
         // Long, not required
         if (options.value) {
            return make.element({ tag: 'div', properties: { className: 'field input longText flex all-parent-column' }, children: [
               make.element({ tag: 'label', properties: { htmlFor: options.id, className: 'flex all-child-span1' }, children: [
                  make.text(options.label)
               ]}),
               make.element({ tag: 'textarea', properties: { id: options.id, name: options.id, placeholder: options.placeholder, value: options.value, className: 'flex all-child-span100' }, children: [] })
            ]});
         }
         else {
            return make.element({ tag: 'div', properties: { className: 'field input longText flex all-parent-column' }, children: [
               make.element({ tag: 'label', properties: { htmlFor: options.id, className: 'flex all-child-span1' }, children: [
                  make.text(options.label)
               ]}),
               make.element({ tag: 'textarea', properties: { id: options.id, name: options.id, placeholder: options.placeholder, className: 'flex all-child-span100' }, children: [] })
            ]});
         }
      }
   }
}

/**
 * Randomize IDs
 */
export const randomizeID = (s: string): string => {
   // Generate a random number (e.g. 0.2894437916976895)
   const randomNumber = Math.random() * 1000000;
   // Cut off everything after the decimal point, so we'll have 6 degrees of freedom
   return s + '-' + randomNumber.toFixed();
}

/**
 * Convert a Crossref date month to a translatable string
 */
export const translateMonth = (d: CommonReplacements) => (s: string): string => {
   const monthNumber = Number(s);
   if (monthNumber === 1) {
      return d.months.jan
   }
   else if (monthNumber === 2) {
      return d.months.feb
   }
   else if (monthNumber === 3) {
      return d.months.mar
   }
   else if (monthNumber === 4) {
      return d.months.apr
   }
   else if (monthNumber === 5) {
      return d.months.may
   }
   else if (monthNumber === 6) {
      return d.months.jun
   }
   else if (monthNumber === 7) {
      return d.months.jul
   }
   else if (monthNumber === 8) {
      return d.months.aug
   }
   else if (monthNumber === 9) {
      return d.months.sep
   }
   else if (monthNumber === 10) {
      return d.months.oct
   }
   else if (monthNumber === 11) {
      return d.months.nov
   }
   else if (monthNumber === 12) {
      return d.months.dec
   }
   else {
      throw new Error('Invalid month string->num supplied for translation.')
   }
}

/**
 * Make local language path
 */
export const makeLocalPath = (lang: SupportedLanguage) => (pageData: PageMetadataMinimum): string => '/' + lang + pageData.localURL;