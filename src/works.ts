/**
 * Make Work pages to display to the end-user
 */

import { HastElement, make, } from '@octogroup/hast-typescript';
import { makeOrderedList, PostedContentType, LicenseOption, WorkRelationship, ModerationStatus, FileType, translateMonth, } from './common';
import { MarXivDataWithoutSitemap, ActiveWorkData } from './context';
import { wrapContribs } from './workListing';
import { GenericContributor, Institution, Relationship, Citation, ConferencePaper, Database, CrossrefDate, Publisher, PeerReview, } from './submitForms';

/**
 * Import translatable dictionaries
 */
import { StaticWorkText, DynamicWorkText, WorkText, CommonReplacements, Translatables } from './translatables';


export type WorkTypeSubmit = 'preprint' | 'postprint' | 'publishedReport' | 'unpublishedReport' | 'thesis' | 'database' | 'dataset' | 'letter' | 'workingPaper' | 'peerReview' | 'copyrightedPaper' | 'oaPaper' | 'poster' | 'publishedConferencePaper';
export type WorkTypeBadge = 'preprint' | 'postprint' | 'report' | 'thesis' | 'dissertation' | 'database' | 'dataset' | 'letter' | 'workingPaper' | 'peerReview' | 'journalArticle' | 'paper' | 'poster' | 'conferencePaper' | 'conferenceProceedings' | 'presentation' | 'supplementalInfo' | 'other';
export interface WorkType {
   submitFormType: WorkTypeSubmit
   badgeType: WorkTypeBadge
}
/**
 * OCTO Work Type
 * Allows moderator to set the Badge that will display on the Work page (and Work Listing).
 */
export type OctoWorkTypeAliases = 'preprint' | 'postprint' | 'reportPublished' | 'reportUnpublished' | 'thesis' | 'dissertation' | 'database' | 'dataset' | 'letter' | 'workingPaper' | 'peerReview' | 'copyrightedPaper' | 'oaPaper' | 'poster' | 'conferenceProceedings' | 'conferencePaper' | 'presentation' | 'supplementalInfo' | 'other';
export const convertIntoOctoWorkType = (type: OctoWorkTypeAliases) => (badge?: WorkTypeBadge): WorkType => {
   switch (type) {
      case 'preprint':
         return {
            submitFormType: 'preprint',
            badgeType: 'preprint'
         }
      case 'postprint':
         return {
            submitFormType: 'postprint',
            badgeType: 'postprint'
         }
      case 'reportPublished': {
         return {
            submitFormType: 'publishedReport',
            badgeType: 'report'
         }
      }
      case 'reportUnpublished':
         return {
            submitFormType: 'unpublishedReport',
            badgeType: 'report'
         }
      case 'thesis':
         return {
            submitFormType: 'thesis',
            badgeType: 'thesis'
         }
      case 'dissertation':
         return {
            submitFormType: 'thesis',
            badgeType: 'dissertation'
         }
      case 'database':
         return {
            submitFormType: 'database',
            badgeType: 'database'
         }
      case 'dataset':
         return {
            submitFormType: 'dataset',
            badgeType: 'dataset'
         }
      case 'letter':
         return {
            submitFormType: 'letter',
            badgeType: 'letter'
         }
      case 'workingPaper':
         return {
            submitFormType: 'workingPaper',
            badgeType: 'workingPaper'
         }
      case 'peerReview':
         return {
            submitFormType: 'peerReview',
            badgeType: 'peerReview'
         }
      case 'poster':
         return {
            submitFormType: 'poster',
            badgeType: 'poster'
         }
      case 'conferenceProceedings':
         return {
            submitFormType: 'publishedConferencePaper',
            badgeType: 'conferenceProceedings'
         }
      case 'conferencePaper':
         return {
            submitFormType: 'publishedConferencePaper',
            badgeType: 'conferencePaper'
         }
      case 'presentation':
         return {
            submitFormType: 'poster',
            badgeType: 'presentation'
         }
      case 'supplementalInfo':
         return {
            submitFormType: 'poster',
            badgeType: 'supplementalInfo'
         }
      case 'other':
         return {
            submitFormType: 'poster',
            badgeType: 'other'
         }
      case 'copyrightedPaper':
         if (badge) {
            return {
               submitFormType: 'copyrightedPaper',
               badgeType: badge
            }
         }
         else {
            throw new Error('Badge required.')
         }
      case 'oaPaper':
         if (badge) {
            return {
               submitFormType: 'oaPaper',
               badgeType: badge
            }
         }
         else {
            throw new Error('Badge required.')
         }
      default:
         throw new Error('Invalid OctoWorkType provided to the convert helper function.')
   }
};

// Moderation

/**
 * @param publicationDate 21 May 2019
 * @param publicationYear 2019
 * @param submittedBy Author who submitted the work
 * @param fileSize 8 MB, 1 GB, etc.
 */
export interface WorkData extends ActiveWorkData {
   // Database ID used to make DOIs and canonical URLs
   workID: string
   // OCTO Work Type tell us which submission-form the work uses, and which badge to display to the user on the work-page
   octoWorkType: OctoWorkTypeAliases
   badgeType: WorkTypeBadge
   // Moderation status tells us badge data and notices to display on edit forms
   moderationStatus: ModerationStatus
   moderationMessage?: string
   // Crossref data
   postedContentType?: PostedContentType // Required for Crossref, if Posted Content
   // Basic information
   title: string // Required for Crossref
   language: string
   peerReviewed?: boolean
   subtitle?: string
   publisherDOI?: string
   // Original publication date
   originalPublicationDate: CrossrefDate
   // Publisher
   publisher?: Publisher
   // License & copyright
   license: LicenseOption
   copyrightHolder?: string
   // Description
   description: string
   // Conference Paper
   conferencePaper?: ConferencePaper
   // Database
   database?: Database
   // Dataset
   parentDatabaseDOI?: string
   // Peer Review
   peerReview?: PeerReview
   // Contribututors
   contributors: GenericContributor[]
   submittedBy: string
   // Institutions
   institutions?: Institution[]
   // Relationships between works
   relationships?: Relationship[]
   // Citations
   citations?: Citation[]
   // The backend should send us formatted Citations as References
   references?: string[]
   // MarXiv DOI
   marxivDOI?: string
   // File data
   fileType?: FileType
   fileSize?: string
   downloadLink?: string
   // VoR Cost
   vorCost?: string
   // Stats
   downloads?: number
   // If withdrawn
   withdrawnDate?: string
   withdrawnReason?: string
}

/**
 * Make DOI links
 */
export const makeDOILink = (doi: string): HastElement => {
   const prefix = 'https://doi.org/';
   const link = prefix + doi;
   return make.element({ tag: 'a', properties: { href: link }, children: [
      make.text(link)
   ]})
}

/**
 * Format Title, Date, and Authorship
 */
export const formatOriginalPublicationDate = (d: CommonReplacements) => (date: CrossrefDate): string => {
   if (date.month && date.day) {
      return date.day + ' ' + translateMonth(d)(date.month) + ' ' + date.year
   }
   else if (date.month) {
      return translateMonth(d)(date.month) + ' ' + date.year
   }
   else {
      return date.year
   }
}
export const makeDate = (d: CommonReplacements) => (work: WorkData): HastElement => make.element({ tag: 'h4', properties: { className: 'publicationDate' }, children: [
   make.text( formatOriginalPublicationDate(d)(work.originalPublicationDate) )
]});

export const makeTitle = (work: WorkData): HastElement => make.element({ tag: 'h1', properties: { className: 'title' }, children: [
   make.text(work.title)
]});

export const makeAuthors = (d: StaticWorkText) => (work: WorkData): HastElement => make.element({ tag: 'div', properties: { className: 'authors flex all-parent-row' }, children: wrapContribs(d)(work.contributors) });

/**
 * DOI and VOR
 * New (unaccepted) papers will not have a DOI minted, so replace with the placeholder
 */
export const makeMarXivDOI = (d: StaticWorkText) => (work: WorkData): HastElement => {
   if (work.marxivDOI) {
      return make.element({ tag: 'div', properties: { className: 'paperDOI flex all-parent-row'}, children: [
         make.element({ tag: 'div', properties: { className: 'label' }, children: [
            make.text(d.doi)
         ]}),
         makeDOILink(work.marxivDOI)
      ]})
   }
   else {
      return make.element({ tag: 'div', properties: { className: 'paperDOI flex all-parent-row'}, children: [
         make.element({ tag: 'div', properties: { className: 'label' }, children: [
            make.text(d.doi)
         ]}),
         make.element({ tag: 'div', children: [
            make.text(d.pending)
         ]})
      ]})
   }
}

export const makeVoRDOI = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.publisherDOI) {
      return make.element({ tag: 'div', properties: { className: 'vorDOI flex all-parent-row'}, children: [
         make.element({ tag: 'div', properties: { className: 'label' }, children: [
            make.text(d.vor)
         ]}),
         makeDOILink(work.publisherDOI)
      ]})
   }
   else {
      return undefined
   }
}

/**
 * Translate
 */
export const translateWorkTypeBadge = (d: StaticWorkText) => (work: ActiveWorkData): string => {
   switch(work.badgeType) {
      case 'preprint':
         return d.badges.preprint
      case 'postprint':
         return d.badges.postprint
      case 'report':
         return d.badges.report
      case 'thesis':
         return d.badges.thesis
      case 'dissertation':
         return d.badges.dissertation
      case 'database':
         return d.badges.database
      case 'dataset':
         return d.badges.dataset
      case 'letter':
         return d.badges.letter
      case 'workingPaper':
         return d.badges.workingPaper
      case 'peerReview':
         return d.badges.peerReview
      case 'journalArticle':
         return d.badges.journalArticle
      case 'paper':
         return d.badges.paper
      case 'poster':
         return d.badges.poster
      case 'conferenceProceedings':
         return d.badges.conferenceProceedings
      case 'conferencePaper':
         return d.badges.conferencePaper
      case 'presentation':
         return d.badges.presentation
      case 'supplementalInfo':
         return d.badges.supplementalInfo
      case 'other':
         return d.badges.other
      default:
         throw new Error('Invalid badge type supplied to translate.')
   }
}

export const translateLicenseBadge = (d: StaticWorkText) => (work: ActiveWorkData): string => {
   switch (work.license) {
      case 'cc0':
         return d.licensesAbbreviated.cc0
      case 'afl':
         return d.licensesAbbreviated.afl
      case 'ccBy':
         return d.licensesAbbreviated.ccby
      case 'ccBySa':
         return d.licensesAbbreviated.ccbysa
      case 'ccByNd':
         return d.licensesAbbreviated.ccbynd
      case "ccByNc":
         return d.licensesAbbreviated.ccbync
      case "ccByNcSa":
         return d.licensesAbbreviated.ccbyncsa
      case "ccByNcNd":
         return d.licensesAbbreviated.ccbyncnd
      case 'gpl':
         return d.licensesAbbreviated.gpl
      case 'mit':
         return d.licensesAbbreviated.mit
      case 'ecl':
         return d.licensesAbbreviated.ecl
      case 'noLicense':
         return d.licensesAbbreviated.none
      default:
         throw new Error('Invalid license type supplied to translate.')
   }
}

export const translateLicenseName = (d: StaticWorkText) => (work: ActiveWorkData): string => {
   switch (work.license) {
      case 'cc0':
         return d.licensesFull.cc0
      case 'afl':
         return d.licensesFull.afl
         case 'ccBy':
         return d.licensesFull.ccby
      case 'ccBySa':
         return d.licensesFull.ccbysa
      case 'ccByNd':
         return d.licensesFull.ccbynd
      case "ccByNc":
         return d.licensesFull.ccbync
      case "ccByNcSa":
         return d.licensesFull.ccbyncsa
      case "ccByNcNd":
         return d.licensesFull.ccbyncnd
      case 'gpl':
         return d.licensesFull.gpl
      case 'mit':
         return d.licensesFull.mit
      case 'ecl':
         return d.licensesFull.ecl
      case 'noLicense':
         return d.licensesFull.none
      default:
         throw new Error('Invalid license name supplied to translate.')
   }
}

export const translateRelationships = (d: StaticWorkText) => (relationship: WorkRelationship): string => {
   switch (relationship) {
      case 'relatedMaterial':
         return d.relationships.relatedMaterial
      case 'reviewOf':
         return d.relationships.reviewOf
      case 'commentOn':
         return d.relationships.commentOn
      case 'replyTo':
         return d.relationships.replyTo
      case 'derivedFrom':
         return d.relationships.derivedFrom
      case "basedOn":
         return d.relationships.basedOn
      case "basisFor":
         return d.relationships.basisFor
      case "preprintOf":
         return d.relationships.preprintOf
      case 'manuscriptOf':
         return d.relationships.manuscriptOf
      case 'translationOf':
         return d.relationships.translationOf
      case 'replaces':
         return d.relationships.replaces
      case 'replacedBy':
         return d.relationships.replacedBy
      case 'sameAs':
         return d.relationships.sameAs
      default:
         throw new Error('Invalid related-material type supplied to translate.')
   }
}

export const translatePeerReviewType = (d: StaticWorkText) => (work: ActiveWorkData): string => {
   if (work.peerReview) {
      switch (work.peerReview.type) {
         case 'refereeReport':
            return d.peerReview.type.refereeReport
         case 'editorReport':
            return d.peerReview.type.editorReport
         case 'authorComment':
            return d.peerReview.type.authorComment
         case 'communityComment':
            return d.peerReview.type.communityComment
         case 'aggregateReport':
            return d.peerReview.type.aggregateReport
         case 'recommendation':
            return d.peerReview.type.recommendation
         default:
            throw new Error('Invalid peer-review type supplied to translate.')
      }
   }
   else {
      throw new Error('Work is not a Peer Review.')
   }
}

export const translatePeerReviewRecommendation = (d: StaticWorkText) => (work: ActiveWorkData): string => {
   if (work.peerReview) {
      switch (work.peerReview.recommendation) {
         case 'majorRevision':
            return d.peerReview.recommendationType.majorRevision
         case 'minorRevision':
            return d.peerReview.recommendationType.minorRevision
         case 'reject':
            return d.peerReview.recommendationType.reject
         case 'resubmit':
            return d.peerReview.recommendationType.resubmit
         case 'accept':
            return d.peerReview.recommendationType.accept
         case "acceptReservations":
            return d.peerReview.recommendationType.acceptReservations
         default:
            throw new Error('Invalid peer-review recommendation supplied to translate.')
      }
   }
   else {
      throw new Error('Work is not a Peer Review.')
   }
}

/**
 * Works submitted without a specific license (i.e. none) will have an undefined href.
 */
export const translateLicenseLink = (d: StaticWorkText) => (work: ActiveWorkData): string | undefined => {
   switch (work.license) {
      case 'cc0':
         return d.licensesLinks.cc0
      case 'afl':
         return d.licensesLinks.afl
      case 'ccBy':
         return d.licensesLinks.ccby
      case 'ccBySa':
         return d.licensesLinks.ccbysa
      case 'ccByNd':
         return d.licensesLinks.ccbynd
      case "ccByNc":
         return d.licensesLinks.ccbync
      case "ccByNcSa":
         return d.licensesLinks.ccbyncsa
      case "ccByNcNd":
         return d.licensesLinks.ccbyncnd
      case 'gpl':
         return d.licensesLinks.gpl
      case 'mit':
         return d.licensesLinks.mit
      case 'ecl':
         return d.licensesLinks.ecl
      case 'noLicense':
         return undefined
      default:
         throw new Error('Invalid license type supplied to translate an href.')
   }
}

/**
 * License, Type, and Copyright holder
 */
export const makeWorkTypeBadge = (d: StaticWorkText) => (work: WorkData): HastElement => make.element({ tag: 'div', properties: { className: 'type wrapper badge orange center' }, children: [
   make.element({ tag: 'div', properties: { className: 'type' }, children: [
      make.text( translateWorkTypeBadge(d)(work) )
   ]})
]})

export const makeWorkLicenseBadge = (d: StaticWorkText) => (work: WorkData): HastElement => make.element({ tag: 'div', properties: { className: 'license wrapper badge body center' }, children: [
   make.element({ tag: 'div', properties: { className: 'license' }, children: [
      make.text( translateLicenseBadge(d)(work) )
   ]})
]})

export const makeLicenseString = (d: DynamicWorkText) => (work: WorkData): HastElement => {
   if (work.license === 'noLicense') {
      return d.copyright.none
   }
   else {
      return d.copyright.licensed
   }
}

/**
 * Download count
 * Returns undefined for works without a download count.
 */
export const makeDownloadCountBadge = (context: MarXivDataWithoutSitemap) => (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.downloads) {
      return make.element({ tag: 'div', properties: { className: 'downloadsWrapper flex all-child-span1 center' }, children: [
         make.element({ tag: 'div', properties: { className: 'downloads badge grey flex all-parent-row' }, children: [
            make.element({ tag: 'div', properties: { className: 'icon wrapper' }, children: [
               make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.download), className: 'icon' }, children: []})
            ]}),
            make.element({ tag: 'div', properties: { className: 'count wrapper flex all-parent-column center' }, children: [
               make.element({ tag: 'div', properties: { className: 'count badge flex all-child-span1' }, children: [
                  make.text( work.downloads.toString() )
               ]}),
               make.element({ tag: 'div', properties: { className: 'string flex all-child-span1' }, children: [
                  make.text(d.downloads)
               ]})
            ]})
         ]})
      ]})
   }
   else {
      return undefined
   }
}

/**
 * Institutional information
 */
export const formatName = (institution: Institution): string => {
   if (institution.acronym) {
      return institution.name + " (" + institution.acronym + ").";
   }
   else {
      return institution.name + ".";
   }
}

export const formatInstitutionalInfo = (institutionList: Institution[]): HastElement => {
   const formattedInstitutions: HastElement[] = new Array;
   for (let i = 0; i < institutionList.length; i++) {
      // Name & Acronym
      let formattedName = formatName(institutionList[i]);
      if (institutionList[i].location && institutionList[i].department) {
         formattedInstitutions.push( make.element({ tag: 'div', children: [
            make.text(formattedName + " " + institutionList[i].department + ". " + institutionList[i].location + ".")
         ]}) );
      }
      else if (institutionList[i].location) {
         formattedInstitutions.push( make.element({ tag: 'div', children: [
            make.text(formattedName + " " + institutionList[i].location + ".")
         ]}) );
      }
      else if (institutionList[i].department) {
         formattedInstitutions.push( make.element({ tag: 'div', children: [
            make.text(formattedName + " " + institutionList[i].department + ".")
         ]}) );
      }
      else {
         formattedInstitutions.push( make.element({ tag: 'div', children: [
            make.text(formattedName)
         ]}) );
      }
   }
   // Return the formatted element
   return make.element({ tag: 'div', properties: { id: 'institutions' }, children: formattedInstitutions })
}

export const makeInstitutionalInfo = (work: WorkData): HastElement | undefined => {
   if (work.institutions) {
      return formatInstitutionalInfo(work.institutions)
   }
   else {
      return undefined
   }
}

/**
 * Tombstone (withdrawn) information
 */
export const makeTombstoneNotice = (d: DynamicWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.withdrawnDate) {
      return make.element({ tag: 'div', properties: { className: 'tombstoneNotice' }, children: [
         d.tombstoneNotice
      ]})
   }
   else {
      return undefined
   }
}

export const makeTombstoneReason = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.withdrawnDate && work.withdrawnReason) {
      return make.element({ tag: 'div', properties: { className: 'tombstoneReason' }, children: [
         make.element({ tag: 'h3', children: [
            make.text(d.reasonTitle)
         ]}),
         make.element({ tag: 'p', children: [
            make.text(work.withdrawnReason)
         ]})
      ]})
   }
   else {
      return undefined
   }
}

/**
 * Upper metadata wrappers
 */
export const makeRightWrapper = (context: MarXivDataWithoutSitemap) => (d: StaticWorkText) => (work: WorkData): HastElement => make.element({ tag: 'div', properties: { className: 'metadataWrapper flex all-parent-column all-child-span1' }, children: [
   make.element({ tag: 'div', properties: { className: 'metadata flex all-parent-column tablet-portrait-parent-row all-child-span1' }, children: [
      makeWorkTypeBadge(d)(work),
      makeWorkLicenseBadge(d)(work)
   ]}),
   makeDownloadCountBadge(context)(d)(work) as HastElement
]})

export const makeLeftWrapper = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (work: WorkData): HastElement => make.element({ tag: 'div', properties: { className: 'publicationDate wrapper' }, children: [
   makeDate(d.commonReplacements)(work),
   makeAuthors(d.works.static)(work),
   makeInstitutionalInfo(work) as HastElement,
   makeMarXivDOI(d.works.static)(work),
   makeVoRDOI(d.works.static)(work) as HastElement
]})

export const makeInfoAboveAbstract = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (work: WorkData): HastElement => make.element({ tag: 'div', children: [
   makeTitle(work),
   make.element({ tag: 'div', properties: { className: 'flex all-parent-column tablet-portrait-parent-row' }, children: [
      makeLeftWrapper(context)(d)(work),
      makeRightWrapper(context)(d.works.static)(work)
   ]})
]})

/**
 * Peer Review statement, as Crossref requires
 */
export const makePeerReviewStatement = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.peerReviewed) {
      return undefined
   }
   else {
      return make.element({ tag: 'div', properties: { className: 'peerReview' }, children: [
         make.text(d.peerReviewStatement)
      ]})
   }
}

/**
 * Description / Abstract
 */
export const makeDescriptionWrapper = (d: StaticWorkText) => (work: WorkData): HastElement => make.element({ tag: 'div', properties: { className: 'abstract wrapper' }, children: [
   make.element({ tag: 'h3', properties: { className: 'abstract' }, children: [
      make.text(d.description)
   ]}),
   make.element({ tag: 'p', properties: { className: 'abstract' }, children: [
      make.text(work.description)
   ]})
]})

/**
 * Download button
 */
export const makeDownloadButton = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.withdrawnDate || work.withdrawnReason) {
      // This work was withdrawn, so don't offer the download button
      return undefined
   }
   else {
      // Make the download link button
      if (work.downloadLink && work.fileType && work.fileSize) {
         return make.element({ tag: 'a', properties: { className: 'download button blue flex all-parent-column center normalWhiteLink', href: work.downloadLink }, children: [
            make.element({ tag: 'div', properties: { className: 'download' }, children: [
               make.text(d.download + ' (' + work.fileType + ', ' + work.fileSize + ')')
            ]})
         ]})
      }
      else if (work.downloadLink && work.fileType) {
         return make.element({ tag: 'a', properties: { className: 'download button blue flex all-parent-column center normalWhiteLink', href: work.downloadLink }, children: [
            make.element({ tag: 'div', properties: { className: 'download' }, children: [
               make.text(d.download + ' (' + work.fileType + ')')
            ]})
         ]}) 
      }
      else if (work.downloadLink) {
         return make.element({ tag: 'a', properties: { className: 'download button blue flex all-parent-column center normalWhiteLink', href: work.downloadLink }, children: [
            make.element({ tag: 'div', properties: { className: 'download' }, children: [
               make.text(d.download)
            ]})
         ]}) 
      }
      else {
         // If there is no download link. just don't print the button.
         return undefined
      }
   }
}

/**
 * Related Materials
 */
export const formatRelatedMaterials = (d: StaticWorkText) => (relationships: Relationship[]): HastElement => {
   let formattedRelatedMaterials: HastElement[] = new Array;
   for (let i = 0; i < relationships.length; i++) {
      const prefix: string = translateRelationships(d)(relationships[i].type) + ' ';
      const doiLink: string = 'https://doi.org/' + relationships[i].doi;
      formattedRelatedMaterials.push( make.element({tag: 'div', properties: { className: 'relationship' }, children: [
         make.text(prefix),
         make.element({ tag: 'a', properties: { href: doiLink }, children: [
            make.text(relationships[i].doi)
         ]})
      ]}) )
   }
   // Return the formatted relationships
   return make.element({ tag: 'div', properties: { id: 'relatedMaterials' }, children: [
      make.element({ tag: 'h2', children: [
         make.text(d.relatedMaterials)
      ]}),
      make.element({ tag: 'div', children: formattedRelatedMaterials })
   ]})
}

export const makeRelatedMaterials = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.relationships) {
      return formatRelatedMaterials(d)(work.relationships)
   }
   else {
      return undefined
   }
}

/**
 * Database-to-Dataset relationships
 */
export const makeDatasetToDatabase = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.parentDatabaseDOI) {
      return make.element({ tag: 'div', properties: { className: 'flex all-parent-row', id: 'parentDatabase' }, children: [
         make.element({ tag: 'div', properties: { className: 'label' }, children: [
            make.text(d.datasetCollection)
         ]}),
         makeDOILink(work.parentDatabaseDOI)
      ]})
   }
   else {
      return undefined
   }
}

/**
 * Peer Reviews
 */
export const formatTypeOfPeerReview = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.peerReview && work.peerReview.type) {
      return make.element({ tag: 'div', properties: { className: 'flex all-parent-row' }, children: [
         make.element({ tag: 'div', properties: { className: 'label' }, children: [
            make.text(d.peerReview.typeOfReview)
         ]}),
         make.element({ tag: 'div', children: [
            make.text( translatePeerReviewType(d)(work) )
         ]})
      ]})
   }
   else {
      return undefined
   }
}

export const formatPeerReviewRecommendation = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.peerReview && work.peerReview.recommendation) {
      return make.element({ tag: 'div', properties: { className: 'flex all-parent-row' }, children: [
         make.element({ tag: 'div', properties: { className: 'label' }, children: [
            make.text(d.peerReview.recommendation)
         ]}),
         make.element({ tag: 'div', children: [
            make.text( translatePeerReviewRecommendation(d)(work) )
         ]})
      ]})
   }
   else {
      return undefined
   }
}

export const formatCompetingInterests = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.peerReview && work.peerReview.competingInterestStatement) {
      return make.element({ tag: 'div', properties: { className: 'flex all-parent-row' }, children: [
         make.element({ tag: 'div', properties: { className: 'label' }, children: [
            make.text(d.peerReview.competingInterests)
         ]}),
         make.element({ tag: 'div', children: [
            make.text(work.peerReview.competingInterestStatement)
         ]})
      ]})
   }
   else {
      return undefined
   }
}

export const makePeerReviewInformation = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.peerReview && work.peerReview.conducted === 'prePublication') {
      return make.element({ tag: 'div', properties: { id: 'peerReviewInformation' }, children: [
         make.element({ tag: 'div', children: [
            make.text(d.peerReview.prePublication + ' '),
            makeDOILink(work.peerReview.doiofReviewedWork)
         ]}),
         formatTypeOfPeerReview(d)(work) as HastElement,
         formatPeerReviewRecommendation(d)(work) as HastElement,
         formatCompetingInterests(d)(work) as HastElement
      ]})
   }
   else if (work.peerReview && work.peerReview.conducted === 'postPublication') {
      return make.element({ tag: 'div', properties: { id: 'peerReviewInformation' }, children: [
         make.element({ tag: 'div', children: [
            make.text(d.peerReview.postPublication + ' '),
            makeDOILink(work.peerReview.doiofReviewedWork)
         ]}),
         formatTypeOfPeerReview(d)(work) as HastElement,
         formatPeerReviewRecommendation(d)(work) as HastElement,
         formatCompetingInterests(d)(work) as HastElement
      ]})
   }
   else {
      // This is not a peer-review
      return undefined
   }
}

/**
 * References / Citations
 */
export const formatReferences = (references: string[]): HastElement => {
   // Turn our array of strings into an array of text nodes, so our function can auto-format
   const nodeReferences = references.map( (value) => make.text(value) );
   return makeOrderedList({ listContent: nodeReferences })
}

export const makeReferences = (d: StaticWorkText) => (work: WorkData): HastElement | undefined => {
   if (work.references) {
      return make.element({ tag: 'div', properties: { id: 'references' }, children: [
         make.element({ tag: 'h2', children: [
            make.text(d.references)
         ]}),
         formatReferences(work.references)
      ]})
   }
   else {
      return undefined
   }
}

/**
 * PDF preview
 * 
 * For testing with Azure, the link should be:
 * <a href="./pdf/web/viewer.html?file=/html/pdf/test.pdf" target="_blank">Open PDF in a new window</a>
 * 
 * For local testing, the link should be:
 * <a href="./pdf/web/viewer.html?file=/pdf/test.pdf" target="_blank">Open PDF in a new window</a>
 */
export const makePDFPreview = (d: StaticWorkText) => (context: MarXivDataWithoutSitemap) => (work: WorkData): HastElement | undefined => {
   if (work.fileType === 'PDF' && work.downloadLink) {
      const viewerStarter = '/pdf/web/viewer.html?file=';
      const viewer = viewerStarter + work.downloadLink;
      return make.element({ tag: 'div', properties: { id: 'preview', className: 'preview flex all-parent-column center' }, children: [
         make.element({ tag: 'embed', properties: {id: 'embed', type: 'application/pdf', src: work.downloadLink }, children: []}),
         make.element({ tag: 'a', properties: { href: viewer, target: '_blank' }, children: [
            make.text(d.openPDF)
         ]}),
      ]})
   }
   else {
      return undefined
   }
}

/** 
 *  Make a standard Work page
 */
export const makeWorkPage = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (work: WorkData): HastElement => make.element({ tag: 'div', properties: { className: 'paper' }, children: [
   makeInfoAboveAbstract(context)(d)(work),
   makePeerReviewStatement(d.works.static)(work) as HastElement,
   makePeerReviewInformation(d.works.static)(work) as HastElement,
   makeDatasetToDatabase(d.works.static)(work) as HastElement,
   makeTombstoneNotice(d.works.dynamic)(work) as HastElement,
   makeTombstoneReason(d.works.static)(work) as HastElement,
   makeDescriptionWrapper(d.works.static)(work),
   makeDownloadButton(d.works.static)(work) as HastElement,
   makePDFPreview(d.works.static)(context)(work) as HastElement,
   makeLicenseString(d.works.dynamic)(work),
   makeRelatedMaterials(d.works.static)(work) as HastElement,
   makeReferences(d.works.static)(work) as HastElement
]})