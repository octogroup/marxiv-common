/**
 * Work Submission Forms
 */
import { LANGUAGES_LIST } from './langList';
import { HastElement, make, } from '@octogroup/hast-typescript';
import { SelectOption, NameStyle, ContribRole, ContribSequence, WorkRelationship, PeerReviewConducted, PeerReviewType, PeerReviewRecommendation, } from './common';
import { MarXivDataWithoutSitemap } from './context';

/**
 * Import the local dictionary
 */
import { SubmissionFormText, Translatables, FormMessageText } from './translatables';

/**
 * Local Interfaces
 */
export interface OptionalCollapsibleBlock {
   title: string
   content: HastElement[]
   minimized: boolean
}

export interface PreviewBarItem {
   order: number
   displayText: string
}

export interface Contrib extends PreviewBarItem {
   type: 'std' | 'org' | 'anon'
}
export interface ContribStd extends Contrib {
   type: 'std'
   surname: string
   nameStyle: NameStyle
   role: ContribRole
   sequence: ContribSequence
   givenName?: string
   email?: string
   affiliation?: string
   orcid?: string
   suffix?: string
}
export interface ContribOrg extends Contrib {
   type: 'org'
   name: string
   role: ContribRole
   sequence: ContribSequence
}
export interface ContribAnon extends Contrib {
   type: 'anon'
   role: ContribRole
   sequence: ContribSequence
   affiliation?: string
}
export type GenericContributor = ContribStd | ContribOrg | ContribAnon;

export interface Institution extends PreviewBarItem {
   name: string
   acronym?: string
   location?: string
   department?: string
}

export interface Relationship extends PreviewBarItem {
   doi: string
   type: WorkRelationship
}

export interface Citation extends PreviewBarItem {
   doi?: string
   isbn?: string
   issn?: string
   articleTitle?: string
   journalTitle?: string
   surname?: string
   volume?: string
   issue?: string
   page?: string
   year?: string
   edition?: string
   component?: string
   seriesTitle?: string
   volumeTitle?: string
   unstructured?: string
}

export interface ConferenceEventDates {
   startYear: string
   startMonth: string
   startDay: string
   endYear: string
   endMonth: string
   endDay: string
}

export interface ConferenceEvent {
   name: string
   dates: ConferenceEventDates
   theme?: string
   acronym?: string
   number?: string
   sponsor?: string
   location?: string
}

export interface ConferencePaper {
   conferenceEvent: ConferenceEvent
   fullText: boolean
   firstPage?: string
   lastPage?: string
}

export interface CrossrefDate {
   year: string
   month?: string
   day?: string
}

export interface Database {
   title: string
   description: string
   datePublished: CrossrefDate
   dateCreated?: CrossrefDate
   dateUpdated?: CrossrefDate
}

export interface Publisher {
   name: string
   location?: string
}

export interface PeerReview {
   doiofReviewedWork: string
   conducted: PeerReviewConducted
   type: PeerReviewType
   recommendation: PeerReviewRecommendation
   competingInterestStatement?: string
}

/**
 * Make optional collapsible blocks
 */
// export const makeOptionalCollapsible = (d: SubmissionFormText) => (options: OptionalCollapsibleBlock): HastElement => {
//    if (options.minimized) {
//       return make.element({ tag: 'div', properties: { className: 'badge blue flex all-parent-row right' }, children: [
//          make.element({ tag: 'button', properties: { className: 'collapsible', type: 'button' }, children: [
//             make.element({ tag: 'div', properties: { className: 'collapsible-title' }, children: [
//             make.text(d.basic.optional.fieldTitle)
//          ]})
//          ]})
//       ]})
//    }
//    else {
//       return make.element({ tag: 'div', properties: { className: 'badge blue flex all-parent-row right' }, children: [
//          make.element({ tag: 'button', properties: { className: 'collapsible', type: 'button' }, children: [
//             make.element({ tag: 'div', properties: { className: 'collapsible-title' }, children: [
//                make.text(d.basic.optional.fieldTitle)
//             ]}),
//             make.element({ tag: 'div', properties: { className: 'collapsible-content' }, children: options.content })
//          ]}),
//       ]})
//    }
// }

/**
 * Make form components.
 * 
 * Paypal donation form
 */
// export const makeDonationForm = (d: SubmissionFormText): HastElement => {
//    const formContent = make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.support.p1)
//       ]}),
//       make.element({ tag: 'div', properties: { className: 'flex all-parent-column' }, children: [
//          make.element({ tag: 'div', properties: {id: 'paypal', className: 'flex all-child-span1 center' }, children: [
//             make.element({ tag: 'div', properties: { className: 'button blue' }, children: [
//                make.element({ tag: 'a', properties: { href: d.support.paypalLink, target: '_blank', className: 'normalWhiteLink' }, children: [
//                   make.text(d.support.button)
//                ]})
//             ]}),
//             make.element({ tag: 'p', children: [
//                make.text(d.support.p2)
//             ]}),
//          ]}),
//       ]}),
//    ]});
//    return makeForm(d.support.formTitle)([formContent]);
// }

/**
 * Language selection drop-down
 * 
 * const nativeLangs: string[] = Object.values(LANGUAGES_LIST).map(l => l.nativeName);
 * const englishLangs: string[] = Object.values(LANGUAGES_LIST).map(l => l.name);
 */
export const makeLanguageOptions = (d: Translatables) => (selected?: string): SelectOption[] => {
   let langAcc: SelectOption[] = new Array; 
   Object.entries(LANGUAGES_LIST).map((value) => {
      let option: SelectOption = {
         label: value[1].nativeName,
         value: value[0],
      }
      if (selected && option.value === selected) {
         langAcc.push({
            ...option,
            selected: true
         });
      }
      else {
         langAcc.push(option)
      }
   })
   return langAcc
}

// export const makeLanguageSelectList = (d: Translatables) => (selected: string): HastElement => makeSelectList({
//    label: d.forms.submit.basic.optional.language.fieldTitle,
//    id: randomizeID('language'),
//    required: false,
//    multi: false,
//    options: makeLanguageOptions(d)(selected),
// })

/**
 * Crossref Dates
 */
// export const makeOriginalPublicationDate = (d: SubmissionFormText) => (prefill?: CrossrefDate): HastElement => make.element({ tag: 'div', properties: { className: 'group flex all-parent-column' }, children: [
//    make.element({ tag: 'div', properties: { className: 'groupLabel flex all-child-span1' }, children: [
//       make.text(d.basic.originalPublicationDate.fieldTitle)
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'fields flex all-child-span1' }, children: [
//       // Year
//       makeTextInput({ label: d.basic.originalPublicationDate.year.fieldTitle, placeholder: d.basic.originalPublicationDate.year.default, id: randomizeID('originalPublicationDateYear'), required: true, short: true, value: prefill ? prefill.year : undefined }),
//       // Month
//       makeTextInput({ label: d.basic.originalPublicationDate.month.fieldTitle, placeholder: d.basic.originalPublicationDate.month.default, id: randomizeID('originalPublicationDateMonth'), required: false, short: true, value: prefill ? prefill.month : undefined }),
//       // Day
//       makeTextInput({ label: d.basic.originalPublicationDate.day.fieldTitle, placeholder: d.basic.originalPublicationDate.day.default, id: randomizeID('originalPublicationDateDay'), required: false, short: true, value: prefill ? prefill.day : undefined }),
//    ]})
// ]})

/**
 * Generic File uploads
 */
// export const makeUploadFileGenericContent = (d: SubmissionFormText): HastElement => make.element({ tag: 'div', properties: { className: 'field' }, children: [
//    make.element({ tag: 'div', properties: { className: 'center' }, children: [
//       make.text(d.files.generic.dragAndDrop)
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'separator'}, children: [
//       make.element({ tag: 'div', children: [
//          make.text(d.files.generic.or)
//       ]}),
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'button blue choose' }, children: [
//       make.text(d.files.generic.chooseFile)
//    ]})
// ]});
// export const makeUploadFileGeneric = (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.files.generic.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.files.generic.miniDescription)
//    ]}),
//    content: makeUploadFileGenericContent(d),
//    id: 'fileUpload',
//    collapsible: 'maxFile',
// });

/**
 * PDF File uploads
 */
// export const makeUploadFilePDFContent = (d: SubmissionFormText): HastElement => make.element({ tag: 'div', properties: { className: 'field' }, children: [
//    make.element({ tag: 'div', properties: { className: 'center' }, children: [
//       make.text(d.files.pdf.dragAndDrop)
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'separator'}, children: [
//       make.element({ tag: 'div', children: [
//          make.text(d.files.pdf.or)
//       ]}),
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'button blue choose' }, children: [
//       make.text(d.files.pdf.choosePDF)
//    ]})
// ]});
// export const makeUploadFilePDF = (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.files.pdf.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.files.pdf.miniDescription)
//    ]}),
//    content: makeUploadFilePDFContent(d),
//    id: 'fileUpload',
//    collapsible: 'maxFile',
// });

/**
 * Dataset File uploads
 */
// export const makeUploadFileDatasetContent = (d: SubmissionFormText): HastElement => make.element({ tag: 'div', properties: { className: 'field' }, children: [
//    make.element({ tag: 'div', properties: { className: 'center' }, children: [
//       make.text(d.files.dataset.dragAndDrop)
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'separator'}, children: [
//       make.element({ tag: 'div', children: [
//          make.text(d.files.generic.or)
//       ]}),
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'button blue choose' }, children: [
//       make.text(d.files.dataset.chooseCompressedArchive)
//    ]})
// ]});
// export const makeUploadFileDataset = (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.files.dataset.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.files.dataset.miniDescription)
//    ]}),
//    content: makeUploadFileDatasetContent(d),
//    id: 'fileUpload',
//    collapsible: 'maxFile',
// });

/**
 * Formal Peer Review
 */
// export const makeFormalPeerReview = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => make.element({ tag: 'div', properties: { className: 'peerReviewQuestion field'}, children: [
//    make.element({ tag: 'div', properties: { className: 'description' }, children: [
//       make.text(d.basic.peerReview.fieldTitle)
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'flex all-parent-row center' }, children: [
//       makeRadioInput({
//          label: d.basic.peerReview.yes,
//          id: 'peerReviewed',
//          name: 'formalPeerReview',
//          value: 'peerReviewed',
//          selected: context.workData.peerReviewed ? true : false,
//       }),
//       makeRadioInput({
//          label: d.basic.peerReview.no,
//          id: 'notPeerReviewed',
//          name: 'formalPeerReview',
//          value: 'notPeerReviewed',
//          selected: context.workData.peerReviewed ? true : false
//       })
//    ]})
// ]});

/**
 * Basic Information for Posted Content
 */
// export const makeBasicInformationPostedContent = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeCollapsibleForm({
//    title: d.forms.submit.basic.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.forms.submit.basic.formMinidescription)
//    ]}),
//    content: make.element({ tag: 'div', children: [
//       // Title
//       makeTextInput({
//          label: d.forms.submit.basic.title.fieldTitle,
//          placeholder: d.forms.submit.basic.title.default,
//          id: 'workTitle',
//          required: true,
//          short: true,
//          value: context.workData.title
//       }),
//       // Subtitle
//       makeTextInput({
//          label: d.forms.submit.basic.optional.subtitle.fieldTitle,
//          placeholder: d.forms.submit.basic.optional.subtitle.default,
//          id: 'workSubtitle',
//          required: false,
//          short: true,
//          value: context.workData.subtitle
//       }),
//       // Publisher's DOI
//       makeTextInput({
//          label: d.forms.submit.basic.publisherDOI.fieldTitle,
//          placeholder: d.forms.submit.basic.publisherDOI.default,
//          id: 'vorDOI',
//          required: false,
//          short: true,
//          value: context.workData.publisherDOI
//       }),
//       // Langugage
//       makeLanguageSelectList(d)(context.workData.language ? context.workData.language : 'en'),
//       // Formal Peer Review
//       makeFormalPeerReview(context)(d.forms.submit),
//       // Original Publication Date
//       makeOriginalPublicationDate(d.forms.submit)(context.workData.originalPublicationDate),
//       // Description
//       makeTextInput({
//          label: d.forms.submit.basic.description.fieldTitle,
//          placeholder: d.forms.submit.basic.description.default,
//          id: 'description',
//          required: true,
//          short: false,
//          value: context.workData.description
//       }),
//    ]}),
//    id: 'basicInformationPostedContent',
//    collapsible: 'maxBasic',
// });

// Remove the Formal Peer Review question
// export const makeBasicInformationPostedContentNoPeerReview = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeCollapsibleForm({
//    title: d.forms.submit.basic.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.forms.submit.basic.formMinidescription)
//    ]}),
//    content: make.element({ tag: 'div', children: [
//       // Title
//       makeTextInput({
//          label: d.forms.submit.basic.title.fieldTitle,
//          placeholder: d.forms.submit.basic.title.default,
//          id: 'workTitle',
//          required: true,
//          short: true,
//          value: context.workData.title
//       }),
//       // Subtitle
//       makeTextInput({
//          label: d.forms.submit.basic.optional.subtitle.fieldTitle,
//          placeholder: d.forms.submit.basic.optional.subtitle.default,
//          id: 'workSubtitle',
//          required: false,
//          short: true,
//          value: context.workData.subtitle
//       }),
//       // Publisher's DOI
//       makeTextInput({
//          label: d.forms.submit.basic.publisherDOI.fieldTitle,
//          placeholder: d.forms.submit.basic.publisherDOI.default,
//          id: 'vorDOI',
//          required: false,
//          short: true,
//          value: context.workData.publisherDOI
//       }),
//       // Langugage
//       makeLanguageSelectList(d)(context.workData.language ? context.workData.language : 'en'),
//       // Original Publication Date
//       makeOriginalPublicationDate(d.forms.submit)(context.workData.originalPublicationDate),
//       // Description
//       makeTextInput({
//          label: d.forms.submit.basic.description.fieldTitle,
//          placeholder: d.forms.submit.basic.description.default,
//          id: 'description',
//          required: true,
//          short: false,
//          value: context.workData.description
//       }),
//    ]}),
//    id: 'basicInformationPostedContent',
//    collapsible: 'maxBasic',
// });

// Remove the Publisher's DOI
// export const makeBasicInformationNoDOIorPeerReview = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeCollapsibleForm({
//    title: d.forms.submit.basic.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.forms.submit.basic.formMinidescription)
//    ]}),
//    content: make.element({ tag: 'div', children: [
//       // Title
//       makeTextInput({
//          label: d.forms.submit.basic.title.fieldTitle,
//          placeholder: d.forms.submit.basic.title.default,
//          id: 'workTitle',
//          required: true,
//          short: true,
//          value: context.workData.title
//       }),
//       // Subtitle
//       makeTextInput({
//          label: d.forms.submit.basic.optional.subtitle.fieldTitle,
//          placeholder: d.forms.submit.basic.optional.subtitle.default,
//          id: 'workSubtitle',
//          required: false,
//          short: true,
//          value: context.workData.subtitle
//       }),
//       // Langugage
//       makeLanguageSelectList(d)(context.workData.language ? context.workData.language : 'en'),
//       // Original Publication Date
//       makeOriginalPublicationDate(d.forms.submit)(context.workData.originalPublicationDate),
//       // Description
//       makeTextInput({
//          label: d.forms.submit.basic.description.fieldTitle,
//          placeholder: d.forms.submit.basic.description.default,
//          id: 'description',
//          required: true,
//          short: false,
//          value: context.workData.description
//       }),
//    ]}),
//    id: 'basicInformationPostedContent',
//    collapsible: 'maxBasic',
// });

/**
 * Contributors
 */
export const makeNameStyleOptions = (d: SubmissionFormText) =>  [
   {
      label: d.contributors.contributor.nameStyle.western,
      value: 'western'
   },
   {
      label: d.contributors.contributor.nameStyle.eastern,
      value: 'eastern'
   },
   {
      label: d.contributors.contributor.nameStyle.islensk,
      value: 'islensk'
   },
   {
      label: d.contributors.contributor.nameStyle.givenOnly,
      value: 'givenOnly'
   },
];

// export const makeSelectNameStyle = (d: SubmissionFormText) => (selected: NameStyle): HastElement => makeSelectList({
//    label: d.contributors.contributor.nameStyle.fieldTitle,
//    id: 'contributorNameStyle',
//    required: true,
//    multi: false,
//    options: makeNameStyleOptions(d),
//    selected: selected
// });

export const makeContributorRoleOptions = (d: SubmissionFormText) =>  [
   {
      label: d.contributors.contributor.role.author,
      value: 'author'
   },
   {
      label: d.contributors.contributor.role.editor,
      value: 'editor'
   },
   {
      label: d.contributors.contributor.role.chair,
      value: 'chair'
   },
   {
      label: d.contributors.contributor.role.reviewer,
      value: 'reviewer'
   },
   {
      label: d.contributors.contributor.role.reviewerAst,
      value: 'reviewAssistant'
   },
   {
      label: d.contributors.contributor.role.statsReviewer,
      value: 'statsReviewer'
   },
   {
      label: d.contributors.contributor.role.externalReviewer,
      value: 'reviewerExternal'
   },
   {
      label: d.contributors.contributor.role.reader,
      value: 'reader'
   },
   {
      label: d.contributors.contributor.role.translator,
      value: 'translator'
   },
];

// export const makeContributorRole = (d: SubmissionFormText) => (selected: ContribRole): HastElement => makeSelectList({
//    label: d.contributors.contributor.role.fieldTitle,
//    id: 'contributorRole',
//    required: true,
//    multi: false,
//    options: makeContributorRoleOptions(d),
//    selected: selected
// });

// export const makeContributorSequence = (d: SubmissionFormText) => (selected: ContribSequence): HastElement => {
//    // Radio groups must share a common name, but we don't want to clash with many contributors.
//    const name = 'contribSequence';
//    if (selected === 'additional') {
//       return make.element({ tag: 'div', properties: { className: 'authorSequence flex all-parent-row center field' }, children: [
//          makeRadioInput({
//             label: d.contributors.contributor.firstAuthor,
//             id: 'first',
//             name: name,
//             value: 'first',
//          }),
//          makeRadioInput({
//             label: d.contributors.contributor.additionalAuthor,
//             id: 'additional',
//             name: name,
//             value: 'additional',
//             selected: true
//          })
//       ]})
//    }
//    else {
//       return make.element({ tag: 'div', properties: { className: 'authorSequence flex all-parent-row center field' }, children: [
//          makeRadioInput({
//             label: d.contributors.contributor.firstAuthor,
//             id: 'first',
//             name: name,
//             value: 'first',
//             selected: true
//          }),
//          makeRadioInput({
//             label: d.contributors.contributor.additionalAuthor,
//             id: 'additional',
//             name: name,
//             value: 'additional',
//          })
//       ]})
//    }
// }

// export const makeContributorsContributorOptionals = (d: SubmissionFormText) => (contributor?: ContribStd): HastElement => makeOptionalCollapsible(d)({
//    title: d.basic.optional.fieldTitle,
//    minimized: false,
//    content: [
//       // Suffix
//       makeTextInput({
//          label: d.contributors.contributor.suffix.fieldTitle,
//          placeholder: d.contributors.contributor.suffix.default,
//          id: 'contribSuffix',
//          required: false,
//          short: true,
//          value: contributor ? contributor.suffix : undefined
//       }),
//       // Name Style
//       makeSelectNameStyle(d)(contributor ? contributor.nameStyle : 'western'),
//       // Role
//       makeContributorRole(d)(contributor ? contributor.role : 'author'),
//       // Sequence
//       makeContributorSequence(d)(contributor ? contributor.sequence : 'additional')
//    ],
// })

// export const makeContributorsOtherOptionals = (d: SubmissionFormText) => (contributor?: ContribOrg | ContribAnon): HastElement => makeOptionalCollapsible(d)({
//    title: d.basic.optional.fieldTitle,
//    minimized: false,
//    content: [
//       // Role
//       makeContributorRole(d)(contributor ? contributor.role : 'author'),
//       // Sequence
//       makeContributorSequence(d)(contributor ? contributor.sequence : 'additional')
//    ],
// })

// export const makeContributorsContributorGroup = (context: MarXivDataWithoutSitemap) => (d: FormText) => (contributor?: ContribStd): HastElement => make.element({ tag: 'div', properties: { className: 'group' }, children: [
//    // Group Title
//    make.element({ tag: 'div', properties: { className: 'groupLabel' }, children: [
//       make.text(d.submit.contributors.contributor.formTitle)
//    ]}),
//    // Given name
//    makeTextInput({ label: d.join.fields.givenName, placeholder: d.join.fields.givenNameDefault, id: 'contribGiven', required: false, short: true, value: contributor ? contributor.givenName : undefined }),
//    // Surname
//    makeTextInput({ label: d.join.fields.surName, placeholder: d.join.fields.surNameDefault, id: 'contribSurname', required: true, short: true, value: contributor ? contributor.surname : undefined }),
//    // Email
//    makeInputEmailField(d)({ id: 'contribEmail', required: false, value: contributor ? contributor.email : undefined }),
//    // Affiliation
//    makeTextInput({ label: d.submit.contributors.contributor.affiliation.fieldTitle, placeholder: d.submit.contributors.contributor.affiliation.default, id: 'contribAffiliation', required: false, short: true, value: contributor ? contributor.affiliation : undefined }),
//    // ORCID
//    makeTextInput({ label: d.join.fields.orcid, placeholder: d.join.fields.orcidDefault, id: 'contribORCID', required: false, short: true, value: contributor ? contributor.orcid : undefined }),
//    // Optionals
//    makeContributorsContributorOptionals(d.submit)(contributor)
// ]})


// export const makeContributorAddButtons = (d: SubmissionFormText): HastElement[] => [
//    // Add contributor
//    make.element({ tag: 'div', properties: { className: 'addAuthor button orange flex all-child-span1' }, children: [
//       make.text(d.contributors.contributor.add)
//    ]}),
// ];

/**
 * Handle generic preview items
 */
// export const makePreviewItems = (content: PreviewBarItem[]): HastElement[] => {
//    // Sort the array of data first
//    const sortedContent = content.sort( (fst, snd) => fst.order - snd.order );
//    // Now turn the sorted data into DOM elements
//    let formattedItems: HastElement[] = new Array;
//    for (let i = 0; i < sortedContent.length; i++) {
//       formattedItems.push( make.element({ tag: 'div', properties: { className: 'savedInput flex all-parent-row' }, children: [
//          make.text( sortedContent[i].displayText )
//       ]}));
//    }
//    return formattedItems
// }

/**
 * Contributors Preview Bar
 */
// export const makeContribPreviewBar = (d: SubmissionFormText) => (content?: GenericContributor[]): HastElement => {
//    if (content) {
//       const formattedData: HastElement[] = makePreviewItems(content);
//       return make.element({ tag: 'div', properties: { id: 'contribList', className: 'inputTextMulti tooltip flex all-parent-row' }, children: formattedData });
//    }
//    else {
//       // Fill in default text
//       return make.element({ tag: 'div', properties: { id: 'contribList', className: 'inputTextMulti tooltip flex all-parent-row' }, children: [
//          // Content
//          make.element({ tag: 'div', properties: { className: 'emptyList' }, children: [
//             make.text(d.contributors.previewDefault)
//          ]})
//       ]});
//    }
// }

// export const chooseContributorsGroup = (context: MarXivDataWithoutSitemap) => (d: FormText) => (selectedContrib?: ContribStd): HastElement => {
//    if (selectedContrib) {
//       return makeContributorsContributorGroup(context)(d)(selectedContrib)
//    }
//    else {
//       // Default to an empty standard contributor
//       return makeContributorsContributorGroup(context)(d)(undefined)
//    }
// }

/**
 * @function chooseContributorsGroup Displays the appropriate form group for the selected contrib type.
 * The idea being that if the user wants to add an Org, they should click the " Add organization" button
 * which should trigger a CB that passes-through an empty ContribOrg as the selectedContrib parameter
 */
// export const makeContributors = (context: MarXivDataWithoutSitemap) => (d: FormText) => (selectedContrib?: ContribStd): HastElement => makeCollapsibleForm({
//    title: d.submit.contributors.formTitle,
//    miniDescription: d.submit.contributors.description,
//    content: make.element({ tag: 'div', properties: { className: 'authorship' }, children: [
//       // Description
//       d.submit.contributors.description,
//       // Preview bar
//       makeContribPreviewBar(d.submit)(context.workData.contributors),
//       // Groups
//       chooseContributorsGroup(context)(d)(selectedContrib),
//       // Add buttons
//       make.element({ tag: 'div', properties: { className: 'flex all-parent-row center' }, children: makeContributorAddButtons(d.submit) }),
//    ]}),
//    id: 'contributors',
//    collapsible: 'maxContribs',
// });

/**
 * Sponsoring/hosting Institutions
 */
// export const makeInstitutionsPreviewBar = (d: SubmissionFormText) => (content?: Institution[]): HastElement => {
//    if (content) {
//       const formattedData: HastElement[] = makePreviewItems(content);
//       return make.element({ tag: 'div', properties: { id: 'institutionList', className: 'inputTextMulti tooltip flex all-parent-row' }, children: formattedData });
//    }
//    else {
//       // Fill in default text
//       return make.element({ tag: 'div', properties: { id: 'institutionList', className: 'inputTextMulti tooltip flex all-parent-row' }, children: [
//          // Content
//          make.element({ tag: 'div', properties: { className: 'emptyList' }, children: [
//             make.text(d.institutions.previewDefault)
//          ]})
//       ]});
//    }
// }

// export const makeInstitutionsGroup = (d: SubmissionFormText) => (selectedInstitution?: Institution): HastElement => make.element({ tag: 'div', properties: { className: 'group' }, children: [
//    // Label
//    make.element({ tag: 'div', properties: { className: 'groupLabel' }, children: [
//       make.text(d.institutions.institution.formTitle)
//    ]}),
//    // Name
//    makeTextInput({
//       label: d.institutions.institution.name.fieldTitle,
//       placeholder: d.institutions.institution.name.default,
//       id: 'instutitionName',
//       required: true,
//       short: true,
//       value: selectedInstitution ? selectedInstitution.name : undefined
//    }),
//    // Acronym
//    makeTextInput({
//       label: d.institutions.institution.acronym.fieldTitle,
//       placeholder: d.institutions.institution.acronym.default,
//       id: 'instutitionAcronym',
//       required: false,
//       short: true,
//       value: selectedInstitution ? selectedInstitution.acronym : undefined
//    }),
//    // Location
//    makeTextInput({
//       label: d.institutions.institution.location.fieldTitle,
//       placeholder: d.institutions.institution.location.default,
//       id: 'instutitionPlace',
//       required: false,
//       short: true,
//       value: selectedInstitution ? selectedInstitution.location : undefined
//    }),
//    // Department
//    makeTextInput({
//       label: d.institutions.institution.department.fieldTitle,
//       placeholder: d.institutions.institution.department.default,
//       id: 'instutitionDepartment',
//       required: false,
//       short: true,
//       value: selectedInstitution ? selectedInstitution.department : undefined
//    }),
// ]})

// export const makeInstitutions = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText) => (selectedInstitution?: Institution): HastElement => makeCollapsibleForm({
//    title: d.institutions.formTitle,
//    miniDescription: d.institutions.description,
//    content: make.element({ tag: 'div', properties: { className: 'institution' }, children: [
//       // Description
//       d.institutions.description,
//       // Preview bar
//       makeInstitutionsPreviewBar(d)(context.workData.institutions),
//       // Form group
//       makeInstitutionsGroup(d)(selectedInstitution),
//       // Add button
//       make.element({ tag: 'div', properties: { className: 'flex all-parent-row center' }, children: [
//          make.element({ tag: 'div', properties: { className: 'addInstitution button orange' }, children: [
//             make.text(d.institutions.institution.add)
//          ]}),
//       ]}),
//    ]}),
//    id: 'institutions',
//    collapsible: 'maxInstitutions',
// });

/**
 * License and Copyright
 */
export const makeLicenseOptions = (d: Translatables): SelectOption[] => [
   {
      label: d.forms.submit.license.license.cc0,
      value: 'cc0'
   },
   {
      label: d.forms.submit.license.license.afl,
      value: 'afl'
   },
   {
      label: d.forms.submit.license.license.ccBy,
      value: 'ccBy'
   },
   {
      label: d.forms.submit.license.license.ccBySa,
      value: 'ccBySa'
   },
   {
      label: d.forms.submit.license.license.ccByNd,
      value: 'ccByNd'
   },
   {
      label: d.forms.submit.license.license.ccByNc,
      value: 'ccByNc'
   },
   {
      label: d.forms.submit.license.license.ccByNcSa,
      value: 'ccByNcSa'
   },
   {
      label: d.forms.submit.license.license.ccByNcNd,
      value: 'ccByNcNd'
   },
   {
      label: d.forms.submit.license.license.gpl,
      value: 'gpl'
   },
   {
      label: d.forms.submit.license.license.mit,
      value: 'mit'
   },
   {
      label: d.forms.submit.license.license.ecl,
      value: 'ecl'
   },
   {
      label: d.forms.submit.license.license.none,
      value: 'noLicense'
   },
]

// export const makeCopyrightHolder = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => {
//    if (context.workData.license && context.workData.license === 'noLicense') {
//       // The user has selected to not apply a license to their work, so they must input the copyright holder.
//       return make.element({ tag: 'div', properties: { id: 'licenseCopyrighted' }, children: [
//          make.element({ tag: 'p', children: [
//             make.text(d.license.noLicense)
//          ]}),
//          makeTextInput({
//             label: d.license.copyrightHolder.fieldTitle,
//             placeholder: d.license.copyrightHolder.default,
//             id: 'copyright',
//             required: true,
//             short: true,
//             value: context.workData.copyrightHolder
//          })
//       ]})
//    }
//    else {
//       // The user chose a standard license, so just return an empty div to hide this field.
//       return make.element({ tag: 'div', children: []})
//    }
// }

// export const makeLicensePreprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeCollapsibleForm({
//    title: d.forms.submit.license.formTitle,
//    miniDescription: d.forms.submit.license.description,
//    content: make.element({ tag: 'div', properties: { className: 'license' }, children: [
//       // Description
//       d.forms.submit.license.description,
//       // Select license
//       makeSelectList({
//          label: d.forms.submit.license.license.formTitle,
//          id: 'workLicense',
//          required: true,
//          multi: false,
//          options: makeLicenseOptions(d),
//          selected: context.workData.license
//       }),
//       // Copyright holder
//       makeCopyrightHolder(context)(d.forms.submit),
//    ]}),
//    id: 'license',
//    collapsible: 'maxLicense',
// });

// export const makeLicensePostprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeCollapsibleForm({
//    title: d.forms.submit.license.formTitle,
//    miniDescription: d.forms.submit.license.description,
//    content: make.element({ tag: 'div', properties: { className: 'license' }, children: [
//       // Description
//       d.forms.submit.license.description,
//       d.forms.submit.license.descriptionPostprint,
//       // Select license
//       makeSelectList({
//          label: d.forms.submit.license.license.formTitle,
//          id: 'workLicense',
//          required: true,
//          multi: false,
//          options: makeLicenseOptions(d),
//          selected: context.workData.license
//       }),
//       // Copyright holder
//       makeCopyrightHolder(context)(d.forms.submit),
//    ]}),
//    id: 'license',
//    collapsible: 'maxLicense',
// });

// export const makeLicenseCopyrighted = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeCollapsibleForm({
//    title: d.forms.submit.license.formTitle,
//    miniDescription: d.forms.submit.license.description,
//    content: make.element({ tag: 'div', properties: { className: 'license' }, children: [
//       // Description
//       d.forms.submit.license.description,
//       // Select license
//       makeSingleSelectListWithDefault({
//          label: d.forms.submit.license.license.formTitle,
//          id: 'workLicense',
//          required: true,
//          multi: false,
//          options: makeLicenseOptions(d),
//          selected: 'noLicense'
//       }),
//       // Copyright holder
//       makeCopyrightHolder(context)(d.forms.submit),
//    ]}),
//    id: 'license',
//    collapsible: 'maxLicense',
// });

/**
 * Pre-register a DOI
 * Note that we haven't made translatables for this yet, but we did work on this in 'marxivsite' (forms.tsx line 548)
 */

/**
 * Customize the MarXiv DOI
 */
// export const customizeDOI = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeForm(d.assignDOI.formTitle)([
//    // Description
//    d.assignDOI.description,
//    // Input the custom DOI
//    makeTextInput({
//       label: d.assignDOI.marxivDOI.fieldTitle,
//       placeholder: d.assignDOI.marxivDOI.default,
//       id: 'marxivDOI',
//       required: false,
//       short: true,
//       value: context.workData.marxivDOI
//    }),
//    // Show the user the full DOI
//    make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.text(d.assignDOI.doiReplacement),
//       make.element({ tag: 'span', properties: { className: 'strong' }, children: [
//          make.text( d.assignDOI.marxivPrefix + undefinedValueToString(context.workData.marxivDOI) )
//       ]}),
//    ]}),
// ]);

/**
 * Relationships between works
 */
export const makeRelationshipOptions = (d: Translatables): SelectOption[] => [
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.relatedMaterial,
      value: 'relatedMaterial'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.reviewOf,
      value: 'reviewOf'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.commentOn,
      value: 'commentOn'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.replyTo,
      value: 'replyTo'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.derivedFrom,
      value: 'derivedFrom'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.basedOn,
      value: 'basedOn'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.basisFor,
      value: 'basisFor'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.preprintOf,
      value: 'preprintOf'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.manuscriptOf,
      value: 'manuscriptOf'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.translationOf,
      value: 'translationOf'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.replacedBy,
      value: 'replacedBy'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.replaces,
      value: 'replaces'
   },
   {
      label: d.forms.submit.relationships.relationship.typeOfRelationship.sameAs,
      value: 'sameAs'
   },
];

// export const makeRelationshipsPreviewBar = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText) => (content?: Relationship[]): HastElement => {
//    if (content) {
//       const formattedData: HastElement[] = makePreviewItems(content);
//       return make.element({ tag: 'div', properties: { id: 'relationshipList', className: 'inputTextMulti tooltip flex all-parent-row' }, children: formattedData });
//    }
//    else {
//       // Fill in default text
//       return make.element({ tag: 'div', properties: { id: 'relationshipList', className: 'inputTextMulti tooltip flex all-parent-row' }, children: [
//          // Content
//          make.element({ tag: 'div', properties: { className: 'emptyList' }, children: [
//             make.text(d.relationships.previewDefault)
//          ]}),
//       ]});
//    }
// }

// export const makeRelationshipsGroup = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (selectedRelationship?: Relationship): HastElement => make.element({ tag: 'div', properties: { className: 'group' }, children: [
//    // Label
//    make.element({ tag: 'div', properties: { className: 'groupLabel' }, children: [
//       make.text(d.forms.submit.relationships.relationship.formTitle)
//    ]}),
//    // DOI
//    makeTextInput({
//       label: d.forms.submit.relationships.relationship.doi.fieldTitle,
//       placeholder: d.forms.submit.relationships.relationship.doi.default,
//       id: 'relatedDOI',
//       required: true,
//       short: true,
//       value: selectedRelationship ? selectedRelationship.doi : undefined
//    }),
//    // Type
//    makeSelectList({
//       label: d.forms.submit.relationships.relationship.typeOfRelationship.fieldTitle,
//       id: 'relationship',
//       required: true,
//       multi: false,
//       options: makeRelationshipOptions(d),
//       selected: selectedRelationship ? selectedRelationship.type : undefined
//    }),
// ]});

// export const makeRelationships = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (selectedRelationship?: Relationship): HastElement => makeCollapsibleForm({
//    title: d.forms.submit.relationships.formTitle,
//    miniDescription: d.forms.submit.relationships.description,
//    content: make.element({ tag: 'div', properties: { className: 'relationships' }, children: [
//       // Description
//       d.forms.submit.relationships.description,
//       // Preview bar
//       makeRelationshipsPreviewBar(context)(d.forms.submit)(context.workData.relationships),
//       // Form group
//       makeRelationshipsGroup(context)(d)(selectedRelationship),
//       // Add button
//       make.element({ tag: 'div', properties: { className: 'flex all-parent-row center' }, children: [
//          make.element({ tag: 'div', properties: { className: 'addRelationship button orange' }, children: [
//             make.text(d.forms.submit.relationships.relationship.add)
//          ]}),
//       ]}),
//    ]}),
//    id: 'relationships',
//    collapsible: 'maxRelationships',
// });

/**
 * Citations
 */
// export const makeCitationsPreviewBar = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText) => (content?: Citation[]): HastElement => {
//    if (content) {
//       const formattedData: HastElement[] = makePreviewItems(content);
//       return make.element({ tag: 'div', properties: { id: 'citationList', className: 'inputTextMulti tooltip flex all-parent-row' }, children: formattedData });
//    }
//    else {
//       // Fill in default text
//       return make.element({ tag: 'div', properties: { id: 'citationList', className: 'inputTextMulti tooltip flex all-parent-row' }, children: [
//          // Content
//          make.element({ tag: 'div', properties: { className: 'emptyList' }, children: [
//             make.text(d.relationships.previewDefault)
//          ]})
//       ]});
//    }
// }

// export const makeCitationsGroup = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText) => (selectedCitation?: Citation): HastElement => make.element({ tag: 'div', properties: { className: 'group' }, children: [
//    // Label
//    make.element({ tag: 'div', properties: { className: 'groupLabel' }, children: [
//       make.text(d.citations.citation.formTitle)
//    ]}),
//    // DOI
//    makeTextInput({
//       label: d.citations.citation.doi.fieldTitle,
//       placeholder: d.citations.citation.doi.default,
//       id: randomizeID('citationDOI'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.doi : undefined
//    }),
//    // ISBN
//    makeTextInput({
//       label: d.citations.citation.isbn.fieldTitle,
//       placeholder: d.citations.citation.isbn.default,
//       id: randomizeID('citationISBN'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.isbn : undefined
//    }),
//    // ISSN
//    makeTextInput({
//       label: d.citations.citation.issn.fieldTitle,
//       placeholder: d.citations.citation.issn.default,
//       id: randomizeID('citationISSN'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.issn : undefined
//    }),
//    // Article title
//    makeTextInput({
//       label: d.citations.citation.articleTitle.fieldTitle,
//       placeholder: d.citations.citation.articleTitle.default,
//       id: randomizeID('citationArticleTitle'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.articleTitle : undefined
//    }),
//    // Journal title
//    makeTextInput({
//       label: d.citations.citation.journalTitle.fieldTitle,
//       placeholder: d.citations.citation.journalTitle.default,
//       id: randomizeID('citationJournalTitle'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.journalTitle : undefined
//    }),
//    // Surname of the first author
//    makeTextInput({
//       label: d.citations.citation.surname.fieldTitle,
//       placeholder: d.citations.citation.surname.default,
//       id: randomizeID('citationSurname'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.surname : undefined
//    }),
//    // Volume
//    makeTextInput({
//       label: d.citations.citation.volume.fieldTitle,
//       placeholder: d.citations.citation.volume.default,
//       id: randomizeID('citationVolume'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.volume : undefined
//    }),
//    // Issue
//    makeTextInput({
//       label: d.citations.citation.issue.fieldTitle,
//       placeholder: d.citations.citation.issue.default,
//       id: randomizeID('citationIssue'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.issue : undefined
//    }),
//    // First page
//    makeTextInput({
//       label: d.citations.citation.page.fieldTitle,
//       placeholder: d.citations.citation.page.default,
//       id: randomizeID('citationPage'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.page : undefined
//    }),
//    // Year
//    makeTextInput({
//       label: d.citations.citation.year.fieldTitle,
//       placeholder: d.citations.citation.year.default,
//       id: randomizeID('citationYear'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.year : undefined
//    }),
//    // Edition number
//    makeTextInput({
//       label: d.citations.citation.edition.fieldTitle,
//       placeholder: d.citations.citation.edition.default,
//       id: randomizeID('citationEdition'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.edition : undefined
//    }),
//    // Component number
//    makeTextInput({
//       label: d.citations.citation.component.fieldTitle,
//       placeholder: d.citations.citation.component.default,
//       id: randomizeID('citationComponent'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.component : undefined
//    }),
//    // Series title
//    makeTextInput({
//       label: d.citations.citation.seriesTitle.fieldTitle,
//       placeholder: d.citations.citation.seriesTitle.default,
//       id: randomizeID('citationSeriesTitle'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.seriesTitle : undefined
//    }),
//    // Volume title
//    makeTextInput({
//       label: d.citations.citation.volumeTitle.fieldTitle,
//       placeholder: d.citations.citation.volumeTitle.default,
//       id: randomizeID('citationVolumeTitle'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.volumeTitle : undefined
//    }),
//    // Unstructured citation
//    makeTextInput({
//       label: d.citations.citation.unstructured.fieldTitle,
//       placeholder: d.citations.citation.unstructured.default,
//       id: randomizeID('citationUnstructured'),
//       required: false,
//       short: true,
//       value: selectedCitation ? selectedCitation.unstructured : undefined
//    }),
// ]});

// export const makeCitations = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText) => (selectedCitation?: Citation): HastElement => makeCollapsibleForm({
//    title: d.citations.formTitle,
//    miniDescription: d.citations.description,
//    content: make.element({ tag: 'div', properties: { className: 'citations' }, children: [
//       // Description
//       d.citations.description,
//       // Preview bar
//       makeCitationsPreviewBar(context)(d)(context.workData.citations),
//       // Form group
//       makeCitationsGroup(context)(d)(selectedCitation),
//       // Add button
//       make.element({ tag: 'div', properties: { className: 'flex all-parent-row center' }, children: [
//          make.element({ tag: 'div', properties: { className: 'addCitation button orange' }, children: [
//             make.text(d.citations.citation.add)
//          ]}),
//       ]}),
//    ]}),
//    id: 'relationships',
//    collapsible: 'maxRelationships',
// });

// /**
//  * Conference Events
//  */
// export const makeConferenceDatesGroup = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => make.element({ tag: 'div', properties: { className: 'group' }, children: [
//    // Label
//    make.element({ tag: 'div', properties: { className: 'groupLabel' }, children: [
//       make.text(d.conferenceEvent.dates.formTitle)
//    ]}),
//    // Start year
//    makeTextInput({
//       label: d.conferenceEvent.dates.startYear.fieldTitle,
//       placeholder: d.conferenceEvent.dates.startYear.default,
//       id: randomizeID('startYear'),
//       required: true,
//       short: true,
//       value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.dates.startYear : undefined
//    }),
//    // Start month
//    makeTextInput({
//       label: d.conferenceEvent.dates.startMonth.fieldTitle,
//       placeholder: d.conferenceEvent.dates.startMonth.default,
//       id: randomizeID('startMonth'),
//       required: true,
//       short: true,
//       value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.dates.startMonth : undefined
//    }),
//    // Start day
//    makeTextInput({
//       label: d.conferenceEvent.dates.startDay.fieldTitle,
//       placeholder: d.conferenceEvent.dates.startDay.default,
//       id: randomizeID('startDay'),
//       required: true,
//       short: true,
//       value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.dates.startDay : undefined
//    }),
//    // End year
//    makeTextInput({
//       label: d.conferenceEvent.dates.endYear.fieldTitle,
//       placeholder: d.conferenceEvent.dates.endYear.default,
//       id: randomizeID('endYear'),
//       required: true,
//       short: true,
//       value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.dates.endYear : undefined
//    }),
//    // End month
//    makeTextInput({
//       label: d.conferenceEvent.dates.endMonth.fieldTitle,
//       placeholder: d.conferenceEvent.dates.endMonth.default,
//       id: randomizeID('endMonth'),
//       required: true,
//       short: true,
//       value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.dates.endMonth : undefined
//    }),
//    // End day
//    makeTextInput({
//       label: d.conferenceEvent.dates.endDay.fieldTitle,
//       placeholder: d.conferenceEvent.dates.endDay.default,
//       id: randomizeID('endDay'),
//       required: true,
//       short: true,
//       value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.dates.endDay : undefined
//    }),
// ]});

// export const makeConferenceEvent = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.conferenceEvent.formTitle,
//    miniDescription: make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.conferenceEvent.description)
//       ]})
//    ]}),
//    content: make.element({ tag: 'div', properties: { className: 'citations' }, children: [
//       // Description
//       make.element({ tag: 'div', properties: { className: 'field' }, children: [
//          make.element({ tag: 'div', properties: { className: 'description' }, children: [
//             make.text(d.conferenceEvent.description)
//          ]})
//       ]}),
//       // Conference name
//       makeTextInput({
//          label: d.conferenceEvent.conferenceName.fieldTitle,
//          placeholder: d.conferenceEvent.conferenceName.default,
//          id: randomizeID('conferenceEventName'),
//          required: true,
//          short: true,
//          value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.name : undefined
//       }),
//       // Theme
//       makeTextInput({
//          label: d.conferenceEvent.theme.fieldTitle,
//          placeholder: d.conferenceEvent.theme.default,
//          id: randomizeID('conferenceEventTheme'),
//          required: false,
//          short: true,
//          value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.theme : undefined
//       }),
//       // Acronym
//       makeTextInput({
//          label: d.conferenceEvent.acronym.fieldTitle,
//          placeholder: d.conferenceEvent.acronym.default,
//          id: randomizeID('conferenceEventAcronym'),
//          required: false,
//          short: true,
//          value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.acronym : undefined
//       }),
//       // Number
//       makeTextInput({
//          label: d.conferenceEvent.number.fieldTitle,
//          placeholder: d.conferenceEvent.number.default,
//          id: randomizeID('conferenceEventNumber'),
//          required: false,
//          short: true,
//          value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.number : undefined
//       }),
//       // Sponsor
//       makeTextInput({
//          label: d.conferenceEvent.sponsor.fieldTitle,
//          placeholder: d.conferenceEvent.sponsor.default,
//          id: randomizeID('conferenceEventSponsor'),
//          required: false,
//          short: true,
//          value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.sponsor : undefined
//       }),
//       // Location
//       makeTextInput({
//          label: d.conferenceEvent.location.fieldTitle,
//          placeholder: d.conferenceEvent.location.default,
//          id: randomizeID('conferenceEventLocation'),
//          required: false,
//          short: true,
//          value: context.workData.conferencePaper ? context.workData.conferencePaper.conferenceEvent.location : undefined
//       }),
//       // Dates group
//       makeConferenceDatesGroup(context)(d),
//    ]}),
//    id: 'conferenceEvent',
//    collapsible: 'maxConferenceEvent',
// });

// /**
//  * Conference Papers
//  */
// export const makeConferencePaper = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.conferencePaper.formTitle,
//    miniDescription: make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.conferencePaper.description)
//       ]})
//    ]}),
//    content: make.element({ tag: 'div', properties: { className: 'citations' }, children: [
//       // Description
//       make.element({ tag: 'div', properties: { className: 'field' }, children: [
//          make.element({ tag: 'div', properties: { className: 'description' }, children: [
//             make.text(d.conferencePaper.description)
//          ]})
//       ]}),
//       // Type
//       make.element({ tag: 'div', properties: { className: 'conferencePaperType field'}, children: [
//          make.element({ tag: 'div', properties: { className: 'description' }, children: [
//             make.text(d.conferencePaper.type.fieldTitle)
//          ]}),
//          make.element({ tag: 'div', properties: { className: 'flex all-parent-row center' }, children: [
//             makeRadioInput({
//                label: d.conferencePaper.type.fullText,
//                id: randomizeID('conferencePaperFullText'),
//                name: 'conferencePaperType',
//                value: 'conferencePaperFullText',
//                selected: context.workData.conferencePaper && context.workData.conferencePaper.fullText === true ? true : undefined
//             }),
//             makeRadioInput({
//                label: d.conferencePaper.type.abstract,
//                id: randomizeID('conferencePaperAbstractOnly'),
//                name: 'conferencePaperType',
//                value: 'conferencePaperAbstractOnly',
//                selected: context.workData.conferencePaper && context.workData.conferencePaper.fullText === false ? true : undefined
//             })
//          ]}),
//       ]}),
//       // First page
//       makeTextInput({
//          label: d.conferencePaper.firstPage.fieldTitle,
//          placeholder: d.conferencePaper.firstPage.default,
//          id: randomizeID('conferencePaperFirstPage'),
//          required: false,
//          short: true,
//          value: context.workData.conferencePaper ? context.workData.conferencePaper.firstPage : undefined
//       }),
//       // Last page
//       makeTextInput({
//          label: d.conferencePaper.lastPage.fieldTitle,
//          placeholder: d.conferencePaper.lastPage.default,
//          id: randomizeID('conferencePaperLastPage'),
//          required: false,
//          short: true,
//          value: context.workData.conferencePaper ? context.workData.conferencePaper.lastPage : undefined
//       }),
//    ]}),
//    id: 'conferencePaper',
//    collapsible: 'maxConferencePaper',
// });

// /**
//  * Database information
//  * A Database is a collection of Datasets. Have only one dataset? You still gotta mint a DOI for the database + the dataset.
//  */
// export const makeDatabaseOptionalInfo = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeOptionalCollapsible(d)({
//    title: d.basic.formTitleDatabase,
//    minimized: false,
//    content: [
//       // Created Date
//       make.element({ tag: 'div', properties: { className: 'group flex all-parent-column' }, children: [
//          make.element({ tag: 'div', properties: { className: 'groupLabel flex all-child-span1' }, children: [
//             make.text(d.basic.database.createdDate)
//          ]}),
//          make.element({ tag: 'div', properties: { className: 'fields flex all-child-span1' }, children: [
//             // Year
//             makeTextInput({
//                label: d.basic.originalPublicationDate.year.fieldTitle,
//                placeholder: d.basic.originalPublicationDate.year.default,
//                id: randomizeID('databaseCreatedYear'),
//                required: true,
//                short: true,
//                value: context.workData.database && context.workData.database.dateCreated ? context.workData.database.dateCreated.year : undefined 
//             }),
//             // Month
//             makeTextInput({
//                label: d.basic.originalPublicationDate.month.fieldTitle,
//                placeholder: d.basic.originalPublicationDate.month.default,
//                id: randomizeID('databaseCreatedMonth'),
//                required: false,
//                short: true,
//                value: context.workData.database && context.workData.database.dateCreated ? context.workData.database.dateCreated.month : undefined 
//             }),
//             // Day
//             makeTextInput({
//                label: d.basic.originalPublicationDate.day.fieldTitle,
//                placeholder: d.basic.originalPublicationDate.day.default,
//                id: randomizeID('databaseCreatedDay'),
//                required: false,
//                short: true,
//                value: context.workData.database && context.workData.database.dateCreated ? context.workData.database.dateCreated.day : undefined 
//             }),
//          ]}),
//       ]}),
//       // Updated Date
//       make.element({ tag: 'div', properties: { className: 'group flex all-parent-column' }, children: [
//          make.element({ tag: 'div', properties: { className: 'groupLabel flex all-child-span1' }, children: [
//             make.text(d.basic.database.updatedDate)
//          ]}),
//          make.element({ tag: 'div', properties: { className: 'fields flex all-child-span1' }, children: [
//             // Year
//             makeTextInput({
//                label: d.basic.originalPublicationDate.year.fieldTitle,
//                placeholder: d.basic.originalPublicationDate.year.default,
//                id: randomizeID('databaseUpdatedYear'),
//                required: true,
//                short: true,
//                value: context.workData.database && context.workData.database.dateUpdated ? context.workData.database.dateUpdated.year : undefined 
//             }),
//             // Month
//             makeTextInput({
//                label: d.basic.originalPublicationDate.month.fieldTitle,
//                placeholder: d.basic.originalPublicationDate.month.default,
//                id: randomizeID('databaseUpdatedMonth'),
//                required: false,
//                short: true,
//                value: context.workData.database && context.workData.database.dateUpdated ? context.workData.database.dateUpdated.month : undefined 
//             }),
//             // Day
//             makeTextInput({
//                label: d.basic.originalPublicationDate.day.fieldTitle,
//                placeholder: d.basic.originalPublicationDate.day.default,
//                id: randomizeID('databaseUpdatedDay'),
//                required: false,
//                short: true,
//                value: context.workData.database && context.workData.database.dateUpdated ? context.workData.database.dateUpdated.day : undefined 
//             }),
//          ]}),
//       ]}),
//    ],
// });

// export const makeBasicInformationDatabase = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.basic.formTitleDatabase,
//    miniDescription: make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.basic.formMinidescription)
//       ]})
//    ]}),
//    content: make.element({ tag: 'div', children: [
//       // Title
//       makeTextInput({
//          label: d.basic.title.fieldTitle,
//          placeholder: d.basic.title.defaultDatabase,
//          id: 'workTitle',
//          required: true,
//          short: true,
//          value: context.workData.title
//       }),
//       // Description
//       makeTextInput({
//          label: d.basic.description.fieldTitle,
//          placeholder: d.basic.description.defaultDatabase,
//          id: 'description',
//          required: true,
//          short: false,
//          value: context.workData.description
//       }),
//       // Original Publication Date
//       makeOriginalPublicationDate(d)(context.workData.originalPublicationDate),
//       // Optional info
//       makeDatabaseOptionalInfo(context)(d),
//    ]}),
//    id: 'basicInformationDatabase',
//    collapsible: 'maxBasic',
// });

// /**
//  * Database Publisher
//  */
// export const makePublisherDatabase = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.publisher.formTitle,
//    miniDescription: make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.publisher.descriptionDatabase)
//       ]})
//    ]}),
//    content: make.element({ tag: 'div', children: [
//       // Description
//       make.element({ tag: 'div', properties: { className: 'field' }, children: [
//          make.element({ tag: 'div', properties: { className: 'description' }, children: [
//             make.text(d.publisher.descriptionDatabase)
//          ]})
//       ]}),
//       // Publisher name
//       makeTextInput({
//          label: d.publisher.name.formTitle,
//          placeholder: d.publisher.name.default,
//          id: 'publisherName',
//          required: true,
//          short: true,
//          value: context.workData.publisher ? context.workData.publisher.name : undefined
//       }),
//       // Publisher location
//       makeTextInput({
//          label: d.publisher.location.formTitle,
//          placeholder: d.publisher.location.default,
//          id: 'publisherLocation',
//          required: false,
//          short: true,
//          value: context.workData.publisher ? context.workData.publisher.location : undefined
//       }),
//    ]}),
//    id: 'publisher',
//    collapsible: 'maxPublisher',
// });

// /**
//  * Dataset Information
//  */
// export const makeBasicInformationDataset = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.basic.formTitleDataset,
//    miniDescription: make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.basic.formMinidescription)
//       ]})
//    ]}),
//    content: make.element({ tag: 'div', children: [
//       // Title
//       makeTextInput({
//          label: d.basic.title.fieldTitle,
//          placeholder: d.basic.title.defaultDataset,
//          id: 'workTitle',
//          required: true,
//          short: true,
//          value: context.workData.title
//       }),
//       // Description
//       makeTextInput({
//          label: d.basic.description.fieldTitle,
//          placeholder: d.basic.description.defaultDataset,
//          id: 'description',
//          required: true,
//          short: false,
//          value: context.workData.description
//       }),
//       // Original Publication Date
//       makeOriginalPublicationDate(d)(context.workData.originalPublicationDate),
//       // Optional info
//       makeDatabaseOptionalInfo(context)(d),
//    ]}),
//    id: 'basicInformationDataset',
//    collapsible: 'maxBasic',
// });

// /**
//  * Database DOI for Datasets
//  */
// export const makeDatabaseDOIForm = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.databaseDOI.formTitle,
//    miniDescription:  d.databaseDOI.description,
//    content: make.element({ tag: 'div', children: [
//       // Description
//       d.databaseDOI.description,
//       // DOI
//       makeTextInput({
//          label: d.databaseDOI.databaseDOI.fieldTitle,
//          placeholder: d.databaseDOI.databaseDOI.default,
//          id: 'databaseDOI',
//          required: true,
//          short: true,
//          value: context.workData.parentDatabaseDOI ? context.workData.parentDatabaseDOI : undefined
//       }),
//    ]}),
//    id: 'databaseDOIForm',
//    collapsible: 'maxDatabaseDOI',
// });

// /**
//  * Publisher
//  */
// export const makePublisher = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => makeCollapsibleForm({
//    title: d.publisher.formTitle,
//    miniDescription: make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.publisher.description)
//       ]})
//    ]}),
//    content: make.element({ tag: 'div', children: [
//       // Description
//       make.element({ tag: 'div', properties: { className: 'field' }, children: [
//          make.element({ tag: 'div', properties: { className: 'description' }, children: [
//             make.text(d.publisher.description)
//          ]})
//       ]}),
//       // Publisher name
//       makeTextInput({
//          label: d.publisher.name.formTitle,
//          placeholder: d.publisher.name.default,
//          id: 'publisherName',
//          required: true,
//          short: true,
//          value: context.workData.publisher ? context.workData.publisher.name : undefined
//       }),
//       // Publisher location
//       makeTextInput({
//          label: d.publisher.location.formTitle,
//          placeholder: d.publisher.location.default,
//          id: 'publisherLocation',
//          required: false,
//          short: true,
//          value: context.workData.publisher ? context.workData.publisher.location : undefined
//       }),
//    ]}),
//    id: 'publisher',
//    collapsible: 'maxPublisher',
// });

// /**
//  * Peer Review
//  */
// export const makePeerReviewTypeOptions = (d: Translatables): SelectOption[] => [
//    {
//       label: d.forms.submit.peerReview.type.refereeReport,
//       value: 'refereeReport'
//    },
//    {
//       label: d.forms.submit.peerReview.type.editorReport,
//       value: 'editorReport'
//    },
//    {
//       label: d.forms.submit.peerReview.type.authorComment,
//       value: 'authorComment'
//    },
//    {
//       label: d.forms.submit.peerReview.type.communityComment,
//       value: 'communityComment'
//    },
//    {
//       label: d.forms.submit.peerReview.type.aggregateReport,
//       value: 'aggregateReport'
//    },
//    {
//       label: d.forms.submit.peerReview.type.recommendation,
//       value: 'recommendation'
//    },
// ];

// export const makePeerReviewRecommendationOptions = (d: Translatables): SelectOption[] => [
//    {
//       label: d.forms.submit.peerReview.recommendation.majorRevision,
//       value: 'majorRevision'
//    },
//    {
//       label: d.forms.submit.peerReview.recommendation.minorRevision,
//       value: 'minorRevision'
//    },
//    {
//       label: d.forms.submit.peerReview.recommendation.reject,
//       value: 'reject'
//    },
//    {
//       label: d.forms.submit.peerReview.recommendation.resubmit,
//       value: 'resubmit'
//    },
//    {
//       label: d.forms.submit.peerReview.recommendation.accept,
//       value: 'accept'
//    },
//    {
//       label: d.forms.submit.peerReview.recommendation.acceptReservations,
//       value: 'acceptReservations'
//    },
// ];

// export const makePeerReviewConducted = (context: MarXivDataWithoutSitemap) => (d: SubmissionFormText): HastElement => make.element({ tag: 'div', properties: { className: 'peerReviewInformation field'}, children: [
//    make.element({ tag: 'div', properties: { className: 'description' }, children: [
//       make.text(d.peerReview.prePost.fieldTitle)
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'flex all-parent-row center' }, children: [
//       makeRadioInput({
//          label: d.peerReview.prePost.prePublication,
//          id: 'prePublication',
//          name: 'peerReviewConducted',
//          value: 'prePublication',
//          selected: context.workData.peerReview && context.workData.peerReview.conducted === 'prePublication' ? true : undefined
//       }),
//       makeRadioInput({
//          label: d.peerReview.prePost.postPublication,
//          id: 'postPublication',
//          name: 'peerReviewConducted',
//          value: 'postPublication',
//          selected: context.workData.peerReview && context.workData.peerReview.conducted === 'postPublication' ? true : undefined
//       })
//    ]})
// ]});

// export const makePeerReviewInformation = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeCollapsibleForm({
//    title: d.forms.submit.peerReview.formTitle,
//    miniDescription: make.element({ tag: 'div', properties: { className: 'field' }, children: [
//       make.element({ tag: 'div', properties: { className: 'description' }, children: [
//          make.text(d.forms.submit.peerReview.description)
//       ]})
//    ]}),
//    content: make.element({ tag: 'div', children: [
//       // Description
//       make.element({ tag: 'div', properties: { className: 'field' }, children: [
//          make.element({ tag: 'div', properties: { className: 'description' }, children: [
//             make.text(d.forms.submit.peerReview.description)
//          ]})
//       ]}),
//       // Conducted
//       makePeerReviewConducted(context)(d.forms.submit),
//       // DOI of reviewed work
//       makeTextInput({
//          label: d.forms.submit.peerReview.doiOfReviewedWork.fieldTitle,
//          placeholder: d.forms.submit.peerReview.doiOfReviewedWork.default,
//          id: 'doiOfReviewedWork',
//          required: true,
//          short: true,
//          value: context.workData.peerReview ? context.workData.peerReview.doiofReviewedWork : undefined
//       }),
//       // Type
//       makeSelectList({
//          label: d.forms.submit.peerReview.type.fieldTitle,
//          id: 'peerReviewType',
//          required: true,
//          multi: false,
//          options: makePeerReviewTypeOptions(d),
//          selected: context.workData.peerReview ? context.workData.peerReview.type : undefined
//       }),
//       // Recommendation
//       makeSelectList({
//          label: d.forms.submit.peerReview.recommendation.fieldTitle,
//          id: 'peerReviewRecommendation',
//          required: false,
//          multi: false,
//          options: makePeerReviewRecommendationOptions(d),
//          selected: context.workData.peerReview ? context.workData.peerReview.recommendation : undefined
//       }),
//       // Competing Interest Statement
//       makeTextInput({
//          label: d.forms.submit.peerReview.competingInterests.fieldTitle,
//          placeholder: d.forms.submit.peerReview.competingInterests.default,
//          id: 'competingInterestStatement',
//          required: false,
//          short: false,
//          value: context.workData.peerReview ? context.workData.peerReview.competingInterestStatement : undefined
//       }),
//    ]}),
//    id: 'peerReviewInformation',
//    collapsible: 'maxPeerReview',
// });

/**
 * Make submission forms for each of the Share buttons:
 *  - preprint,
 *  - oaPaper (as preprint),
 *  - postprint (as preprint),
 *  - unCopyrightedPaper (as preprint),
 *  - unpublishedReport (as preprint),
 *  - thesis (as preprint),
 *  - posterEtc (as preprint),
 *  - letter (as preprint),
 *  - workingPaper (as preprint),
 *  - publishedReport,
 *  - publishedConferencePaper
 *  - peerReview
 *  - database
 *  - dataset (child of database)
 * 
 * Note that Unpublished Conference Papers should use the exact same submission form as preprints (no changes whatsoever)
 */

/**
 * Preprint Submission Form
 */
// export const makeSubmissionFormPreprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.preprint)('uploadPreprint')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Open Access paper
 */
// export const makeSubmissionFormOA = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.openAccess)('uploadOA')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Postprint
 */
// export const makeSubmissionFormPostprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.postprint)('uploadPostprint')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information minus Peer Review (since it's a postprint, it's been peer-reviewed already)
//    makeBasicInformationPostedContentNoPeerReview(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePostprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Un-copyrighted paper
 */
// export const makeSubmissionFormUncopyrighted = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.unCopyrighted)('uploadUncopyrighted')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicenseCopyrighted(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Unpublished Report
 */
// export const makeSubmissionFormReportUnpublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.unpublishedReport)('uploadReportUnpublished')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Thesis / Dissertation
 */
// export const makeSubmissionFormThesis = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.thesis)('uploadThesis')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Poster, Presentation, or Supplementary Information
 */
// export const makeSubmissionFormPoster = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.posterPresentationSI)('uploadPoster')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Letter
 */
// export const makeSubmissionFormLetter = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.letter)('uploadLetter')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Working Paper
 */
// export const makeSubmissionFormWorkingPaper = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.workingPaper)('uploadWorkingPaper')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Published Report
 */
// export const makeSubmissionFormReportPublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.publishedReport)('uploadReportPublished')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information minus Publisher's DOI and Peer-Review question
//    makeBasicInformationNoDOIorPeerReview(context)(d),
//    // Publisher
//    // makePublisher(context)(d.forms.submit),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Assign DOI
//    customizeDOI(context)(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Published Conference Paper
 */
// export const makeSubmissionFormConferencePaperPublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.publishedConference)('uploadConferencePaperPublished')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // BBasic Information minus Publisher's DOI and Peer-Review question
//    makeBasicInformationNoDOIorPeerReview(context)(d),
//    // Conference Paper Info
//    makeConferencePaper(context)(d.forms.submit),
//    // Conference Event Info
//    makeConferenceEvent(context)(d.forms.submit),
//    // Supporting Institutions
//    makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    makeCitations(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Assign DOI
//    customizeDOI(context)(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Peer Review
 */
// export const makeSubmissionFormPeerReview = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.peerReview)('uploadPeerReview')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Upload PDF File
//    makeUploadFilePDF(d.forms.submit),
//    // Basic Information minus Publisher's DOI and Peer-Review question
//    makeBasicInformationNoDOIorPeerReview(context)(d),
//    // Peer Review Information
//    makePeerReviewInformation(context)(d),
//    // Supporting Institutions
//    makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Hide relationships, since this will be handled automatically
//    // License
//    makeLicensePreprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Database, a collection for Datasets
 */
// export const makeSubmissionFormDatabase = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.database)('uploadDatabase')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Basic Information for a Database
//    makeBasicInformationDatabase(context)(d.forms.submit),
//    // Publisher
//    makePublisherDatabase(context)(d.forms.submit),
//    // Supporting Institutions
//    makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    makeCitations(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

/**
 * Dataset, a child of a Database
 */
// export const makeSubmissionFormDataset = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.submit.titles.dataset)('uploadDataset')([
//    // Disclaimer
//    d.forms.submit.topDisclaimer,
//    // Database DOI
//    makeDatabaseDOIForm(context)(d.forms.submit),
//    // Dataset file upload
//    makeUploadFileDataset(d.forms.submit),
//    // Basic Information for a Dataset
//    makeBasicInformationDataset(context)(d.forms.submit),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Support MarXiv
//    makeDonationForm(d.forms.submit),
//    // Form end
//    makeFormEnd(d),
// ]);

export const makeReactSubmissionForm = (d: FormMessageText) => (context: MarXivDataWithoutSitemap) => make.element({ tag: 'div', properties: { id: 'reactSubmitForm' }, children: [
   make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.reactSubmitFormBundle), type: 'module', async: 'async' }, children:[] }),
   make.element({ tag: 'h2', children: [
      make.text(d.loading.loading + ' ' + d.loading.form + '...')
   ]})
]});