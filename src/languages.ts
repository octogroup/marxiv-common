/**
 * Determine the trasnlatables to use to build a page.
 */
import { MarXivDataWithoutSitemap } from './context';
import { makeEnglishTranslatables } from './translatablesEnglish';
import { Translatables } from './translatables';

/**
 * Make a list of supported languages
 */
export type SupportedLanguage = 'en';

/**
 * Supply the appropriate translatables
 */
export interface TranslatableMetadata {
   translatables: Translatables
   code: SupportedLanguage
}

export const supplyTranslatables = (context: MarXivDataWithoutSitemap) => (language: SupportedLanguage): TranslatableMetadata => {
   switch (language) {
      case 'en':
         return {
            translatables: makeEnglishTranslatables(context)(context.workData),
            code: 'en'
         };
      default:
         return {
            translatables: makeEnglishTranslatables(context)(context.workData),
            code: 'en'
         };
   }
}