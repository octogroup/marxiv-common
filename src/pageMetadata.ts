/**
 * Implementation for PageMetadata
 */
import { HastElement, } from '@octogroup/hast-typescript';
import { PageMetadata, PageMetadataMinimum, WorkMetadata, MarXivDataWithoutSitemap, UseWorkHead, } from './context';
import { Translatables, TranslatablePageMetadata, FormMessageText } from './translatables';
import { makeHomepageContent } from './homepage';
import { makeWhyMarXiv, makeMarXivTeam, makeStaticAmbassadors, makeMarXivForOrgs, makeCodeOfConduct, makePrivacyPolicy, makeCelebrate, makeLicenseOptions, makeConfirmNewSubmission, makeConfirmUpdated, makeConfirmResubmit, makeConfirmNewAccount, makeConfirmVerifyEmail, makeErrorPageNotFound, makeErrorPageGeneral, makeErrorPageAccessDenied, makeTypesOfWorks, } from './staticPages';
import { makeWorkListing, } from './workListing';
import { makeWorkPage, WorkData } from './works';
import { makeSelfArchivingPolicies, } from './selfArchivingPolicies';
import { makeSubmissionGuidelines, } from './guidelines';
import { makeUserRegistrationForm, makeUserLoginForm, makeUserResetPasswordForm, makeUserAccountPage, makeEditUserAccountForm, makeUserLogout, makeAdminEditUserAccountForm } from './userForms';
import { makeReactDashboard, makeReactAdminDashboard } from './dashboards';
import { makeReactSubmissionForm, } from './submitForms';
import { makeReactEditForm } from './editForms';
import { makeReactModerationForm } from './moderateForms';
import { makeGoogleSearch } from './googleSearch';
import { PaginatorData, makeWorkListingPaginatedPage } from './workListing';

// Homepage
export const makePageMetadataHomepageMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.homepage.title,
   localURL: '/',
   depth: 'primary',
});

export const makePageMetadataHomepage = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataHomepageMinimum(d.pageMetadata),
   description: d.pageMetadata.homepage.description,
   bodyContent: makeHomepageContent(context)(d),
   noGutter: true,
   homepageHeader: true,
   submenuToDisplay: 'none'
});

// Why MarXiv?
export const makePageMetadataWhyMarXivMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.whyMarXiv.title,
   localURL: '/why',
   depth: 'secondary',
});

export const makePageMetadataWhyMarXiv = (d: Translatables): PageMetadata => ({
   ...makePageMetadataWhyMarXivMinimum(d.pageMetadata),
   description: d.pageMetadata.whyMarXiv.description,
   bodyContent: makeWhyMarXiv(d.pages.staticPages),
   submenuToDisplay: 'why'
});

// MarXiv Team
export const makePageMetadataTeamMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.team.title,
   localURL: '/why/team',
   depth: 'tertiary',
});

export const makePageMetadataTeam = (d: Translatables): PageMetadata => ({
   ...makePageMetadataTeamMinimum(d.pageMetadata),
   description: d.pageMetadata.team.description,
   bodyContent: makeMarXivTeam(d.pages.staticPages),
   submenuToDisplay: 'why'
});

// MarXiv Ambassadors
export const makePageMetadataAmbassadorsMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.ambassadors.title,
   localURL: '/why/ambassadors',
   depth: 'tertiary',
});

export const makePageMetadataAmbassadors = (d: Translatables): PageMetadata => ({
   ...makePageMetadataAmbassadorsMinimum(d.pageMetadata),
   description: d.pageMetadata.ambassadors.description,
   bodyContent: makeStaticAmbassadors(d.pages.staticPages),
   submenuToDisplay: 'why'
});

// Work Listing
export const makePageMetadataWorkListingMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.workListing.title,
   localURL: '/works',
   depth: 'secondary',
});

export const makePageMetadataWorkListing = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataWorkListingMinimum(d.pageMetadata),
   description: d.pageMetadata.workListing.description,
   bodyContent: makeWorkListing(context)(d)(context.works),
   submenuToDisplay: 'none'
});

// Work Listing Pagination
export const makePageMetadataWorkListingPaginationMinimum = (d: TranslatablePageMetadata) => (pages: PaginatorData[]): PageMetadataMinimum[] => {
   // Loop over the Works array
   let workListingPaginationMetadata: PageMetadataMinimum[] = new Array;
   for (let i = 0; i < pages.length; i++) {
      let page = pages[i];
      workListingPaginationMetadata.push({
         title: d.workListing.title,
         localURL: '/page/' + page.currentPage.toString(),
         depth: 'tertiary',
      });
   }
   return workListingPaginationMetadata
}

export const makePageMetadataWorkListingPagination = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (pages: PaginatorData[]): PageMetadata[] => {
   // Make an empty array to mutate.
   let workListingPaginatedPages: PageMetadata[] = new Array;
   for (let i = 0; i < pages.length; i++) {

      workListingPaginatedPages.push({
         title: d.pageMetadata.workListing.title,
         localURL: '/page/' + pages[i].currentPage.toString(),
         depth: 'tertiary',
         description: d.pageMetadata.workListing.description,
         submenuToDisplay: 'none',
         noBots: true,
         bodyContent: makeWorkListingPaginatedPage(context)(d)(pages[i])
      });
   }
   return workListingPaginatedPages
}

// Works Pages
export const makePageMetadataWorksMinimum = (works: WorkData[]): (UseWorkHead & PageMetadataMinimum)[] => {
   // Loop over the Works array
   let worksMetadata: (UseWorkHead & PageMetadataMinimum)[] = new Array;
   for (let i = 0; i < works.length; i++) {
      let work = works[i];
      worksMetadata.push({
         title: work.title,
         localURL: '/works/' + work.workID,
         useWorkHead: true,
         depth: 'tertiary'
      });
   }
   return worksMetadata
}

export const makePageMetadataWorks = (context: MarXivDataWithoutSitemap) => (d: Translatables): WorkMetadata[] => {
   // Loop over the Works array
   let works: WorkMetadata[] = new Array;
   for (let i = 0; i < context.works.length; i++) {
      let work = context.works[i];
      works.push({
         title: work.title,
         description: work.description,
         localURL: '/works/' + work.workID,
         bodyContent: makeWorkPage(context)(d)(work),
         useWorkHead: true,
         submenuToDisplay: 'none',
         depth: 'tertiary'
      });
   }
   return works
}

// MarXiv for Orgs
export const makePageMetadataOrgsMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.forOrgs.title,
   localURL: '/orgs',
   depth: 'secondary',
});

export const makePageMetadataOrgs = (d: Translatables): PageMetadata => ({
   ...makePageMetadataOrgsMinimum(d.pageMetadata),
   description: d.pageMetadata.forOrgs.description,
   bodyContent: makeMarXivForOrgs(d.pages.staticPages),
   submenuToDisplay: 'none'
});

// Share Guide pages
export const makePageMetadataShareMinimum = (d: TranslatablePageMetadata) => (path?: string): PageMetadataMinimum => ({
   title: d.submit.title,
   localURL: path ? '/share/' + path : '/share',
   depth: 'tertiary',
});

export const makePageMetadataShare = (d: Translatables) => (bodyFn: HastElement) => (path?: string): PageMetadata => ({
   ...makePageMetadataShareMinimum(d.pageMetadata)(path),
   description: d.pageMetadata.submit.description,
   bodyContent: bodyFn,
   submenuToDisplay: 'share'
});

// Self-archiving policies
export const makePageMetadataSelfArchivingPoliciesMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.selfArchivingPolicies.title,
   localURL: '/policies',
   depth: 'secondary',
});

export const makePageMetadataSelfArchivingPolicies = (d: Translatables): PageMetadata => ({
   ...makePageMetadataSelfArchivingPoliciesMinimum(d.pageMetadata),
   description: d.pageMetadata.selfArchivingPolicies.description,
   bodyContent: makeSelfArchivingPolicies(d.pages.selfArchivingPolicies),
   submenuToDisplay: 'share'
});

// Full submission guidelines
export const makePageMetadataSubmissionGuidelinesMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.submissionGuidelines.title,
   localURL: '/guidelines',
   depth: 'secondary',
});

export const makePageMetadataSubmissionGuidelines = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataSubmissionGuidelinesMinimum(d.pageMetadata),
   description: d.pageMetadata.submissionGuidelines.description,
   bodyContent: makeSubmissionGuidelines(context)(d.pages.submissionGuidelines),
   submenuToDisplay: 'share'
});

// Code of Conduct
export const makePageMetadataCodeOfConductMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.codeOfConduct.title,
   localURL: '/conduct',
   depth: 'secondary',
});

export const makePageMetadataCodeOfConduct = (d: Translatables): PageMetadata => ({
   ...makePageMetadataCodeOfConductMinimum(d.pageMetadata),
   description: d.pageMetadata.codeOfConduct.description,
   bodyContent: makeCodeOfConduct(d.pages.staticPages),
   submenuToDisplay: 'share'
});

// Privacy Policy
export const makePageMetadataPrivacyPolicyMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.privacyPolicy.title,
   localURL: '/privacy',
   depth: 'secondary',
});

export const makePageMetadataPrivacyPolicy = (d: Translatables): PageMetadata => ({
   ...makePageMetadataPrivacyPolicyMinimum(d.pageMetadata),
   description: d.pageMetadata.privacyPolicy.description,
   bodyContent: makePrivacyPolicy(d.pages.staticPages),
   submenuToDisplay: 'none'
});

// Celebration page with otter gif
export const makePageMetadataCelebrateMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.celebrate.title,
   localURL: '/celebrate',
   depth: 'secondary',
});

export const makePageMetadataCelebrate = (d: Translatables): PageMetadata => ({
   ...makePageMetadataCelebrateMinimum(d.pageMetadata),
   description: d.pageMetadata.celebrate.description,
   bodyContent: makeCelebrate(d.pages.staticPages),
   noBots: true,
   submenuToDisplay: 'none'
});

// License Information
export const makePageMetadataLicensesMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.licenses.title,
   localURL: '/licenses',
   depth: 'secondary',
});

export const makePageMetadataLicenses = (d: Translatables): PageMetadata => ({
   ...makePageMetadataLicensesMinimum(d.pageMetadata),
   description: d.pageMetadata.licenses.description,
   bodyContent: makeLicenseOptions(d),
   submenuToDisplay: 'none'
});

// Types of Works Accepted / Metadata
export const makePageMetadataTypesOfWorksAcceptedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.typesOfWork.title,
   localURL: '/metadata',
   depth: 'secondary',
});

export const makePageMetadataTypesOfWorksAccepted = (d: Translatables): PageMetadata => ({
   ...makePageMetadataTypesOfWorksAcceptedMinimum(d.pageMetadata),
   description: d.pageMetadata.typesOfWork.description,
   bodyContent: makeTypesOfWorks(d.pages.staticPages),
   submenuToDisplay: 'none'
});

// Join MarXiv
export const makePageMetadataJoinMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.join.title,
   localURL: '/join',
   depth: 'secondary',
});

export const makePageMetadataJoin = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataJoinMinimum(d.pageMetadata),
   description: d.pageMetadata.join.description,
   bodyContent: makeUserRegistrationForm(d.forms.messages)(context),
   submenuToDisplay: 'join',
   isReactPage: true,
});

// Login to MarXiv
export const makePageMetadataLoginMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.login.title,
   localURL: '/login',
   depth: 'secondary',
});

export const makePageMetadataLogin = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataLoginMinimum(d.pageMetadata),
   description: d.pageMetadata.login.description,
   bodyContent: makeUserLoginForm(d.forms.messages)(context),
   submenuToDisplay: 'join',
   isReactPage: true,
});

// Reset Password
export const makePageMetadataResetPasswordMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.resetPassword.title,
   localURL: '/reset',
   depth: 'secondary',
});

export const makePageMetadataResetPassword = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataResetPasswordMinimum(d.pageMetadata),
   description: d.pageMetadata.resetPassword.description,
   bodyContent: makeUserResetPasswordForm(d.forms.messages)(context),
   submenuToDisplay: 'join',
   isReactPage: true,
});

// Logout
export const makePageMetadataLogoutMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.logout.title,
   localURL: '/logout',
   depth: 'secondary',
});

export const makePageMetadataLogout = (d: Translatables): PageMetadata => ({
   ...makePageMetadataLogoutMinimum(d.pageMetadata),
   description: d.pageMetadata.logout.description,
   bodyContent: makeUserLogout(d),
   submenuToDisplay: 'none',
   isReactPage: true,
});

// User Dashboards
export const makePageMetadataUserDashboardsMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.user.dashboard.title,
   localURL: '/user/dashboard',
   depth: 'tertiary',
})

export const makePageMetadataUserDashboards = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   title: d.pageMetadata.user.dashboard.title,
   description: d.pageMetadata.user.dashboard.description,
   localURL: '/user/dashboard',
   bodyContent: makeReactDashboard(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   depth: 'tertiary',
   isReactPage: true,
})

// User Account
export const makePageMetadataUserAccountMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.user.account.title,
   localURL: '/user/account',
   depth: 'tertiary',
});

export const makePageMetadataUserAccount = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataUserAccountMinimum(d.pageMetadata),
   description: d.pageMetadata.user.account.description,
   bodyContent: makeUserAccountPage(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Edit User Account
export const makePageMetadataEditUserAccountMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.user.editAccount.title,
   localURL: '/user/editAccount',
   depth: 'tertiary',
});

export const makePageMetadataEditUserAccount = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataEditUserAccountMinimum(d.pageMetadata),
   description: d.pageMetadata.user.editAccount.description,
   bodyContent: makeEditUserAccountForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Confirm new submission
export const makePageMetadataConfirmNewSubmissionMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.confirm.newSubmission.title,
   localURL: '/confirm/newSubmission',
   depth: 'tertiary',

});

export const makePageMetadataConfirmNewSubmission = (d: Translatables): PageMetadata => ({
   ...makePageMetadataConfirmNewSubmissionMinimum(d.pageMetadata),
   description: d.pageMetadata.confirm.newSubmission.description,
   bodyContent: makeConfirmNewSubmission(d.pages.staticPages),
   noBots: true,
   submenuToDisplay: 'user'
});

// Confirm updated work
export const makePageMetadataConfirmUpdatedWorkMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.confirm.updatedWork.title,
   localURL: '/confirm/updated',
   depth: 'tertiary',
});

export const makePageMetadataConfirmUpdatedWork = (d: Translatables): PageMetadata => ({
   ...makePageMetadataConfirmUpdatedWorkMinimum(d.pageMetadata),
   description: d.pageMetadata.confirm.updatedWork.description,
   bodyContent: makeConfirmUpdated(d.pages.staticPages),
   noBots: true,
   submenuToDisplay: 'user'
});

// Confirm re-submitted work
export const makePageMetadataConfirmResubmitMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.confirm.resubmit.title,
   localURL: '/confirm/resubmit',
   depth: 'tertiary',
});

export const makePageMetadataConfirmResubmit = (d: Translatables): PageMetadata => ({
   ...makePageMetadataConfirmResubmitMinimum(d.pageMetadata),
   description: d.pageMetadata.confirm.resubmit.description,
   bodyContent: makeConfirmResubmit(d.pages.staticPages),
   noBots: true,
   submenuToDisplay: 'user'
});

// Confirm new MarXiv account
export const makePageMetadataConfirmNewAccountMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.confirm.newAccount.title,
   localURL: '/confirm/newAccount',
   depth: 'tertiary',
});

export const makePageMetadataConfirmNewAccount = (d: Translatables): PageMetadata => ({
   ...makePageMetadataConfirmNewAccountMinimum(d.pageMetadata),
   description: d.pageMetadata.confirm.newAccount.description,
   bodyContent: makeConfirmNewAccount(d.pages.staticPages),
   noBots: true,
   submenuToDisplay: 'user'
});

// Verify Email Address confirmation
export const makePageMetadataConfirmVerifyEmailMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.confirm.verifyEmail.title,
   localURL: '/confirm/email',
   depth: 'tertiary',
});

export const makePageMetadataConfirmVerifyEmail = (d: Translatables): PageMetadata => ({
   ...makePageMetadataConfirmVerifyEmailMinimum(d.pageMetadata),
   description: d.pageMetadata.confirm.verifyEmail.description,
   bodyContent: makeConfirmVerifyEmail(d.pages.staticPages),
   noBots: true,
   submenuToDisplay: 'user'
});

/**
 * Submission Forms
 */
// Upload a preprint
export const makePageMetadataSharePreprintMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.upload.preprint.title,
   localURL: '/submit/preprint',
   depth: 'tertiary',
});

export const makePageMetadataSharePreprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataSharePreprintMinimum(d.pageMetadata),
   description: d.pageMetadata.upload.preprint.description,
   bodyContent: makeReactSubmissionForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Upload a postprint
export const makePageMetadataSharePostrintMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.upload.postprint.title,
   localURL: '/submit/postprint',
   depth: 'tertiary',
});

export const makePageMetadataSharePostprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataSharePostrintMinimum(d.pageMetadata),
   description: d.pageMetadata.upload.postprint.description,
   bodyContent: makeReactSubmissionForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Upload an OA work
export const makePageMetadataShareOAMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.upload.openAccess.title,
   localURL: '/submit/oa',
   depth: 'tertiary',
});

export const makePageMetadataShareOA = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataShareOAMinimum(d.pageMetadata),
   description: d.pageMetadata.upload.openAccess.description,
   bodyContent: makeReactSubmissionForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Upload an uncopyrighted work
export const makePageMetadataShareUncopyrightedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.upload.unCopyrighted.title,
   localURL: '/submit/uncopyrighted',
   depth: 'tertiary',
});

export const makePageMetadataShareUncopyrighted = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataShareUncopyrightedMinimum(d.pageMetadata),
   description: d.pageMetadata.upload.unCopyrighted.description,
   bodyContent: makeReactSubmissionForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Upload an unpublished report
export const makePageMetadataShareReportUnpublishedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.upload.unpublishedReport.title,
   localURL: '/submit/reportUnpublished',
   depth: 'tertiary',
});

export const makePageMetadataShareReportUnpublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataShareReportUnpublishedMinimum(d.pageMetadata),
   description: d.pageMetadata.upload.unpublishedReport.description,
   bodyContent: makeReactSubmissionForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Upload a thesis
export const makePageMetadataShareThesisMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.upload.thesis.title,
   localURL: '/submit/thesis',
   depth: 'tertiary',
});

export const makePageMetadataShareThesis = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataShareThesisMinimum(d.pageMetadata),
   description: d.pageMetadata.upload.thesis.description,
   bodyContent: makeReactSubmissionForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Upload a database
// export const makePageMetadataShareDatabaseMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.upload.database.title,
//    localURL: '/submit/database',
//    depth: 'tertiary',
// });

// export const makePageMetadataShareDatabase = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataShareDatabaseMinimum(d.pageMetadata),
//    description: d.pageMetadata.upload.database.description,
//    bodyContent: makeReactSubmissionForm(context),
//    noBots: true,
//    submenuToDisplay: 'user',
// });

// Upload a dataset
// export const makePageMetadataShareDatasetMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.upload.dataset.title,
//    localURL: '/submit/dataset',
//    depth: 'tertiary',
// });

// export const makePageMetadataShareDataset = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataShareDatasetMinimum(d.pageMetadata),
//    description: d.pageMetadata.upload.dataset.description,
//    bodyContent: makeReactSubmissionForm(context),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Upload a poster, etc.
export const makePageMetadataSharePosterMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.upload.posterPresentationSI.title,
   localURL: '/submit/poster',
   depth: 'tertiary',
});

export const makePageMetadataSharePoster = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataSharePosterMinimum(d.pageMetadata),
   description: d.pageMetadata.upload.posterPresentationSI.description,
   bodyContent: makeReactSubmissionForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Upload a letter
export const makePageMetadataShareLetterMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.upload.letter.title,
   localURL: '/submit/letter',
   depth: 'tertiary',
});

export const makePageMetadataShareLetter = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataShareLetterMinimum(d.pageMetadata),
   description: d.pageMetadata.upload.letter.description,
   bodyContent: makeReactSubmissionForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Upload a working paper
export const makePageMetadataShareWorkingPaperMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.upload.workingPaper.title,
   localURL: '/submit/workingPaper',
   depth: 'tertiary',
});

export const makePageMetadataShareWorkingPaper = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataShareWorkingPaperMinimum(d.pageMetadata),
   description: d.pageMetadata.upload.workingPaper.description,
   bodyContent: makeReactSubmissionForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Upload a peer review
// export const makePageMetadataSharePeerReviewMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.upload.peerReview.title,
//    localURL: '/submit/peerReview',
//    depth: 'tertiary',
// });

// export const makePageMetadataSharePeerReview = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataSharePeerReviewMinimum(d.pageMetadata),
//    description: d.pageMetadata.upload.peerReview.description,
//    bodyContent: makeReactSubmissionForm(context),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Upload a published report
// export const makePageMetadataShareReportPublishedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.upload.publishedReport.title,
//    localURL: '/submit/reportPublished',
//    depth: 'tertiary',
// });

// export const makePageMetadataShareReportPublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataShareReportPublishedMinimum(d.pageMetadata),
//    description: d.pageMetadata.upload.publishedReport.description,
//    bodyContent: makeReactSubmissionForm(context),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Upload a conference
// export const makePageMetadataShareConferenceMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.upload.publishedConferencePaper.title,
//    localURL: '/submit/conference',
//    depth: 'tertiary',
// });

// export const makePageMetadataShareConference = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataShareConferenceMinimum(d.pageMetadata),
//    description: d.pageMetadata.upload.publishedConferencePaper.description,
//    bodyContent: makeReactSubmissionForm(context),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

/**
 * Edit Forms
 */
// Edit a preprint
export const makePageMetadataEditPreprintMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.edit.preprint.title,
   localURL: '/edit/preprint',
   depth: 'tertiary',
});

export const makePageMetadataEditPreprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataEditPreprintMinimum(d.pageMetadata),
   description: d.pageMetadata.edit.preprint.description,
   bodyContent: makeReactEditForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Edit a postprint
export const makePageMetadataEditPostprintMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.edit.postprint.title,
   localURL: '/edit/postprint',
   depth: 'tertiary',
});

export const makePageMetadataEditPostprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataEditPostprintMinimum(d.pageMetadata),
   description: d.pageMetadata.edit.postprint.description,
   bodyContent: makeReactEditForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Edit an OA work
export const makePageMetadataEditOAMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.edit.openAccess.title,
   localURL: '/edit/oa',
   depth: 'tertiary',
});

export const makePageMetadataEditOA = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataEditOAMinimum(d.pageMetadata),
   description: d.pageMetadata.edit.openAccess.description,
   bodyContent: makeReactEditForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Edit an uncopyrighted work
export const makePageMetadataEditUncopyrightedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.edit.unCopyrighted.title,
   localURL: '/edit/uncopyrighted',
   depth: 'tertiary',
});

export const makePageMetadataEditUncopyrighted = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataEditUncopyrightedMinimum(d.pageMetadata),
   description: d.pageMetadata.edit.unCopyrighted.description,
   bodyContent: makeReactEditForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Edit an unpublished report
export const makePageMetadataEditReportUnpublishedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.edit.unpublishedReport.title,
   localURL: '/edit/reportUnpublished',
   depth: 'tertiary',
});

export const makePageMetadataEditReportUnpublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataEditReportUnpublishedMinimum(d.pageMetadata),
   description: d.pageMetadata.edit.unpublishedReport.description,
   bodyContent: makeReactEditForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Edit a thesis
export const makePageMetadataEditThesisMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.edit.thesis.title,
   localURL: '/edit/thesis',
   depth: 'tertiary',
});

export const makePageMetadataEditThesis = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataEditThesisMinimum(d.pageMetadata),
   description: d.pageMetadata.edit.thesis.description,
   bodyContent: makeReactEditForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Edit a database
// export const makePageMetadataEditDatabaseMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.edit.database.title,
//    localURL: '/edit/database',
//    depth: 'tertiary',
// });


// export const makePageMetadataEditDatabase = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataEditDatabaseMinimum(d.pageMetadata),
//    description: d.pageMetadata.edit.database.description,
//    bodyContent: makeEditFormDatabase(context)(d),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Edit a dataset
// export const makePageMetadataEditDatasetMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.edit.dataset.title,
//    localURL: '/edit/dataset',
//    depth: 'tertiary',
// });

// export const makePageMetadataEditDataset = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataEditDatasetMinimum(d.pageMetadata),
//    description: d.pageMetadata.edit.dataset.description,
//    bodyContent: makeEditFormDataset(context)(d),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Edit a poster, etc.
export const makePageMetadataEditPosterMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.edit.posterPresentationSI.title,
   localURL: '/edit/poster',
   depth: 'tertiary',
});

export const makePageMetadataEditPoster = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataEditPosterMinimum(d.pageMetadata),
   description: d.pageMetadata.edit.posterPresentationSI.description,
   bodyContent: makeReactEditForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Edit a letter
export const makePageMetadataEditLetterMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.edit.letter.title,
   localURL: '/edit/letter',
   depth: 'tertiary',
});

export const makePageMetadataEditLetter = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataEditLetterMinimum(d.pageMetadata),
   description: d.pageMetadata.edit.letter.description,
   bodyContent: makeReactEditForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Edit a working paper
export const makePageMetadataEditWorkingPaperMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.edit.workingPaper.title,
   localURL: '/edit/workingPaper',
   depth: 'tertiary',
});

export const makePageMetadataEditWorkingPaper = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataEditWorkingPaperMinimum(d.pageMetadata),
   description: d.pageMetadata.edit.workingPaper.description,
   bodyContent: makeReactEditForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Edit a peer review
// export const makePageMetadataEditPeerReviewMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.edit.peerReview.title,
//    localURL: '/edit/peerReview',
//    depth: 'tertiary',
// });

// export const makePageMetadataEditPeerReview = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataEditPeerReviewMinimum(d.pageMetadata),
//    description: d.pageMetadata.edit.peerReview.description,
//    bodyContent: makeEditFormPeerReview(context)(d),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Edit a published report
// export const makePageMetadataEditReportPublishedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.edit.publishedReport.title,
//    localURL: '/edit/reportPublished',
//    depth: 'tertiary',
// });

// export const makePageMetadataEditReportPublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataEditReportPublishedMinimum(d.pageMetadata),
//    description: d.pageMetadata.edit.publishedReport.description,
//    bodyContent: makeEditFormReportPublished(context)(d),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Edit a conference
// export const makePageMetadataEditConferenceMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.edit.publishedConferencePaper.title,
//    localURL: '/edit/conference',
//    depth: 'tertiary',
// });

// export const makePageMetadataEditConference = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataEditConferenceMinimum(d.pageMetadata),
//    description: d.pageMetadata.edit.publishedConferencePaper.description,
//    bodyContent: makeEditFormConferencePaperPublished(context)(d),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

/**
 * Moderation Forms
 */
// Moderate a preprint
export const makePageMetadataModeratePreprintMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.moderate.preprint.title,
   localURL: '/moderate/preprint',
   depth: 'tertiary',
});

export const makePageMetadataModeratePreprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataModeratePreprintMinimum(d.pageMetadata),
   description: d.pageMetadata.moderate.preprint.description,
   bodyContent: makeReactModerationForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Moderate a postprint
export const makePageMetadataModeratePostprintMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.moderate.postprint.title,
   localURL: '/moderate/postprint',
   depth: 'tertiary',
});

export const makePageMetadataModeratePostprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataModeratePostprintMinimum(d.pageMetadata),
   description: d.pageMetadata.moderate.postprint.description,
   bodyContent: makeReactModerationForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Moderate an OA work
export const makePageMetadataModerateOAMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.moderate.openAccess.title,
   localURL: '/moderate/oa',
   depth: 'tertiary',
});

export const makePageMetadataModerateOA = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataModerateOAMinimum(d.pageMetadata),
   description: d.pageMetadata.moderate.openAccess.description,
   bodyContent: makeReactModerationForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Moderate an uncopyrighted work
export const makePageMetadataModerateUncopyrightedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.moderate.unCopyrighted.title,
   localURL: '/moderate/uncopyrighted',
   depth: 'tertiary',
});

export const makePageMetadataModerateUncopyrighted = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataModerateUncopyrightedMinimum(d.pageMetadata),
   description: d.pageMetadata.moderate.unCopyrighted.description,
   bodyContent: makeReactModerationForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Moderate an unpublished report
export const makePageMetadataModerateReportUnpublishedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.moderate.unpublishedReport.title,
   localURL: '/moderate/reportUnpublished',
   depth: 'tertiary',
});

export const makePageMetadataModerateReportUnpublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataModerateReportUnpublishedMinimum(d.pageMetadata),
   description: d.pageMetadata.moderate.unpublishedReport.description,
   bodyContent: makeReactModerationForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Moderate a thesis
export const makePageMetadataModerateThesisMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.moderate.thesis.title,
   localURL: '/moderate/thesis',
   depth: 'tertiary',
});

export const makePageMetadataModerateThesis = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataModerateThesisMinimum(d.pageMetadata),
   description: d.pageMetadata.moderate.thesis.description,
   bodyContent: makeReactModerationForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Moderate a database
// export const makePageMetadataModerateDatabaseMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.moderate.database.title,
//    localURL: '/moderate/database',
//    depth: 'tertiary',
// });

// export const makePageMetadataModerateDatabase = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataModerateDatabaseMinimum(d.pageMetadata),
//    description: d.pageMetadata.moderate.database.description,
//    bodyContent: makeModerateFormDatabase(context)(d),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Moderate a dataset
// export const makePageMetadataModerateDatasetMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.moderate.dataset.title,
//    localURL: '/moderate/dataset',
//    depth: 'tertiary',
// });

// export const makePageMetadataModerateDataset = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataModerateDatasetMinimum(d.pageMetadata),
//    description: d.pageMetadata.moderate.dataset.description,
//    bodyContent: makeModerateFormDataset(context)(d),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Moderate a poster, etc.
export const makePageMetadataModeratePosterMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.moderate.posterPresentationSI.title,
   localURL: '/moderate/poster',
   depth: 'tertiary',
});

export const makePageMetadataModeratePoster = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataModeratePosterMinimum(d.pageMetadata),
   description: d.pageMetadata.moderate.posterPresentationSI.description,
   bodyContent: makeReactModerationForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Moderate a letter
export const makePageMetadataModerateLetterMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.moderate.letter.title,
   localURL: '/moderate/letter',
   depth: 'tertiary',
});

export const makePageMetadataModerateLetter = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataModerateLetterMinimum(d.pageMetadata),
   description: d.pageMetadata.moderate.letter.description,
   bodyContent: makeReactModerationForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Moderate a working paper
export const makePageMetadataModerateWorkingPaperMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.moderate.workingPaper.title,
   localURL: '/moderate/workingPaper',
   depth: 'tertiary',
});

export const makePageMetadataModerateWorkingPaper = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataModerateWorkingPaperMinimum(d.pageMetadata),
   description: d.pageMetadata.moderate.workingPaper.description,
   bodyContent: makeReactModerationForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'user',
   isReactPage: true,
});

// Moderate a peer review
// export const makePageMetadataModeratePeerReviewMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.moderate.peerReview.title,
//    localURL: '/moderate/peerReview',
//    depth: 'tertiary',
// });

// export const makePageMetadataModeratePeerReview = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataModeratePeerReviewMinimum(d.pageMetadata),
//    description: d.pageMetadata.moderate.peerReview.description,
//    bodyContent: makeModerateFormPeerReview(context)(d),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Moderate a published report
// export const makePageMetadataModerateReportPublishedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.moderate.publishedReport.title,
//    localURL: '/moderate/reportPublished',
//    depth: 'tertiary',
// });

// export const makePageMetadataModerateReportPublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataModerateReportPublishedMinimum(d.pageMetadata),
//    description: d.pageMetadata.moderate.publishedReport.description,
//    bodyContent: makeModerateFormReportPublished(context)(d),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

// Moderate a conference
// export const makePageMetadataModerateConferenceMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
//    title: d.moderate.publishedConferencePaper.title,
//    localURL: '/moderate/conference',
//    depth: 'tertiary',
// });

// export const makePageMetadataModerateConference = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
//    ...makePageMetadataModerateConferenceMinimum(d.pageMetadata),
//    description: d.pageMetadata.moderate.publishedConferencePaper.description,
//    bodyContent: makeModerateFormConferencePaperPublished(context)(d),
//    noBots: true,
//    submenuToDisplay: 'user'
// });

/**
 * Error pages
 */
// Not Found
export const makePageMetadataNotFoundMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.error.notFound.title,
   localURL: '/error/notFound',
   depth: 'tertiary',
});

export const makePageMetadataNotFound = (d: Translatables): PageMetadata => ({
   ...makePageMetadataNotFoundMinimum(d.pageMetadata),
   description: d.pageMetadata.error.notFound.description,
   bodyContent: makeErrorPageNotFound(d.pages.staticPages),
   noBots: true,
   submenuToDisplay: 'none',
});

// Access Denied
export const makePageMetadataAccessDeniedMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.error.accessDenied.title,
   localURL: '/error/accessDenied',
   depth: 'tertiary',
});

export const makePageMetadataAccessDenied = (d: Translatables): PageMetadata => ({
   ...makePageMetadataAccessDeniedMinimum(d.pageMetadata),
   description: d.pageMetadata.error.accessDenied.description,
   bodyContent: makeErrorPageAccessDenied(d.pages.staticPages),
   noBots: true,
   submenuToDisplay: 'none'
});

// General 500 error
export const makePageMetadataUhohMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.error.uhoh.title,
   localURL: '/error/uhoh',
   depth: 'tertiary',
});

export const makePageMetadataUhoh = (d: Translatables): PageMetadata => ({
   ...makePageMetadataUhohMinimum(d.pageMetadata),
   description: d.pageMetadata.error.uhoh.description,
   bodyContent: makeErrorPageGeneral(d.pages.staticPages),
   noBots: true,
   submenuToDisplay: 'none'
});

/**
 * Custom Google Search
 */
export const makePageMetadataGoogleSearchMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.googleSearch.title,
   localURL: '/search',
   depth: 'secondary',
});

export const makePageMetadataGoogleSearch = (d: Translatables): PageMetadata => ({
   ...makePageMetadataGoogleSearchMinimum(d.pageMetadata),
   description: d.pageMetadata.googleSearch.description,
   bodyContent: makeGoogleSearch(d),
   submenuToDisplay: 'none'
});

/**
 * Admin Dashboard
 */
export const makePageMetadataAdminDashboardMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.admin.title,
   localURL: '/admin',
   depth: 'secondary',
});

export const makePageMetadataAdminDashboard = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataAdminDashboardMinimum(d.pageMetadata),
   description: d.pageMetadata.admin.description,
   bodyContent: makeReactAdminDashboard(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'none'
});

export const makePageMetadataAdminEditAccountMinimum = (d: TranslatablePageMetadata): PageMetadataMinimum => ({
   title: d.adminEditAccount.title,
   localURL: '/adminEditAccount',
   depth: 'secondary',
});

export const makePageMetadataAdminEditAccount = (context: MarXivDataWithoutSitemap) => (d: Translatables): PageMetadata => ({
   ...makePageMetadataAdminEditAccountMinimum(d.pageMetadata),
   description: d.pageMetadata.adminEditAccount.description,
   bodyContent: makeAdminEditUserAccountForm(d.forms.messages)(context),
   noBots: true,
   submenuToDisplay: 'none'
});