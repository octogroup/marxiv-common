/**
 * Make user-account forms like login, join, etc.
 */
import { getNames } from 'country-list';
import { HastElement, make, } from '@octogroup/hast-typescript';
import { SelectOption, } from './common';
import { MarXivDataWithoutSitemap } from './context';

/**
 * Import the dictionary
 */
import { JobOptions, Translatables, FormMessageText } from './translatables';

/**
 * Login
 */
// export const makeLoginPasswordField = (d: FormText): HastElement => make.element({ tag: 'div', properties: { className: 'field input password flex all-parent-row center' }, children: [
//    make.element({ tag: 'label', properties: { htmlFor: 'password'}, children: [
//       make.text(d.join.fields.password)
//    ]}),
//    make.element({ tag: 'input', properties: { type: 'password', name: 'password', id: 'password', className: 'required', required: true, placeholder: d.join.fields.passwordDefault }, children: [] })
// ]})

// export const makeLoginUserIDField = (d: FormText): HastElement => make.element({ tag: 'div', properties: { className: 'field input shortText flex all-parent-row center' }, children: [
//    make.element({ tag: 'label', properties: { htmlFor: 'username'}, children: [
//       make.text((d.login.usernameOrEmail))
//    ]}),
//    make.element({ tag: 'input', properties: { type: 'text', name: 'username', id: 'username', className: 'required', required: true, placeholder: d.login.usernameOrEmailDefault }, children: [] })
// ]})

// export const makeUserLoginForm = (d: Translatables): HastElement => make.element({ tag: 'div', properties: { id: 'login' }, children: [
//    makeForm(d.forms.login.formTitle)([
//       makeLoginUserIDField(d.forms),
//       makeLoginPasswordField(d.forms),
//       makeFormEnd(d)
//    ])
// ]})

/**
 * React login form
 */
export const makeUserLoginForm = (d: FormMessageText) => (context: MarXivDataWithoutSitemap): HastElement => make.element({
   tag: 'div', properties: { id: 'reactUserLogin' }, children: [
      make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.userLoginBundle), type: 'module', async: 'async' }, children: [] }),
      make.element({ tag: 'h2', children: [
         make.text(d.loading.loading + ' ' + d.loading.form + '...')
      ]})
   ]
});

/**
 * Reset password
 */
export const makeUserResetPasswordForm = (d: FormMessageText) => (context: MarXivDataWithoutSitemap): HastElement => make.element({
   tag: 'div', properties: { id: 'reactPasswordReset' }, children: [
      make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.passwordResetBundle), type: 'module', async: 'async' }, children: [] }),
      make.element({ tag: 'h2', children: [
         make.text(d.loading.loading + ' ' + d.loading.form + '...')
      ]})
   ]
})

/**
 * Logout
 */
export const makeUserLogout = (d: Translatables): HastElement => make.element({
   tag: 'div', properties: { id: 'logout' }, children: [
      make.element({
         tag: 'h1', children: [
            make.text(d.forms.logout.formTitle)
         ]
      }),
      // Run the logout script
      make.element({ tag: 'div', children: [] })
   ]
})

/**
 * Email address input field
 * 
 * @param id Used for both HTML ID and Name properties. DO NOT DUPLICATE IDs! (HTML IDs are unique, per the spec)
 * @param required Should this be a required form element?
 * @param value If provided, the initial value of the input field.
 */
// export interface EmailInputOptions {
//    id: string
//    required: boolean
//    value?: string
// }
// export const makeInputEmailField = (d: FormText) => (options: EmailInputOptions): HastElement => {
//    if (options.required) {
//       if (options.value) {
//          return make.element({ tag: 'div', properties: { className: 'field input email flex all-parent-row center' }, children: [
//             make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
//                make.text(d.join.fields.emailAddress)
//             ]}),
//             make.element({ tag: 'input', properties: { type: 'email', name: options.id, id: options.id, required: true, className: 'required', placeholder: d.join.fields.emailAddressDefault, value: options.value }, children: [] })
//          ]})
//       }
//       else {
//          return make.element({ tag: 'div', properties: { className: 'field input email flex all-parent-row center' }, children: [
//             make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
//                make.text(d.join.fields.emailAddress)
//             ]}),
//             make.element({ tag: 'input', properties: { type: 'email', name: options.id, id: options.id, required: true, className: 'required', placeholder: d.join.fields.emailAddressDefault }, children: [] })
//          ]})
//       }
//    }
//    else {
//       if (options.value) {
//          return make.element({ tag: 'div', properties: { className: 'field input email flex all-parent-row center' }, children: [
//             make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
//                make.text(d.join.fields.emailAddress)
//             ]}),
//             make.element({ tag: 'input', properties: { type: 'email', name: options.id, id: options.id, placeholder: d.join.fields.emailAddressDefault, value: options.value }, children: [] })
//          ]})
//       }
//       else {
//          return make.element({ tag: 'div', properties: { className: 'field input email flex all-parent-row center' }, children: [
//             make.element({ tag: 'label', properties: { htmlFor: options.id }, children: [
//                make.text(d.join.fields.emailAddress)
//             ]}),
//             make.element({ tag: 'input', properties: { type: 'email', name: options.id, id: options.id, placeholder: d.join.fields.emailAddressDefault }, children: [] })
//          ]})
//       }
//    }
// }

// export const makeInputEmailConfirmField = (d: FormText) => (id: string, value?: string): HastElement => {
//    if (value) { 
//       return make.element({ tag: 'div', properties: { className: 'field input email flex all-parent-row center' }, children: [
//          make.element({ tag: 'label', properties: { htmlFor: id }, children: [
//             make.text(d.join.fields.confirmEmailAddress)
//          ]}),
//          make.element({ tag: 'input', properties: { type: 'email', name: id, id: id, required: true, className: 'required', placeholder: d.join.fields.emailAddressDefault, value: value }, children: [] })
//       ]})
//    }
//    else {
//       return make.element({ tag: 'div', properties: { className: 'field input email flex all-parent-row center' }, children: [
//          make.element({ tag: 'label', properties: { htmlFor: id }, children: [
//             make.text(d.join.fields.confirmEmailAddress)
//          ]}),
//          make.element({ tag: 'input', properties: { type: 'email', name: id, id: id, required: true, className: 'required', placeholder: d.join.fields.emailAddressDefault }, children: [] })
//       ]})
//    }
// }

/**
 * Password input field
 */
// export const makeInputPasswordField = (d: FormText) => (id: string): HastElement => make.element({ tag: 'div', properties: { className: 'field input password flex all-parent-row center' }, children: [
//    make.element({ tag: 'label', properties: { htmlFor: id }, children: [
//       make.text(d.join.fields.password)
//    ]}),
//    make.element({ tag: 'input', properties: { type: 'email', name: id, id: id, required: true, className: 'required', placeholder: d.join.fields.passwordDefault }, children: [] })
// ]})

// export const makeInputPasswordConfirmField = (d: FormText) => (id: string): HastElement => make.element({ tag: 'div', properties: { className: 'field input password flex all-parent-row center' }, children: [
//    make.element({ tag: 'label', properties: { htmlFor: id }, children: [
//       make.text(d.join.fields.confirmPassword)
//    ]}),
//    make.element({ tag: 'input', properties: { type: 'email', name: id, id: id, required: true, className: 'required', placeholder: d.join.fields.passwordDefault }, children: [] })
// ]})

// export const makeUpdatePasswordField = (d: FormText) => (id: string): HastElement => make.element({ tag: 'div', properties: { className: 'field input password flex all-parent-row center' }, children: [
//    make.element({ tag: 'label', properties: { htmlFor: id }, children: [
//       make.text(d.join.fields.password)
//    ]}),
//    make.element({ tag: 'input', properties: { type: 'email', name: id, id: id, placeholder: d.join.fields.passwordDefault }, children: [] })
// ]})

// export const makeUpdatePasswordConfirmField = (d: FormText) => (id: string): HastElement => make.element({ tag: 'div', properties: { className: 'field input password flex all-parent-row center' }, children: [
//    make.element({ tag: 'label', properties: { htmlFor: id }, children: [
//       make.text(d.join.fields.confirmPassword)
//    ]}),
//    make.element({ tag: 'input', properties: { type: 'email', name: id, id: id, placeholder: d.join.fields.passwordDefault }, children: [] })
// ]})

/**
 * Country selection drop-down
 */
export const makeCountrySelectOptions = (): SelectOption[] => {
   const countryArray: string[] = getNames();
   let countryOptions: SelectOption[] = new Array;
   for (let i = 0; i < countryArray.length; i++) {
      countryOptions.push({
         value: countryArray[i].toLowerCase().replace(/ /g, ''),
         label: countryArray[i]
      });
   }
   // Return the formatted array.
   return countryOptions
}

export const translateCountrySelectOption = (countryOptions: SelectOption[]) => (selectedCountry: string): string => {
   let label: string = '';
   for (let i = 0; i < countryOptions.length; i++) {
      if (countryOptions[i].value === selectedCountry) {
         label = countryOptions[i].label;
      }
   }
   return label
}

// type CountrySelectList = fl.Omit<SelectList, 'options'>;
// export const makeCountrySelect = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (options: CountrySelectList): HastElement => makeSelectList({ ...options, options: makeCountrySelectOptions(d) });

// export const makeCountrySelectWithDefault = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (options: CountrySelectList): HastElement => {
//    if (options.selected && Array.isArray(options.selected)) {
//       throw new Error('Only one country may be selected.')
//    }
//    else if (options.selected) {
//       // Modify the countrySelectOptions to add the selected attribute
//       const countrySelectOptions = makeCountrySelectOptions(d);
//       let selectedOptions: SelectOption[] = new Array;
//       for (let i = 0; i < countrySelectOptions.length; i++) {
//          let opt = countrySelectOptions[i];
//          if (opt.value === options.selected) {
//             selectedOptions.push({
//                ...opt,
//                selected: true
//             })
//          }
//          else {
//             selectedOptions.push(opt)
//          }
//       }
//       return makeSelectList({ label: d.forms.join.fields.country, id: 'country', required: false, multi: false, options: selectedOptions })
//    }
//    else {
//       // Make a regular country list
//       return makeCountrySelect(context)(d)(options)
//    }
// }

/**
 * Newsletter radios
 */
// export const makeNewsletterYes = (d: Translatables): HastElement => makeRadioInput({
//    label: d.commonReplacements.yes,
//    id: 'newsletterYes',
//    value: 'newsletterYes',
//    name: 'newsletter'
// });

// export const makeNewsletterNo = (d: Translatables): HastElement => makeRadioInput({
//    label: d.commonReplacements.no,
//    id: 'newsletterNo',
//    value: 'newsletterNo',
//    name: 'newsletter'
// });

/**
 * Join / Register
 */
export type JobOptionValues = 'researcher' | 'conservationist' | 'undergrad' | 'grad' | 'student' | 'comms' | 'teaching' | 'naturalRes' | 'projectManager' | 'policyAnalysis' | 'consultant' | 'policy' | 'fisher' | 'regulator' | 'recFisher' | 'citSci' | 'dataScience' | 'chem' | 'atmos' | 'fisheries' | 'biology' | 'psychology' | 'political' | 'econ' | 'lobby' | 'otherSocialSci' | 'otherNatSci' | 'otherInter' | 'otherNonSci' | 'journalist' | 'charity' | 'military' | 'extraction';

export const makeJobOptions = (d: Translatables): SelectOption[] => {
   // Turn the Job Options object into an array of [key, value] pairs
   const kvArray: [string, string][] = Object.entries(d.forms.join.jobOptions);
   // Iterate over the newly-formed array to our SelectOption array.
   let options: SelectOption[] = new Array;
   for (let i = 0; i < kvArray.length; i++) {
      options.push({
         value: kvArray[i][0],
         label: kvArray[i][1]
      })
   }
   // Return the formatted array.
   return options
}

export const translateJobOption = (d: JobOptions) => (jobOption: JobOptionValues): string => {
   switch (jobOption) {
      case 'researcher':
         return d.researcher
      case 'conservationist':
         return d.conservationist
      case 'undergrad':
         return d.undergrad
      case 'grad':
         return d.grad
      case 'student':
         return d.student
      case 'comms':
         return d.comms
      case 'teaching':
         return d.teaching
      case 'naturalRes':
         return d.naturalRes
      case 'projectManager':
         return d.projectManager
      case 'policyAnalysis':
         return d.policyAnalysis
      case 'consultant':
         return d.consultant
      case 'policy':
         return d.policy
      case 'fisher':
         return d.fisher
      case 'regulator':
         return d.regulator
      case 'citSci':
         return d.citSci
      case 'recFisher':
         return d.recFisher
      case 'dataScience':
         return d.dataScience
      case 'chem':
         return d.chem
      case 'atmos':
         return d.atmos
      case 'fisheries':
         return d.fisheries
      case 'biology':
         return d.biology
      case 'psychology':
         return d.psychology
      case 'political':
         return d.political
      case 'lobby':
         return d.lobby
      case 'otherSocialSci':
         return d.otherSocialSci
      case 'otherNatSci':
         return d.otherNatSci
      case 'otherInter':
         return d.otherInter
      case 'otherNonSci':
         return d.otherNonSci
      case 'journalist':
         return d.journalist
      case 'charity':
         return d.charity
      case 'extraction':
         return d.extraction
      case 'military':
         return d.military
      default:
         throw new Error('Invalid job option supplied to translate.')
   }
}

// export const makeUserRegistrationFormContent = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { id: 'register' }, children: [
//    // Username
//    makeTextInput({ label: d.forms.join.fields.username, placeholder: d.forms.join.fields.usernameDefault, id: 'username', required: true, short: true }),
//    // Email address
//    makeInputEmailField(d.forms)({ id: 'email', required: true }),
//    makeInputEmailConfirmField(d.forms)('emailConfirm'),
//    // Password
//    makeInputPasswordField(d.forms)('password'),
//    makeInputPasswordConfirmField(d.forms)('passwordConfirm'),
//    // Spacer
//    make.element({ tag: 'div', properties: { className: 'spacer verticalBuffer2' }, children: []}),
//    // Given name
//    makeTextInput({ label: d.forms.join.fields.givenName, placeholder: d.forms.join.fields.givenNameDefault, id: 'given', required: true, short: true }),
//    // Surname
//    makeTextInput({ label: d.forms.join.fields.surName, placeholder: d.forms.join.fields.surNameDefault, id: 'surname', required: true, short: true }),
//    // Org
//    makeTextInput({ label: d.forms.join.fields.organization, placeholder: d.forms.join.fields.organizationDefault, id: 'org', required: false, short: true }),
//    // Job
//    makeSelectList({ label: d.forms.join.fields.jobFocus, id: 'job', required: false, multi: true, options: makeJobOptions(d) }),
//    // OCRID
//    makeTextInput({ label: d.forms.join.fields.orcid, placeholder: d.forms.join.fields.orcidDefault, id: 'orcid', required: false, short: true }),
//    // Country
//    makeCountrySelect(context)(d)({ label: d.forms.join.fields.country, id: 'country', required: false, multi: false }),
//    // Spacer
//    make.element({ tag: 'div', properties: { className: 'spacer verticalBuffer2' }, children: []}),
//    // Newsletter
//    make.element({ tag: 'div', properties: { className: 'newsletter flex all-parent-row' }, children: [
//       make.element({ tag: 'div', properties: { className: 'field inputTitle flex all-child-span2' }, children: [
//          make.text(d.forms.join.fields.newsletter)
//       ]}),
//       make.element({ tag: 'div', properties: { className: 'radioGroup flex all-parent-row flex all-child-span1 center' }, children: [
//          makeNewsletterYes(d),
//          makeNewsletterNo(d)
//       ]})
//    ]}),
//    // Form end
//    makeFormEnd(d),
//    // Disclaimer
//    d.forms.join.fields.terms
// ]})

export const makeUserRegistrationForm = (d: FormMessageText) => (context: MarXivDataWithoutSitemap): HastElement => make.element({
   tag: 'div', properties: { id: 'reactUserRegister' }, children: [
      make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.userRegisterBundle), type: 'module', async: 'async' }, children: [] }),
      make.element({ tag: 'h2', children: [
         make.text(d.loading.loading + ' ' + d.loading.page + '...')
      ]})
   ]
})

/**
 * User account pages
 */
// export const undefinedValueToString = (b: string | undefined): string => {
//    if (b) {
//       return b
//    }
//    else {
//       return ''
//    }
// }
// export const multiToString = (b: string[] | undefined): string => {
//    if (b) {
//       return b.join('; ');
//    }
//    else {
//       return ''
//    }
// }
// export const newsletterBooleanToString = (d: Translatables) => (b: boolean | undefined): string => {
//    if (b) {
//       return d.commonReplacements.yes
//    }
//    else {
//       return d.commonReplacements.no
//    }
// }
// export const makeUserAccountContent = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement[] => {
//    if (context.userData.userType === 'anonymous') {
//       return [
//          make.element({ tag: 'h2', children: [
//             make.text('No user account information to display.')
//          ]})
//       ]
//    }
//    else {
//       return [
//          // Page title
//          make.element({ tag: 'h1', properties: { className: 'title field' }, children: [
//             make.text(d.forms.userAccount.title)
//          ]}),
//          // Username 
//          make.element({ tag: 'div', properties: { className: 'userName field' }, children: [
//             make.element({ tag: 'span', properties: { className: 'label' }, children: [
//                make.text(d.forms.join.fields.username)
//             ]}),
//             make.element({ tag: 'span', properties: { className: 'content' }, children: [
//                make.text(context.userData.userName)
//             ]})
//          ]}),
//          // Email address
//          make.element({ tag: 'div', properties: { className: 'emailAddress field' }, children: [
//             make.element({ tag: 'span', properties: { className: 'label' }, children: [
//                make.text(d.forms.join.fields.emailAddress)
//             ]}),
//             make.element({ tag: 'span', properties: { className: 'content' }, children: [
//                make.text(context.userData.emailAddress)
//             ]})
//          ]}),
//          // Given name
//          make.element({ tag: 'div', properties: { className: 'givenName field' }, children: [
//             make.element({ tag: 'span', properties: { className: 'label' }, children: [
//                make.text(d.forms.join.fields.givenName)
//             ]}),
//             make.element({ tag: 'span', properties: { className: 'content' }, children: [
//                make.text(context.userData.givenName)
//             ]})
//          ]}),
//          // Surname
//          make.element({ tag: 'div', properties: { className: 'surName field' }, children: [
//             make.element({ tag: 'span', properties: { className: 'label' }, children: [
//                make.text(d.forms.join.fields.surName)
//             ]}),
//             make.element({ tag: 'span', properties: { className: 'content' }, children: [
//                make.text(context.userData.surname)
//             ]})
//          ]}),
//          // Organization
//          make.element({ tag: 'div', properties: { className: 'organization field' }, children: [
//             make.element({ tag: 'span', properties: { className: 'label' }, children: [
//                make.text(d.forms.join.fields.organization)
//             ]}),
//             make.element({ tag: 'span', properties: { className: 'content' }, children: [
//                make.text( undefinedValueToString(context.userData.organization) )
//             ]})
//          ]}),
//          // Job Focus
//          make.element({ tag: 'div', properties: { className: 'jobFocus field' }, children: [
//             make.element({ tag: 'span', properties: { className: 'label' }, children: [
//                make.text(d.forms.userAccount.jobFocus)
//             ]}),
//             make.element({ tag: 'span', properties: { className: 'content' }, children: [
//                make.text( multiToString(context.userData.jobFocus) )
//             ]})
//          ]}),
//          // OCRID
//          make.element({ tag: 'div', properties: { className: 'orcid field' }, children: [
//             make.element({ tag: 'span', properties: { className: 'label' }, children: [
//                make.text(d.forms.join.fields.orcid)
//             ]}),
//             make.element({ tag: 'span', properties: { className: 'content' }, children: [
//                make.text( undefinedValueToString(context.userData.orcid) )
//             ]})
//          ]}),
//          // Country
//          make.element({ tag: 'div', properties: { className: 'country field' }, children: [
//             make.element({ tag: 'span', properties: { className: 'label' }, children: [
//                make.text(d.forms.join.fields.country)
//             ]}),
//             make.element({ tag: 'span', properties: { className: 'content' }, children: [
//                make.text( undefinedValueToString(context.userData.country) )
//             ]})
//          ]}),
//          // Timezone
//          make.element({ tag: 'div', properties: { className: 'timezone field' }, children: [
//             make.element({ tag: 'span', properties: { className: 'label' }, children: [
//                make.text(d.forms.join.fields.timezone)
//             ]}),
//             make.element({ tag: 'span', properties: { className: 'content' }, children: [
//                make.text( undefinedValueToString(context.userData.timezone) )
//             ]})
//          ]}),
//          // MarXiv Newsletter
//          make.element({ tag: 'div', properties: { className: 'newsletter field' }, children: [
//             make.element({ tag: 'span', properties: { className: 'label' }, children: [
//                make.text(d.forms.join.fields.newsletter)
//             ]}),
//             make.element({ tag: 'span', properties: { className: 'content' }, children: [
//                make.text( newsletterBooleanToString(d)(context.userData.newsletter) )
//             ]})
//          ]}),
//          // Button to edit user account
//          make.element({ tag: 'div', properties: { className: 'editWrapper' }, children: [
//             make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.user.editAccount), className: 'button blue' }, children: [
//                make.text(d.forms.userAccount.editButton)
//             ]})
//          ]})
//       ]
//    }
// }

export const makeUserAccountPage = (d: FormMessageText) => (context: MarXivDataWithoutSitemap): HastElement => make.element({
   tag: 'div', properties: { id: 'reactUserAccount' }, children: [
      make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.userAccountBundle), type: 'module', async: 'async' }, children: [] }),
      make.element({ tag: 'h2', children: [
         make.text(d.loading.loading + ' ' + d.loading.page + '...')
      ]})
   ]
});

/**
 * Edit user account forms
 */
// export const makEditUserAccountFormContent = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => {
//    if (context.userData.userType === 'anonymous') {
//       return make.element({ tag: 'h2', children: [
//          make.text('No user account information to change.')
//       ]})
//    }
//    else {
//       return make.element({ tag: 'div', properties: { id: 'editUserAccount' }, children: [
//          // Username
//          makeTextInput({ label: d.forms.join.fields.username, placeholder: d.forms.join.fields.usernameDefault, id: 'username', required: true, short: true, value: context.userData.userName }),
//          // Email address
//          makeInputEmailField(d.forms)({ id: 'email', required: true, value: context.userData.emailAddress }),
//          makeInputEmailConfirmField(d.forms)('emailConfirm', context.userData.emailAddress),
//          // Password
//          makeUpdatePasswordField(d.forms)('password'),
//          makeUpdatePasswordConfirmField(d.forms)('passwordConfirm'),
//          // Spacer
//          make.element({ tag: 'div', properties: { className: 'spacer verticalBuffer2' }, children: []}),
//          // Given name
//          makeTextInput({ label: d.forms.join.fields.givenName, placeholder: d.forms.join.fields.givenNameDefault, id: 'given', required: true, short: true, value: context.userData.givenName }),
//          // Surname
//          makeTextInput({ label: d.forms.join.fields.surName, placeholder: d.forms.join.fields.surNameDefault, id: 'given', required: true, short: true, value: context.userData.surname }),
//          // Org
//          makeTextInput({ label: d.forms.join.fields.organization, placeholder: d.forms.join.fields.organizationDefault, id: 'given', required: false, short: true, value: context.userData.organization }),
//          // Job
//          makeSelectList({ label: d.forms.join.fields.jobFocus, id: 'job', required: false, multi: true, options: makeJobOptions(d) }),
//          // OCRID
//          makeTextInput({ label: d.forms.join.fields.orcid, placeholder: d.forms.join.fields.orcidDefault, id: 'orcid', required: false, short: true, value: context.userData.orcid }),
//          // Country
//          makeCountrySelectWithDefault(context)(d)({ label: d.forms.join.fields.country, id: 'country', required: false, multi: false, selected: context.userData.country }),
//          // Spacer
//          make.element({ tag: 'div', properties: { className: 'spacer verticalBuffer2' }, children: []}),
//          // Newsletter
//          make.element({ tag: 'div', properties: { className: 'newsletter flex all-parent-row' }, children: [
//             make.element({ tag: 'div', properties: { className: 'field inputTitle flex all-child-span2' }, children: [
//                make.text(d.forms.join.fields.newsletter)
//             ]}),
//             make.element({ tag: 'div', properties: { className: 'radioGroup flex all-parent-row flex all-child-span1 center' }, children: [
//                makeRadioInput({
//                   label: d.commonReplacements.yes,
//                   id: 'newsletterYes',
//                   value: 'newsletterYes',
//                   name: 'newsletter',
//                   selected: context.userData.newsletter ? true : false
//                }),
//                makeRadioInput({
//                   label: d.commonReplacements.no,
//                   id: 'newsletterNo',
//                   value: 'newsletterNo',
//                   name: 'newsletter',
//                   selected: context.userData.newsletter ? false : true
//                })
//             ]})
//          ]}),
//          // Form end
//          makeFormEnd(d),
//       ]})
//    }
// }

export const makeEditUserAccountForm = (d: FormMessageText) => (context: MarXivDataWithoutSitemap): HastElement => make.element({
   tag: 'div', properties: { id: 'reactEditUserAccount' }, children: [
      make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.editUserAccountBundle), type: 'module', async: 'async' }, children: [] }),
      make.element({ tag: 'h2', children: [
         make.text(d.loading.loading + ' ' + d.loading.form + '...')
      ]})
   ]
});

export const makeAdminEditUserAccountForm = (d: FormMessageText) => (context: MarXivDataWithoutSitemap): HastElement => make.element({
   tag: 'div', properties: { id: 'reactAdminEditUserAccount' }, children: [
      make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.reactAdminEditAccount), type: 'module', async: 'async' }, children: [] }),
      make.element({ tag: 'h2', children: [
         make.text(d.loading.loading + ' ' + d.loading.form + '...')
      ]})
   ]
});