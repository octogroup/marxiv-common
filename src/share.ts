/**
 * Submit / Share Guide
 */

import { HastElement, make, } from '@octogroup/hast-typescript';
import { MarXivDataWithoutSitemap } from './context';
import { makeLocalPath } from './common';

/**
 * Import the dictionary
 */
import { SubmitGuideText } from './translatables';

/**
 * Make the Share Guide content
 * 
 * @param link i.e. href
 * @param text The string displayed to the user
 */
export interface OptionButton {
   link: string
   text: string
}
export const makeOptionButton = (b: OptionButton): HastElement => make.element({ tag: 'a', properties: { className: 'button blue', href: b.link }, children: [
   make.text(b.text)
]})

/**
 * Share the same Title and Subtitle amongst pages
 */
export const commonShareOptionsContent = (d: SubmitGuideText) => [
   make.element({ tag: 'h1', children: [
      make.text(d.shareTitle)
   ]}),
   make.element({ tag: 'p', children: [
      make.text(d.shareSubtitle)
   ]}),
];

/**
 * Splash page to login/register the user
 */
export const makeShareSplash = (context: MarXivDataWithoutSitemap) => (d: SubmitGuideText): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.shareTitle)
   ]}),
   make.element({ tag: 'p', children: [
      make.text(d.spashDescription)
   ]}),
   make.element({ tag: 'div', properties: { className: 'share flex all-parent-column center' }, children: [
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.login), text: d.splashLogin }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.join), text: d.splashRegister }),
   ]})
]});

/**
 * First of 3 Share Guide "options" pages, with:
 * - unpublished paper
 * - published paper
 * - something else
 */
export const makeShareOptions1 = (context: MarXivDataWithoutSitemap) => (d: SubmitGuideText): HastElement => make.element({ tag: 'div', children: [
   ...commonShareOptionsContent(d),
   make.element({ tag: 'div', properties: { className: 'share flex all-parent-column center' }, children: [
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.preprint), text: d.share1OptionUnpublished }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.submissionGuidePublishedOptions), text: d.share1OptionPublished }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.submissionGuide2), text: d.shareSomethingElse }),
   ]})
]});

/**
 * Second of 3 Share Guide "options" pages, with:
 * - report
 * - thesis
 * - conference
 * - database
 * - something else
 */
export const makeShareOptions2 = (context: MarXivDataWithoutSitemap) => (d: SubmitGuideText): HastElement => make.element({ tag: 'div', children: [
   ...commonShareOptionsContent(d),
   make.element({ tag: 'div', properties: { className: 'share flex all-parent-column center' }, children: [
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.submissionGuideReportOptions), text: d.share2OptionReport }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.thesis), text: d.share2OptionThesis }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.submissionGuideConferenceOptions), text: d.share2OptionConference }),
      // makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.submissionGuideDatasetOptions), text: d.share2OptionDataset }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.submissionGuide3), text: d.shareSomethingElse }),
   ]})
]});

/**
 * Final of 3 Share Guide "options" pages, with:
 * - poster
 * - letter
 * - working paper
 * - peer review
 * - go to beginning
 */
export const makeShareOptions3 = (context: MarXivDataWithoutSitemap) => (d: SubmitGuideText): HastElement => make.element({ tag: 'div', children: [
   ...commonShareOptionsContent(d),
   make.element({ tag: 'div', properties: { className: 'share flex all-parent-column center' }, children: [
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.posterPresentationSI), text: d.share3OptionPoster }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.letter), text: d.share3OptionLetter }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.workingPaper), text: d.share3OptionPaper }),
      // makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.peerReview), text: d.share3OptionReview }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.submissionGuide1), text: d.share3OptionReturn }),
   ]})
]});

/**
 * Share a published paper
 */
export const makeSharePublishedPaperOptions = (context: MarXivDataWithoutSitemap) => (d: SubmitGuideText): HastElement => make.element({ tag: 'div', children: [
   ...commonShareOptionsContent(d),
   make.element({ tag: 'div', properties: { className: 'share flex all-parent-column center' }, children: [
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.openAccess), text: d.shareOA }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.submissionGuidePublishedCopyrighted), text: d.sharePaywalled }),
   ]})
]});

/**
 * Share a published, copyrighted paper
 */
export const makeSharePublishedPaperCopyrightedOptions = (d: SubmitGuideText): HastElement => make.element({ tag: 'div', children: [
   ...commonShareOptionsContent(d),
   d.sharePublishedOptions,
]});

/**
 * Share a report
 */
export const makeShareReportOptions = (context: MarXivDataWithoutSitemap) => (d: SubmitGuideText): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.shareTitle)
   ]}),
   d.shareReportOptions,
   make.element({ tag: 'div', properties: { className: 'share flex all-parent-column center' }, children: [
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.unpublishedReport), text: d.shareReportUnpublished }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.forOrgs), text: d.shareReportPublished })
   ]})
]});

/**
 * Share a conference paper
 */
export const makeShareConferencePaperOptions = (context: MarXivDataWithoutSitemap) => (d: SubmitGuideText): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.shareTitle)
   ]}),
   d.shareConferenceOptions,
   make.element({ tag: 'div', properties: { className: 'share flex all-parent-column center' }, children: [
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.preprint), text: d.shareConferenceUnpublished }),
      makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.forOrgs), text: d.shareConferencePublished })
   ]})
]});

/**
 * Share a database
 */
// export const makeShareDatabaseOptions = (context: MarXivDataWithoutSitemap) => (d: SubmitGuideText): HastElement => make.element({ tag: 'div', children: [
//    make.element({ tag: 'h1', children: [
//       make.text(d.shareTitle)
//    ]}),
//    d.shareDatabaseOptions,
//    make.element({ tag: 'div', properties: { className: 'share flex all-parent-column center' }, children: [
//       makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.database), text: d.shareDatabase }),
//       makeOptionButton({ link: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.dataset), text: d.shareDataset })
//    ]})
// ]});