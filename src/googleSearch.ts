import { HastElement, make, } from '@octogroup/hast-typescript';
import { Translatables, } from './translatables';

/**
 * Embed our Custom Google Search
 */
export const makeGoogleSearch = (d: Translatables): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.pageMetadata.googleSearch.title)
   ]}),
   make.element({ tag: 'script', properties: { async: 'async', src: 'https://cse.google.com/cse.js?cx=016327545550181513809:mrvlpsmm5ku'}, children: [] }),
   make.element({ tag: 'div', properties: { className: 'flex all-parent-column' }, children: [
      make.element({ tag: 'div', properties: { className: 'flex all-child-span100' }, children: [
         make.element({ tag: 'div', properties: { className: 'gcse-searchbox' }, children: []}),
      ]}),
      make.element({ tag: 'div', properties: { className: 'flex all-child-span100' }, children: [
         make.element({ tag: 'div', properties: { className: 'gcse-searchresults' }, children: []}),
      ]}),
   ]}),
]})