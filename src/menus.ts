/**
 * Make menus
 */

import { HastElement, make, HastNode } from '@octogroup/hast-typescript';
import { MarXivDataWithoutSitemap } from './context';
import { makeLocalPath } from './common';

/**
 * Import local dictionaries
 */
import { MainMenuText, UserMenuText, Translatables } from './translatables';

/**
 * Generate a generic menu item.
 */
export interface MenuItem {
   className: string
   title: string
   location: string
}

export const makeMenuLink = (menuItem: MenuItem): HastElement => make.element({
   tag: 'a', properties: { href: menuItem.location, className: menuItem.className }, children: [
      make.text(menuItem.title)
   ]
})

export const makeMenuList = (item: Array<HastNode>): HastElement => make.element({ tag: 'li', children: item })

export const makeMenuLevel = (menuItems: Array<HastElement>): HastElement => make.element({ tag: 'ul', children: menuItems })

/**
 * Interfaces for MarXiv's menus
 */
export type UserMenuType = 'anonymous' | 'user' | 'admin' | 'org';

export type SubmenuType = 'why' | 'share' | 'join' | 'user' | 'none';

/**
 * Main menu
 */
export const makeMainMenuItems = (context: MarXivDataWithoutSitemap) => (d: MainMenuText): HastElement => makeMenuLevel([
   // Home
   makeMenuList([
      makeMenuLink({ title: d.home, className: 'home', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.homepage)})
   ]),
   // Why MarXiv.
   makeMenuList([
      makeMenuLink({ title: d.why, className: 'why', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.why.whyMarXiv)})
   ]),
   // Papers
   makeMenuList([
      makeMenuLink({ title: d.papers, className: 'papers', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.works.workListing)})
   ]),
   // For Orgs
   makeMenuList([
      makeMenuLink({ title: d.orgs, className: 'orgs', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.forOrgs)})
   ]),
]);

export const makeMobileMainMenu = (context: MarXivDataWithoutSitemap) => (d: MainMenuText) => (checked: boolean): HastElement => make.element({
   tag: 'div', properties: { id: 'mainMenuMobile' }, children: [
      make.element({ tag: 'input', properties: { type: 'checkbox', id: 'mainMenuInput', checked: checked }, children: [] }),
      make.element({ tag: 'label', properties: { htmlFor: 'mainMenuInput' }, children: [] }),
      make.element({
         tag: 'div', properties: { className: 'menuContent' }, children: [
            makeMainMenuItems(context)(d)
         ]
      })
   ]
});

export const makeMainMenu = (context: MarXivDataWithoutSitemap) => (d: MainMenuText): HastElement => make.element({
   tag: 'div', properties: { id: 'mainMenu' }, children: [
      // Mobile menu
      makeMobileMainMenu(context)(d)(false),
      // Add script for the mobile Main Menu toggling 
      make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.mobileMainMenu), type: 'module', defer: 'defer' }, children: []}),
      // Desktop menu
      make.element({
         tag: 'div', properties: { id: 'mainMenuDesktop' }, children: [
            make.element({
               tag: 'div', properties: { className: 'menuContent' }, children: [
                  makeMainMenuItems(context)(d)
               ]
            })
         ]
      })
   ]
})

/**
 * The "User Menu" which is now actually our Share guide menu item
 */
export const makeUserMenuItems = (context: MarXivDataWithoutSitemap) => (d: MainMenuText): HastElement => makeMenuLevel([
   makeMenuList([
      makeMenuLink({ title: d.submit, className: 'submit', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.splash)})
   ])
]);

export const makeUserMenu = (context: MarXivDataWithoutSitemap) => (d: MainMenuText): HastElement => make.element({
   tag: 'div', properties: { id: 'userMenu' }, children: [
      make.element({
         tag: 'div', properties: { id: 'userMenuDesktop' }, children: [
            make.element({
               tag: 'div', properties: { className: 'menuContent' }, children: [
                  makeUserMenuItems(context)(d)
               ]
            })
         ]
      })
   ]
})

/**
 * Submenu
 */
export const makeShareSubmenu = (context: MarXivDataWithoutSitemap) => (d: MainMenuText): HastElement[] => [
   make.element({
      tag: 'div', properties: { className: 'submissions subMenu flex all-parent-row' }, children: [
         // Self-Archiving Policies of Major Publishers
         makeMenuLink({ title: d.selfArchivingPolicies, className: 'policies normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.selfArchivingPolicies)}),
         // Full Submission Guidelines
         makeMenuLink({ title: d.submissionGuidelines, className: 'guidelines normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.submissionGuidelines)}),
         // Code of Conduct
         makeMenuLink({ title: d.codeOfConduct, className: 'conduct normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.codeOfConduct)}),
      ]
   })
]

export const makeWhySubmenu = (context: MarXivDataWithoutSitemap) => (d: MainMenuText): HastElement[] => [
   make.element({
      tag: 'div', properties: { className: 'whyMarXiv subMenu flex all-parent-row' }, children: [
         // Team
         makeMenuLink({ title: d.team, className: 'team normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.why.team)}),
         // MarXiv Ambassadors
         makeMenuLink({ title: d.ambassadors, className: 'ambassadors normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.why.ambassadors)}),
      ]
   })
]

export const makeJoinSubmenu = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement[] => [
   make.element({
      tag: 'div', properties: { className: 'join subMenu flex all-parent-row' }, children: [
         // Join
         makeMenuLink({ title: d.userMenuText.join, className: 'join normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.join)}),
         // Login
         makeMenuLink({ title: d.userMenuText.login, className: 'login normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.login)}),
         // Reset password
         makeMenuLink({ title: d.forms.resetPassword.formTitle, className: 'resetPassword normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.resetPassword)}),
      ]
   })
]

export const makeUserSubmenu = (context: MarXivDataWithoutSitemap) => (d: UserMenuText): HastElement[] => [
   make.element({
      tag: 'div', properties: { className: 'user subMenu flex all-parent-row' }, children: [
         // User dashboard
         makeMenuLink({ title: d.userDashboard, className: 'dashboard normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.user.dashboard)}),
         // User Account
         makeMenuLink({ title: d.account, className: 'account normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.user.account)}),
         // Logout
         makeMenuLink({ title: d.logout, className: 'logout normalWhiteLink flex all-child-span1', location: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.logout)}),
      ]
   })
]

export const makeSubmenu = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (submenuToDisplay: SubmenuType): HastElement => {
   if (submenuToDisplay === 'join') {
      return make.element({ tag: 'div', properties: { id: 'submenus', className: 'about flex all-parent-column center' }, children: makeJoinSubmenu(context)(d)})
   }
   else if (submenuToDisplay === 'share') { 
      return make.element({ tag: 'div', properties: { id: 'submenus', className: 'about flex all-parent-column center' }, children: makeShareSubmenu(context)(d.mainMenuText)})
   }
   else if (submenuToDisplay === 'why') {
      return make.element({ tag: 'div', properties: { id: 'submenus', className: 'about flex all-parent-column center' }, children: makeWhySubmenu(context)(d.mainMenuText)})
   }
   else if (submenuToDisplay === 'user') {
      return make.element({ tag: 'div', properties: { id: 'submenus', className: 'about flex all-parent-column center' }, children: makeUserSubmenu(context)(d.userMenuText)})
   }
   else {
      return make.element({tag: 'span', children:[]})
   }
}