/**
 * Make our 'static' pages
 */

import { HastElement, make, } from '@octogroup/hast-typescript';
import { makeBulletList, } from './common';

/**
 * Import the dictionary
 */
import { TeamMember, Ambassador, AmbassadorEmeritus, StaticPagesData, Translatables } from './translatables'

/**
 * Why MarXiv?
 */
export const makeWhyMarXiv = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', properties: { className: 'title' }, children: [
      make.text(d.whyTitle)
   ]}),
   d.whyBody
]})

/**
 * MarXiv Team
 */
export const makeTeamMembers = (teamMembers: TeamMember[]): HastElement => {
   let team: HastElement[] = new Array;
   for (let i = 0; i < teamMembers.length; i++) {
      team.push(make.element({ tag: 'p', children: [
         make.element({ tag: 'span', properties: { className: 'strong' }, children: [
            make.text(teamMembers[i].name)
         ]}),
         make.text(', ' + teamMembers[i].title)
      ]}))
   }
   return make.element({ tag: 'div', children: team })
}

export const makeMarXivTeam = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.teamTitle)
   ]}),
   d.teamContent,
   make.element({ tag: 'h2', children: [
      make.text(d.teamMarXivTitle)
   ]}),
   makeTeamMembers(d.teamMarXiv),
   make.element({ tag: 'h2', children: [
      make.text(d.teamAdvisoryTitle)
   ]}),
   make.element({ tag: 'p', children: [
      make.text(d.teamAdvisorySubtitle)
   ]}),
   makeTeamMembers(d.teamAdvisory),
]})

/**
 * MarXiv Ambassadors
 */
export const makeAmbassadors2019 = (ambassadors: Ambassador[]): HastElement => {
   let team: HastElement[] = new Array;
   for (let i = 0; i < ambassadors.length; i++) {
      let mailto = 'mailto:' + ambassadors[i].email;
      let twitterHref = ambassadors[i].twitter ? 'https://twitter.com/' + ambassadors[i].twitter : false;
      let twitterLink = twitterHref ? make.element({ tag: 'a', properties: { href: twitterHref }, children: [ make.text('@' + ambassadors[i].twitter) ]}) : false;
      if (twitterLink) {
         team.push( make.element({ tag: 'p', children: [
            make.element({ tag: 'span', properties: { className: 'strong' }, children: [
               make.text(ambassadors[i].institution + ' (' + ambassadors[i].country + '):')
            ]}),
            make.text(' '),
            make.element({ tag: 'a', properties: { href: mailto }, children: [
               make.text(ambassadors[i].name)
            ]}),
            make.text(' ('),
            twitterLink,
            make.text(')')
         ]}))
      }
      else {
         team.push( make.element({ tag: 'p', children: [
            make.element({ tag: 'span', properties: { className: 'strong' }, children: [
               make.text(ambassadors[i].institution + ' (' + ambassadors[i].country + '):')
            ]}),
            make.text(' '),
            make.element({ tag: 'a', properties: { href: mailto }, children: [
               make.text(ambassadors[i].name)
            ]})
         ]}))
      }
   }
   return make.element({ tag: 'div', children: team })
}

export const makeAmbassadorsEmeritus = (ambassadors: AmbassadorEmeritus[]): HastElement => {
   let team: HastElement[] = new Array;
   for (let i = 0; i < ambassadors.length; i++) {
      team.push(make.element({ tag: 'p', children: [
         make.element({ tag: 'span', properties: { className: 'strong' }, children: [
            make.text(ambassadors[i].institution + ':')
         ]}),
         make.text(' ' + ambassadors[i].name)
      ]}))
   }
   return make.element({ tag: 'div', children: team })
}

export const makeStaticAmbassadors = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.ambassadorTitle),
   ]}),
   make.element({ tag: 'p', children: [
      make.text(d.ambassadorP1)
   ]}),
   make.element({ tag: 'p', children: [
      make.text(d.ambassadorP2)
   ]}),
   make.element({ tag: 'h2', children: [
      make.text(d.ambassador2019Title),
   ]}),
   makeAmbassadors2019(d.ambassadors2019),
   make.element({ tag: 'h2', children: [
      make.text(d.ambassador2018Title),
   ]}),
   make.element({ tag: 'p', children: [
      make.text(d.ambassador2018Subtitle)
   ]}),
   makeAmbassadorsEmeritus(d.ambassadorsEmeritus)
]})

/**
 * Code of Conduct
 */
export const makeCodeOfConduct = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.codeTitle)
   ]}),
   make.element({ tag: 'h2', children: [
      make.text(d.codePledgeTitle)
   ]}),
   d.codePledgeContent,
   make.element({ tag: 'h2', children: [
      make.text(d.codeStandardsTitle)
   ]}),
   makeBulletList(d.codeStandardsList1),
   makeBulletList(d.codeStandardsList2),
   make.element({ tag: 'h2', children: [
      make.text(d.codeEnforcementTitle)
   ]}),
   d.codeEnforcementContent,
   make.element({ tag: 'h2', children: [
      make.text(d.codeAttributionTitle)
   ]}),
   d.codeAttributionContent
]})

/**
 * Privacy Policy
 */
export const makePrivacyPolicy = (d: StaticPagesData) => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.privacyTitle)
   ]}),
   d.privacyContent
]})

/**
 * MarXiv for Organizations
 */
export const makeMarXivForOrgs = (d: StaticPagesData) => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.orgsTitle)
   ]}),
   d.orgsContent
]})

/**
 * Types of Works Accepted (metadata links from homepage)
 */
export const makeTypesOfWorks = (d: StaticPagesData) => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.worksTitle)
   ]}),
   d.worksContent
]})

/**
 * License Options
 */
export const makeLicenseOptions = (d: Translatables) => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.pages.staticPages.licenseOptions.title)
   ]}),
   d.pages.staticPages.licenseOptions.description,
   // Licenses for Papers
   // CC0
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.cc0)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.cc0,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.cc0 }, children: [
         make.text(d.works.static.licensesLinks.cc0)
      ]})
   ]}),
   // CC BY
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.ccby)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.ccBy,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.ccby }, children: [
         make.text(d.works.static.licensesLinks.ccby)
      ]})
   ]}),
   // AFL
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.afl)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.afl,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.afl }, children: [
         make.text(d.works.static.licensesLinks.afl)
      ]})
   ]}),
   // CC BY-SA
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.ccbysa)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.ccBySa,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.ccbysa }, children: [
         make.text(d.works.static.licensesLinks.ccbysa)
      ]})
   ]}),
   // CC BY-ND
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.ccbynd)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.ccByNd,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.ccbynd }, children: [
         make.text(d.works.static.licensesLinks.ccbynd)
      ]})
   ]}),
   // CC BY-NC
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.ccbync)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.ccByNc,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.ccbync }, children: [
         make.text(d.works.static.licensesLinks.ccbync)
      ]})
   ]}),
   // CC BY-NC-SA
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.ccbyncsa)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.ccByNcSa,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.ccbyncsa }, children: [
         make.text(d.works.static.licensesLinks.ccbyncsa)
      ]})
   ]}),
   // CC BY-NC-ND
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.ccbyncnd)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.ccByNcNd,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.ccbyncnd }, children: [
         make.text(d.works.static.licensesLinks.ccbyncnd)
      ]})
   ]}),
   // Spacer
   make.element({ tag: 'div', properties: { className: 'spacer3' }, children: [] }),
   // Licenses for Data
   make.element({ tag: 'h2', children: [
      make.text(d.pages.staticPages.licenseOptions.softwareTitle)
   ]}),
   // GPL
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.gpl)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.gpl,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.gpl }, children: [
         make.text(d.works.static.licensesLinks.gpl)
      ]})
   ]}),
   // MIT
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.mit)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.mit,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.mit }, children: [
         make.text(d.works.static.licensesLinks.mit)
      ]})
   ]}),
   // ECL
   make.element({ tag: 'h3', children: [
      make.text(d.works.static.licensesFull.ecl)
   ]}),
   d.pages.staticPages.licenseOptions.descriptions.ecl,
   make.element({ tag: 'p', children: [
      make.text(d.pages.staticPages.licenseOptions.learnMore + ': '),
      make.element({ tag: 'a', properties: { href: d.works.static.licensesLinks.ecl }, children: [
         make.text(d.works.static.licensesLinks.ecl)
      ]})
   ]}),
]})


/**
 * Done page with otter GIF
 */
export const makeCelebrate = (d: StaticPagesData): HastElement => make.element({ tag: 'div', properties: { className: 'celebrate'}, children: [
   make.element({ tag: 'div', properties: { className: 'giphy flex all-parent-column'}, children: [
      make.element({ tag: 'iframe', properties: { id:'giphyIframe' , src: d.doneGiphy, className: 'flex all-child-span100 center', allowFullScreen: true, frameBorder: '0' }, children: []})
   ]})
]})

/**
 * Confirmation pages
 * 
 * Confirmation page for new submissions
 */
export const makeConfirmNewSubmission = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.confirmNewTitle)
   ]}),
   d.confirmNewText,
   d.confirmSocialSharing,
   d.confirmSignature
]})

/**
 * Confirmation page for updating/editing an approved submission
 */
export const makeConfirmUpdated = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.confirmUpdatedTitle)
   ]}),
   d.confirmSocialSharing,
   d.confirmSignature
]})

/**
 * Confirmation page for resubmitting a rejected work (edits to rejected works)
 */
export const makeConfirmResubmit = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.confirmResubmitTitle)
   ]}),
   d.confirmResubmitText,
   d.confirmSignature
]})

/**
 * Confirmation page for new user accounts
 */
export const makeConfirmNewAccount = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.confirmNewAccountTitle)
   ]}),
   d.confirmNewAccountText
]})

/**
 * Confirmation page for verifying an email address
 */
export const makeConfirmVerifyEmail = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.confirmVerifyEmailTitle)
   ]}),
   d.confirmVerifyEmailText
]})

/**
 * 404 Error Page
 */
export const makeErrorPageNotFound = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.errors.notFound.title)
   ]}),
   d.errors.notFound.description
]})

/**
 * Access Denied Error Page
 */
export const makeErrorPageAccessDenied = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.errors.accessDenied.title)
   ]}),
   d.errors.accessDenied.description
]})

/**
 * General Error Page
 */
export const makeErrorPageGeneral = (d: StaticPagesData): HastElement => make.element({ tag: 'div', children: [
   make.element({ tag: 'h1', children: [
      make.text(d.errors.general.title)
   ]}),
   d.errors.general.description
]})