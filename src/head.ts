/**
 * Make the HTML Head
 */

import { HastElement, HastHead, make, } from '@octogroup/hast-typescript';
import { reCAPTCHAActionStrings, makeReCAPTCHAEndpoint, } from '@octogroup/recaptchav3';

import { MarXivDataWithoutSitemap, PageMetadata, PageMetadataMinimum, } from './context';
import { GenericContributor, CrossrefDate, ConferencePaper, } from './submitForms';
import { contribStdToString } from './workListing';
import { SupportedLanguage } from './languages';

/**
 * We need to require Polyfill at the very top of the Head!
 * <script crossorigin="anonymous" src="https://polyfill.io/v3/polyfill.min.js?flags=gated&features=es2018%2CIntersectionObserver%2CIntersectionObserverEntry"/>
 */

/**
 * Setup reCAPTCHA
 */
const reCAPTCHAv3SiteKey = '';
const reCAPTCHAsrcEndpoint = makeReCAPTCHAEndpoint(reCAPTCHAv3SiteKey);

/**
 * Setup Google Analytics
 */
const gaSrc = 'https://www.googletagmanager.com/gtag/js?id=UA-110792802-4';

const makeStaticHeadChildren = (context: MarXivDataWithoutSitemap) => [
   make.element({tag: 'meta', children: [], properties: { httpEquiv: 'Content-Type', content: 'text/html; charset=utf-8' }}),
   make.element({tag: 'meta', children: [], properties: { name:"format-detection", content:"telephone=no" }}),
   make.element({tag: 'meta', children: [], properties: { name: 'viewport', content: 'width=device-width, initial-scale=1.0' }}),
   make.element({tag: 'link', children: [], properties: { rel: 'stylesheet', href: context.linkToAsset(context.currentPage)(context.assets.css.style) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'apple-touch-icon', sizes: '57x57', href: context.linkToAsset(context.currentPage)(context.assets.images.apple57) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'apple-touch-icon', sizes: '60x60', href: context.linkToAsset(context.currentPage)(context.assets.images.apple60) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'apple-touch-icon', sizes: '72x72', href: context.linkToAsset(context.currentPage)(context.assets.images.apple72) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'apple-touch-icon', sizes: '76x76', href: context.linkToAsset(context.currentPage)(context.assets.images.apple76) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'apple-touch-icon', sizes: '114x114', href: context.linkToAsset(context.currentPage)(context.assets.images.apple114) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'apple-touch-icon', sizes: '120x120', href: context.linkToAsset(context.currentPage)(context.assets.images.apple120) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'apple-touch-icon', sizes: '144x144', href: context.linkToAsset(context.currentPage)(context.assets.images.apple144) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'apple-touch-icon', sizes: '152x152', href: context.linkToAsset(context.currentPage)(context.assets.images.apple152) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'apple-touch-icon', sizes: '180x180', href: context.linkToAsset(context.currentPage)(context.assets.images.apple180) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'icon', type: 'image/png', sizes: '192x192', href: context.linkToAsset(context.currentPage)(context.assets.images.android192) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'icon', type: 'image/png', sizes: '144x144', href: context.linkToAsset(context.currentPage)(context.assets.images.android144) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'icon', type: 'image/png', sizes: '96x96', href: context.linkToAsset(context.currentPage)(context.assets.images.android96) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'icon', type: 'image/png', sizes: '72x72', href: context.linkToAsset(context.currentPage)(context.assets.images.android72) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'icon', type: 'image/png', sizes: '48x48', href: context.linkToAsset(context.currentPage)(context.assets.images.android48) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'icon', type: 'image/png', sizes: '36x36', href: context.linkToAsset(context.currentPage)(context.assets.images.android36) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'icon', type: 'image/png', sizes: '32x32', href: context.linkToAsset(context.currentPage)(context.assets.images.fav32) }}),
   make.element({tag: 'link', children: [], properties: { rel: 'icon', type: 'image/png', sizes: '16x16', href: context.linkToAsset(context.currentPage)(context.assets.images.fav16) }}),
   make.element({tag: 'meta', children: [], properties: { name: 'msapplication-TileColor', content: '#ffffff' }}),
   make.element({tag: 'meta', children: [], properties: { name: 'msapplication-TileImage', content: context.linkToAsset(context.currentPage)(context.assets.images.msApplication) }}),
   make.element({tag: 'meta', children: [], properties: { name: 'theme-color', content: '#ffffff' }}),
   // Load reCAPTCHA
   make.element({ tag: 'script', children: [], properties: { src: reCAPTCHAsrcEndpoint, defer: 'defer' }}),
   // Load Google Analytics
   make.element({ tag: 'script', children: [], properties: { src: gaSrc, async: 'async', }}),
   // Run GA Configuration
   make.element({ tag: 'script', children: [], properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.gtagSetup) }}),
]

export const makeTestingHead = (context: MarXivDataWithoutSitemap): HastHead => ({
   type: 'element',
   tagName: 'head',
   children: makeStaticHeadChildren(context)
})

/**
 * Make Highwire Press Metatags
 */
export const makeAuthorMetatags = (contribs: GenericContributor[]): HastElement[] => {
   const authorTags: HastElement[] = new Array;
   for (let i = 0; i < contribs.length; i++) {
      let contrib = contribs[i];
      switch (contrib.type) {
         case 'std':
               authorTags.push(make.element({ tag: 'meta', properties: { name: 'citation_author', content: contribStdToString(contrib) }, children: [] }))
            break;
         case 'org':
               authorTags.push(make.element({ tag: 'meta', properties: { name: 'citation_author', content: contrib.name }, children: [] }))
            break;
         case 'anon':
            // Don't provide Google Scholar data for anonymous contribs
            break;
         default:
            throw new Error('Invalid contributor type supplied to wrapContribs.')
      }
   }
   return authorTags
}

export const makeDateMetatags = (date: CrossrefDate): HastElement => {
   if (date.day && date.month) {
      return make.element({ tag: 'meta', properties: { name: 'citation_publication_date', content: date.year + '/' + date.month + '/' + date.day }, children: [] })
   }
   else {
      return make.element({ tag: 'meta', properties: { name: 'citation_publication_date', content: date.year }, children: [] })
   }
}

export const makeConferenceTitleTag = (work: ConferencePaper): HastElement => make.element({ tag: 'meta', properties: { name: 'citation_conference_title', content: work.conferenceEvent.name }, children: [] });

export const makeWorkMetatags = (context: MarXivDataWithoutSitemap): HastElement[] => {
   if (context.workData.contributors && context.workData.originalPublicationDate) {
      const contribTags = makeAuthorMetatags(context.workData.contributors);
      // Provide unique defaults for PDFs vs other file types.
      if (context.workData.downloadLink && context.workData.fileType && context.workData.fileType === 'PDF') {
         switch (context.workData.octoWorkType) {
            case 'conferencePaper' :
               return [
                  // Title
                  make.element({ tag: 'meta', properties: { name: 'citation_title', content: context.workData.title }, children: [] }),
                  // Conference Title 
                  makeConferenceTitleTag(context.workData.conferencePaper as ConferencePaper),
                  // Publication date
                  makeDateMetatags(context.workData.originalPublicationDate),
                  // Pages
                  make.element({ tag: 'meta', properties: { name: 'citation_firstpage', content: context.workData.conferencePaper ? context.workData.conferencePaper.firstPage : undefined }, children: [] }),
                  make.element({ tag: 'meta', properties: { name: 'citation_lastpage', content: context.workData.conferencePaper ? context.workData.conferencePaper.lastPage : undefined }, children: [] }),
                  // Authors
                  ...contribTags,
                  // PDF
                  make.element({ tag: 'meta', properties: { name: 'citation_pdf_url', content: context.workData.downloadLink }, children: [] }),
               ]
            case 'conferenceProceedings' :
               return [
                  // Title
                  make.element({ tag: 'meta', properties: { name: 'citation_title', content: context.workData.title }, children: [] }),
                  // Conference Title 
                  makeConferenceTitleTag(context.workData.conferencePaper as ConferencePaper),
                  // Publication date
                  makeDateMetatags(context.workData.originalPublicationDate),
                  // Pages
                  make.element({ tag: 'meta', properties: { name: 'citation_firstpage', content: context.workData.conferencePaper ? context.workData.conferencePaper.firstPage : undefined }, children: [] }),
                  make.element({ tag: 'meta', properties: { name: 'citation_lastpage', content: context.workData.conferencePaper ? context.workData.conferencePaper.lastPage : undefined }, children: [] }),
                  // Authors
                  ...contribTags,
                  // PDF
                  make.element({ tag: 'meta', properties: { name: 'citation_pdf_url', content: context.workData.downloadLink }, children: [] }),
               ]
            case 'reportPublished' :
               return [
                  // Title
                  make.element({ tag: 'meta', properties: { name: 'citation_title', content: context.workData.title }, children: [] }),
                  // Institutional Publisher
                  make.element({ tag: 'meta', properties: { name: 'citation_technical_report_institution', content: context.workData.publisher ? context.workData.publisher.name : 'OCTO: Open Communications for The Ocean' }, children: [] }),
                  // Publication date
                  makeDateMetatags(context.workData.originalPublicationDate),
                  // Authors
                  ...contribTags,
                  // PDF
                  make.element({ tag: 'meta', properties: { name: 'citation_pdf_url', content: context.workData.downloadLink }, children: [] }),
               ]
            default:
               return [
                  // Title
                  make.element({ tag: 'meta', properties: { name: 'citation_title', content: context.workData.title }, children: [] }),
                  // Publication date
                  makeDateMetatags(context.workData.originalPublicationDate),
                  // Authors
                  ...contribTags,
                  // PDF
                  make.element({ tag: 'meta', properties: { name: 'citation_pdf_url', content: context.workData.downloadLink }, children: [] }),
               ]
         }
      }
      else {
         // This work is not a traditional PDF.
         switch (context.workData.octoWorkType) {
            default:
               return [
                  // Title
                  make.element({ tag: 'meta', properties: { name: 'citation_title', content: context.workData.title }, children: [] }),
                  // Publication date
                  makeDateMetatags(context.workData.originalPublicationDate),
                  // Authors
                  ...contribTags,
               ]
         }
      }
   }
   else {
      // Return an empty array if we're missing data.
      return [
         make.element({ tag: 'div', children: [] })
      ]
   }
}

export const makeCanonicalURL = (path: string) => (langCode: SupportedLanguage): string => {
   const domain = 'https://www.marxiv.org/' + langCode;
   if (path === '/') {
      return domain
   }
   else {
      return domain + path;
   }
}
export const makeDefaultLanguageLink = (path: string) => (langCode: SupportedLanguage): HastElement => make.element({ tag: 'link', properties: { rel: 'alternate', hrefLang: 'x-default', href: makeCanonicalURL(path)(langCode) }, children: []});
export const makeLanguageLink = (path: string) => (langCode: SupportedLanguage): HastElement => make.element({ tag: 'link', properties: { rel: 'alternate', hrefLang: langCode, href: makeCanonicalURL(path)(langCode) }, children: []});
export const makeTitleAndMetadata = (pageMetadata: PageMetadata): HastElement[] => [
   make.element({ tag: 'title', children: [
      make.text(pageMetadata.title)
   ]}),
   make.element({ tag: 'meta', properties: { name: 'description', content: pageMetadata.description }, children: [] }),
   make.element({ tag: 'link', properties: { rel: 'canonical', href: makeCanonicalURL(pageMetadata.localURL)('en') }, children: [] }),
   // Default language is English.
   makeDefaultLanguageLink(pageMetadata.localURL)('en'),
   // Currently, only the English is available.
   makeLanguageLink(pageMetadata.localURL)('en'),
]

/**
 * Call the appropriate reCAPTCHA action
 */
const determinePageLoadType = (pageMetadata: PageMetadataMinimum): reCAPTCHAActionStrings => {
   if (pageMetadata.localURL === '/') {
      return 'homepage'
   }
   else if (pageMetadata.localURL.startsWith('/join')) {
      return 'register'
   }
   else if (pageMetadata.localURL.startsWith('/login')) {
      return 'login'
   }
   else if (pageMetadata.localURL.startsWith('/reset')) {
      return 'reset'
   }
   else if (pageMetadata.localURL.startsWith('/submit/')) {
      return 'submit'
   }
   else {
      // Default to a general page-load
      return 'page'
   }
}

export const reCaptchaPageLoad = (context: MarXivDataWithoutSitemap): HastElement => {
   switch (determinePageLoadType(context.currentPage)) {
      case 'page':
         return make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.onLoadPage), defer: 'defer', type: 'module' }, children: [] });
      case 'homepage':
         return make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.onLoadHomepage), defer: 'defer', type: 'module' }, children: [] });
      default:
         // Default to a regular page
         return make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.onLoadPage), defer: 'defer', type: 'module' }, children: [] });
   }
}

/**
 * Make the Head content
 */
export const makeHeadContent = (context: MarXivDataWithoutSitemap) => (pageMetadata: PageMetadata): HastElement[] => {
   const recaptcha = reCaptchaPageLoad(context);
   const titleAndDescription = makeTitleAndMetadata(pageMetadata);
   if (pageMetadata.useWorkHead) {
      // Make the Highwire Metatags
      const googleScholarTags = makeWorkMetatags(context);
      // Grab the static head data
      return [
         ...titleAndDescription,
         ...makeStaticHeadChildren(context),
         ...googleScholarTags,
         // reCAPTCHA onLoads
         recaptcha,
         // Setup onClick event listeners for reCAPTCHA & Google Analytics
         make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.onClickSetup), defer: 'defer', type: 'module' }, children: [] }),
         // Insert HidePDF script
         make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.hidePDF), type: 'module', defer: 'defer' }, children: []})
      ]
   }
   else {
      if (pageMetadata.noBots) {
         if (pageMetadata.isReactPage) {
            return [
               // Don't crawl this page
               make.element({ tag: 'meta', properties: { name: 'robots', content: 'noindex, nofollow' }, children: [] }),
               ...titleAndDescription,
               ...makeStaticHeadChildren(context),
            ]
         }
         else {
            return [
               // Don't crawl this page
               make.element({ tag: 'meta', properties: { name: 'robots', content: 'noindex, nofollow' }, children: [] }),
               ...titleAndDescription,
               ...makeStaticHeadChildren(context), 
               // reCAPTCHA onLoads
               recaptcha,
               // Setup onClick event listeners for reCAPTCHA & Google Analytics
               make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.onClickSetup), defer: 'defer', type: 'module' }, children: [] }),
            ]
         }
      }
      else {
         if (pageMetadata.isReactPage) {
            // Don't add the onClick script
            return [
               ...titleAndDescription,
               ...makeStaticHeadChildren(context),
            ]
         }
         else if (context.currentPage.localURL === '/guidelines') {
            return [
               ...titleAndDescription,
               ...makeStaticHeadChildren(context),
               // reCAPTCHA onLoads
               recaptcha,
               // Setup onClick event listeners for reCAPTCHA & Google Analytics
               make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.onClickSetup), defer: 'defer', type: 'module' }, children: [] }),
               // Add the Collapsible Block setup script
               make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.collapsibleSetup), defer: 'defer', type: 'module' }, children: [] }),
            ]
         }
         else {
            // Crawl the page, per usual
            return [
               ...titleAndDescription,
               ...makeStaticHeadChildren(context),
               // reCAPTCHA onLoads
               recaptcha,
               // Setup onClick event listeners for reCAPTCHA & Google Analytics
               make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.onClickSetup), defer: 'defer', type: 'module' }, children: [] }),
            ]
         }
      }
   }
}

/**
 * Wrap in a Hast Head HastElement
 */
export const makeHead = (context: MarXivDataWithoutSitemap) => (pageMetadata: PageMetadata): HastHead => ({
   type: 'element',
   tagName: 'head',
   children: makeHeadContent(context)(pageMetadata),
})