/**
 * Make Self-Archiving Policies
 */

import { HastElement, make, appendChildNode } from '@octogroup/hast-typescript';

/**
 * Import local interfaces
 */
import { Policy, Publisher, } from './translatables';

/**
 * Import local dictionary
 */
import { SelfArchivingPoliciesText, } from './translatables';

/**
 * Make the self-archiving policies content
 */
const makeLicenseListOptions = (policy: Policy): HastElement[] => {
   let acc: HastElement[] = new Array;
   policy.allowedLicenses.forEach(
      (license) => {
         acc.push(make.element({ tag: 'li', children: [
            make.text(license)
         ]}))
      }
   );
   return acc
}

export const makeLicenseList = (dictionary: SelfArchivingPoliciesText) => (policy: Policy): HastElement | undefined => {
   if (policy.allowedLicenses[0] === 'all') {
      // The author can share with whichever license they want
      const description = policy.forWorkType === 'preprint' ? dictionary.licenseDescriptionAnyPreprint : dictionary.licenseDescriptionAnyPostprint;
      return make.element({ tag: 'p', children: [
         make.text(description)
      ]})
   }
   else if (policy.allowedLicenses[0] === 'noSharing') {
      // The author _cannot_ share this version at all
      return undefined
   }
   else if (policy.allowedLicenses[0] === 'noLicense') {
      // The author _cannot_ license their work
      const description = policy.forWorkType === 'preprint' ? dictionary.licenseDescriptionNonePreprint : dictionary.licenseDescriptionNonePostprint;
      return make.element({ tag: 'p', children: [
         make.text(description)
      ]})
   }
   else {
      // License options are limited to an array
      const description = policy.forWorkType === 'preprint' ? dictionary.licenseDescriptionLimitedPreprint : dictionary.licenseDescriptionLimitedPostprint;
      return make.element({ tag: 'p', children: [
         make.element({ tag: 'span', children: [
            make.text(description)
         ]}),
         // Display license options alphabetically
         make.element({ tag: 'ul', children: makeLicenseListOptions(policy).sort() })
      ]})
   }
}

export const makeCoverpage = (dictionary: SelfArchivingPoliciesText) => (policy: Policy): HastElement | undefined => {
   if (policy.coverPage) {
      const description = policy.forWorkType === 'preprint' ? dictionary.coverpageDescriptionPreprint : dictionary.coverpageDescriptionPostprint;
      return make.element({ tag: 'p', children: [
         make.element({ tag: 'span', children: [
            make.text(description)
         ]}),
         make.element({ tag: 'blockquote', children: [
            make.element({ tag: 'p', properties: { className: 'emphasis' }, children: [
               make.text(policy.coverPage)
            ]})
         ]})
      ]})
   }
   else {
      return undefined
   }
}

export const makePolicy = (d: SelfArchivingPoliciesText) => (policy: Policy): HastElement => {
   // Assemble the parts
   const source: HastElement = make.element({ tag: 'span', properties: { className: 'source' }, children: [
      make.text(' ('),
      make.element({ tag: 'a', properties: { href: policy.source }, children: [
         make.text(d.source)
      ]}),
      make.text(').')
   ]});
   const stdPolicy: HastElement = make.element({ tag: 'div', children: [
      appendChildNode(policy.policy)(source)
   ]});
   const licenses = makeLicenseList(d)(policy);
   const coverPage = makeCoverpage(d)(policy);
   // Return the appropriate elements
   if (licenses && coverPage) {
      return make.element({ tag: 'div', children: [
         stdPolicy,
         licenses,
         coverPage
      ]})
   }
   else if (licenses) {
      return make.element({ tag: 'div', children: [
         stdPolicy,
         licenses
      ]})
   }
   else if (coverPage) { 
      return make.element({ tag: 'div', children: [
         stdPolicy,
         coverPage
      ]})
   }
   else {
      return stdPolicy
   }
}

export const makePublisherPolicies = (d: SelfArchivingPoliciesText): HastElement => {
   let acc = new Array;
   d.journals.forEach((journal) => {
      acc.push(journal.publisher)
   })
   // Filter out duplicates
   const unqiuePublishers: Publisher[] = [...new Set(acc)];
   // Sort the list alphabetically
   const alphaPublishers = unqiuePublishers.sort((a, b) => {
      return a.name.localeCompare(b.name)
   })
   // Make the Hast
   let content: HastElement[] = new Array;
   alphaPublishers.forEach((publisher) => {
      content.push( make.element({ tag: 'h3', children: [
         make.text(publisher.name)
      ]}) )
      // Preprints
      content.push( makePolicy(d)(publisher.defaultPolicyPreprint) )
      // Postprints
      content.push( makePolicy(d)(publisher.defaultPolicyPostprint) )
   })
   return make.element({ tag: 'div', children: content })
}

export const makeJournalPolicies = (d: SelfArchivingPoliciesText): HastElement => {
   // Sort the list alphabetically
   const alphaJournals = d.journals.sort((a, b) => {
      return a.title.localeCompare(b.title)
   })
   let content: HastElement[] = new Array;
   alphaJournals.forEach((journal) => {
      content.push( make.element({ tag: 'h3', children: [
         make.text(journal.title)
      ]}))
      // Preprints
      if (journal.overridingPreprintPolicy) {
         content.push( makePolicy(d)(journal.overridingPreprintPolicy) )
      }
      else {
         content.push( makePolicy(d)(journal.publisher.defaultPolicyPreprint) )
      }
      // Postprints
      if (journal.overridingPostprintPolicy) {
         content.push( makePolicy(d)(journal.overridingPostprintPolicy) )
      }
      else {
         content.push( makePolicy(d)(journal.publisher.defaultPolicyPostprint) )
      }
   })
   return make.element({ tag: 'div', children: content })
}

export const makeSelfArchivingPolicies = (d: SelfArchivingPoliciesText): HastElement => {
   const titleAndIntro: HastElement = make.element({ tag: 'div', children: d.titleAndIntro });
   const spacer = make.element({ tag: 'div', properties: { className: 'spacer4' }, children: [] });
   const bookChapterPolicies: HastElement = make.element({ tag: 'div', children: d.bookChaptersPolicies });
   const defaultPublisherPoliciesTitle: HastElement = make.element({ tag: 'h2', children: [
      make.text(d.defaultPublisherPoliciesTitle)
   ]});
   const journalPoliciesTitle: HastElement = make.element({ tag: 'h2', children: [
      make.text(d.journalPoliciesTitle)
   ]});
   return make.element({ tag: 'div', properties: { className: 'policies' }, children: [
      titleAndIntro,
      spacer,
      bookChapterPolicies,
      spacer,
      defaultPublisherPoliciesTitle,
      makePublisherPolicies(d),
      spacer,
      journalPoliciesTitle,
      makeJournalPolicies(d)
   ]})
}