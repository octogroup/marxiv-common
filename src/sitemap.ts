/**
 * Define the MarXiv sitemap.
 * 
 * We should use this to generate an XML sitemap for Google and Bing.
 */
import { PageMetadata, PageMetadataMinimum, MarXivDataWithoutSitemap, UseWorkHead, } from './context';
import { Translatables, TranslatablePageMetadata } from './translatables';
import { makePageMetadataHomepage, makePageMetadataAmbassadors, makePageMetadataTeam, makePageMetadataWhyMarXiv, makePageMetadataWorkListing, makePageMetadataWorks, makePageMetadataOrgs, makePageMetadataShare, makePageMetadataSelfArchivingPolicies, makePageMetadataSubmissionGuidelines, makePageMetadataCodeOfConduct, makePageMetadataPrivacyPolicy, makePageMetadataCelebrate, makePageMetadataLicenses, makePageMetadataJoin, makePageMetadataLogin, makePageMetadataResetPassword, makePageMetadataUserDashboards, makePageMetadataUserAccount, makePageMetadataEditUserAccount, makePageMetadataConfirmNewAccount, makePageMetadataConfirmNewSubmission, makePageMetadataConfirmResubmit, makePageMetadataConfirmUpdatedWork, makePageMetadataConfirmVerifyEmail, makePageMetadataSharePreprint, makePageMetadataSharePostprint, makePageMetadataShareOA, makePageMetadataShareUncopyrighted, makePageMetadataShareReportUnpublished, makePageMetadataShareThesis, makePageMetadataSharePoster, makePageMetadataShareLetter, makePageMetadataShareWorkingPaper, makePageMetadataEditLetter, makePageMetadataEditOA, makePageMetadataEditPoster, makePageMetadataEditPostprint, makePageMetadataEditPreprint, makePageMetadataEditReportUnpublished, makePageMetadataEditThesis, makePageMetadataEditUncopyrighted, makePageMetadataEditWorkingPaper, makePageMetadataModerateLetter, makePageMetadataModerateOA, makePageMetadataModeratePoster, makePageMetadataModeratePostprint, makePageMetadataModeratePreprint, makePageMetadataModerateReportUnpublished, makePageMetadataModerateThesis, makePageMetadataModerateUncopyrighted, makePageMetadataModerateWorkingPaper, makePageMetadataNotFound, makePageMetadataAccessDenied, makePageMetadataUhoh, makePageMetadataTypesOfWorksAccepted, makePageMetadataHomepageMinimum, makePageMetadataAmbassadorsMinimum, makePageMetadataCelebrateMinimum, makePageMetadataCodeOfConductMinimum, makePageMetadataConfirmNewAccountMinimum, makePageMetadataConfirmNewSubmissionMinimum, makePageMetadataConfirmResubmitMinimum, makePageMetadataConfirmUpdatedWorkMinimum, makePageMetadataConfirmVerifyEmailMinimum, makePageMetadataEditOAMinimum, makePageMetadataEditLetterMinimum, makePageMetadataEditPosterMinimum, makePageMetadataEditPostprintMinimum, makePageMetadataEditPreprintMinimum, makePageMetadataEditReportUnpublishedMinimum, makePageMetadataEditThesisMinimum, makePageMetadataEditUncopyrightedMinimum, makePageMetadataEditUserAccountMinimum, makePageMetadataEditWorkingPaperMinimum, makePageMetadataJoinMinimum, makePageMetadataLicensesMinimum, makePageMetadataLoginMinimum, makePageMetadataModerateLetterMinimum, makePageMetadataModerateOAMinimum, makePageMetadataModeratePosterMinimum, makePageMetadataModeratePostprintMinimum, makePageMetadataModeratePreprintMinimum, makePageMetadataModerateReportUnpublishedMinimum, makePageMetadataModerateThesisMinimum, makePageMetadataModerateUncopyrightedMinimum, makePageMetadataModerateWorkingPaperMinimum, makePageMetadataNotFoundMinimum, makePageMetadataOrgsMinimum, makePageMetadataPrivacyPolicyMinimum, makePageMetadataResetPasswordMinimum, makePageMetadataSelfArchivingPoliciesMinimum, makePageMetadataShareLetterMinimum, makePageMetadataSharePosterMinimum, makePageMetadataShareOAMinimum, makePageMetadataSharePreprintMinimum, makePageMetadataShareReportUnpublishedMinimum, makePageMetadataShareThesisMinimum, makePageMetadataShareUncopyrightedMinimum, makePageMetadataShareWorkingPaperMinimum, makePageMetadataSubmissionGuidelinesMinimum, makePageMetadataShareMinimum, makePageMetadataTeamMinimum, makePageMetadataTypesOfWorksAcceptedMinimum, makePageMetadataUserAccountMinimum, makePageMetadataUserDashboardsMinimum, makePageMetadataWhyMarXivMinimum, makePageMetadataWorkListingMinimum, makePageMetadataWorksMinimum, makePageMetadataSharePostrintMinimum, makePageMetadataAccessDeniedMinimum, makePageMetadataUhohMinimum, makePageMetadataLogoutMinimum, makePageMetadataLogout, makePageMetadataGoogleSearch, makePageMetadataGoogleSearchMinimum, makePageMetadataWorkListingPaginationMinimum, makePageMetadataWorkListingPagination, makePageMetadataAdminDashboardMinimum, makePageMetadataAdminDashboard, makePageMetadataAdminEditAccountMinimum, makePageMetadataAdminEditAccount, } from './pageMetadata';
import { makeShareOptions1, makeShareOptions2, makeShareOptions3, makeSharePublishedPaperOptions, makeShareConferencePaperOptions, makeShareReportOptions, makeSharePublishedPaperCopyrightedOptions, makeShareSplash, } from './share';
import { WorkData } from './works';
import { makePaginatorDataArray } from './workListing';
import { SupportedLanguage } from './languages'; 

export type MarXivSitemapMinimum = MarXivSitemapT<PageMetadataMinimum>

export type MarXivSitemap = MarXivSitemapT<PageMetadata>

export interface MarXivSitemapT<P> {
   // Main menu items
   homepage: P // index.html
   why: {
      whyMarXiv: P // parent why.html
      team: P
      ambassadors: P
   }
   works: {
      workListing: P // parent works.html
      paginatedWorksPages: P[] // Displayed as marxiv.test/pages/1.html
      works: (UseWorkHead & P)[] // All of our works, displayed as marxiv.test/works/workDOI.html
   }
   forOrgs: P
   share: {
      splash: P
      submissionGuide1: P
      submissionGuidePublishedOptions: P
      submissionGuidePublishedCopyrighted: P
      submissionGuide2: P
      submissionGuideReportOptions: P
      submissionGuideConferenceOptions: P
      // submissionGuideDatasetOptions: P
      submissionGuide3: P
      selfArchivingPolicies: P
      submissionGuidelines: P
      codeOfConduct: P
   }
   // Custom Google Search
   search: P
   // Minor pages
   privacyPolicy: P
   celebrate: P
   licenses: P
   worksAccepted: P
   // User menu
   join: P
   login: P
   resetPassword: P
   logout: P
   // User pages
   user: {
      dashboard: P
      account: P
      editAccount: P
   }
   // General confirmation pages (no need to make specific since google won't be allowed to crawl)
   confirm: {
      newSubmission: P
      updatedWork: P
      resubmit: P
      newAccount: P
      verifyEmail: P
   }
   // Upload works
   submit: {
      preprint: P
      postprint: P
      openAccess: P
      unCopyrighted: P
      unpublishedReport: P
      thesis: P
      // database: P
      // dataset: P
      posterPresentationSI: P
      letter: P
      workingPaper: P
      // peerReview: P
      // publishedReport: P
      // publishedConferencePaper: P
   }
   // Edit works
   edit: {
      preprint: P
      postprint: P
      openAccess: P
      unCopyrighted: P
      unpublishedReport: P
      thesis: P
      // database: P
      // dataset: P
      posterPresentationSI: P
      letter: P
      workingPaper: P
      // peerReview: P
      // publishedReport: P
      // publishedConferencePaper: P
   }
   // Moderate works
   moderate: {
      preprint: P
      postprint: P
      openAccess: P
      unCopyrighted: P
      unpublishedReport: P
      thesis: P
      // database: P
      // dataset: P
      posterPresentationSI: P
      letter: P
      workingPaper: P
      // peerReview: P
      // publishedReport: P
      // publishedConferencePaper: P
   }
   // Error Pages
   error: {
      notFound: P
      accessDenied: P
      uhoh: P
   }
   // Admin Dashboard
   admin: P
   adminEditAccount: P
}

/**
 * Generate the minimal sitemap, first
 */
export const makeMinimalMarXivSitemap = (requestedLanguage: SupportedLanguage) => (works: WorkData[])=> (d: TranslatablePageMetadata): MarXivSitemapMinimum => ({
   homepage: makePageMetadataHomepageMinimum(d),
   why: {
      whyMarXiv: makePageMetadataWhyMarXivMinimum(d),
      team: makePageMetadataTeamMinimum(d),
      ambassadors: makePageMetadataAmbassadorsMinimum(d),
   },
   works: {
      workListing: makePageMetadataWorkListingMinimum(d), // Parent papers.html
      paginatedWorksPages: makePageMetadataWorkListingPaginationMinimum(d)(makePaginatorDataArray(requestedLanguage)({ works: works, itemsPerPage: 10 })), // Child workListing pages, displayed as marxiv.test/pages/1.html
      works: makePageMetadataWorksMinimum(works), // All of our works, displayed as marxiv.test/papers/workDOI.html
   },
   forOrgs: makePageMetadataOrgsMinimum(d),
   share: {
      splash: makePageMetadataShareMinimum(d)(undefined),
      submissionGuide1: makePageMetadataShareMinimum(d)('some'),
      submissionGuidePublishedOptions: makePageMetadataShareMinimum(d)('published'),
      submissionGuidePublishedCopyrighted: makePageMetadataShareMinimum(d)('copyrighted'),
      submissionGuide2: makePageMetadataShareMinimum(d)('more'),
      submissionGuideReportOptions: makePageMetadataShareMinimum(d)('report'),
      submissionGuideConferenceOptions: makePageMetadataShareMinimum(d)('conference'),
      // submissionGuideDatasetOptions: makePageMetadataShareMinimum(d)('dataset'),
      submissionGuide3: makePageMetadataShareMinimum(d)('most'),
      selfArchivingPolicies: makePageMetadataSelfArchivingPoliciesMinimum(d),
      submissionGuidelines: makePageMetadataSubmissionGuidelinesMinimum(d),
      codeOfConduct: makePageMetadataCodeOfConductMinimum(d),
   },
   // Custom Google Search
   search: makePageMetadataGoogleSearchMinimum(d),
   // Minor pages
   privacyPolicy: makePageMetadataPrivacyPolicyMinimum(d),
   celebrate: makePageMetadataCelebrateMinimum(d),
   licenses: makePageMetadataLicensesMinimum(d),
   worksAccepted: makePageMetadataTypesOfWorksAcceptedMinimum(d),
   // User menu
   join: makePageMetadataJoinMinimum(d),
   login: makePageMetadataLoginMinimum(d),
   resetPassword: makePageMetadataResetPasswordMinimum(d),
   logout: makePageMetadataLogoutMinimum(d),
   // User pages
   user: {
      dashboard: makePageMetadataUserDashboardsMinimum(d),
      account: makePageMetadataUserAccountMinimum(d),
      editAccount: makePageMetadataEditUserAccountMinimum(d),
   },
   // General confirmation pages (no need to make specific since google won't be allowed to crawl)
   confirm: {
      newSubmission: makePageMetadataConfirmNewSubmissionMinimum(d),
      updatedWork: makePageMetadataConfirmUpdatedWorkMinimum(d),
      resubmit: makePageMetadataConfirmResubmitMinimum(d),
      newAccount: makePageMetadataConfirmNewAccountMinimum(d),
      verifyEmail: makePageMetadataConfirmVerifyEmailMinimum(d),
   },
   // Upload works
   submit: {
      preprint: makePageMetadataSharePreprintMinimum(d),
      postprint: makePageMetadataSharePostrintMinimum(d),
      openAccess: makePageMetadataShareOAMinimum(d),
      unCopyrighted: makePageMetadataShareUncopyrightedMinimum(d),
      unpublishedReport: makePageMetadataShareReportUnpublishedMinimum(d),
      thesis: makePageMetadataShareThesisMinimum(d),
      // database: makePageMetadataShareDatabaseMinimum(d),
      // dataset: makePageMetadataShareDatasetMinimum(d),
      posterPresentationSI: makePageMetadataSharePosterMinimum(d),
      letter: makePageMetadataShareLetterMinimum(d),
      workingPaper: makePageMetadataShareWorkingPaperMinimum(d),
      // peerReview: makePageMetadataSharePeerReviewMinimum(d),
      // publishedReport: makePageMetadataShareReportPublishedMinimum(d),
      // publishedConferencePaper: makePageMetadataShareConferenceMinimum(d),
   },
   // Edit works
   edit: {
      preprint: makePageMetadataEditPreprintMinimum(d),
      postprint: makePageMetadataEditPostprintMinimum(d),
      openAccess: makePageMetadataEditOAMinimum(d),
      unCopyrighted: makePageMetadataEditUncopyrightedMinimum(d),
      unpublishedReport: makePageMetadataEditReportUnpublishedMinimum(d),
      thesis: makePageMetadataEditThesisMinimum(d),
      // database: makePageMetadataEditDatabaseMinimum(d),
      // dataset: makePageMetadataEditDatasetMinimum(d),
      posterPresentationSI: makePageMetadataEditPosterMinimum(d),
      letter: makePageMetadataEditLetterMinimum(d),
      workingPaper: makePageMetadataEditWorkingPaperMinimum(d),
      // peerReview: makePageMetadataEditPeerReviewMinimum(d),
      // publishedReport: makePageMetadataEditReportPublishedMinimum(d),
      // publishedConferencePaper: makePageMetadataEditConferenceMinimum(d),
   },
   // Moderate works
   moderate: {
      preprint: makePageMetadataModeratePreprintMinimum(d),
      postprint: makePageMetadataModeratePostprintMinimum(d),
      openAccess: makePageMetadataModerateOAMinimum(d),
      unCopyrighted: makePageMetadataModerateUncopyrightedMinimum(d),
      unpublishedReport: makePageMetadataModerateReportUnpublishedMinimum(d),
      thesis: makePageMetadataModerateThesisMinimum(d),
      // database: makePageMetadataModerateDatabaseMinimum(d),
      // dataset: makePageMetadataModerateDatasetMinimum(d),
      posterPresentationSI: makePageMetadataModeratePosterMinimum(d),
      letter: makePageMetadataModerateLetterMinimum(d),
      workingPaper: makePageMetadataModerateWorkingPaperMinimum(d),
      // peerReview: makePageMetadataModeratePeerReviewMinimum(d),
      // publishedReport: makePageMetadataModerateReportPublishedMinimum(d),
      // publishedConferencePaper: makePageMetadataModerateConferenceMinimum(d),
   },
   // Error Pages
   error: {
      notFound: makePageMetadataNotFoundMinimum(d),
      accessDenied: makePageMetadataAccessDeniedMinimum(d),
      uhoh: makePageMetadataUhohMinimum(d),
   },
   // Admin Dashboard
   admin: makePageMetadataAdminDashboardMinimum(d),
   adminEditAccount: makePageMetadataAdminEditAccountMinimum(d),
});

/**
 * Implement the Sitemap
 */
export const makeMarXivSitemap = (context: MarXivDataWithoutSitemap) => (d: Translatables): MarXivSitemap => ({
   homepage: makePageMetadataHomepage(context)(d),
   why: {
      whyMarXiv: makePageMetadataWhyMarXiv(d),
      team: makePageMetadataTeam(d),
      ambassadors: makePageMetadataAmbassadors(d),
   },
   works: {
      workListing: makePageMetadataWorkListing(context)(d), // parent works.html
      works: makePageMetadataWorks(context)(d), // All of our works, displayed as marxiv.org/works/ID.html
      paginatedWorksPages: makePageMetadataWorkListingPagination(context)(d)(makePaginatorDataArray(context.requestedLanguage)({ works: context.works, itemsPerPage: 10 }))
   },
   forOrgs: makePageMetadataOrgs(d),
   share: {
      splash: makePageMetadataShare(d)( makeShareSplash(context)(d.pages.submitGuide) )(undefined),
      submissionGuide1: makePageMetadataShare(d)( makeShareOptions1(context)(d.pages.submitGuide) )('some'),
      submissionGuidePublishedOptions: makePageMetadataShare(d)( makeSharePublishedPaperOptions(context)(d.pages.submitGuide) )('published'),
      submissionGuidePublishedCopyrighted: makePageMetadataShare(d)( makeSharePublishedPaperCopyrightedOptions(d.pages.submitGuide) )('copyrighted'),
      submissionGuide2: makePageMetadataShare(d)( makeShareOptions2(context)(d.pages.submitGuide) )('more'),
      submissionGuideReportOptions: makePageMetadataShare(d)( makeShareReportOptions(context)(d.pages.submitGuide) )('report'),
      submissionGuideConferenceOptions: makePageMetadataShare(d)( makeShareConferencePaperOptions(context)(d.pages.submitGuide) )('conference'),
      // submissionGuideDatasetOptions: makePageMetadataShare(d)( makeShareDatabaseOptions(context)(d.pages.submitGuide) )('dataset'),
      submissionGuide3: makePageMetadataShare(d)( makeShareOptions3(context)(d.pages.submitGuide) )('most'),
      selfArchivingPolicies: makePageMetadataSelfArchivingPolicies(d),
      submissionGuidelines: makePageMetadataSubmissionGuidelines(context)(d),
      codeOfConduct: makePageMetadataCodeOfConduct(d),
   },
   // Google Search
   search: makePageMetadataGoogleSearch(d),
   // Minor pages
   privacyPolicy: makePageMetadataPrivacyPolicy(d),
   celebrate: makePageMetadataCelebrate(d),
   licenses: makePageMetadataLicenses(d),
   worksAccepted: makePageMetadataTypesOfWorksAccepted(d),
   // User menu
   join: makePageMetadataJoin(context)(d),
   login: makePageMetadataLogin(context)(d),
   resetPassword: makePageMetadataResetPassword(context)(d),
   logout: makePageMetadataLogout(d),
   // User pages
   user: {
      dashboard: makePageMetadataUserDashboards(context)(d),
      account: makePageMetadataUserAccount(context)(d),
      editAccount: makePageMetadataEditUserAccount(context)(d),
   },
   // General confirmation pages (no need to make specific since google won't be allowed to crawl)
   confirm: {
      newSubmission: makePageMetadataConfirmNewSubmission(d),
      updatedWork: makePageMetadataConfirmUpdatedWork(d),
      resubmit: makePageMetadataConfirmResubmit(d),
      newAccount: makePageMetadataConfirmNewAccount(d),
      verifyEmail: makePageMetadataConfirmVerifyEmail(d),
   },
   // Upload works
   submit: {
      preprint: makePageMetadataSharePreprint(context)(d),
      postprint: makePageMetadataSharePostprint(context)(d),
      openAccess: makePageMetadataShareOA(context)(d),
      unCopyrighted: makePageMetadataShareUncopyrighted(context)(d),
      unpublishedReport: makePageMetadataShareReportUnpublished(context)(d),
      thesis: makePageMetadataShareThesis(context)(d),
      // database: makePageMetadataShareDatabase(context)(d),
      // dataset: makePageMetadataShareDataset(context)(d),
      posterPresentationSI: makePageMetadataSharePoster(context)(d),
      letter: makePageMetadataShareLetter(context)(d),
      workingPaper: makePageMetadataShareWorkingPaper(context)(d),
      // peerReview: makePageMetadataSharePeerReview(context)(d),
      // publishedReport: makePageMetadataShareReportPublished(context)(d),
      // publishedConferencePaper: makePageMetadataShareConference(context)(d),
   },
   // Edit works
   edit: {
      preprint: makePageMetadataEditPreprint(context)(d),
      postprint: makePageMetadataEditPostprint(context)(d),
      openAccess: makePageMetadataEditOA(context)(d),
      unCopyrighted: makePageMetadataEditUncopyrighted(context)(d),
      unpublishedReport: makePageMetadataEditReportUnpublished(context)(d),
      thesis: makePageMetadataEditThesis(context)(d),
      // database: makePageMetadataEditDatabase(context)(d),
      // dataset: makePageMetadataEditDataset(context)(d),
      posterPresentationSI: makePageMetadataEditPoster(context)(d),
      letter: makePageMetadataEditLetter(context)(d),
      workingPaper: makePageMetadataEditWorkingPaper(context)(d),
      // peerReview: makePageMetadataEditPeerReview(context)(d),
      // publishedReport: makePageMetadataEditReportPublished(context)(d),
      // publishedConferencePaper: makePageMetadataEditConference(context)(d),
   },
   // Moderate works
   moderate: {
      preprint: makePageMetadataModeratePreprint(context)(d),
      postprint: makePageMetadataModeratePostprint(context)(d),
      openAccess: makePageMetadataModerateOA(context)(d),
      unCopyrighted: makePageMetadataModerateUncopyrighted(context)(d),
      unpublishedReport: makePageMetadataModerateReportUnpublished(context)(d),
      thesis: makePageMetadataModerateThesis(context)(d),
      // database: makePageMetadataModerateDatabase(context)(d),
      // dataset: makePageMetadataModerateDataset(context)(d),
      posterPresentationSI: makePageMetadataModeratePoster(context)(d),
      letter: makePageMetadataModerateLetter(context)(d),
      workingPaper: makePageMetadataModerateWorkingPaper(context)(d),
      // peerReview: makePageMetadataModeratePeerReview(context)(d),
      // publishedReport: makePageMetadataModerateReportPublished(context)(d),
      // publishedConferencePaper: makePageMetadataModerateConference(context)(d),
   },
   // Error Pages
   error: {
      notFound: makePageMetadataNotFound(d),
      accessDenied: makePageMetadataAccessDenied(d),
      uhoh: makePageMetadataUhoh(d),
   },
   // Admin Dashboard
   admin: makePageMetadataAdminDashboard(context)(d),
   adminEditAccount: makePageMetadataAdminEditAccount(context)(d),
});

/**
 * CSS, JS, and image assets
 * 
 * @param location Relative to the root domain, e.g. `/css/style.css`
 */
export interface Asset {
   location: string
}
export interface JSAssets {
   // Scripts
   mobileMainMenu: Asset
   hidePDF: Asset
   collapsibleSetup: Asset
   handleSearchSubmit: Asset
   // reCAPTCHA
   onLoadPage: Asset
   onLoadHomepage: Asset
   onLoadInteractive: Asset
   onClickSetup: Asset
   // GAnalytics / GTag
   gtagSetup: Asset
   // React
   editUserAccountBundle: Asset
   passwordResetBundle: Asset
   reactDashboardBundle: Asset
   reactAdminBundle: Asset
   reactAdminEditAccount: Asset
   reactSubmitFormBundle: Asset
   reactModerationFormBundle: Asset
   reactEditFormBundle: Asset
   userAccountBundle: Asset
   userLoginBundle: Asset
   userRegisterBundle: Asset
}
export interface CSSAssets {
   style: Asset
}
export interface ImageAssets {
   // Favicons
   android36: Asset
   android48: Asset
   android72: Asset
   android96: Asset
   android144: Asset
   android192: Asset
   apple57: Asset
   apple60: Asset
   apple72: Asset
   apple76: Asset
   apple114: Asset
   apple120: Asset
   apple144: Asset
   apple152: Asset
   apple180: Asset
   fav16: Asset
   fav32: Asset
   fav96: Asset
   msApplication: Asset
   // Icons
   questionCircle: Asset
   arrowCircleUp: Asset
   creativeCommons: Asset
   checkCircle: Asset
   chevronRight: Asset
   clipboardList: Asset
   download: Asset
   facebookSquare: Asset
   fileAlt: Asset
   fileUpload: Asset
   twitterSquare: Asset
   piggyBank: Asset
   quoteLeft: Asset
   quoteRight: Asset
   search: Asset
   // Logos
   octoWhite: Asset
   octoClearOrange: Asset
   marxivVert: Asset
   marxivLines: Asset
}
export interface MarXivAssets {
   js: JSAssets
   css: CSSAssets
   images: ImageAssets
}

export const staticAssets: MarXivAssets = {
   js: {
      mobileMainMenu: {
         location: '/js/mobileMainMenu.js'
      },
      hidePDF: {
         location: '/js/hidePDF.js'
      },
      collapsibleSetup: {
         location: '/js/collapsibleSetup.js'
      },
      onLoadPage: {
         location: '/js/onLoadPage.js'
      },
      onLoadHomepage: {
         location: '/js/onLoadHomepage.js'
      },
      onLoadInteractive: {
         location: '/js/onLoadInteractive.js'
      },
      onClickSetup: {
         location: '/js/onClickSetup.js'
      },
      editUserAccountBundle: {
         location: '/js/editUserAccountBundle.js'
      },
      passwordResetBundle: {
         location: '/js/passwordResetBundle.js'
      },
      reactDashboardBundle: {
         location: '/js/reactDashboardBundle.js'
      },
      reactAdminBundle: {
         location: '/js/reactAdminBundle.js'
      },
      reactAdminEditAccount: {
         location: '/js/reactAdminEditAccountBundle.js'
      },
      reactSubmitFormBundle: {
         location: '/js/reactSubmitFormBundle.js'
      },
      reactEditFormBundle: {
         location: '/js/reactEditFormBundle.js'
      },
      reactModerationFormBundle: {
         location: '/js/reactModerationFormBundle.js'
      },
      userAccountBundle: {
         location: '/js/userAccountBundle.js'
      },
      userLoginBundle: {
         location: '/js/userLoginBundle.js'
      },
      userRegisterBundle: {
         location: '/js/userRegisterBundle.js'
      },
      gtagSetup: {
         location: '/js/gtagSetup.js'
      },
      handleSearchSubmit: {
         location: '/js/handleSearchSubmit.js'
      },
   },
   css: {
      // style.css
      style: {
         location: '/css/style.css'
      },
   },
   images: {
      // Favicons
      fav16: {
         location: '/images/favicons/favicon-16x16.png'
      },
      fav32: {
         location: '/images/favicons/favicon-32x32.png'
      },
      fav96: {
         location: '/images/favicons/favicon-96x96.png'
      },
      android36: {
         location: '/images/favicons/android-icon-36x36.png'
      },
      android48: {
         location: '/images/favicons/android-icon-48x48.png'
      },
      android72: {
         location: '/images/favicons/android-icon-72x72.png'
      },
      android96: {
         location: '/images/favicons/android-icon-96x96.png'
      },
      android144: {
         location: '/images/favicons/android-icon-144x144.png'
      },
      android192: {
         location: '/images/favicons/favicon-192x192.png'
      },
      apple57: {
         location: '/images/favicons/apple-icon-57x57.png'
      },
      apple60: {
         location: '/images/favicons/apple-icon-60x60.png'
      },
      apple72: {
         location: '/images/favicons/apple-icon-72x72.png'
      },
      apple76: {
         location: '/images/favicons/apple-icon-76x76.png'
      },
      apple114: {
         location: '/images/favicons/apple-icon-114x114.png'
      },
      apple120: {
         location: '/images/favicons/apple-icon-120x120.png'
      },
      apple144: {
         location: '/images/favicons/apple-icon-144x144.png'
      },
      apple152: {
         location: '/images/favicons/apple-icon-152x152.png'
      },
      apple180: {
         location: '/images/favicons/apple-icon-180x180.png'
      },
      msApplication: {
         location: '/images/favicons/ms-icon-144x144.png'
      },
      // Icons
      questionCircle: {
         location: '/images/icons/question-circle.svg'
      },
      arrowCircleUp: {
         location: '/images/icons/arrow-circle-up.svg'
      },
      creativeCommons: {
         location: '/images/icons/creative-commons.svg'
      },
      checkCircle: {
         location: '/images/icons/check-circle.svg'
      },
      chevronRight: {
         location: '/images/icons/chevron-right.svg'
      },
      clipboardList: {
         location: '/images/icons/clipboard-list.svg'
      },
      download: {
         location: '/images/icons/download.svg'
      },
      facebookSquare: {
         location: '/images/icons/facebook-square.svg'
      },
      fileAlt: {
         location: '/images/icons/file-alt.svg'
      },
      fileUpload: {
         location: '/images/icons/file-upload.svg'
      },
      twitterSquare: {
         location: '/images/icons/twitter-square.svg'
      },
      piggyBank: {
         location: '/images/icons/piggy-bank.svg'
      },
      quoteLeft: {
         location: '/images/icons/quote-left.svg'
      },
      quoteRight: {
         location: '/images/icons/quote-right.svg'
      },
      search: {
         location: '/images/icons/search.svg'
      },
      // Logos
      octoWhite: {
         location: '/images/logos/octoWhite.png'
      },
      octoClearOrange: {
         location: '/images/logos/octoClearOrange.png'
      },
      marxivVert: {
         location: '/images/logos/marxivVert.png'
      },
      marxivLines: {
         location: '/images/logos/marxivLines.png'
      },
   },
}