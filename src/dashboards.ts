/**
 * User and moderator dashboards
 */
import { make, } from '@octogroup/hast-typescript';
import { MarXivDataWithoutSitemap, ActiveWorkData, } from './context';

/**
 * Import the local dictionary.
 */
import { StaticWorkText, FormMessageText } from './translatables';

/**
 * Local translatables.
 */
export const translateModerationStatus = (d: StaticWorkText) => (work: ActiveWorkData): string => {
   switch (work.moderationStatus) {
      case 'pending':
         return d.moderation.pending
      case 'approved':
         return d.moderation.approved
      case 'rejected':
         return d.moderation.rejected
      case 'withdrawn':
         return d.moderation.withdrawn
      default:
         throw new Error('Invalid moderation status supplied to translate.')
   }
}

/**
 * Make the components.
 */
// export const makeModerationBadge = (d: StaticWorkText) => (work: ActiveWorkData): HastElement => {
//    if (work.moderationStatus === 'rejected' || work.moderationStatus === 'withdrawn') {
//       return make.element({ tag: 'div', properties: { className: 'badge red center'}, children: [
//          make.text(translateModerationStatus(d)(work))
//       ]})
//    }
//    else if (work.moderationStatus === 'pending') {
//       return make.element({ tag: 'div', properties: { className: 'badge orange center'}, children: [
//          make.text(translateModerationStatus(d)(work))
//       ]})
//    }
//    else if (work.marxivDOI) {
//       return make.element({ tag: 'div', properties: { className: 'badge grey center'}, children: [
//          make.text(d.doi + ': ' + work.marxivDOI)
//       ]})
//    }
//    else {
//       return make.element({ tag: 'div', properties: { className: 'badge grey center'}, children: [
//          make.text(translateModerationStatus(d)(work))
//       ]})
//    }
// }

// export const makeTypeBadge = (d: StaticWorkText) => (work: ActiveWorkData): HastElement => make.element({ tag: 'div', properties: { className: 'badge orange center' }, children: [
//    make.text( translateWorkTypeBadge(d)(work) )
// ]})

// export const makeDateBadge = (d: CommonReplacements) => (work: ActiveWorkData): HastElement => {
//    if (work.originalPublicationDate) {
//       return make.element({ tag: 'div', properties: { className: 'badge blue center' }, children: [
//          make.text( formatOriginalPublicationDate(d)(work.originalPublicationDate) )
//       ]});
//    }
//    else {
//       return make.element({ tag: 'div', properties: { className: 'badge blue center' }, children: [
//          make.text(d.notSet)
//       ]});
//    }
// }

// export const makeLicenseBadge = (d: StaticWorkText) => (work: ActiveWorkData): HastElement => make.element({ tag: 'div', properties: { className: 'badge body center' }, children: [
//    make.text( translateLicenseBadge(d)(work) )
// ]})

// export const makeSubmitterBadge = (d: CommonReplacements) => (work: ActiveWorkData): HastElement => make.element({ tag: 'div', properties: { className: 'badge grey center' }, children: [
//    make.text(work.submittedBy ? work.submittedBy : d.notSet)
// ]})

/**
 * Make dashboards for regular users
 */
// export const makeUserWorkTeaser = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (work: ActiveWorkData): HastElement => make.element({ tag: 'div', properties: { className: 'paper teaser' }, children: [
//    make.element({ tag: 'h2', children: [
//       make.element({ tag: 'a', properties: { href: '/' + context.requestedLanguage + '/works/'+ work.workID }, children: [
//          make.text(work.title ? work.title : d.commonReplacements.notSet)
//       ]})
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'metadataBar flex all-parent-row' }, children: [
//       makeDateBadge(d.commonReplacements)(work),
//       makeTypeBadge(d.works.static)(work),
//       makeLicenseBadge(d.works.static)(work),
//       makeModerationBadge(d.works.static)(work)
//    ]}),
//    make.element({ tag: 'p', children: [
//       make.text(work.description ? work.description : d.commonReplacements.notSet)
//    ]}),
// ]})

// export const combineUserWorkTeasers = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (works: ActiveWorkData[]): HastElement[] => {
//    let teasers: HastElement[] = new Array;
//    for (let i = 0; i < works.length; i++) {
//       teasers.push( makeUserWorkTeaser(context)(d)(works[i]) )
//    }
//    return teasers
// }

// export const makeUserDashboard = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (works: ActiveWorkData[]): HastElement => make.element({ tag: 'div', children: [
//    make.element({ tag: 'h1', children: [
//       make.text(d.works.static.dashboards.userTitle)
//    ]}),
//    make.element({ tag: 'div', properties: { id: 'userDashboardContent' }, children: combineUserWorkTeasers(context)(d)(works) })
// ]})

/**
 * Make dashboards for admins
 */
// export const makeAdminWorkTeaser = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (work: ActiveWorkData): HastElement => make.element({ tag: 'div', properties: { className: 'paper teaser' }, children: [
//    make.element({ tag: 'h2', children: [
//       make.element({ tag: 'a', properties: { href: '/' + context.requestedLanguage + '/works/'+ work.workID }, children: [
//          make.text(work.title ? work.title : d.commonReplacements.notSet)
//       ]})
//    ]}),
//    make.element({ tag: 'div', properties: { className: 'metadataBar flex all-parent-row' }, children: [
//       makeDateBadge(d.commonReplacements)(work),
//       makeTypeBadge(d.works.static)(work),
//       makeLicenseBadge(d.works.static)(work),
//       makeSubmitterBadge(d.commonReplacements)(work)
//    ]}),
//    make.element({ tag: 'p', children: [
//       make.text(work.description ? work.description : d.commonReplacements.notSet)
//    ]}),
// ]})

// export const combineAdminWorkTeasers = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (works: ActiveWorkData[]): HastElement[] => {
//    let teasers: HastElement[] = new Array;
//    for (let i = 0; i < works.length; i++) {
//       teasers.push( makeAdminWorkTeaser(context)(d)(works[i]) )
//    }
//    return teasers
// }

// export const makeAdminDashboard = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (works: ActiveWorkData[]): HastElement => make.element({ tag: 'div', children: [
//    make.element({ tag: 'h1', children: [
//       make.text(d.works.static.dashboards.adminTitle)
//    ]}),
//    make.element({ tag: 'div', properties: { id: 'adminDashboardContent' }, children: combineAdminWorkTeasers(context)(d)(works) })
// ]})

export const makeReactDashboard = (d: FormMessageText) => (context: MarXivDataWithoutSitemap) => make.element({ tag: 'div', properties: { id: 'reactDashboard' }, children: [
   make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.reactDashboardBundle), type: 'module', async: 'async' }, children:[] }),
   make.element({ tag: 'h2', children: [
      make.text(d.loading.loading + ' ' + d.loading.dashboard + '...')
   ]})
]});

export const makeReactAdminDashboard = (d: FormMessageText) => (context: MarXivDataWithoutSitemap) => make.element({ tag: 'div', properties: { id: 'reactAdminDashboard' }, children: [
   make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.reactAdminBundle), type: 'module', async: 'async' }, children:[] }),
   make.element({ tag: 'h2', children: [
      make.text(d.loading.loading + ' ' + d.loading.dashboard + '...')
   ]})
]});