/**
 * Make the homepage
 */

import { HastElement, make, } from '@octogroup/hast-typescript';
import { MarXivDataWithoutSitemap } from './context';
import { makeLocalPath, } from './common';

/**
 * Import the dictionary
 */
import { Translatables, } from './translatables';

/**
 * Mobile buttons
 */
export const makeShareButton = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.splash) }, children: [
   make.element({ tag: 'div', properties: { className: 'button wrapper light flex all-parent-row' }, children: [
      make.element({ tag: 'div', properties: { className: 'flex all-parent-column all-child-span3' }, children: [
         make.element({ tag: 'div', properties: { className: 'button title' }, children: [
            make.text(d.pages.homepage.shareButtonTitle)
         ]}),
         make.element({ tag: 'div', properties: { className: 'button subtitle' }, children: [
            make.text(d.pages.homepage.shareButtonSubtitle)
         ]})
      ]}),
      make.element({ tag: 'div', properties: { className: 'button icon flex all-child-span1 right' }, children: [
         make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.chevronRight), className: 'chevron' }, children: []})
      ]})
   ]})
]})

export const makeVisitButton = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.works.workListing) }, children: [
   make.element({ tag: 'div', properties: { className: 'button wrapper regular flex all-parent-row' }, children: [
      make.element({ tag: 'div', properties: { className: 'flex all-parent-column all-child-span3' }, children: [
         make.element({ tag: 'div', properties: { className: 'button title' }, children: [
            make.text(d.pages.homepage.visitButtonTitle)
         ]}),
         make.element({ tag: 'div', properties: { className: 'button subtitle' }, children: [
            make.text(d.pages.homepage.visitButtonSubtitle)
         ]})
      ]}),
      make.element({ tag: 'div', properties: { className: 'button icon flex all-child-span1 right' }, children: [
         make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.chevronRight), className: 'chevron' }, children: []})
      ]})
   ]})
]})

export const makeLearnButton = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.splash) }, children: [
   make.element({ tag: 'div', properties: { className: 'button wrapper dark flex all-parent-row' }, children: [
      make.element({ tag: 'div', properties: { className: 'flex all-parent-column all-child-span3' }, children: [
         make.element({ tag: 'div', properties: { className: 'button title' }, children: [
            make.text(d.pages.homepage.learnButtonTitle)
         ]}),
         make.element({ tag: 'div', properties: { className: 'button subtitle' }, children: [
            make.text(d.pages.homepage.learnButtonSubtitle)
         ]})
      ]}),
      make.element({ tag: 'div', properties: { className: 'button icon flex all-child-span1 right' }, children: [
         make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.chevronRight), className: 'chevron' }, children: []})
      ]})
   ]})
]})

export const makeMobileContent = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'mobile'}, children: [
   makeShareButton(context)(d),
   makeVisitButton(context)(d),
   makeLearnButton(context)(d)
]})

/**
 * Stats icons
 */
export const makeStatSavings = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'statWrapper flex all-parent-column tablet-landscape-parent-row all-child-span1' }, children: [
   make.element({ tag: 'div', properties: { className: 'icon center' }, children: [
      make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.piggyBank), className: 'icon' }, children: []})
   ]}),
   make.element({ tag: 'div', properties: { className: 'statTextWrapper center' }, children: [
      make.element({ tag: 'div', properties: { className: 'stat upper' }, children: [
         make.text(d.pages.homepage.statSavedUpper)
      ]}),
      make.element({ tag: 'div', properties: { id: 'statCost', className: 'stat middle' }, children: [
         make.text(d.commonReplacements.costSavings)
      ]}),
      make.element({ tag: 'div', properties: { className: 'stat lower' }, children: [
         make.text(d.pages.homepage.statSavedLower)
      ]})
   ]})
]})

export const makeStatDownloads = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'statWrapper flex all-parent-column tablet-landscape-parent-row all-child-span1' }, children: [
   make.element({ tag: 'div', properties: { className: 'icon center' }, children: [
      make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.download), className: 'icon' }, children: []})
   ]}),
   make.element({ tag: 'div', properties: { className: 'statTextWrapper center' }, children: [
      make.element({ tag: 'div', properties: { className: 'stat upper' }, children: [
         make.text(d.pages.homepage.statDownloadsUpper)
      ]}),
      make.element({ tag: 'div', properties: { id: 'statDownload', className: 'stat middle' }, children: [
         make.text(d.commonReplacements.downloadCount)
      ]}),
      make.element({ tag: 'div', properties: { className: 'stat lower' }, children: [
         make.text(d.pages.homepage.statDownloadsLower)
      ]})
   ]})
]})

export const makeStats = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'home lead center'}, children: [
   make.element({ tag: 'h3', children: [
      make.text(d.pages.homepage.statsTitle)
   ]}),
   make.element({ tag: 'h4', children: [
      make.text(d.pages.homepage.statsSubtitle)
   ]}),
   make.element({ tag: 'div', properties: { className: 'flex all-parent-row' }, children: [
      makeStatSavings(context)(d),
      makeStatDownloads(context)(d)
   ]})
]})

/**
 * Desktop content
 */
export const makeDesktopContent = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'desktop flex all-parent-column' }, children: [
   // Submit section
   make.element({ tag: 'div', properties: { className: 'submit'}, children: [
      make.element({ tag: 'div', properties: { className: 'title'}, children: [
         make.text(d.pages.homepage.desktopSubmitTitle)
      ]}),
      make.element({ tag: 'div', properties: { className: 'subtitle' }, children: [
         make.text(d.pages.homepage.desktopSubmitSubtitle)
      ]}),
      make.element({ tag: 'div', properties: { className: 'steps flex all-parent-row' }, children: [
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.works.workListing), className: 'upload flex all-child-span3 center normalWhiteLink' }, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.fileUpload), className: 'icon' }, children: []}),
            make.element({ tag: 'div', properties: { className: 'text noBreak' }, children: [
               make.text(d.pages.homepage.uploadText)
            ]})
         ]}),
         make.element({ tag: 'div', properties: { className: 'chevronWrapper flex all-child-span1 center' }, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.chevronRight), className: 'chevron' }, children: []})
         ]}),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.worksAccepted), className: 'upload flex all-child-span3 center normalWhiteLink' }, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.clipboardList), className: 'icon' }, children: []}),
            make.element({ tag: 'div', properties: { className: 'text noBreak' }, children: [
               make.text(d.pages.homepage.metadataText)
            ]})
         ]}),
         make.element({ tag: 'div', properties: { className: 'chevronWrapper flex all-child-span1 center' }, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.chevronRight), className: 'chevron' }, children: []})
         ]}),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.licenses), className: 'upload flex all-child-span3 center normalWhiteLink' }, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.creativeCommons), className: 'icon' }, children: []}),
            make.element({ tag: 'div', properties: { className: 'text noBreak' }, children: [
               make.text(d.pages.homepage.licenseText)
            ]})
         ]}),
         make.element({ tag: 'div', properties: { className: 'chevronWrapper flex all-child-span1 center' }, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.chevronRight), className: 'chevron' }, children: []})
         ]}),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.celebrate), className: 'upload flex all-child-span3 center normalWhiteLink' }, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.checkCircle), className: 'icon' }, children: []}),
            make.element({ tag: 'div', properties: { className: 'text noBreak' }, children: [
               make.text(d.pages.homepage.doneText)
            ]})
         ]}),
         make.element({ tag: 'div', properties: { className: 'chevronWrapper flex all-child-span1 center' }, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.chevronRight), className: 'chevron' }, children: []})
         ]})
      ]}),
      make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.splash), className: 'orangeButton button orange normalWhiteLink flex all-parent-column center' }, children: [
         make.text(d.pages.homepage.submitText)
      ]})
   ]}),
   // Works section
   make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.logout), className: 'visit normalWhiteLink noHover flex all-parent-row all-child-span100' }, children: [
      make.element({ tag: 'div', properties: { className: 'file flex all-child-span1 right' }, children: [
         make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.fileAlt), className: 'icon' }, children: []})
      ]}),
      make.element({ tag: 'div', properties: { className: 'wrapper flex all-child-span3' }, children: [
         make.element({ tag: 'div', properties: { className: 'title' }, children: [
            make.text(d.pages.homepage.visitTitle)
         ]}),
         make.element({ tag: 'div', children: [
            make.text(d.pages.homepage.visitSubtitle)
         ]})
      ]}),
      make.element({ tag: 'div', properties: { className: 'chevronWrapper flex all-child-span1' }, children: [
         make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.chevronRight), className: 'chevron' }, children: []})
      ]})
   ]}),
   // Testamonials section
   make.element({ tag: 'div', properties: { className: 'testamonials'}, children: [
      make.element({ tag: 'div', properties: { className: 'title'}, children: [
         make.text(d.pages.homepage.testamonialsTitle)
      ]}),
      make.element({ tag: 'div', properties: { className: 'testamonial flex all-parent-row' }, children: [
         make.element({ tag: 'div', properties: { className: 'person flex all-child-span1'}, children: [
            make.text(d.pages.homepage.testamonial1Name)
         ]}),
         make.element({ tag: 'div', properties: { className: 'text flex all-child-span2'}, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.quoteLeft), className: 'quoteOpen' }, children: []}),
            make.text(d.pages.homepage.testamonial1Text),
            make.element({tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.quoteRight), className: 'quoteClosed' }, children: []})
         ]})
      ]}),
      make.element({ tag: 'div', properties: { className: 'testamonial flex all-parent-row' }, children: [
         make.element({ tag: 'div', properties: { className: 'person flex all-child-span1'}, children: [
            make.text(d.pages.homepage.testamonial2Name)
         ]}),
         make.element({ tag: 'div', properties: { className: 'text flex all-child-span2'}, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.quoteLeft), className: 'quoteOpen' }, children: []}),
            make.text(d.pages.homepage.testamonial2Text),
            make.element({tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.quoteRight), className: 'quoteClosed' }, children: []})
         ]})
      ]}),
      make.element({ tag: 'div', properties: { className: 'testamonial flex all-parent-row' }, children: [
         make.element({ tag: 'div', properties: { className: 'person flex all-child-span1'}, children: [
            make.text(d.pages.homepage.testamonial3Name)
         ]}),
         make.element({ tag: 'div', properties: { className: 'text flex all-child-span2'}, children: [
            make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.quoteLeft), className: 'quoteOpen' }, children: []}),
            make.text(d.pages.homepage.testamonial3Text),
            make.element({tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.quoteRight), className: 'quoteClosed' }, children: []})
         ]})
      ]})
   ]})
]})

/**
 * Combine to generate the homepage content
 */
export const makeWrappedBody = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'home content'}, children: [
   makeMobileContent(context)(d),
   makeDesktopContent(context)(d)
]})

export const makeHomepageContent = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', children: [
   makeStats(context)(d),
   makeWrappedBody(context)(d)
]})