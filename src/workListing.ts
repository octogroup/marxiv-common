/**
 * Works listing
 */

import { HastElement, make, } from '@octogroup/hast-typescript';
import { MarXivDataWithoutSitemap } from './context';
import { translateWorkTypeBadge, translateLicenseBadge, WorkData, formatOriginalPublicationDate, } from './works';
import { GenericContributor, ContribStd, } from './submitForms';
import { SupportedLanguage, } from './languages';

/**
 * Import the dictionaries
 */
import { WorkListingText, Translatables, StaticWorkText } from './translatables'; 

/**
 * Define local interfaces
 */
export interface WorkListingData {
   works: WorkData[]
   itemsPerPage: number
}

/**
 * Paginators
 */
export interface PaginatorData {
   currentPage: number
   worksToDisplay: WorkData[]
   nextPageLink?: string
   previousPageLink?: string
}

export const makePaginatorBar = (translatables: Translatables) => (pagination: PaginatorData): HastElement[] => {
   if (pagination.nextPageLink && pagination.previousPageLink) {
      return [
         make.element({ tag: 'a', properties: { className: 'paginator page previous', href: pagination.previousPageLink }, children: [
            make.text('<<')
         ]}),
         make.element({ tag: 'div', properties: { className: 'paginator page' }, children: [
            make.text(translatables.commonReplacements.page + ' ' + pagination.currentPage.toString())
         ]}),
         make.element({ tag: 'a', properties: { className: 'paginator page next', href: pagination.nextPageLink }, children: [
            make.text('>>')
         ]})
      ]
   }
   else if (pagination.nextPageLink) { 
      return [
         make.element({ tag: 'div', properties: { className: 'paginator page' }, children: [
            make.text(translatables.commonReplacements.page + ' ' + pagination.currentPage.toString())
         ]}),
         make.element({ tag: 'a', properties: { className: 'paginator page next', href: pagination.nextPageLink }, children: [
            make.text('>>')
         ]})
      ]
   }
   else if (pagination.previousPageLink) {
      return [
         make.element({ tag: 'a', properties: { className: 'paginator page previous', href: pagination.previousPageLink }, children: [
            make.text('<<')
         ]}),
         make.element({ tag: 'div', properties: { className: 'paginator page' }, children: [
            make.text(translatables.commonReplacements.page + ' ' + pagination.currentPage.toString())
         ]})
      ]
   }
   else {
      return [
         make.element({ tag: 'div', properties: { className: 'paginator page' }, children: [
            make.text(translatables.commonReplacements.page + ' ' + pagination.currentPage.toString())
         ]})
      ]
   }
}

export const makePaginatorDataArray = (requestedLanguage: SupportedLanguage) => (worksData: WorkListingData): PaginatorData[] => {
   const worksArray: WorkData[] = worksData.works;
   const items: number = worksArray.length;
   const itemsPP: number = worksData.itemsPerPage;
   const prefix = '/' + requestedLanguage + '/page/';

   if (itemsPP >= items) {
      // There is only a single page of items
      return [
         {
            currentPage: 1,
            worksToDisplay: worksArray
         }
      ]
   }
   else {
      // We must paginate
      // Round up the number of pages, so 2.4 --> 3.
      const numberOfPages: number = Math.ceil(items / itemsPP);
      let paginatedPages: PaginatorData[] = new Array;
      for (let i = 0; i < numberOfPages; i++) {
         let currentPage: number = i + 1;
         let nextPage: number = i + 2;
         let previousPage: number = i;

         if (i === 0) {
            // First page
            paginatedPages.push({
               currentPage: currentPage,
               nextPageLink: prefix + nextPage.toString(),
               worksToDisplay: worksArray.slice(0, itemsPP)
            })
         }
         else if (i === (numberOfPages - 1)) {
            // Last page
            const startFinalSlice: number = itemsPP * (numberOfPages - 1);
            paginatedPages.push({
               currentPage: currentPage,
               previousPageLink: prefix + previousPage.toString(),
               worksToDisplay: worksArray.slice(startFinalSlice, items)
            })
         }
         else {
            // Inner pages
            const startSlice: number = itemsPP * (i + 1);
            const endSlice: number =  itemsPP * (i + 2);
            paginatedPages.push({
               currentPage: currentPage,
               previousPageLink: prefix + previousPage.toString(),
               nextPageLink: prefix + nextPage.toString(),
               worksToDisplay: worksArray.slice(startSlice, endSlice)
            })
         }
      }
      return paginatedPages
   }
}

export const makePaginator = (translatables: Translatables) => (pagination: PaginatorData): HastElement => make.element({ tag: 'div', properties: { className: 'paginator wrapper flex all-parent-row center', id: 'pagination'}, children: makePaginatorBar(translatables)(pagination) });

/**
 * Make the content
 */
export const makeWelcomeUpper = (context: MarXivDataWithoutSitemap) => (d: WorkListingText): HastElement => make.element({ tag: 'div', properties: { className: 'upper' }, children: [
   make.element({ tag: 'h1', children: [
      make.text(d.workListingWelcome)
   ]}),
   make.element({ tag: 'div', properties: { className: 'subtitle' }, children: [
      make.text(d.workListingSubtitle)
   ]}),
   make.element({ tag: 'form', properties: { className: 'form' }, children: [
      make.element({ tag: 'div', properties: { className: 'field flex all-parent-row' }, children: [
         make.element({ tag: 'div', properties: {className: 'searchInput' }, children: [
            make.element({ tag: 'input', properties: { type: 'search', id: 'workSearch', name: 'search', placeholder: d.workListingSearchWorks }, children: []})
         ]}),
         make.element({ tag: 'button', properties: { type: 'button', className: 'button search', id: 'workSearchButton' }, children: [
            make.text(d.workListingSearch)
         ]}),
         // Add event-listener script to bump search to Google Custom Search
         make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.handleSearchSubmit), defer: 'defer' }, children: []}),
      ]})
   ]})
]});

export const contribStdToString = (contrib: ContribStd): string => {
   if (contrib.givenName && contrib.suffix) {
      return contrib.givenName + ' ' + contrib.surname + ', ' + contrib.suffix
   }
   else if (contrib.givenName) {
      return contrib.givenName + ' ' + contrib.surname
   }
   else {
      return contrib.surname
   }
}

export const wrapContribs = (d: StaticWorkText) => (contribs: GenericContributor[]): HastElement[] => {
   let wrappedContribs: HastElement[] = new Array;
   for (let i = 0; i < contribs.length; i++) {
      let contrib = contribs[i];
      switch (contrib.type) {
         case 'std':
            wrappedContribs.push(make.element({ tag: 'div', properties: { className: 'author' }, children: [
               make.text(contribStdToString(contrib))
            ]}))
            break;
         case 'org':
            wrappedContribs.push(make.element({ tag: 'div', properties: { className: 'author' }, children: [
               make.text(contrib.name)
            ]}))
            break;
         case 'anon':
            if (contrib.affiliation) {
               wrappedContribs.push(make.element({ tag: 'div', properties: { className: 'author' }, children: [
                  make.text(d.anonymousContrib + ', ' + contrib.affiliation)
               ]}))
            }
            else {
               wrappedContribs.push(make.element({ tag: 'div', properties: { className: 'author' }, children: [
                  make.text(d.anonymousContrib)
               ]}))
            }
            break;
         default:
            throw new Error('Invalid contributor type supplied to wrapContribs.')
      }
   }
   return wrappedContribs
}

export const makeSummary = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (work: WorkData): HastElement => {
   // Make all the parts
   const title: HastElement = make.element({ tag: 'h2', properties: { className: 'title' }, children: [
      make.element({ tag: 'a', properties: { href: '/' + context.requestedLanguage + '/works/'+ work.workID, className: 'normalHeadingLink' }, children: [
         make.text(work.title)
      ]})
   ]});
   const dateBadge: HastElement = make.element({ tag: 'div', properties: { className: 'date badge blue' }, children: [
      make.text( formatOriginalPublicationDate(d.commonReplacements)(work.originalPublicationDate) )
   ]});
   const typeBadge: HastElement = make.element({ tag: 'div', properties: { className: 'type badge orange' }, children: [
      make.text( translateWorkTypeBadge(d.works.static)(work) )
   ]});
   const licenseBadge: HastElement = make.element({ tag: 'div', properties: { className: 'license badge body' }, children: [
      make.text( translateLicenseBadge(d.works.static)(work) )
   ]});
   const authors: HastElement = make.element({ tag: 'div', properties: { className: 'authors flex all-parent-row' }, children: wrapContribs(d.works.static)(work.contributors)});
   const abstract: HastElement = make.element({ tag: 'p', properties: { className: 'abtract' }, children: [
      make.text(work.description)
   ]});
   // Return the formatted summary
   return make.element({tag: 'div', properties: { className: 'summary' }, children:[
      title,
      make.element({ tag: 'div', properties: { className: 'metadata flex all-parent-row' }, children: [
         dateBadge,
         typeBadge,
         licenseBadge
      ]}),
      authors,
      abstract
   ]})
};

/**
 * Make a paginated Works page
 */
export const makeSummaries = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (works: WorkData[]): HastElement => {
   let summariesArray: HastElement[] = new Array;
   works.map( (value) => summariesArray.push( makeSummary(context)(d)(value) ) );
   return make.element({ tag: 'div', children: summariesArray })
};

export const makeSummariesAndPaginator = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (options: PaginatorData): HastElement => {
   return make.element({ tag: 'div', children: [
      makeSummaries(context)(d)(options.worksToDisplay),
      makePaginator(d)(options)
   ]});
};

export const makeWorkListMain = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (works: WorkData[]): HastElement => {
   // First we'll need to make paginated data, and display just the first chunk for /works.html
   const firstPagePaginationInfo: PaginatorData = makePaginatorDataArray(context.requestedLanguage)({
      works: works,
      itemsPerPage: 10
   })[0];
   return make.element({ tag: 'div', properties: { className: 'main' }, children: [
      make.element({ tag: 'h2', children: [
         make.text(d.pages.workListing.workListingRecent)
      ]}),
      makeSummariesAndPaginator(context)(d)(firstPagePaginationInfo)
   ]})
};

export const makeWorkListing = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (works: WorkData[]): HastElement => make.element({ tag: 'div', properties: { className: 'paperList' }, children: [
   makeWelcomeUpper(context)(d.pages.workListing),
   makeWorkListMain(context)(d)(works)
]});

export const makeWorkListingPaginatedPage = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (options: PaginatorData): HastElement => make.element({ tag: 'div', properties: { className: 'paperList' }, children: [
   makeWelcomeUpper(context)(d.pages.workListing),
   make.element({ tag: 'div', properties: { className: 'main' }, children: [
      make.element({ tag: 'h2', children: [
         make.text(d.pages.workListing.workListingRecent)
      ]}),
      makeSummariesAndPaginator(context)(d)(options)
   ]})
]});