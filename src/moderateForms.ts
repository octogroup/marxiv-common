/**
 * Work Moderation Forms
 */
import { make, } from '@octogroup/hast-typescript';
import { SelectOption } from './common';
import { MarXivDataWithoutSitemap } from './context';

/**
 * Import the local dictionary
 */
import { Translatables, StaticWorkText, FormMessageText } from './translatables';

/**
 * Embed the React form
 */
export const makeReactModerationForm = (d: FormMessageText) => (context: MarXivDataWithoutSitemap) => make.element({ tag: 'div', properties: { id: 'reactModerationForm' }, children: [
   make.element({ tag: 'script', properties: { src: context.linkToAsset(context.currentPage)(context.assets.js.reactModerationFormBundle), type: 'module', async: 'async' }, children:[] }),
   make.element({ tag: 'h2', children: [
      make.text(d.loading.loading + ' ' + d.loading.form + '...')
   ]})
]});

/**
 * Message to Submitter
 */
// export const makeMessageToSubmitter = (context: MarXivDataWithoutSitemap) => (d: ModerateFormText): HastElement => makeForm(d.messageToSubmitter.formTitle)([
//    // Description
//    d.messageToSubmitter.description,
//    // Message
//    makeTextInput({
//       label: d.messageToSubmitter.message.fieldTitle,
//       placeholder: d.messageToSubmitter.message.default,
//       id: randomizeID('submitterMessage'),
//       required: true,
//       short: false,
//       value: context.workData.moderationMessage
//    }),
// ]);

/**
 * Moderation Form-End
 */
// export const makeFormEndModeration = (d: Translatables): HastElement => make.element({ tag: 'div', properties: { className: 'form end flex all-parent-row' }, children: [
//    // Reject
//    make.element({ tag: 'div', properties: { className: 'flex all-child-span1 center' }, children: [
//       make.element({ tag: 'button', properties: { className: 'button cancel', type: 'button' }, children: [
//          make.text(d.commonReplacements.reject)
//       ]})
//    ]}),
//    // Approve
//    make.element({ tag: 'div', properties: { className: 'flex all-child-span1 center' }, children: [
//       make.element({ tag: 'button', properties: { className: 'submit', type: 'button' }, children: [
//          make.text(d.commonReplacements.approve)
//       ]})
//    ]}),
// ]});

export const makeBadgeTypeOptions = (d: StaticWorkText): SelectOption[] => [
   {
      label: d.badges.preprint,
      value: 'preprint'
   },
   {
      label: d.badges.postprint,
      value: 'postprint'
   },
   {
      label: d.badges.report,
      value: 'report'
   },
   {
      label: d.badges.thesis,
      value: 'thesis'
   },
   {
      label: d.badges.dissertation,
      value: 'dissertation'
   },
   {
      label: d.badges.database,
      value: 'database'
   },
   {
      label: d.badges.dataset,
      value: 'dataset'
   },
   {
      label: d.badges.letter,
      value: 'letter'
   },
   {
      label: d.badges.workingPaper,
      value: 'workingPaper'
   },
   {
      label: d.badges.peerReview,
      value: 'peerReview'
   },
   {
      label: d.badges.journalArticle,
      value: 'journalArticle'
   },
   {
      label: d.badges.paper,
      value: 'paper'
   },
   {
      label: d.badges.poster,
      value: 'poster'
   },
   {
      label: d.badges.conferenceProceedings,
      value: 'conferenceProceedings'
   },
   {
      label: d.badges.conferencePaper,
      value: 'conferencePaper'
   },
   {
      label: d.badges.presentation,
      value: 'presentation'
   },
   {
      label: d.badges.supplementalInfo,
      value: 'supplementalInfo'
   },
   {
      label: d.badges.other,
      value: 'other'
   },
];

// export const makeSelectBadgeType = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeSelectList({
//    label: d.forms.moderate.octoWorkType.badgeTypeFieldTitle,
//    id: randomizeID('badgeType'),
//    required: true,
//    multi: false,
//    options: makeBadgeTypeOptions(d.works.static),
//    selected: context.workData.badgeType
// })

export const makeOctoWorkTypeOptions = (d: StaticWorkText): SelectOption[] => [
   {
      label: d.badges.preprint,
      value: 'preprint'
   },
   {
      label: d.badges.postprint,
      value: 'postprint'
   },
   {
      label: d.badges.reportPublished,
      value: 'reportPublished'
   },
   {
      label: d.badges.reportUnpublished,
      value: 'reportUnpublished'
   },
   {
      label: d.badges.thesis,
      value: 'thesis'
   },
   {
      label: d.badges.dissertation,
      value: 'dissertation'
   },
   {
      label: d.badges.database,
      value: 'database'
   },
   {
      label: d.badges.dataset,
      value: 'dataset'
   },
   {
      label: d.badges.letter,
      value: 'letter'
   },
   {
      label: d.badges.workingPaper,
      value: 'workingPaper'
   },
   {
      label: d.badges.peerReview,
      value: 'peerReview'
   },
   {
      label: d.badges.poster,
      value: 'poster'
   },
   {
      label: d.badges.conferenceProceedings,
      value: 'conferenceProceedings'
   },
   {
      label: d.badges.conferencePaper,
      value: 'conferencePaper'
   },
   {
      label: d.badges.presentation,
      value: 'presentation'
   },
   {
      label: d.badges.supplementalInfo,
      value: 'supplementalInfo'
   },
   {
      label: d.badges.other,
      value: 'other'
   },
   {
      label: d.badges.copyrightedPaper,
      value: 'copyrightedPaper'
   },
   {
      label: d.badges.oaPaper,
      value: 'oaPaper'
   },
];

// export const makeSelectOctoWorkType = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeSelectList({
//    label: d.forms.moderate.octoWorkType.fieldTitle,
//    id: randomizeID('octoWorkType'),
//    required: true,
//    multi: false,
//    options: makeOctoWorkTypeOptions(d.works.static),
//    selected: context.workData.octoWorkType
// })

// Now that we have the OctoWorkType and Badge, make a form for them
// export const makeOctoWorkTypeFormGroup = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => {
//    // We only need to provide badges for OA and copyrighted papers. All other badge:work combos are known.
//    if (context.workData.octoWorkType === 'oaPaper' || context.workData.octoWorkType === 'copyrightedPaper') {
//       return make.element({ tag: 'div', properties: { className: 'group flex all-parent-column' }, children: [
//          make.element({ tag: 'div', properties: { className: 'groupLabel flex all-child-span1' }, children: [
//             make.text(d.forms.moderate.octoWorkType.fieldTitle)
//          ]}),
//          make.element({ tag: 'div', properties: { className: 'fields flex all-child-span1' }, children: [
//             // Select the OCTO work type
//             makeSelectOctoWorkType(context)(d),
//             // Select the badge type
//             makeSelectBadgeType(context)(d),
//          ]})
//       ]});
//    }
//    else {
//       return make.element({ tag: 'div', properties: { className: 'group flex all-parent-column' }, children: [
//          make.element({ tag: 'div', properties: { className: 'groupLabel flex all-child-span1' }, children: [
//             make.text(d.forms.moderate.octoWorkType.fieldTitle)
//          ]}),
//          make.element({ tag: 'div', properties: { className: 'fields flex all-child-span1' }, children: [
//             // Select the OCTO work type
//             makeSelectOctoWorkType(context)(d)
//          ]})
//       ]});
//    }
// }

/**
 * Posted Content Type
 */
export const makePostedContentTypeOptions = (d: Translatables): SelectOption[] => [
   {
      label: d.forms.submit.basic.type.preprint,
      value: 'preprint',
   },
   {
      label: d.forms.submit.basic.type.workingPaper,
      value: 'workingPaper',
   },
   {
      label: d.forms.submit.basic.type.letter,
      value: 'letter',
   },
   {
      label: d.forms.submit.basic.type.thesis,
      value: 'dissertation',
   },
   {
      label: d.forms.submit.basic.type.report,
      value: 'report',
   },
   {
      label: d.forms.submit.basic.type.other,
      value: 'other',
   },
]
// export const makePostedContentType = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeSelectList({
//    label: d.forms.submit.basic.type.fieldTitle,
//    id: randomizeID('postedContentType'),
//    required: false,
//    multi: false,
//    options: makePostedContentTypeOptions(d),
//    selected: context.workData.postedContentType
// })

/**
 * Preview a PDF
 */
// export const makePreviewPDFFormContent  = (context: MarXivDataWithoutSitemap) => (d: StaticWorkText): HastElement => {
//    const viewerStarter = '/pdf/web/viewer.html?file=';
//    const viewer = viewerStarter + context.workData.downloadLink;
//    return make.element({ tag: 'div', properties: { id: 'preview', className: 'preview flex all-parent-column center' }, children: [
//       make.element({ tag: 'embed', properties: {id: 'embed', type: 'application/pdf', src: context.workData.downloadLink }, children: []}),
//       make.element({ tag: 'a', properties: { href: viewer, target: '_blank' }, children: [
//          make.text(d.openPDF)
//       ]}),
//       // Insert HidePDF script
//       make.element({ tag: 'script', properties: { src: 'hidePDF.js' }, children: []})
//    ]})
// }
// export const makePreviewPDFForm = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeCollapsibleForm({
//    title: d.forms.submit.files.pdf.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.forms.submit.files.pdf.formTitle)
//    ]}),
//    content: makePreviewPDFFormContent(context)(d.works.static),
//    id: randomizeID('moderatePDFFile'),
//    collapsible: 'maxFile',
// })

/**
 * Download dataset files
 */
// export const makeDatasetDownload = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => {
//    // Make the download link button
//    if (context.workData.downloadLink && context.workData.fileType && context.workData.fileSize) {
//       return make.element({ tag: 'a', properties: { className: 'download button blue flex all-parent-column center normalWhiteLink', href: context.workData.downloadLink }, children: [
//          make.element({ tag: 'div', properties: { className: 'download' }, children: [
//             make.text(d.works.static.download + ' (' + context.workData.fileType + ', ' + context.workData.fileSize + ')')
//          ]})
//       ]})
//    }
//    else if (context.workData.downloadLink && context.workData.fileType) {
//       return make.element({ tag: 'a', properties: { className: 'download button blue flex all-parent-column center normalWhiteLink', href: context.workData.downloadLink }, children: [
//          make.element({ tag: 'div', properties: { className: 'download' }, children: [
//             make.text(d.works.static.download + ' (' + context.workData.fileType + ')')
//          ]})
//       ]}) 
//    }
//    else if (context.workData.downloadLink) {
//       return make.element({ tag: 'a', properties: { className: 'download button blue flex all-parent-column center normalWhiteLink', href: context.workData.downloadLink }, children: [
//          make.element({ tag: 'div', properties: { className: 'download' }, children: [
//             make.text(d.works.static.download)
//          ]})
//       ]}) 
//    }
//    else {
//       // If there is no download link. just don't print the button.
//       throw new Error("Missing required download link for the moderation form.")
//    }
// };

// export const makeDatasetDownloadForm = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeCollapsibleForm({
//    title: d.forms.submit.files.dataset.formTitle,
//    miniDescription: make.element({ tag: 'p', children: [
//       make.text(d.forms.submit.files.dataset.formTitle)
//    ]}),
//    content: makeDatasetDownload(context)(d),
//    id: randomizeID('moderateDatasetFile'),
//    collapsible: 'maxFile',
// })

// /**
//  * Preview / download generic files
//  */
// export const makePreviewandDownloadGeneric = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => {
//    if (context.workData.fileType === 'PDF') {
//       return makeCollapsibleForm({
//          title: d.forms.submit.files.generic.formTitle,
//          miniDescription: make.element({ tag: 'p', children: [
//             make.text( d.forms.submit.files.generic.formTitle)
//          ]}),
//          content: makePreviewPDFFormContent(context)(d.works.static),
//          id: randomizeID('moderateGenericFile'),
//          collapsible: 'maxFile',
//       });
//    }
//    else {
//       return makeCollapsibleForm({
//          title: d.forms.submit.files.generic.formTitle,
//          miniDescription: make.element({ tag: 'p', children: [
//             make.text( d.forms.submit.files.generic.formTitle)
//          ]}),
//          content: makeDatasetDownload(context)(d),
//          id: randomizeID('moderateGenericFile'),
//          collapsible: 'maxFile',
//       });
//    }
// }

/**
 * Moderator fields
 */
// export const determineIfPostedContent = (type: OctoWorkTypeAliases): boolean => {
//    switch (type) {
//       case 'preprint':
//          return true
//       case 'oaPaper':
//          return true
//       case 'postprint':
//          return true
//       case 'copyrightedPaper':
//          return true
//       case 'reportUnpublished':
//          return true
//       case 'thesis':
//          return true
//       case 'supplementalInfo':
//          return true
//       case 'letter':
//          return true
//       case 'workingPaper':
//          return true
//       case 'poster':
//          return true
//       case 'other':
//          return true
//       case 'presentation':
//          return true
//       case 'dissertation':
//          return true
//       default:
//          return false
//    }
// }

// export const makeVoRCost = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeTextInput({
//    label: d.forms.moderate.vorCost.fieldTitle,
//    placeholder: d.forms.moderate.vorCost.default,
//    id: randomizeID('vorCost'),
//    required: false,
//    short: true,
//    value: context.workData.vorCost,
// });

// export const makeModeratePostedContentExtrasContent = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', children: [
//    // Posted Content Type
//    makePostedContentType(context)(d),
//    // VOR Cost
//    makeVoRCost(context)(d),
//    // OCTO Work Type
//    makeOctoWorkTypeFormGroup(context)(d),
// ]})

// export const makeModerateExtrasContent = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', children: [
//    // OCTO Work Type
//    makeOctoWorkTypeFormGroup(context)(d),
// ]})

// export const makeModerationExtras = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => {
//    if (context.workData.octoWorkType) {
//       if ( determineIfPostedContent(context.workData.octoWorkType) ) {
//          // The work is posted content
//          return makeCollapsibleForm({
//             title: d.forms.moderate.moderateExtras.formTitle,
//             miniDescription: make.element({ tag: 'p', children: [
//                make.text(d.forms.moderate.moderateExtras.miniDescription)
//             ]}),
//             content: makeModeratePostedContentExtrasContent(context)(d),
//             id: randomizeID('moderateExtrasPostedContent'),
//             collapsible: 'maxModeration',
//          });
//       }
//       else {
//          // The work is not posted content
//          return makeCollapsibleForm({
//             title: d.forms.moderate.moderateExtras.formTitle,
//             miniDescription: make.element({ tag: 'p', children: [
//                make.text(d.forms.moderate.moderateExtras.miniDescription)
//             ]}),
//             content: makeModerateExtrasContent(context)(d),
//             id: randomizeID('moderateExtrasPostedContent'),
//             collapsible: 'maxModeration',
//          });
//       }
//    }
//    else {
//       throw new Error('Cannot moderate a work without an intial octoWorkType set.')
//    }
// }

/**
 * Moderate a Preprint
 */
// export const makeModerateFormPreprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.preprint)('moderatePreprint')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Moderation extras
//    makeModerationExtras(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate an Open Access paper
 */
// export const makeModerateFormOA = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.openAccess)('moderateOA')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Moderation extras
//    makeModerationExtras(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate a Postprint
 */
// export const makeModerateFormPostprint = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.postprint)('moderatePostprint')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information minus Peer Review (since it's a postprint, it's been peer-reviewed already)
//    makeBasicInformationPostedContentNoPeerReview(context)(d),
//    // Moderation extras
//    makeModerationExtras(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePostprint(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate an Un-copyrighted paper
 */
// export const makeModerateFormUncopyrighted = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.unCopyrighted)('moderateUncopyrighted')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Moderation extras
//    makeModerationExtras(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicenseCopyrighted(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate an Unpublished Report
 */
// export const makeModerateFormReportUnpublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.unpublishedReport)('moderateReportUnpublished')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Moderation extras
//    makeModerationExtras(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate a Thesis / Dissertation
 */
// export const makeModerateFormThesis = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.thesis)('moderateThesis')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Moderation extras
//    makeModerationExtras(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate a Poster, Presentation, or Supplementary Information
 */
// export const makeModerateFormPoster = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.posterPresentationSI)('moderatePoster')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Moderation extras
//    makeModerationExtras(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Letter edit
 */
// export const makeModerateFormLetter = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.letter)('moderateLetter')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Moderation extras
//    makeModerationExtras(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate a Working Paper
 */
// export const makeModerateFormWorkingPaper = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.workingPaper)('moderateWorkingPaper')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information
//    makeBasicInformationPostedContent(context)(d),
//    // Moderation extras
//    makeModerationExtras(context)(d),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate a Published Report
 */
// export const makeModerateFormReportPublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.publishedReport)('moderateReportPublished')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information minus Publisher's DOI and Peer-Review question
//    makeBasicInformationNoDOIorPeerReview(context)(d),
//    // Publisher
//    // makePublisher(context)(d.forms.submit),
//    // Supporting Institutions
//    // makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    // makeCitations(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Assign DOI
//    customizeDOI(context)(d.forms.submit),
//    // Moderation extras 
//    makeModerationExtras(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate a Published Conference Paper
 */
// export const makeModerateFormConferencePaperPublished = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.publishedConference)('moderateConferencePaperPublished')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information minus Publisher's DOI and Peer-Review question
//    makeBasicInformationNoDOIorPeerReview(context)(d),
//    // Conference Paper Info
//    makeConferencePaper(context)(d.forms.submit),
//    // Conference Event Info
//    makeConferenceEvent(context)(d.forms.submit),
//    // Supporting Institutions
//    makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    makeCitations(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Assign DOI
//    customizeDOI(context)(d.forms.submit),
//    // Moderation extras 
//    makeModerationExtras(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate a Peer Review
 */
// export const makeModerateFormPeerReview = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.peerReview)('moderatePeerReview')([
//    // Preview and Download File
//    makePreviewPDFForm(context)(d),
//    // Basic Information minus Publisher's DOI and Peer-Review question
//    makeBasicInformationNoDOIorPeerReview(context)(d),
//    // Peer Review Information
//    makePeerReviewInformation(context)(d),
//    // Supporting Institutions
//    makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Hide relationships, since this will be handled automatically
//    // License
//    makeLicensePreprint(context)(d),
//    // Moderation extras 
//    makeModerationExtras(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate a Database, a collection for Datasets
 */
// export const makeModerateFormDatabase = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.database)('moderateDatabase')([
//    // Basic Information for a Database
//    makeBasicInformationDatabase(context)(d.forms.submit),
//    // Publisher
//    makePublisherDatabase(context)(d.forms.submit),
//    // Supporting Institutions
//    makeInstitutions(context)(d.forms.submit)(undefined),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // Citations
//    makeCitations(context)(d.forms.submit)(undefined),
//    // Relationships
//    makeRelationships(context)(d)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Moderation extras 
//    makeModerationExtras(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);

/**
 * Moderate a Dataset, a child of a Database
 */
// export const makeModerateFormDataset = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => makeFormPage(d.forms.moderate.titles.dataset)('moderateDataset')([
//    // Database DOI
//    makeDatabaseDOIForm(context)(d.forms.submit),
//    // Download the dataset
//    makeDatasetDownloadForm(context)(d),
//    // Basic Information for a Dataset
//    makeBasicInformationDataset(context)(d.forms.submit),
//    // Contributors
//    makeContributors(context)(d.forms)(undefined),
//    // License
//    makeLicensePreprint(context)(d),
//    // Moderation extras 
//    makeModerationExtras(context)(d),
//    // Message to Submitter
//    makeMessageToSubmitter(context)(d.forms.moderate),
//    // Form end
//    makeFormEndModeration(d),
// ]);
