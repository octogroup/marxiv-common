
import { HastRoot } from '@octogroup/hast-typescript';

/**
 * Local imports
 */
import { MarXivData, MarXivDataWithoutTranslatables, ActiveWorkData, PageMetadata, WorkMetadata, MarXivDataWithoutSitemap, companionFns, GenericAuthenticatedUserData } from './context';
import { makeEnglishTranslatables, englishPageMetadata } from './translatablesEnglish';
import { Translatables, MainMenuText, FormMessageText, FormText, CommonReplacements, SubmissionFormText } from './translatables';
import { makeMarXivSitemap, makeMinimalMarXivSitemap, MarXivSitemap, staticAssets } from './sitemap';
import { makePageFromSitemap, buildHastRoot, makePage } from './buildPage';
import { supplyTranslatables, TranslatableMetadata, SupportedLanguage } from './languages';
import { SubmenuType, UserMenuType, makeMobileMainMenu, } from './menus';
import { makeJobOptions, makeCountrySelectOptions, translateJobOption, translateCountrySelectOption, JobOptionValues } from './userForms';
import { makeBadgeTypeOptions, makeOctoWorkTypeOptions, makePostedContentTypeOptions, } from './moderateForms';
import { PostedContentType, NameStyle, ContribRole, ContribSequence, WorkRelationship, PeerReviewConducted, PeerReviewType, PeerReviewRecommendation, LicenseOption, SelectOption, makeLocalPath } from './common';
import { makeLanguageOptions, makeLicenseOptions, makeRelationshipOptions, makeNameStyleOptions, makeContributorRoleOptions, } from './submitForms';
import { translateModerationStatus } from './dashboards';
import { WorkTypeSubmit, WorkTypeBadge, WorkData, WorkType } from './works';
import { Languages, LANGUAGES_LIST } from './langList';

/**
 * Import our testing data
 */
import { dummyInstitutions, dummyPeerReview, dummyRefs, dummyRelatedMaterials, dummyPaginationDataArray, dummyUserData, dummyWork, dummyWorksForListing, testingActiveWork, } from './testingData';

/**
 * Export for the Front- and Back-ends
 */
export { MarXivData, MarXivDataWithoutTranslatables, ActiveWorkData, PageMetadata, WorkMetadata, MarXivDataWithoutSitemap, makeEnglishTranslatables, englishPageMetadata, Translatables, MainMenuText, makeMarXivSitemap, makeMinimalMarXivSitemap, MarXivSitemap, makePageFromSitemap, buildHastRoot, supplyTranslatables, TranslatableMetadata, SupportedLanguage, SubmenuType, UserMenuType, makeMobileMainMenu, FormMessageText, CommonReplacements, FormText, makeJobOptions, makeCountrySelectOptions, makeBadgeTypeOptions, makeOctoWorkTypeOptions, makePostedContentTypeOptions, makeLanguageOptions, makeLicenseOptions, makeRelationshipOptions, makeNameStyleOptions, makeContributorRoleOptions, SubmissionFormText, PostedContentType, NameStyle, ContribRole, ContribSequence, WorkRelationship, PeerReviewConducted, PeerReviewType, PeerReviewRecommendation, companionFns, staticAssets, makePage, LicenseOption, SelectOption, translateModerationStatus, WorkTypeSubmit, WorkTypeBadge, WorkData, WorkType, translateJobOption, translateCountrySelectOption, GenericAuthenticatedUserData, makeLocalPath, dummyUserData, JobOptionValues, Languages, LANGUAGES_LIST }; 

/**
 * Build a minimal sitemap to generate the ChangeURLCB
 */
export const englishMarXivMinimalSitemap = makeMinimalMarXivSitemap('en')(dummyWorksForListing)(englishPageMetadata);

/**
 * Get the requested path from the Window
 */
// const requestedPath = window.location.pathname;

/**
 * EVERYTHING to build a page MUST be in the context at this point, or we cannot build the Sitemap.
 */
export const testingContextMinimal = {
   requestedPath: '/submit/preprint',
   requestedLanguage: 'en' as SupportedLanguage,
   // Menus
   submenuToDisplay: 'none' as SubmenuType,
   userMenuToDisplay: 'anonymous' as UserMenuType,
   // Pagination
   paginationData: {
      currentPage: 1,
      hasNextPage: true,
      hasPreviousPage: false
   },
   paginationCBs: {
      nextPageCB: (event: Event) => {},
      previousPageCB: (event: Event) => {},
   },
   gtag: {},
   // User data
   userData: dummyUserData,
   // Active Work Data
   workData: testingActiveWork,
   // Work listing
   works: dummyWorksForListing,
   // The user's works
   userWorks: [],
}

/**
 * Testing context without translatables
 */
export const testingContextWithoutTranslatables: MarXivDataWithoutSitemap = {
   minimalSitemap: englishMarXivMinimalSitemap,
   assets: staticAssets,
   currentPage: {
      title: '',
      localURL: '',
      depth: 'primary'
   },
   ...companionFns,
   ...testingContextMinimal,
};

/**
 * Make the English-language translatables
 */
export const translatablesEnglish: TranslatableMetadata = supplyTranslatables(testingContextWithoutTranslatables)('en');

/**
 * Make the English-language sitemap
 */
export const sitemapEnglish = makeMarXivSitemap(testingContextWithoutTranslatables)(translatablesEnglish.translatables);

/**
 * Assemble the Context
 */
export const testingContext: MarXivData = {
   ... testingContextWithoutTranslatables,
   ...translatablesEnglish,
   sitemap: sitemapEnglish,
};

/**
 * Make the HTML element 
 */
export const generateHTMLPage = (context: MarXivDataWithoutTranslatables) => (translatables: TranslatableMetadata) => (reqPath: string): HastRoot => buildHastRoot( makePageFromSitemap(reqPath)(context)(translatables.translatables) )(translatables.code);
