export const LANGUAGES_LIST = {
   aa: {
      name: 'Afar',
      nativeName: 'Afaraf',
   },
   ab: {
      name: 'Abkhaz',
      nativeName: 'Ð°Ò§ÑÑƒÐ° Ð±Ñ‹Ð·ÑˆÓ™Ð°',
   },
   ae: {
      name: 'Avestan',
      nativeName: 'avesta',
   },
   af: {
      name: 'Afrikaans',
      nativeName: 'Afrikaans',
   },
   ak: {
      name: 'Akan',
      nativeName: 'Akan',
   },
   am: {
      name: 'Amharic',
      nativeName: 'áŠ áˆ›áˆ­áŠ›',
   },
   an: {
      name: 'Aragonese',
      nativeName: 'aragonÃ©s',
   },
   ar: {
      name: 'Arabic',
      nativeName: 'Ø§Ù„Ù„ØºØ© Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©',
   },
   as: {
      name: 'Assamese',
      nativeName: 'à¦…à¦¸à¦®à§€à¦¯à¦¼à¦¾',
   },
   av: {
      name: 'Avaric',
      nativeName: 'Ð°Ð²Ð°Ñ€ Ð¼Ð°Ñ†Ó€',
   },
   ay: {
      name: 'Aymara',
      nativeName: 'aymar aru',
   },
   az: {
      name: 'Azerbaijani',
      nativeName: 'azÉ™rbaycan dili',
   },
   ba: {
      name: 'Bashkir',
      nativeName: 'Ð±Ð°ÑˆÒ¡Ð¾Ñ€Ñ‚ Ñ‚ÐµÐ»Ðµ',
   },
   be: {
      name: 'Belarusian',
      nativeName: 'Ð±ÐµÐ»Ð°Ñ€ÑƒÑÐºÐ°Ñ Ð¼Ð¾Ð²Ð°',
   },
   bg: {
      name: 'Bulgarian',
      nativeName: 'Ð±ÑŠÐ»Ð³Ð°Ñ€ÑÐºÐ¸ ÐµÐ·Ð¸Ðº',
   },
   bh: {
      name: 'Bihari',
      nativeName: 'à¤­à¥‹à¤œà¤ªà¥à¤°à¥€',
   },
   bi: {
      name: 'Bislama',
      nativeName: 'Bislama',
   },
   bm: {
      name: 'Bambara',
      nativeName: 'bamanankan',
   },
   bn: {
      name: 'Bengali',
      nativeName: 'à¦¬à¦¾à¦‚à¦²à¦¾',
   },
   bo: {
      name: 'Tibetan Standard',
      nativeName: 'à½–à½¼à½‘à¼‹à½¡à½²à½‚',
   },
   br: {
      name: 'Breton',
      nativeName: 'brezhoneg',
   },
   bs: {
      name: 'Bosnian',
      nativeName: 'bosanski jezik',
   },
   ca: {
      name: 'Catalan',
      nativeName: 'catalÃ ',
   },
   ce: {
      name: 'Chechen',
      nativeName: 'Ð½Ð¾Ñ…Ñ‡Ð¸Ð¹Ð½ Ð¼Ð¾Ñ‚Ñ‚',
   },
   ch: {
      name: 'Chamorro',
      nativeName: 'Chamoru',
   },
   co: {
      name: 'Corsican',
      nativeName: 'corsu',
   },
   cr: {
      name: 'Cree',
      nativeName: 'á“€á¦áƒá”­ááá£',
   },
   cs: {
      name: 'Czech',
      nativeName: 'ÄeÅ¡tina',
   },
   cu: {
      name: 'Old Church Slavonic',
      nativeName: 'Ñ©Ð·Ñ‹ÐºÑŠ ÑÐ»Ð¾Ð²Ñ£Ð½ÑŒÑÐºÑŠ',
   },
   cv: {
      name: 'Chuvash',
      nativeName: 'Ñ‡Ó‘Ð²Ð°Ñˆ Ñ‡Ó—Ð»Ñ…Ð¸',
   },
   cy: {
      name: 'Welsh',
      nativeName: 'Cymraeg',
   },
   da: {
      name: 'Danish',
      nativeName: 'dansk',
   },
   de: {
      name: 'German',
      nativeName: 'Deutsch',
   },
   dv: {
      name: 'Divehi',
      nativeName: 'Dhivehi',
   },
   dz: {
      name: 'Dzongkha',
      nativeName: 'à½¢à¾«à½¼à½„à¼‹à½',
   },
   ee: {
      name: 'Ewe',
      nativeName: 'EÊ‹egbe',
   },
   el: {
      name: 'Greek',
      nativeName: 'Î•Î»Î»Î·Î½Î¹ÎºÎ¬',
   },
   en: {
      name: 'English',
      nativeName: 'English',
   },
   eo: {
      name: 'Esperanto',
      nativeName: 'Esperanto',
   },
   es: {
      name: 'Spanish',
      nativeName: 'EspaÃ±ol',
   },
   et: {
      name: 'Estonian',
      nativeName: 'eesti',
   },
   eu: {
      name: 'Basque',
      nativeName: 'euskara',
   },
   fa: {
      name: 'Persian',
      nativeName: 'ÙØ§Ø±Ø³ÛŒ',
   },
   ff: {
      name: 'Fula',
      nativeName: 'Fulfulde',
   },
   fi: {
      name: 'Finnish',
      nativeName: 'suomi',
   },
   fj: {
      name: 'Fijian',
      nativeName: 'Vakaviti',
   },
   fo: {
      name: 'Faroese',
      nativeName: 'fÃ¸royskt',
   },
   fr: {
      name: 'French',
      nativeName: 'FranÃ§ais',
   },
   fy: {
      name: 'Western Frisian',
      nativeName: 'Frysk',
   },
   ga: {
      name: 'Irish',
      nativeName: 'Gaeilge',
   },
   gd: {
      name: 'Scottish Gaelic',
      nativeName: 'GÃ idhlig',
   },
   gl: {
      name: 'Galician',
      nativeName: 'galego',
   },
   gn: {
      name: 'GuaranÃ­',
      nativeName: "AvaÃ±e'áº½",
   },
   gu: {
      name: 'Gujarati',
      nativeName: 'àª—à«àªœàª°àª¾àª¤à«€',
   },
   gv: {
      name: 'Manx',
      nativeName: 'Gaelg',
   },
   ha: {
      name: 'Hausa',
      nativeName: 'Ù‡ÙŽÙˆÙØ³ÙŽ',
   },
   he: {
      name: 'Hebrew',
      nativeName: '×¢×‘×¨×™×ª',
   },
   hi: {
      name: 'Hindi',
      nativeName: 'à¤¹à¤¿à¤¨à¥à¤¦à¥€',
   },
   ho: {
      name: 'Hiri Motu',
      nativeName: 'Hiri Motu',
   },
   hr: {
      name: 'Croatian',
      nativeName: 'hrvatski jezik',
   },
   ht: {
      name: 'Haitian',
      nativeName: 'KreyÃ²l ayisyen',
   },
   hu: {
      name: 'Hungarian',
      nativeName: 'magyar',
   },
   hy: {
      name: 'Armenian',
      nativeName: 'Õ€Õ¡ÕµÕ¥Ö€Õ¥Õ¶',
   },
   hz: {
      name: 'Herero',
      nativeName: 'Otjiherero',
   },
   ia: {
      name: 'Interlingua',
      nativeName: 'Interlingua',
   },
   id: {
      name: 'Indonesian',
      nativeName: 'Indonesian',
   },
   ie: {
      name: 'Interlingue',
      nativeName: 'Interlingue',
   },
   ig: {
      name: 'Igbo',
      nativeName: 'Asá»¥sá»¥ Igbo',
   },
   ii: {
      name: 'Nuosu',
      nativeName: 'ê†ˆêŒ ê’¿ Nuosuhxop',
   },
   ik: {
      name: 'Inupiaq',
      nativeName: 'IÃ±upiaq',
   },
   io: {
      name: 'Ido',
      nativeName: 'Ido',
   },
   is: {
      name: 'Icelandic',
      nativeName: 'Ãslenska',
   },
   it: {
      name: 'Italian',
      nativeName: 'Italiano',
   },
   iu: {
      name: 'Inuktitut',
      nativeName: 'áƒá“„á’ƒá‘Žá‘á‘¦',
   },
   ja: {
      name: 'Japanese',
      nativeName: 'æ—¥æœ¬èªž',
   },
   jv: {
      name: 'Javanese',
      nativeName: 'basa Jawa',
   },
   ka: {
      name: 'Georgian',
      nativeName: 'áƒ¥áƒáƒ áƒ—áƒ£áƒšáƒ˜',
   },
   kg: {
      name: 'Kongo',
      nativeName: 'Kikongo',
   },
   ki: {
      name: 'Kikuyu',
      nativeName: 'GÄ©kÅ©yÅ©',
   },
   kj: {
      name: 'Kwanyama',
      nativeName: 'Kuanyama',
   },
   kk: {
      name: 'Kazakh',
      nativeName: 'Ò›Ð°Ð·Ð°Ò› Ñ‚Ñ–Ð»Ñ–',
   },
   kl: {
      name: 'Kalaallisut',
      nativeName: 'kalaallisut',
   },
   km: {
      name: 'Khmer',
      nativeName: 'ážáŸáž˜ážšáž—áž¶ážŸáž¶',
   },
   kn: {
      name: 'Kannada',
      nativeName: 'à²•à²¨à³à²¨à²¡',
   },
   ko: {
      name: 'Korean',
      nativeName: 'í•œêµ­ì–´',
   },
   kr: {
      name: 'Kanuri',
      nativeName: 'Kanuri',
   },
   ks: {
      name: 'Kashmiri',
      nativeName: 'à¤•à¤¶à¥à¤®à¥€à¤°à¥€',
   },
   ku: {
      name: 'Kurdish',
      nativeName: 'KurdÃ®',
   },
   kv: {
      name: 'Komi',
      nativeName: 'ÐºÐ¾Ð¼Ð¸ ÐºÑ‹Ð²',
   },
   kw: {
      name: 'Cornish',
      nativeName: 'Kernewek',
   },
   ky: {
      name: 'Kyrgyz',
      nativeName: 'ÐšÑ‹Ñ€Ð³Ñ‹Ð·Ñ‡Ð°',
   },
   la: {
      name: 'Latin',
      nativeName: 'latine',
   },
   lb: {
      name: 'Luxembourgish',
      nativeName: 'LÃ«tzebuergesch',
   },
   lg: {
      name: 'Ganda',
      nativeName: 'Luganda',
   },
   li: {
      name: 'Limburgish',
      nativeName: 'Limburgs',
   },
   ln: {
      name: 'Lingala',
      nativeName: 'LingÃ¡la',
   },
   lo: {
      name: 'Lao',
      nativeName: 'àºžàº²àºªàº²',
   },
   lt: {
      name: 'Lithuanian',
      nativeName: 'lietuviÅ³ kalba',
   },
   lu: {
      name: 'Luba-Katanga',
      nativeName: 'Tshiluba',
   },
   lv: {
      name: 'Latvian',
      nativeName: 'latvieÅ¡u valoda',
   },
   mg: {
      name: 'Malagasy',
      nativeName: 'fiteny malagasy',
   },
   mh: {
      name: 'Marshallese',
      nativeName: 'Kajin MÌ§ajeÄ¼',
   },
   mi: {
      name: 'MÄori',
      nativeName: 'te reo MÄori',
   },
   mk: {
      name: 'Macedonian',
      nativeName: 'Ð¼Ð°ÐºÐµÐ´Ð¾Ð½ÑÐºÐ¸ Ñ˜Ð°Ð·Ð¸Ðº',
   },
   ml: {
      name: 'Malayalam',
      nativeName: 'à´®à´²à´¯à´¾à´³à´‚',
   },
   mn: {
      name: 'Mongolian',
      nativeName: 'ÐœÐ¾Ð½Ð³Ð¾Ð» Ñ…ÑÐ»',
   },
   mr: {
      name: 'Marathi',
      nativeName: 'à¤®à¤°à¤¾à¤ à¥€',
   },
   ms: {
      name: 'Malay',
      nativeName: 'Ù‡Ø§Ø³ Ù…Ù„Ø§ÙŠÙˆâ€Ž',
   },
   mt: {
      name: 'Maltese',
      nativeName: 'Malti',
   },
   my: {
      name: 'Burmese',
      nativeName: 'á€—á€™á€¬á€…á€¬',
   },
   na: {
      name: 'Nauru',
      nativeName: 'EkakairÅ© Naoero',
   },
   nb: {
      name: 'Norwegian BokmÃ¥l',
      nativeName: 'Norsk bokmÃ¥l',
   },
   nd: {
      name: 'Northern Ndebele',
      nativeName: 'isiNdebele',
   },
   ne: {
      name: 'Nepali',
      nativeName: 'à¤¨à¥‡à¤ªà¤¾à¤²à¥€',
   },
   ng: {
      name: 'Ndonga',
      nativeName: 'Owambo',
   },
   nl: {
      name: 'Dutch',
      nativeName: 'Nederlands',
   },
   nn: {
      name: 'Norwegian Nynorsk',
      nativeName: 'Norsk nynorsk',
   },
   no: {
      name: 'Norwegian',
      nativeName: 'Norsk',
   },
   nr: {
      name: 'Southern Ndebele',
      nativeName: 'isiNdebele',
   },
   nv: {
      name: 'Navajo',
      nativeName: 'DinÃ© bizaad',
   },
   ny: {
      name: 'Chichewa',
      nativeName: 'chiCheÅµa',
   },
   oc: {
      name: 'Occitan',
      nativeName: 'occitan',
   },
   oj: {
      name: 'Ojibwe',
      nativeName: 'áŠá“‚á”‘á“ˆá¯á’§áŽá“',
   },
   om: {
      name: 'Oromo',
      nativeName: 'Afaan Oromoo',
   },
   or: {
      name: 'Oriya',
      nativeName: 'à¬“à¬¡à¬¼à¬¿à¬†',
   },
   os: {
      name: 'Ossetian',
      nativeName: 'Ð¸Ñ€Ð¾Ð½ Ã¦Ð²Ð·Ð°Ð³',
   },
   pa: {
      name: 'Panjabi',
      nativeName: 'à¨ªà©°à¨œà¨¾à¨¬à©€',
   },
   pi: {
      name: 'PÄli',
      nativeName: 'à¤ªà¤¾à¤´à¤¿',
   },
   pl: {
      name: 'Polish',
      nativeName: 'jÄ™zyk polski',
   },
   ps: {
      name: 'Pashto',
      nativeName: 'Ù¾ÚšØªÙˆ',
   },
   pt: {
      name: 'Portuguese',
      nativeName: 'PortuguÃªs',
   },
   qu: {
      name: 'Quechua',
      nativeName: 'Runa Simi',
   },
   rm: {
      name: 'Romansh',
      nativeName: 'rumantsch grischun',
   },
   rn: {
      name: 'Kirundi',
      nativeName: 'Ikirundi',
   },
   ro: {
      name: 'Romanian',
      nativeName: 'RomÃ¢nÄƒ',
   },
   ru: {
      name: 'Russian',
      nativeName: 'Ð ÑƒÑÑÐºÐ¸Ð¹',
   },
   rw: {
      name: 'Kinyarwanda',
      nativeName: 'Ikinyarwanda',
   },
   sa: {
      name: 'Sanskrit',
      nativeName: 'à¤¸à¤‚à¤¸à¥à¤•à¥ƒà¤¤à¤®à¥',
   },
   sc: {
      name: 'Sardinian',
      nativeName: 'sardu',
   },
   sd: {
      name: 'Sindhi',
      nativeName: 'à¤¸à¤¿à¤¨à¥à¤§à¥€',
   },
   se: {
      name: 'Northern Sami',
      nativeName: 'DavvisÃ¡megiella',
   },
   sg: {
      name: 'Sango',
      nativeName: 'yÃ¢ngÃ¢ tÃ® sÃ¤ngÃ¶',
   },
   si: {
      name: 'Sinhala',
      nativeName: 'à·ƒà·’à¶‚à·„à¶½',
   },
   sk: {
      name: 'Slovak',
      nativeName: 'slovenÄina',
   },
   sl: {
      name: 'Slovene',
      nativeName: 'slovenski jezik',
   },
   sm: {
      name: 'Samoan',
      nativeName: "gagana fa'a Samoa",
   },
   sn: {
      name: 'Shona',
      nativeName: 'chiShona',
   },
   so: {
      name: 'Somali',
      nativeName: 'Soomaaliga',
   },
   sq: {
      name: 'Albanian',
      nativeName: 'Shqip',
   },
   sr: {
      name: 'Serbian',
      nativeName: 'ÑÑ€Ð¿ÑÐºÐ¸ Ñ˜ÐµÐ·Ð¸Ðº',
   },
   ss: {
      name: 'Swati',
      nativeName: 'SiSwati',
   },
   st: {
      name: 'Southern Sotho',
      nativeName: 'Sesotho',
   },
   su: {
      name: 'Sundanese',
      nativeName: 'Basa Sunda',
   },
   sv: {
      name: 'Swedish',
      nativeName: 'svenska',
   },
   sw: {
      name: 'Swahili',
      nativeName: 'Kiswahili',
   },
   ta: {
      name: 'Tamil',
      nativeName: 'à®¤à®®à®¿à®´à¯',
   },
   te: {
      name: 'Telugu',
      nativeName: 'à°¤à±†à°²à±à°—à±',
   },
   tg: {
      name: 'Tajik',
      nativeName: 'Ñ‚Ð¾Ò·Ð¸ÐºÓ£',
   },
   th: {
      name: 'Thai',
      nativeName: 'à¹„à¸—à¸¢',
   },
   ti: {
      name: 'Tigrinya',
      nativeName: 'á‰µáŒáˆ­áŠ›',
   },
   tk: {
      name: 'Turkmen',
      nativeName: 'TÃ¼rkmen',
   },
   tl: {
      name: 'Tagalog',
      nativeName: 'Wikang Tagalog',
   },
   tn: {
      name: 'Tswana',
      nativeName: 'Setswana',
   },
   to: {
      name: 'Tonga',
      nativeName: 'faka Tonga',
   },
   tr: {
      name: 'Turkish',
      nativeName: 'TÃ¼rkÃ§e',
   },
   ts: {
      name: 'Tsonga',
      nativeName: 'Xitsonga',
   },
   tt: {
      name: 'Tatar',
      nativeName: 'Ñ‚Ð°Ñ‚Ð°Ñ€ Ñ‚ÐµÐ»Ðµ',
   },
   tw: {
      name: 'Twi',
      nativeName: 'Twi',
   },
   ty: {
      name: 'Tahitian',
      nativeName: 'Reo Tahiti',
   },
   ug: {
      name: 'Uyghur',
      nativeName: 'Ø¦Û‡ÙŠØºÛ‡Ø±Ú†Û•â€Ž',
   },
   uk: {
      name: 'Ukrainian',
      nativeName: 'Ð£ÐºÑ€Ð°Ñ—Ð½ÑÑŒÐºÐ°',
   },
   ur: {
      name: 'Urdu',
      nativeName: 'Ø§Ø±Ø¯Ùˆ',
   },
   uz: {
      name: 'Uzbek',
      nativeName: 'ÐŽÐ·Ð±ÐµÐº',
   },
   ve: {
      name: 'Venda',
      nativeName: 'Tshivená¸“a',
   },
   vi: {
      name: 'Vietnamese',
      nativeName: 'Tiáº¿ng Viá»‡t',
   },
   vo: {
      name: 'VolapÃ¼k',
      nativeName: 'VolapÃ¼k',
   },
   wa: {
      name: 'Walloon',
      nativeName: 'walon',
   },
   wo: {
      name: 'Wolof',
      nativeName: 'Wollof',
   },
   xh: {
      name: 'Xhosa',
      nativeName: 'isiXhosa',
   },
   yi: {
      name: 'Yiddish',
      nativeName: '×™×™Ö´×“×™×©',
   },
   yo: {
      name: 'Yoruba',
      nativeName: 'YorÃ¹bÃ¡',
   },
   za: {
      name: 'Zhuang',
      nativeName: 'SaÉ¯ cueÅ‹Æ…',
   },
   zh: {
      name: 'Chinese',
      nativeName: 'ä¸­æ–‡',
   },
   zu: {
      name: 'Zulu',
      nativeName: 'isiZulu',
   },
};

export type Languages = 'aa' | 'ab' | 'ae' | 'af' | 'ak' | 'am' | 'an' | 'ar' | 'as' | 'av' | 'ay' | 'az' | 'ba' | 'be' | 'bg' | 'bh' | 'bi' | 'bm' | 'bn' | 'bo' | 'br' | 'bs' | 'ca' | 'ce' | 'ch' | 'co' | 'cr' | 'cs' | 'cu' | 'cv' | 'cy' | 'da' | 'de' | 'dv' | 'dz' | 'ee' | 'el' | 'en' | 'eo' | 'es' | 'et' | 'eu' | 'fa' | 'ff' | 'fi' | 'fj' | 'fo' | 'fr' | 'fy' | 'ga' | 'gd' | 'gl' | 'gn' | 'gu' | 'gv' | 'ha' | 'he' | 'hi' | 'ho' | 'hr' | 'ht' | 'hu' | 'hy' | 'hz' | 'ia' | 'id' | 'ie' | 'ig' | 'ii' | 'ik' | 'io' | 'is' | 'it' | 'iu' | 'ja' | 'jv' | 'ka' | 'kg' | 'ki' | 'kj' | 'kk' | 'kl' | 'km' | 'kn' | 'ko' | 'kr' | 'ks' | 'ku' | 'kv' | 'kw' | 'ky' | 'la' | 'lb' | 'lg' | 'li' | 'ln' | 'lo' | 'lt' | 'lu' | 'lv' | 'mg' | 'mh' | 'mi' | 'mk' | 'ml' | 'mn' | 'mr' | 'ms' | 'mt' | 'my' | 'na' | 'nb' | 'nd' | 'ne' | 'ng' | 'nl' | 'nn' | 'no' | 'nr' | 'nv' | 'ny' | 'oc' | 'oj' | 'om' | 'or' | 'os' | 'pa' | 'pi' | 'pl' | 'ps' | 'pt' | 'qu' | 'rm' | 'rn' | 'ro' | 'ru' | 'rw' | 'sa' | 'sc' | 'sd' | 'se' | 'sg' | 'si' | 'sk' | 'sl' | 'sm' | 'sn' | 'so' | 'sq' | 'sr' | 'ss' | 'st' | 'su' | 'sv' | 'sw' | 'ta' | 'te' | 'tg' | 'th' | 'ti' | 'tk' | 'tl' | 'tn' | 'to' | 'tr' | 'ts' | 'tt' | 'tw' | 'ty' | 'ug' | 'uk' | 'ur' | 'uz' | 've' | 'vi' | 'vo' | 'wa' | 'wo' | 'xh' | 'yi' | 'yo' | 'za' | 'zh' | 'zu';