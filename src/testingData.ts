import { ActiveWorkData, BasicUserData } from './context';
import { WorkData, } from './works';
import { PaginatorData, makePaginatorDataArray } from './workListing';
import { Institution, Relationship } from './submitForms';

/**
 * Testing data
 */
export const dummyInstitutions: Institution[] = [
   {
      name: "University of Washington",
      acronym: "UW",
      location: "Seattle, WA",
      department: "School of Marine and Environmental Affairs",
      order: 1,
      displayText: 'School of Marine and Environmental Affairs'
   },
   {
      name: "University of Colorado",
      department: "Department of Biology",
      order: 2,
      displayText: "Department of Biology",
   },
   {
      name: "University of Washington",
      acronym: "UW",
      order: 3,
      displayText: "UW",
   },
   {
      name: "College of Kittens",
      location: "Spokane",
      order: 4,
      displayText: 'College of Kittens',
   },
   {
      name: "Open Communications for The Ocean",
      acronym: "OCTO",
      location: "Like, Everywhere",
      order: 5,
      displayText: 'OCTO'
   },
]

export const dummyRelatedMaterials: Relationship[] = [
   {
      doi: '10.4/wer822/dgdg',
      type: 'basedOn',
      order: 1,
      displayText: '10.4/wer822/dgdg'
   },
   {
      doi: '10.4./wer822',
      type: 'manuscriptOf',
      order: 2,
      displayText: '10.4./wer82'
   },
]

export const dummyPeerReview = {
   conducted: 'pre-publication',
   doi: '10.12/ssfsd.43',
   type: 'editorReport',
   recommendation: 'accept',
   competingInterests: 'None',
};
export const dummyRefs: string[] = [
   "Wehner, Eli. 2019. A treatise on why Juniper should not play near Elis. The Journal of Somewhere. Volume 8.",
]

export const dummyWork: WorkData = {
   title: "Paper Titles Can Be Very Long: But why must that be the case? Let's find out!",
   language: 'en',
   badgeType: 'preprint',
   octoWorkType: 'preprint',
   moderationStatus: 'approved',
   originalPublicationDate: {
      year: '2019',
      month: '9',
      day: '13'
   },
   license: 'ccBy',
   description: 'This is a test work',
   submittedBy: 'Nick Wehner',
   workID: '1',
   contributors: [
      {
         type: 'std',
         givenName: 'Nicholas',
         surname: 'Wehner',
         nameStyle: 'western',
         role: 'author',
         sequence: 'first',
         order: 1,
         displayText: 'Nicholas Wehner',
      },
   ],
   peerReviewed: true,
   downloads: 5,
   downloadLink: '/pdf/test.pdf',
   fileType: 'PDF', // PDF or ZIP, most likely
   fileSize: '5.2 MB', // 8 MB, 1 GB, etc.
   // embeddedLink: string,
   institutions: dummyInstitutions,
   relationships: dummyRelatedMaterials,
   references: dummyRefs,
   // withdrawnDate: '26 July 2019',
   // withdrawnReason: 'This paper was copyrighted by Elsevier',
   // parentDatabaseDOI: '10.12/ssfsd.43',
   // conducted: 'prePublication',
   // doiOfReviewedWork: '10.12/ssfsd.43',
   // typeOfReview: 'editorReport',
   // recommendation: 'accept',
   // competingInterests: 'None',
}

export const dummyWorksForListing = [ dummyWork ];

export const dummyPaginationDataArray: PaginatorData[] = makePaginatorDataArray('en')({
   works: dummyWorksForListing,
   itemsPerPage: 10
});

export const dummyUserData: BasicUserData = {
   userType: 'user',
   emailVerified: true,
   userName: 'nwehner',
   emailAddress: 'nick@octogroup.org',
   givenName: 'Nick',
   surname: 'Wehner',
   newsletter: true,
   organization: 'OCTO',
   country: 'unitedstatesofamerica',
   jobFocus: ['consultant', 'conservationist'],
}


/**
 * Testing Active Work Data
 */
export const testingActiveWork: ActiveWorkData = {
   octoWorkType: 'reportPublished',
   badgeType: 'report',
   moderationStatus: 'pending',
   moderationMessage: undefined,
   postedContentType: undefined,
   title: undefined,
   language: undefined,
   peerReviewed: undefined,
   subtitle: undefined,
   publisherDOI: undefined,
   originalPublicationDate: undefined,
   downloadLink: '/pdf/test.pdf',
   fileType: 'TAR',
   workID: '1'
}
