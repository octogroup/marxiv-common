/**
 * This file contains all the English-language replacements to generate content.
 */
import { HastElement, make, } from '@octogroup/hast-typescript';
import { makeBulletList, makeOrderedList, makeLocalPath, } from './common';
import { MarXivDataWithoutSitemap, ActiveWorkData, } from './context';
import { translateLicenseLink, translateLicenseName, } from './works';
import { makeOptionButton, } from './share';
import { Translatables, CommonReplacements, MainMenuText, UserMenuText, FooterInformation, HeaderText, HomepageText, StaticPagesData, WorkListingText, SubmissionGuidelinesContent, StaticWorkText, SubmitGuideText, SelfArchivingPoliciesText, JobOptions, RegistrationFormText, DynamicWorkText, WorkText, SelfArchivingPoliciesPublishers, SubmissionFormText, ModerateFormText, TranslatablePageMetadata, FormMessageText, } from './translatables';

/**
 * Common Replacements
 */
export const englishCommonReplacements: CommonReplacements = {
   costSavings: '$7,475,319.76',
   downloadCount: '117,551',
   octo: 'OCTO: Open Communications for The Ocean',
   octoWebsite: 'https://www.octogroup.org',
   facebook: '@OpenOCTO on Facebook',
   octoFacebookLink: 'https://facebook.com/openocto',
   twitter: '@MarXivPapers on Twitter',
   octoTwiterLink: 'https://twitter.com/MarXivPapers',
   crossref: 'Crossref',
   crossrefWebsite: 'https://www.crossref.org/',
   slackLink: 'https://marxiv-slack.herokuapp.com/',
   // Form replacements
   page: 'Page',
   yes: 'Yes',
   no: 'No',
   reject: 'Reject',
   approve: 'Approve',
   cancel: 'Cancel',
   reset: 'Reset',
   submit: 'Submit',
   edit: 'Edit',
   notSet: 'Not set',
   selectAndDrag: 'Select and drag an item to reorder the list.',
   optionalInformation: 'Optional information',
   months: {
      jan: 'January',
      feb: 'February',
      mar: 'March',
      apr: 'April',
      may: 'May',
      jun: 'June',
      jul: 'July',
      aug: 'August',
      sep: 'September',
      oct: 'October',
      nov: 'November',
      dec: 'December'
   }
}

/**
 * Menus
 */
export const englishMainMenuText: MainMenuText = {
   home: 'Home',
   why: 'Why MarXiv?',
   papers: 'Papers',
   orgs: 'MarXiv for Organizations',
   submit: 'Submit',
   team: 'Team',
   ambassadors: 'MarXiv Ambassadors',
   selfArchivingPolicies: 'Self-Archiving Policies of Major Publishers',
   submissionGuidelines: 'Full Submission Guidelines',
   codeOfConduct: 'Code of Conduct',
}

export const englisherUserMenuText: UserMenuText = {
   join: 'Join',
   login: 'Login',
   logout: 'Logout',
   adminDashboard: 'Moderate',
   userDashboard: 'My Works',
   orgDashboard: "My Org's Works",
   account: 'My Account'
}

/**
 * Header and Footer
 */
export const englishHeaderText: HeaderText = {
   why: {
      title: 'Why MarΧiv?',
      desktopSubtitle: 'How we increase impact'
   },
   submit: {
      title: 'Submit',
      desktopSubtitle: 'Share your research'
   },
   find: {
      title: 'Find Papers',
      desktopSubtitle: 'Browse the MarΧiv archive'
   },
   subtitle: 'The free repository for ocean and marine-climate science',
   producedBy: 'Produced by OCTO',
   subtitleProduced: 'The free repository for ocean and marine-climate science. Produced by OCTO.',
}

export const makeEnglishFooterInformation = (context: MarXivDataWithoutSitemap): FooterInformation => ({
   mobileFooterProduced: make.element({ tag: 'p', properties: { className: 'footerText'}, children: [
      make.text("MarXiv is produced by "),
      make.element({ tag: 'a', properties: { href: englishCommonReplacements.octoWebsite }, children: [
         make.text(englishCommonReplacements.octo)
      ]})
   ]}),
   licenseText: make.element({ tag: 'div', properties: { className: 'license' }, children: [
      make.text("The text content of this website is available under a "),
      make.element({ tag: 'a', properties: { href: 'https://creativecommons.org/licenses/by-nc/4.0/' }, children: [
         make.text('ccByNc License (Creative Commons Attribution-NonCommercial 4.0 International)')
      ]}),
      make.text(". Design and logos are copyright OCTO 2019 with all rights reserved.")
   ]}),
   privacyText: make.element({ tag: 'a', properties: { className: 'privacy', href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.privacyPolicy) }, children: [
      make.text("MarXiv's privacy policy can be viewed here.")
   ]}),
   homepageTitle: 'MarΧiv (rhymes with "archive") is the free repository for ocean and marine-climate science',
   homepageText: make.element({ tag: 'div', children: [
      make.text("Ocean managers, policymakers, and NGOs simply can't afford costly subscriptions to traditional academic journals. Studies have found that this barrier results in less science being used in environmental management plans."),
      make.text('MarΧiv offers a way to increase access to costly academic literature, legally. An author who retains copyright on their submitted manuscript, known colloquially as a preprint, may upload their manuscript to MarΧiv allowing anyone to download and read the preprint free of charge.'),
      make.text("But MarΧiv isn't just for preprints! You can deposit theses, reports academic posters, and more. With MarXiv you get a free DOI and ensure your work is visible forever."),
   ]})
})

/**
 * Homepage
 */
export const englishHomepageText: HomepageText = {
   shareButtonTitle: "Share your research now!",
   shareButtonSubtitle: "Fill out our simple submission form",
   visitButtonTitle: "Visit the MarΧiv Archive",
   visitButtonSubtitle: "Browse hundreds of papers in marine sciences",
   learnButtonTitle: "Learn More",
   learnButtonSubtitle: "Find our full submission guidelines and more",
   statSavedUpper: "Saved over",
   statSavedLower: "in research access fees",
   statDownloadsUpper: "From",
   statDownloadsLower: "preprint & postprint downloads",
   statsTitle: "If you want your research to be used for ocean management, the public must be able to read more than just the abstract of your research for free.",
   statsSubtitle: "To date, MarΧiv has...",
   desktopSubmitTitle: "Now is the time to submit!",
   desktopSubmitSubtitle: "Submitting your research is easy. Here's how:",
   uploadText: "Upload your work",
   metadataText: "Fill in metadata",
   licenseText: "Choose a license",
   doneText: "Done!",
   submitText: "Submit now",
   visitTitle: "Visit the MarΧiv archive",
   visitSubtitle: "Find hundreds of research articles on ocean and marine-climate science. All for free.",
   testamonialsTitle: "Join the ocean conservation community in sharing your research",
   testamonial1Name: "Dr. Samantha Andrews, Ocean Oculus",
   testamonial1Text: "If we want to cut down the barriers that allow some of us to read and share our science whilst others can’t because of financial circumstances, the finances of our intuitions, or our nations, then we need to find workable solutions. Since both my science and engagement work is primarily ocean-based, supporting initiatives like MarXiv is, for me, a no-brainer.",
   testamonial2Name: "Dr. Max Liboiron, Memorial University",
   testamonial2Text: "Our lab submits our preprints to MarXiv because we do science that affects people and their food webs, and community members need to be able to find our work without a paywall. One of our lab’s commitments is to accessible, meaningful, impactful science and MarXiv helps us achieve that. Moreover, MarXiv has provided support to help us figure out the legal terrain for preprints and open source publishing.",
   testamonial3Name: "Rebecca Hedreen, Buley Library, SCSU",
   testamonial3Text: "While most people associate this type of repository with journal article pre-prints, I'm most excited by the non-article potential. Posters, infographics, reports, that \"it didn't come out the way we planned\" project — it's time that this stuff comes off the wall and out of the drawer and becomes findable, citable, and reusable.",
}

/**
 * Static Pages
 */
export const makeEnglishStaticPagesData = (context: MarXivDataWithoutSitemap): StaticPagesData => ({
   // Why MarXiv?
   whyTitle: 'Why Should You Share Your Research in MarXiv?',
   whyBody: make.element({ tag: 'div', children: [
      make.element({ tag: 'h3', children: [
         make.text('To date*, MarXiv has saved ocean conservationists ' + englishCommonReplacements.costSavings + ' in research access fees from ' + englishCommonReplacements.downloadCount + ' downloads on otherwise pay-walled papers.')
      ]}),
      make.element({ tag: 'h4', children: [
         make.text("That's more money going towards ocean conservation rather than publishers.")
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Most peer-reviewed academic journal articles require a subscription to access the full-text for free. Without an institution paying for a subscription on your behalf, an individual research paper typically costs US $30-60.")
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Due to the cost of pay-walled journal articles the vast majority of government institutions lack subscription access to journals — particularly journals focusing on ocean or environmental issues.")
      ]}),
      make.element({ tag: 'div', properties: { className: 'pullQuote' }, children: [
         make.element({ tag: 'a', properties: { href: 'https://marxiv.org/hv4da/' }, children: [
            make.text('Chris Cvitanovic, 2014')
         ]}),
         make.text(', found that only 14% of the information cited in MPA (Marine Protected Area) management plans was from primary scientific sources, with the high cost of access creating a substantial barrier.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Your research is important and could help decision making! Ensure it is freely available by providing more than just the abstract.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Fortunately, with MarXiv, you can make your pay-walled research available to everyone for free!')
      ]}),
      make.element({ tag: 'div', properties: { className: 'flex all-parent-column' }, children: [
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.splash), className: 'button orange flex all-child-span1 center' }, children: [
            make.text('Share your research now!')
         ]})
      ]}),
      make.element({ tag: 'p', children: [
         make.text('*MarXiv launched in November 2017. This figure is updated daily.')
      ]}),
      make.element({ tag: 'h2', children: [
         make.text("Can I just share my paper in ResearchGate?")
      ]}),
      make.element({ tag: 'p', children: [
         make.text("No. In most cases, it is illegal to share the final full-text manuscript or PDF on for-profit public websites like ResearchGate. "),
         make.element({ tag: 'a', properties: { href: 'http://www.sciencemag.org/news/2017/10/publishers-take-researchgate-court-alleging-massive-copyright-infringement' }, children: [
            make.text('In fact, ResearchGate is facing a lawsuit by academic publishers for mass copyright infringement')
         ]}),
         make.text('.')
      ]}),
      make.element({ tag: 'h2', children: [
         make.text("What are the benefits of sharing my research in MarXiv?")
      ]}),
      make.element({ tag: 'h4', children: [
         make.text("More citations!")
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Across all scientific disciplines, papers available for free with a preprint — often referred to Green Open Access — "),
         make.element({ tag: 'a', properties: { href: 'https://peerj.com/articles/4375/' }, children: [
            make.text('are cited 30% more than the world average')
         ]}),
         make.text('.  Put another way, pay-walled papers are cited 10% less than what would be expected.')
      ]}),
      make.element({ tag: 'h4', children: [
         make.text("More readers!")
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Most governments and government agencies lack subscriptions to academic journals, as do most NGOs or civil society organizations. Think a manager is going to pay $40 for a PDF they might not even cite?"),
      ]}),
      make.element({ tag: 'h4', children: [
         make.text("More discovery!")
      ]}),
      make.element({ tag: 'p', children: [
         make.text("The growing volume of academic literature being published each year makes it difficult for anyone to keep up-to-date with research in their field. As a member of "),
         make.element({ tag: 'a', properties: { href: englishCommonReplacements.crossrefWebsite }, children: [
            make.text(englishCommonReplacements.crossref)
         ]}),
         make.text(', MarXiv publishes metadata for your submission that helps others discover and machine-read your work.')
      ]}),
      make.element({ tag: 'h2', children: [
         make.text("Ready to share your paper?")
      ]}),
      make.element({ tag: 'p', children: [
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.splash) }, children: [
            make.text('Use our simple Submission Guide')
         ]}),
         make.text('. It only takes about 5-10 minutes.')
      ]}),
   ]}),
   // Team
   teamTitle: "MaXiv Team and Advisory Board",
   teamContent: make.element({ tag: 'p', children: [
      make.text('MarXiv would not be possible without support from the '),
      make.element({ tag: 'a', properties: { href: 'https://www.packard.org/' }, children: [
         make.text('David and Lucile Packard Foundation')
      ]}),
      make.text(' MarXiv is coordinated by '),
      make.element({ tag: 'a', properties: { href: englishCommonReplacements.octoWebsite }, children: [
         make.text(englishCommonReplacements.octo)
      ]}),
      make.text('.')
   ]}),
   teamMarXivTitle: "The Marxiv Team",
   teamMarXiv: [
      { name: "Nick Wehner", title: "Director of Open Initiatives, OCTO" },
      { name: "Allie Brown", title: "Communications and Outreach Director, OCTO" },
      { name: "Mike Hay", title: "Data Scientist, OCTO" },
      { name: "Raye Evrard", title: "Project Manager, OCTO" },
      { name: "Sarah Carr", title: "Chief Knowledge Broker, OCTO" },
      { name: "John Davis", title: "President, OCTO" },
   ],
   teamAdvisoryTitle: "Advisory Board",
   teamAdvisorySubtitle: "The MarXiv Advisory Board consists of representatives from the ocean, coastal, and climate science communities, open access experts, and preprint leaders.",
   teamAdvisory: [
      { name: "Amanda Whitmire", title: "Stanford University" },
      { name: "Ashley Farley", title: "Gates Foundation" },
      { name: "Chris Cvitanovic", title: "CSIRO" },
      { name: "Edgar Robles", title: "Universidad del Mar" },
      { name: "Kim Martini", title: "Sea-Bird Scientific" },
      { name: "Lachlan Fetterplace", title: "Fish Thinkers Research" },
      { name: "Max Liboiron", title: "Memorial University" },
      { name: "Nathan Bennett", title: "University of British Columbia" },
      { name: "Sarah Winter Whelan", title: "American Littoral Society" },
   ],
   // Ambassadors
   ambassadorTitle: "MarXiv Ambassadors",
   ambassadorP1: "MarXiv Ambassadors serve as preprint-sharing experts for their local institutions, helping their fellow researchers determine which documents can (and cannot) be legally shared in MarXiv.",
   ambassadorP2: "If you are a member of an Ambassador's institution, we recommend you reach out to them if you have questions about MarXiv.",
   ambassador2019Title: "2019 Ambassadors",
   ambassadors2019: [
      { name: 'Samantha Andrews', institution: 'Memorial University', country: 'Canada', email: 's-andrews@live.ca', twitter: 'oceanoculus' },
      { name: 'Juliano Palacios Abrantes', institution: 'University of British Columbia', country: 'Canada', email: 'j.palacios@oceans.ubc.ca' },
      { name: 'Muhammad Firman Nuruddi', institution: 'Institut Teknologi Bandung', country: 'Indonesia', email: 'firman220700@gmail.com' },
      { name: 'Niccolò Bassan', institution: 'University Iuav of Venice', country: 'Italy', email: 'nbassan@iuav.it' },
      { name: 'Sarai Mijangos', institution: 'Universidad del Mar', country: 'Mexico', email: 'sarai.rosario1993@gmail.com' },
      { name: 'Ngozi Margaret Oguguah', institution: 'Nigerian Institute for Oceanography and Marine Research', country: 'Nigeria', email: 'ngozimoguguah@yahoo.com' },
      { name: 'Rebecca Hedreen', institution: 'Southern Connecticut State University', country: 'USA', email: 'hedreenr1@southernct.edu', twitter: 'disedlibrarian' },
      { name: 'Brendan Turley', institution: 'University of South Carolina - Columbia', country: 'USA', email: 'crabtails@gmail.com', twitter: 'crabtails' },
      { name: 'Nicole Baker', institution: 'University of Washington', country: 'USA', email: 'nbaker493@gmail.com' },
   ],
   ambassador2018Title: "2018 Ambassadors",
   ambassador2018Subtitle: "The MarXiv Team is grateful for our prior MarXiv Ambassadors:",
   ambassadorsEmeritus: [
      { name: 'David Shiffman', institution: 'Simon Fraser University' },
      { name: 'Juliano Palacios Abrantes', institution: 'University of British Columbia' },
      { name: 'Moisés Gallo', institution: 'Pontificia Universidad Católica de Chile' },
      { name: 'Sarai Mijangos', institution: 'Universidad del Mar' },
      { name: 'Ngozi Margaret Oguguah', institution: 'Nigerian Institute for Oceanography and Marine Research' },
      { name: 'Nicole Baker', institution: 'University of Washington' },
   ],
   // Code of Conduct
   codeTitle: "Code of Conduct",
   codePledgeTitle: "Our Pledge",
   codePledgeContent: make.element({ tag: 'p', children: [
      make.text('In the interest of fostering an open and welcoming environment, the '),
      make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.why.team) }, children: [
         make.text('MarXiv Team')
      ]}),
      make.text(' pledges to make participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, appearance, race, religion, or sexual identity/orientation.')
   ]}),
   codeStandardsTitle:  "Our Standards",
   codeStandardsList1: {
      listTitle: make.element({ tag: 'p', children: [
         make.text('Examples of behavior that contributes to creating a positive environment include:')
      ]}),
      listContent: [
         make.text("Using welcoming and inclusive language"),
         make.text("Being respectful of differing viewpoints and experiences"),
         make.text("Gracefully accepting constructive criticism"),
         make.text("Focusing on what is best for the community"),
         make.text("Showing empathy towards other community members")
      ]
   },
   codeStandardsList2: {
      listTitle: make.element({ tag: 'p', children: [
         make.text('Examples of unacceptable behavior by participants include:')
      ]}),
      listContent: [
         make.text("The use of sexualized language or imagery and unwelcome sexual attention or advances"),
         make.text("Trolling, insulting/derogatory comments, and personal or political attacks"),
         make.text("Public or private harassment"),
         make.text("Publishing others’ private information, such as a physical or electronic address, without explicit permission"),
         make.text("Other conduct which could reasonably be considered inappropriate in a professional setting")
      ]
   },
   codeEnforcementTitle: "Enforcement",
   codeEnforcementContent: make.element({ tag: 'div', children: [
      make.element({ tag: 'p', children: [
         make.text("Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by contacting the project team here. All complaints will be reviewed and investigated and will result in a response that is deemed necessary and appropriate to the circumstances. The "),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.why.team) }, children: [
            make.text('MarXiv Team')
         ]}),
         make.text(' is obligated to maintain confidentiality, to the extent permissible by law, with regard to the reporter of an incident. Further details of specific enforcement policies may be posted separately.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Project maintainers who do not follow or enforce the Code of Conduct in good faith may face temporary or permanent repercussions as determined by MarXiv's leadership team at OCTO.")
      ]})
   ]}),
   codeAttributionTitle: "Attribution",
   codeAttributionContent: make.element({ tag: 'p', children: [
      make.text('This Code of Conduct is largely identical to the '),
      make.element({ tag: 'a', properties: { href: 'http://fossilsandshit.com/paleorxiv/code-of-conduct/' }, children: [
         make.text('PaleorXiv Code of Conduct')
      ]}),
      make.text(' (thanks for licensing it under the Creative Commons!), which in turn was adapted from the Contributor Covenant, version 1.4, available at '),
      make.element({ tag: 'a', properties: { href: 'https://www.contributor-covenant.org/version/1/4/code-of-conduct.html' }, children: [
         make.text('https://www.contributor-covenant.org/version/1/4/code-of-conduct.html')
      ]}),
      make.text('.')
   ]}),
   // Privacy Policy
   privacyTitle: "Privacy Policy",
   privacyContent: make.element({ tag: 'div', children: [
      make.element({ tag: 'p', children: [
         make.text("The EU Parliament's General Data Protection Regulation (GDPR) requires that we spell out a bunch of info on privacy and data. We apologize for boring you with all these details. For clarification’s sake, we'll break down our various services under the headings below:")
      ]}),
      // Email
      make.element({ tag: 'h2', children: [
         make.text('Email')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Like most humans, we often write letters to folks. Sometimes this is done on paper, but most of the time we do it electronically with this fancy invention called “electronic mail” or “e-mail” for short. Your e-mail address can be used to personally identify you, or perhaps you and your cat if you both share the same email account. We don’t judge. Anyway, assume if you’ve emailed us or if we’ve emailed you that 1) we know who you are, and 2) Gmail (which OCTO uses) has probably added you to our Address Book (i.e. digital Rolodex). We’re organized folks and don’t like deleting our Address Books or inboxes, so assume you’ll be in there for a while. Though, do let us know if you change your email address so we’re not trying to contact someone else who we think is you -- that’ll really confuse that person. If you don’t want us to know you, please don’t email us. We may share your name, contact info, and the things we think you’re working on with people in our network (i.e., the global ocean management and conservation community) when they ask us things like, “I’m setting up a community-managed MPA in the Philippines. Do you know anyone I could ask for advice?” If you’d rather remain a social recluse, please do let us know. You can email us at '),
         make.element({ tag: 'a', properties: { href: 'mailto:hello@octogroup.org' }, children : [
            make.text('hello@octogroup.org')
         ]}),
         make.text(". We generally think people like to talk about their work, but it's good to know when you don't!")
      ]}),
      // Newsletters
      make.element({ tag: 'h2', children: [
         make.text('Newsletters')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Our Newsletters are a fancier subset of emails. Kind of like listservs, but much more edited posts that you (hopefully) truly want to read. If you don’t want to read them, please unsubscribe because it literally costs us money to send them to you and we’d rather put that money to best use. When you subscribe to our newsletters, we may keep for an indefinite period of time the information you give us (e.g. name, email address, job title, etc.). If you unsubscribe from the newsletter, our system (MailChimp) usually deletes you from our database pretty quickly. If they don't, email "),
         make.element({ tag: 'a', properties: { href: 'mailto:raye@octogroup.org' }, children : [
            make.text('raye@octogroup.org')
         ]}),
         make.text(" and she’ll remove you from our lists. We typically track both opens of, and clicks within, our newsletters so we know what content people like to read. Like, we stopped sending out pay-walled literature in our newsletters because people just weren’t clicking on it (because non-academics can't afford to subscribe to journals).")
      ]}),
      // Webinars
      make.element({ tag: 'h2', children: [
         make.text('Webinars')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("OCTO produces a number of webinars. We normally ask folks to provide us with their name, email address, job title, and organization when registering for these webinars. We typically share the “attendee reports” from our webinar service (Zoom and/or GoToWebinar) with the presenters of the webinar. This allows us all to see who’s asked questions during the webinar so we can follow up with you. It also helps us determine which webinars are popular topics, and which topics we don’t need to cover as often. The data is “processed” by the OCTO Team, and we (and the presenters we share the webinar reports with) do not typically delete this data after any set period of time. Users who object to this data-sharing should not register for OCTO webinars.")
      ]}),
      // Social media
      make.element({ tag: 'h2', children: [
         make.text('Social Media')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("OCTO maintains a few social media accounts, namely on Twitter and Facebook. If you follow/like/re-tweet/beam-up one of our social media accounts, we’ll potentially know the information you publicly share on those platforms. We say “potentially” because we don’t stare at your feed 24/7. That would be creepy. But, occasionally we look people up on social media to “tag” them in relevant posts. Like, if you’ve shared a blog on OpenChannels, we’ll probably tag your Twitter account on the Tweet when we share the blog. If you don’t want us or your grandmother to know what you ate for dinner last Wednesday, then please don’t share that information publicly on social media. That’s TMI, TBH. If you don’t want us to know any of this information about you (at all), please block our accounts on the relevant social networks or you can just not follow us or tag us or use social media at all.")
      ]}),
      // Google Analytics
      make.element({ tag: 'h2', children: [
         make.text('Google Analytics')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("We use the free version of Google Analytics on all our web platforms. We’re a non-profit, which means we don’t have the money for the crazy, stalkery, expensive stuff -- also, the crazy, stalkery, expensive stuff is rather crazy, and stalkery, which creeps us out. Google Analytics doesn’t store any personally-identifiable information about you. In fact, it’s against the Terms of Use for Google Analytics for us to match individuals to Google’s anonymized data (not that we could if we tried). We don’t delete the data in Google Analytics. We also tend to share the aggregated anonymous data (i.e. how many hits a page has) with our Board of Directors and our funders. Google Analytics just tells us that someone, in a particular city, did something on one of our websites at a certain time. Google's privacy policy may be viewed "),
         make.element({ tag: 'a', properties: { href: 'http://www.google.com/analytics/learn/privacy.html' }, children: [
            make.text('here')
         ]}),
         make.text('. You can opt-out of Google Analytics by turning on the "Do Not Track" setting in your browser or '),
         make.element({ tag: 'a', properties: { href: 'https://tools.google.com/dlpage/gaoptout' }, children: [
            make.text('via other methods')
         ]}),
         make.text('.')
      ]}),
      // ReCAPTCHA
      make.element({ tag: 'h2', children: [
         make.text('Google ReCAPTCHA')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("We use Google ReCAPTCHA to prevent automated bots from filling MarXiv with spam. By using MarXiv's website, your data is subject to Google's "),
         make.element({ tag: 'a', properties: { href: 'https://policies.google.com/privacy' }, children: [
            make.text('Privacy Policy')
         ]}),
         make.text(' and '),
         make.element({ tag: 'a', properties: { href: 'https://policies.google.com/terms' }, children: [
            make.text('Terms of Use')
         ]}),
         make.text('.')
      ]}),
      // Crossref
      make.element({ tag: 'h2', children: [
         make.text('Crossref')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("MarXiv submits metadata from deposited works (e.g. your preprint) to "),
         make.element({ tag: 'a', properties: { href: englishCommonReplacements.crossrefWebsite }, children: [
            make.text(englishCommonReplacements.crossref)
         ]}),
         make.text(". Submitted works are part of the scholarly record and cannot be removed, deleted, or forgotten. Submitted metadata are subject to Crossref's "),
         make.element({ tag: 'a', properties: { href: 'https://www.crossref.org/privacy/' }, children: [
            make.text('Privacy Policy')
         ]}),
         make.text('.')
      ]}),
      // Objections
      make.element({ tag: 'h2', children: [
         make.text('Objections')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("If you object to any of the information above, please email us at "),
         make.element({ tag: 'a', properties: { href: 'mailto:hello@octogroup.org' }, children : [
            make.text('hello@octogroup.org')
         ]}),
         make.text("  and let us know. Under the GDPR, you have the right to be forgotten by OCTO. You also have the right to request the data we have on you (which is probably your name, email address, job title, organization, the listservs/newsletters you’ve subscribed to and if you’ve opened those newsletters or not). You also have the right to completely ignore everything we’ve just said -- but you don’t have the right to dislike octopuses. We like them too much.")
      ]}),
   ]}),
   // MarXiv for Organizations
   orgsTitle: "Publishing for Organizations and Scholarly Societies",
   orgsContent: make.element({ tag: 'div', children: [
      make.element({ tag: 'p', children: [
         make.text('MarXiv is primarily intended for individuals to share their work, but OCTO also offers the following publishing services for organizations. These services help lower the costs of publishing, and this revenue helps offset the cost to OCTO for hosting content on MarXiv for free for individuals.')
      ]}),
      make.element({ tag: 'p', children: [
         make.element({ tag: 'a', properties: { href: englishCommonReplacements.octoWebsite }, children: [
            make.text(englishCommonReplacements.octo)
         ]}),
         make.text(", is a 501(c)(3) not-for-profit that has been publishing academic content for over 18 years. Donations to OCTO are tax-deductible under US law (EIN: 91-2099238).")
      ]}),
      make.element({ tag: 'h2', children: [
         make.text('Report Publishing')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Anyone can publish a report in MarXiv, but these reports are categorized by '),
         make.element({ tag: 'a', properties: { href: englishCommonReplacements.crossrefWebsite }, children: [
            make.text(englishCommonReplacements.crossref)
         ]}),
         make.text(' as "posted content" which is reserved for preprints and other unpublished works.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Organizations, however, have the legal right to formally publish their reports, providing additional metadata for discovery. Not only discovery of the report itself but also for the organization as it will be listed as the official publisher. We also give organizations control over their DOIs. Want a specific DOI name? We can do that. Want your DOI to resolve to your organization's website instead of MarXiv? We can do that, too.")
      ]}),
      make.element({ tag: 'p', children: [
         make.text("We even work with you to ensure your reports have the highest-quality metadata possible. Better indexing by scholarly search engines, powered by "),
         make.element({ tag: 'a', properties: { href: englishCommonReplacements.crossrefWebsite }, children: [
            make.text(englishCommonReplacements.crossref)
         ]}),
         make.text(', helps your work to be discovered by those who need it.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Want to learn more? Email Nick Wehner at "),
         make.element({ tag: 'a', properties: { href: 'mailto:nick@octogroup.org' }, children: [
            make.text('nick@octogroup.org')
         ]}),
         make.text('. And thank you! Publishing with OCTO ensures MarXiv will be available for years to come.')
      ]}),
      make.element({ tag: 'h2', children: [
         make.text('Conference Publishing, Hosting, and Archiving')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Anyone can submit a conference paper or conference-related work to MarXiv, but these works are categorized by '),
         make.element({ tag: 'a', properties: { href: englishCommonReplacements.crossrefWebsite }, children: [
            make.text(englishCommonReplacements.crossref)
         ]}),
         make.text(' as "posted content" which is reserved for preprints and other unpublished works.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("A conference's organizers, however, have the legal right to publish official Proceedings. Publishing official Proceedings and Conference Papers provides additional metadata for discovery — not only of the works themselves, but for the conference and its sponsors which are preserved in the scholarly record.")
      ]}),
      make.element({ tag: 'p', children: [
         make.text("We can also create customized portals to capture your participants' abstracts, posters, and presentations before your conference starts. Each work will receive a unique DOI and be preserved forever, helping these works to be cited and shared more often. We can even associate the name of your conference or organization into the DOIs we give you.")
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Want to learn more? Email Nick Wehner at "),
         make.element({ tag: 'a', properties: { href: 'mailto:nick@octogroup.org' }, children: [
            make.text('nick@octogroup.org')
         ]}),
         make.text('. And thank you! Publishing with OCTO ensures MarXiv will be available for years to come.')
      ]}),
      make.element({ tag: 'h2', children: [
         make.text('Need other publishing or website-related help?')
      ]}),
      make.element({ tag: 'p', children: [
         make.element({ tag: 'a', properties: { href: englishCommonReplacements.octoWebsite }, children: [
            make.text('OCTO')
         ]}),
         make.text(' services are open by default. That means just about everything we do we are able to "open source" so that anyone can use our work for free. That includes all of MarXiv, which we built from scratch ourselves. Not to brag, but we have some very talented designers and programmers!')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Let OCTO help you help the ocean. Email Nick Wehner at "),
         make.element({ tag: 'a', properties: { href: 'mailto:nick@octogroup.org' }, children: [
            make.text('nick@octogroup.org')
         ]}),
         make.text(' and tell him what you need help with. We look forward to working with you!')
      ]}),
   ]}),
   // Types of Works Accepted (metadata link from homepage)
   worksTitle: "Types of Works Accepted",
   worksContent: make.element({ tag: 'div', children: [
      makeBulletList({
         listTitle: make.element({ tag: 'p', children: [
            make.text('As a member of '),
            make.element({ tag: 'a', properties: { href: englishCommonReplacements.crossrefWebsite }, children: [
               make.text(englishCommonReplacements.crossref)
            ]}),
            make.text(', MarXiv is able to accept several different types of works:')
         ]}),
         listContent: [
            make.element({ tag: 'span', children: [
               make.element({ tag: 'span', properties: { className: 'strong' }, children: [
                 make.text('Posted content:') 
               ]}),
               make.text(' This includes anything which is "unofficially published" such as scientific manuscripts (preprints), academic posters, abstracts, theses, working papers, and more.')
            ]}),
            make.element({ tag: 'span', children: [
               make.element({ tag: 'span', properties: { className: 'strong' }, children: [
                 make.text('Conferences and related works:') 
               ]}),
               make.text(' Conference papers and proceedings, presentations, abstracts, and more.')
            ]}),
            // make.element({ tag: 'span', children: [
            //    make.element({ tag: 'span', properties: { className: 'strong' }, children: [
            //      make.text('Databases and datasets:') 
            //    ]}),
            //    make.text(' Share your research data separately from your manuscript, or publish metadata about a scientific database.')
            // ]}),
            // make.element({ tag: 'span', children: [
            //    make.element({ tag: 'span', properties: { className: 'strong' }, children: [
            //      make.text('Peer reviews:') 
            //    ]}),
            //    make.text(' Get credit for your peer reviews! Share your reviews so you may easily cite them on your CV.')
            // ]}),
            make.element({ tag: 'span', children: [
               make.element({ tag: 'span', properties: { className: 'strong' }, children: [
                 make.text('Reports:') 
               ]}),
               make.text(' Organizations may officially publish their reports, or anyone may unofficially publish a report as "posted content".')
            ]}),
         ]
      }),
      make.element({ tag: 'h2', children: [
         make.text('Required Metadata')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Crossref requires certain metadata (fields) for each type of work. Below is a list of the required and recommended metadata for each type.')
      ]}),
      make.element({ tag: 'h3', children: [
         make.text('Posted content')
      ]}),
      makeBulletList({
         listTitle:make.element({ tag: 'p', properties: { className: 'strong'}, children: [
            make.text('Required:')
         ]}),
         listContent: [
            make.text('PDF of the submitted work'),
            make.text('Title'),
            make.text('Year of publication'),
            make.text('Abstract or description'),
            make.text('Surname of lead author/contributor'),
            make.text('License or copyright')
         ]
      }),
      makeBulletList({
         listTitle:make.element({ tag: 'p', properties: { className: 'strong'}, children: [
            make.text('Recommended:')
         ]}),
         listContent: [
            make.text('Year, month, and day of publication'),
            make.text("Publisher's DOI if the work has been officially published"),
            make.text('Language of content'),
            make.element({ tag: 'span', children: [
               make.text('Full name, affiliation, and '),
               make.element({ tag: 'a', properties: { href: 'https://orcid.org/' }, children: [
                  make.text('ORCID')
               ]}),
               make.text(' of each author/contributor')
            ]}),
            make.text('Institutional sponsor or host of the work'),
            make.text('DOIs of each reference cited by the work'),
            make.text('DOIs of any related items (e.g. supplementary information)')
         ]
      }),
      make.element({ tag: 'h3', children: [
         make.text('Conference-related works')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Crossref requires published conference papers to be associated with official proceedings. However, only a conference\'s organizers have the legal right to publish official Proceedings. Publishing official Proceedings and Conference Papers provides additional metadata for discovery and grants conference organizers control over DOI names and resolution. MarXiv charges a fee for this service to help us offset the costs of sharing other content for free. Note that anyone may unofficially publish a conference-related work as "posted content". '),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.forOrgs) }, children: [
            make.text('Learn more about our conference publishing options')
         ]}),
         make.text('.')
      ]}),
      makeBulletList({
         listTitle:make.element({ tag: 'p', properties: { className: 'strong'}, children: [
            make.text('Required:')
         ]}),
         listContent: [
            make.text('PDF of the submitted work'),
            make.text('Conference name'),
            make.text('Title of work'),
            make.text('Title of proceedings'),
            make.text('Year of publication'),
            make.text('Abstract or description'),
            make.text('Surname of lead author/contributor'),
            make.text('License or copyright')
         ]
      }),
      makeBulletList({
         listTitle:make.element({ tag: 'p', properties: { className: 'strong'}, children: [
            make.text('Recommended:')
         ]}),
         listContent: [
            make.text('Conference number'),
            make.text('Conference acronym'),
            make.text('Conference location'),
            make.text('Conference beginning and ending dates'),
            make.text('Conference sponsor'),
            make.text('Year, month, and day of publication'),
            make.text("Publisher's DOI if the work has been officially published"),
            make.text('Language of content'),
            make.element({ tag: 'span', children: [
               make.text('Full name, affiliation, and '),
               make.element({ tag: 'a', properties: { href: 'https://orcid.org/' }, children: [
                  make.text('ORCID')
               ]}),
               make.text(' of each author/contributor')
            ]}),
            make.text('Institutional sponsor or host of the work'),
            make.text('DOIs of each reference cited by the work'),
            make.text('DOIs of any related items (e.g. supplementary information)')
         ]
      }),
      // make.element({ tag: 'h3', children: [
      //    make.text('Databases and datasets')
      // ]}),
      // make.element({ tag: 'p', children: [
      //    make.text('A '),
      //    make.element({ tag: 'span', properties: { className: 'emphasis'}, children: [
      //       make.text('database')
      //    ]}),
      //    make.text(' is the metadata (title, contributors, etc.) for a collection of '),
      //    make.element({ tag: 'span', properties: { className: 'emphasis'}, children: [
      //       make.text('datasets')
      //    ]}),
      //    make.text('. To share an individual dataset, you must first describe the database or collection the dataset belongs to. With the DOI of the published database, you will be able to share related datasets.')
      // ]}),
      // makeBulletList({
      //    listTitle:make.element({ tag: 'p', properties: { className: 'strong'}, children: [
      //       make.text('Required:')
      //    ]}),
      //    listContent: [
      //       make.text('Title'),
      //       make.text('Year of publication'),
      //       make.text('Abstract or description'),
      //       make.text('Surname of lead author/contributor'),
      //       make.text('License or copyright')
      //    ]
      // }),
      // makeBulletList({
      //    listTitle:make.element({ tag: 'p', properties: { className: 'strong'}, children: [
      //       make.text('Recommended:')
      //    ]}),
      //    listContent: [
      //       make.text('Year, month, and day of publication'),
      //       make.text("Publisher's DOI if the work has been officially published"),
      //       make.text('Language of content'),
      //       make.element({ tag: 'span', children: [
      //          make.text('Full name, affiliation, and '),
      //          make.element({ tag: 'a', properties: { href: 'https://orcid.org/' }, children: [
      //             make.text('ORCID')
      //          ]}),
      //          make.text(' of each author/contributor')
      //       ]}),
      //       make.text('Institutional sponsor or host of the work'),
      //       make.text('DOIs of each reference cited by the work'),
      //       make.text('DOIs of any related items (e.g. supplementary information)')
      //    ]
      // }),
      // make.element({ tag: 'h3', children: [
      //    make.text('Peer reviews')
      // ]}),
      // makeBulletList({
      //    listTitle:make.element({ tag: 'p', properties: { className: 'strong'}, children: [
      //       make.text('Required:')
      //    ]}),
      //    listContent: [
      //       make.text('PDF of the peer review'),
      //       make.text('Type of review (pre- or post-publication)'),
      //       make.text('DOI of the reviewed work'),
      //       make.text('Title'),
      //       make.text('Year of publication'),
      //       make.text('Abstract or description'),
      //       make.text('Surname of peer-reviewer'),
      //       make.text('License or copyright')
      //    ]
      // }),
      // makeBulletList({
      //    listTitle:make.element({ tag: 'p', properties: { className: 'strong'}, children: [
      //       make.text('Recommended:')
      //    ]}),
      //    listContent: [
      //       make.text('Type of review (referee report, editor report, etc.)'),
      //       make.text('Recommendation of review (accept, reject, major revisions, etc.)'),
      //       make.text('Year, month, and day of publication'),
      //       make.text('Language of content'),
      //       make.element({ tag: 'span', children: [
      //          make.text('Full name, affiliation, and '),
      //          make.element({ tag: 'a', properties: { href: 'https://orcid.org/' }, children: [
      //             make.text('ORCID')
      //          ]}),
      //          make.text(' of the reviewer')
      //       ]}),
      //       make.text('Institutional sponsor or host of the peer review'),
      //       make.text('DOIs of any related items (e.g. supplementary information)')
      //    ]
      // }),
      make.element({ tag: 'h3', children: [
         make.text('Reports')
      ]}),
      makeBulletList({
         listTitle:make.element({ tag: 'p', properties: { className: 'strong'}, children: [
            make.text('Required:')
         ]}),
         listContent: [
            make.text('PDF of the submitted work'),
            make.text('Publisher name and location'),
            make.text('Title'),
            make.text('Year of publication'),
            make.text('Abstract or description'),
            make.text('Surname of lead author/contributor'),
            make.text('License or copyright')
         ]
      }),
      makeBulletList({
         listTitle:make.element({ tag: 'p', properties: { className: 'strong'}, children: [
            make.text('Recommended:')
         ]}),
         listContent: [
            make.text('Year, month, and day of publication'),
            make.text("Publisher's DOI if the work has been officially published"),
            make.text('Language of content'),
            make.element({ tag: 'span', children: [
               make.text('Full name, affiliation, and '),
               make.element({ tag: 'a', properties: { href: 'https://orcid.org/' }, children: [
                  make.text('ORCID')
               ]}),
               make.text(' of each author/contributor')
            ]}),
            make.text('Institutional sponsor or host of the work'),
            make.text('DOIs of each reference cited by the work'),
            make.text('DOIs of any related items (e.g. supplementary information)')
         ]
      }),
   ]}),
   // Done page with otter GIF
   doneGiphy: 'https://giphy.com/embed/ely3apij36BJhoZ234',
   // Common Confirmation Strings
   confirmSocialSharing: make.element({ tag: 'p', children: [
      make.text('Are you going to share this work on social media? You should! Be sure to tag '),
      make.element({ tag: 'a', properties: { href: englishCommonReplacements.octoTwiterLink }, children: [
         make.text(englishCommonReplacements.twitter)
      ]}),
      make.text(' or '),
      make.element({ tag: 'a', properties: { href: englishCommonReplacements.octoFacebookLink }, children: [
         make.text(englishCommonReplacements.facebook)
      ]}),
      make.text(' so we can amplify your efforts.')
   ]}),
   confirmSignature: make.element({ tag: 'p', children: [
      make.text('Thank you for making your ocean science accessible to all!'),
      make.element({ tag: 'br', children: [] }),
      make.text("- Nick Wehner and the rest of the MarXiv Team")
   ]}),
   // Confirmation page for new submissions
   confirmNewTitle: "Thank you for sharing your work!",
   confirmNewText: make.element({ tag: 'div', children: [
      make.element({ tag: 'p', children: [
         make.text('The MarXiv Team will review your work within 72 hours. You will receive an email notification once your work has been reviewed. Works are publicly available after approval.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('If your work is approved, it will receive a DOI which will be detailed in your review confirmation email. If your work is rejected, please address any issues described in your review confirmation email before resubmitting the work.')
      ]})
   ]}),
   // Confirmation page for editing an approved submission
   confirmUpdatedTitle: "Your work has been updated successfully",
   // Confirmation page for edits to rejected works
   confirmResubmitTitle: "Thank you for resubmitting this work!",
   confirmResubmitText: make.element({ tag: 'p', children: [
      make.text('The MarXiv Team will review your changes within 72 hours. You will receive an email notification once this submission has been reviewed. Once your work is approved, it will receive a DOI which will be detailed in your review-confirmation email. Works are publicly available after approval.')
   ]}),
   // Confirmation page for new user accounts
   confirmNewAccountTitle: "Thank you for creating a new MarXiv user account!",
   confirmNewAccountText: make.element({ tag: 'div', children: [
      make.element({ tag: 'p', children: [
         make.text('Please check your email for a confirmation message from hello@octogroup.org to confirm your email address. Once you click the confirmation link in that message, your account will be activated.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Confirmation emails are sent immediately. If you have not received the email message within an hour, please check your "spam" or "junk" folder. Still no confirmation? Email Nick Wehner at '),
         make.element({ tag: 'a', properties: { href: 'mailto:nick@octogroup.org' }, children: [
            make.text('nick@octogroup.org')
         ]}),
         make.text('.')
      ]})
   ]}),
   // Confirmation page for verifying an email address
   confirmVerifyEmailTitle: "Your email address has been verified. Thank you!",
   confirmVerifyEmailText: make.element({ tag: 'p', children: [
      make.text('Your MarXiv user account is now active. Would you like to '),
      make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.splash) }, children: [
         make.text('share your work?')
      ]})
   ]}),
   // License Options
   licenseOptions : {
      title: 'License Options',
      description: make.element({ tag: 'div', children: [
         make.element({ tag: 'p', children: [
            make.text('Authors are given a variety of licenses to choose from when sharing their content with MarXiv. If you have already published your work, however, your publisher may limit your options. For more information about preprint licensing in-general, '),
            make.element({ tag: 'a', properties: { href: 'https://asapbio.org/licensing-faq' }, children: [
               make.text('we recommend reading this article from ASAPbio')
            ]}),
            make.text(' or '),
            make.element({ tag: 'a', properties: { href: 'https://www.youtube.com/watch?v=NC7oquMgNdM&feature=youtu.be' }, children: [
               make.text('watching this video')
            ]}),
            make.text('.'),
         ]}),
         make.element({ tag: 'p', children: [
            make.text('Please be aware that choosing a restricted license (like non-commercial, share-alike, and no-derivs) can prevent your work from being translated, shared in meta-analysis reports or on for-profit platforms like ResearchGate, or even used to solicit donations to environmental non-profits. '),
            make.element({ tag: 'a', properties: { href: 'https://blog.dhimmel.com/biorxiv-licenses/' }, children: [
               make.text('We strongly recommend licensing your work under CC0 or CC BY for a number of reasons')
            ]}),
            make.text('.'),
         ]})
      ]}),
      learnMore: 'Learn more about this license',
      softwareTitle: 'Licenses for Software and Code',
      descriptions: {
         cc0: make.element({ tag: 'p', children: [
            make.text("You can 'opt-out' of copyright by placing your work in the public domain. "),
            make.element({ tag: 'span', properties: { className: 'strong' }, children: [
               make.text('This is a great option for datasets which may not be legally copyrightable in many cases.')
            ]}),
         ]}),
         ccBy: make.element({ tag: 'p', children: [
            make.text("This license lets others share, translate, remix, and build upon your work, even commercially, as long as they credit you for the original creation. "),
            make.element({ tag: 'span', properties: { className: 'strong' }, children: [
               make.text('We recommend this option for most works.')
            ]}),
         ]}),
         afl: make.element({ tag: 'p', children: [
            make.text("This license lets others share and remix your work, provided you are credited as the original author and any adaptations explicitly mention that your original contributions were modified. "),
            make.element({ tag: 'span', properties: { className: 'strong' }, children: [
               make.text('Authors whom have published their work in Nature should choose this license.')
            ]}),
         ]}),
         ccBySa: make.element({ tag: 'p', children: [
            make.text("This license lets others share, translate, remix, and build upon your work, even commercially, as long as they credit you for the original creation and license any new creations under this same license."),
         ]}),
         ccByNd: make.element({ tag: 'p', children: [
            make.text("This license lets others to share your work, including commercially; however, it cannot be shared with others in adapted form, and credit must be provided to you."),
         ]}),
         ccByNc: make.element({ tag: 'p', children: [
            make.text("This license lets others share, translate, remix, and build upon your work, but only for non-commercial purposes, as long as they credit you for the original creation. This will prevent your work from being shared on for-profit platforms like ResearchGate."),
         ]}),
         ccByNcSa: make.element({ tag: 'p', children: [
            make.text("This license lets others share, translate, remix, and build upon your work, but only for non-commercial purposes, as long as they credit you for the original creation and license any new creations under this same license."),
         ]}),
         ccByNcNd: make.element({ tag: 'p', children: [
            make.text("This is the most restrictive of the license options, as it only allows others to share your work with others for non-commercial purposes provided they credit you for the original creation. Your work may not be translated. The non-commercial clause will prevent your work from being shared on for-profit platforms like ResearchGate. "),
            make.element({ tag: 'span', properties: { className: 'strong' }, children: [
               make.text('Only authors whom have published their work with Elsevier or Wiley should choose this option.')
            ]}),
         ]}),
         gpl: make.element({ tag: 'p', children: [
            make.text("This license allows others to share and remix your code, provided new works are also openly-licensed. "),
            make.element({ tag: 'span', properties: { className: 'strong' }, children: [
               make.text('We recommend this option for most software projects.')
            ]}),
         ]}),
         mit: make.element({ tag: 'p', children: [
            make.text("This license allows others to share and remix your code, with no restrictions on how new works may be licensed."),
         ]}),
         ecl: make.element({ tag: 'p', children: [
            make.text("This license allows others to share and remix your code, with no restrictions on how new works may be licensed."),
         ]}),
      },
   },
   // Error pages
   errors: {
      notFound: {
         title: 'Page Not Found',
         description: make.element({ tag: 'p', children: [
            make.text('The requested page does not exist. Please check the URL in your browser for typos.')
         ]}),
      },
      accessDenied: {
         title: 'Access Denied',
         description: make.element({ tag: 'p', children: [
            make.text('You do not have access to this page. Please login to your MarXiv user account and try again.')
         ]}),
      },
      general: {
         title: 'Uh oh! Something went wrong.',
         description: make.element({ tag: 'p', children: [
            make.text('An unexpected error has occured. Please try your request again.')
         ]}),
      },
   },
})

/**
 * Work Listing
 */
export const englishWorkListingText: WorkListingText = {
   workListingWelcome: 'Welcome',
   workListingSubtitle: 'to the free research repository for the ocean and marine-climate sciences',
   workListingSearch: 'Search',
   workListingSearchWorks: 'Search papers and other works...',
   workListingRecent: 'Recent Deposits'
}

/**
 * Full Submission Guidelines
 */
export const makeEnglishSubmissionGuidelinesContent = (context: MarXivDataWithoutSitemap): SubmissionGuidelinesContent => ({
   titleAndIntro: make.element({tag: 'div', properties: { className: 'guidelinesTitle'}, children: [
      make.element({ tag: 'h1', children: [
         make.text('MarXiv Submission Guidelines')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('All documents added to MarXiv are immediately publicly accessible. However, the '),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.why.team) }, children: [
            make.text('MarXiv Team')
         ]}),
         make.text(' reviews all submissions. In the event your submission violates these Submission Guidelines, your paper will be removed from MarXiv. Violations of the '),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.codeOfConduct) }, children: [
            make.text('MarXiv Code of Conduct')
         ]}),
         make.text(' may result in a permanent ban from sharing content in MarXiv.')
      ]}),
      make.element({ tag: 'p', children: [
         make.element({ tag: 'span', properties: { className: 'strong' }, children: [
            make.text('Need help?')
         ]}),
         make.text(' Ask us on Twitter at '),
         make.element({ tag: 'a', properties: { href: englishCommonReplacements.octoTwiterLink }, children: [
            make.text('@MarXivPapers')
         ]}),
         make.text('. Slack user? '),
         make.element({ tag: 'a', properties: { href: englishCommonReplacements.slackLink }, children: [
            make.text('Join the MarXiv Slack group!')
         ]}),
         make.text(' Or, email Nick Wehner at '),
         make.element({ tag: 'a', properties: { href: 'mailto:nick@octogroup.org' }, children: [
            make.text('nick@octogroup.org')
         ]}),
         make.text('.')
      ]})
   ]}),
   beforeSubmit: {
      title: 'Before you submit',
      id: 'beforeSubmit',
      miniDescription: make.element({ tag: 'div', children: [
         make.element({ tag: 'p', children: [
            make.text('Please be sure that 1) you have the right to share your work publicly, and 2) you have consent from all Contributors (i.e. coauthors) to share the work.')
         ]}),
         make.element({tag: 'p', children: [
            make.element({ tag: 'span', properties: { className: 'strong'}, children: [
               make.text('Please convert any Word documents to PDFs before submitting to MarXiv.')
            ]})
         ]})
      ]}),
      content: make.element({ tag: 'div', children: [
         make.element({ tag: 'p', children: [
            make.text('Please be sure that 1) you have the right to share your work publicly, and 2) you have consent from all Contributors (i.e. coauthors) to share the work.')
         ]}),
         make.element({tag: 'p', children: [
            make.element({ tag: 'span', properties: { className: 'strong'}, children: [
               make.text('Please convert any Word documents to PDFs before submitting to MarXiv.')
            ]})
         ]}),
         make.element({ tag: 'h3', children: [
            make.text('How do I determine if I have the legal right to share a work?')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('Generally speaking: if you are the author or creator of a work, and if you have not published the work, you retain a legal right (copyright) over your work. Copyright laws vary from country-to-country, however, so please respect your local laws appropriately.')
         ]}),
         make.element({tag: 'p', children: [
            make.text('If you are a Federal government employee (USA, Canada, and others) and you created your work as per your normal job duties, then in most cases your work is ineligible for copyright protection. This means you can share your work in MarXiv (or anywhere else), even if it has been published elsewhere. For example, in Canada these works are Crown Copyright. In the USA, these works are part of the Public Domain.')
         ]}),
         make.element({tag: 'p', children: [
            make.text('If you have published your work already, what you can do with your work depends on who the copyright holder is. If you published your work under a Creative Commons (e.g. CC-BY) license or otherwise did not waive your legal right to your work, then you have the legal copyright to share your work in MarXiv.')
         ]}),
         make.element({tag: 'p', children: [
            make.text('If you published in a traditional pay-walled journal (by publishers like Elsevier, Springer Nature, Wiley, Oxford Press, etc.) then you have likely transferred your copyright to your publisher. Your publisher can (and will) place restrictions on what you can do with your work now that they hold your legal copyright. '),
            make.element({ tag: 'span', properties: { className: 'strong'}, children: [
               make.text('You may browse our list of '),
               make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.selfArchivingPolicies) }, children: [
                  make.text('Self-Archiving Policies of Major Journals')
               ]}),
               make.text(' which will help you determine the default sharing policies of your journal/publisher.')
            ]}),
            make.text(' You should consult your Copyright Transfer Agreement (CTA) to identify which rights you have kept. Your CTA will outline which rights you (the author) have transferred to the publisher and which rights you (the author) have retained.')
         ]})
      ]}),
      collapsible: 'maxBeforeSubmit',
   },
   transferredCopyright: {
      title: 'If I published my work and transferred copyright to my publisher, what are my options?',
      id: 'transferredCopyright',
      miniDescription: make.element({ tag: 'div', children: [
         make.element({tag: 'p', children: [
            make.text("Most publishers (Elsevier, Wiley, Taylor and Francis, Springer Nature, etc.) allow you to share the manuscript you originally submitted to the journal, commonly called the \"preprint\", in not-for-profit subject-matter-specific repositories like MarXiv at any time. This is the un-peer-reviewed and un-edited text, without the journal's branding. ",),
            make.element({ tag: 'a', properties: { href: 'https://doi.org/10.31230/osf.io/nfbs3' }, children: [
               make.text('Here is an example preprint in MarXiv')
            ]}),
            make.text('.')
         ]})
      ]}),
      content: make.element({ tag: 'div', children: [
         make.element({tag: 'p', children: [
            make.text("Most publishers (Elsevier, Wiley, Taylor and Francis, Springer Nature, etc.) allow you to share the manuscript you originally submitted to the journal, commonly called the \"preprint\", in not-for-profit subject-matter-specific repositories like MarXiv at any time. This is the un-peer-reviewed and un-edited text, without the journal's branding. ",),
            make.element({ tag: 'a', properties: { href: 'https://doi.org/10.31230/osf.io/nfbs3' }, children: [
               make.text('Here is an example preprint in MarXiv')
            ]}),
            make.text('.')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('Additionally, most publishers allow authors to share their accepted manuscript, commonly called the "postprint", but sharing this peer-reviewed version comes with restrictions. Publishers like Springer Nature and Wiley will typically require that postprints are only shared after a 1-2 year embargo period — often longer for the social sciences — so that the publisher may charge for exclusive access during this time. There are often restrictions on how you may license this version, as well: Nature does not allow postprints to be licensed under any Creative Commons license; Elsevier requires postprints use the Creative Commons Attribution-NonCommercial-NoDerivs license, which prevents your work from being translated and used for many non-profit purposes. '),
            make.element({ tag: 'a', properties: { href: 'https://doi.org/10.31230/osf.io/8gjxy' }, children: [
               make.text('Here is an example postprint in MarXiv')
            ]}),
            make.text('.')
         ]}),
         make.element({ tag: 'h4', children: [
            make.text('Need help finding your postprint/Author Accepted Manuscript (AAM)?')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('Publishers typically keep AAMs for 2-5 years in their Journal Submission Systems. '),
            make.element({ tag: 'a', properties: { href: 'https://docs.google.com/document/d/1TPOH664MgMVBW3zfsbbDgaPr6qv_C97Uhj6cESGb3F4/edit?usp=sharing' }, children: [
               make.text('For instructions on how to download your AAM from various publishers, use this handy guide')
            ]}),
            make.text('.')
         ]})
      ]}),
      collapsible: 'maxTransferredCopyright',
   },
   commonQuestions: make.element({ tag: 'h2', children: [
      make.text('Common Questions')
   ]}),
   post: {
      title: 'What can I post to MarXiv?',
      id: 'postGuidelines',
      miniDescription: makeBulletList({
         listContent: [
            make.element({ tag: 'div', children: [
               make.element({tag: 'span', properties: { className: 'strong'}, children: [
                  make.text('Preprints:')
               ]}),
               make.text(' When you submit a manuscript to a journal for peer-review and publication, you should also submit the same manuscript to MarXiv right away.')
            ]}),
            make.element({ tag: 'div', children: [
               make.element({tag: 'span', properties: { className: 'strong'}, children: [
                  make.text('Postprints:')
               ]}),
               make.text(' If you have the rights to share your postprint in a repository like MarXiv, please do so!')
            ]}),
            make.element({ tag: 'div', children: [
               make.element({tag: 'span', properties: { className: 'strong'}, children: [
                  make.text('Reports:')
               ]}),
               make.text(' By sharing your reports in MarXiv, each one will get a unique DOI, and ensure your report is indexed by Google Scholar and other academic indexes.')
            ]}),
            make.element({ tag: 'div', children: [
               make.element({tag: 'span', properties: { className: 'strong'}, children: [
                  make.text('Theses and dissertations:')
               ]}),
               make.text(' Ensure anyone can find your thesis or dissertation by archiving it in MarXiv! This may come as a surprise: some of the most downloaded papers in MarXiv are theses (including two from the OCTO Team who thought no one would ever read their theses).')
            ]}),
            make.element({ tag: 'div', children: [
               make.element({tag: 'span', properties: { className: 'strong'}, children: [
                  make.text('Working papers, conference proceedings, abstracts, posters, etc.:')
               ]}),
               make.text(' MarXiv will gladly accept other scholarly research outputs.')
            ]}),
            make.element({ tag: 'div', children: [
               make.element({tag: 'span', properties: { className: 'strong'}, children: [
                  make.text('Open Access publications:')
               ]}),
               make.text(' Have you already published an Open Access paper? If you have a Creative Commons license, or similar, you can share the work in MarXiv.')
            ]}),
         ]
      }),
      content: make.element({ tag: 'div', children: [
         makeBulletList({
            listContent: [
               make.element({ tag: 'div', children: [
                  make.element({tag: 'span', properties: { className: 'strong'}, children: [
                     make.text('Preprints:')
                  ]}),
                  make.text(' When you submit a manuscript to a journal for peer-review and publication, you should also submit the same manuscript to MarXiv right away.')
               ]}),
               make.element({ tag: 'div', children: [
                  make.element({tag: 'span', properties: { className: 'strong'}, children: [
                     make.text('Postprints:')
                  ]}),
                  make.text(' If you have the rights to share your postprint in a repository like MarXiv, please do so!')
               ]}),
               make.element({ tag: 'div', children: [
                  make.element({tag: 'span', properties: { className: 'strong'}, children: [
                     make.text('Reports:')
                  ]}),
                  make.text(' By sharing your reports in MarXiv, each one will get a unique DOI, and ensure your report is indexed by Google Scholar and other academic indexes.')
               ]}),
               make.element({ tag: 'div', children: [
                  make.element({tag: 'span', properties: { className: 'strong'}, children: [
                     make.text('Theses and dissertations:')
                  ]}),
                  make.text(' Ensure anyone can find your thesis or dissertation by archiving it in MarXiv! This may come as a surprise: some of the most downloaded papers in MarXiv are theses (including two from the OCTO Team who thought no one would ever read their theses).')
               ]}),
               make.element({ tag: 'div', children: [
                  make.element({tag: 'span', properties: { className: 'strong'}, children: [
                     make.text('Working papers, conference proceedings, abstracts, posters, etc.:')
                  ]}),
                  make.text(' MarXiv will gladly accept other scholarly research outputs.')
               ]}),
               make.element({ tag: 'div', children: [
                  make.element({tag: 'span', properties: { className: 'strong'}, children: [
                     make.text('Open Access publications:')
                  ]}),
                  make.text(' Have you already published an Open Access paper? If you have a Creative Commons license, or similar, you can share the work in MarXiv.')
               ]}),
            ]
         }),
         make.element({ tag: 'p', children: [
            make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.worksAccepted) }, children: [
               make.text('Click here for information on the required and recommended metadata for content')
            ]}),
            make.text('.')
         ]})
      ]}),
      collapsible: 'maxPost',
   },
   license: {
      title: 'How should I license my work?',
      id: 'licenseGuidelines',
      miniDescription: make.element({ tag: 'p', children: [
         make.text('MarXiv offers a variety of license options. '),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.licenses) }, children: [
            make.text('Click here to browse the full list')
         ]}),
         make.text('.')
      ]}),
      content: make.element({ tag: 'div', children: [
         make.element({ tag: 'p', children: [
            make.text('MarXiv offers a variety of license options. '),
            make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.licenses) }, children: [
               make.text('Click here to browse the full list')
            ]}),
            make.text('.')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('Please note that some publishers restrict how you may license your preprints and/or postprints! Nature, for example, does not allow postprints to be licensed in the Creative Commons. For more information on publisher policies'),
            make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.selfArchivingPolicies) }, children: [
               make.text('see our list of Self-Archiving Policies of Major Publishers')
            ]}),
            make.text('.')
         ]})
      ]}),
      collapsible: 'maxLicenseOptions',
   },
   preprint: {
      title: 'What is a preprint and a postprint?',
      id: 'preprintGuidelines',
      miniDescription: make.element({ tag: 'div', children: [
         make.element({ tag: 'p', children: [
            make.text("A preprint is the original manuscript submitted to a journal for publication. This is the un-peer-reviewed and un-edited text, without the journal's branding. "),
            make.element({ tag: 'a', properties: { href: 'https://doi.org/10.31230/osf.io/nfbs3' }, children: [
               make.text('Here is an example preprint in MarXiv')
            ]}),
            make.text('.')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('A postprint is the version of your work which the journal accepted for publication. It is also commonly called the "author\'s accepted manuscript" or "AAM". The postprint has been edited and peer-reviewed, but it lacks typesetting and any branding from the journal. '),
            make.element({ tag: 'a',  properties: { href: 'https://doi.org/10.31230/osf.io/8gjxy' }, children: [
               make.text('Here is an example postprint in MarXiv')
            ]}),
            make.text('.')
         ]})
      ]}),
      content: make.element({ tag: 'div', children: [
         make.element({ tag: 'p', children: [
            make.text("A preprint is the original manuscript submitted to a journal for publication. This is the un-peer-reviewed and un-edited text, without the journal's branding. "),
            make.element({ tag: 'a', properties: { href: 'https://doi.org/10.31230/osf.io/nfbs3' }, children: [
               make.text('Here is an example preprint in MarXiv')
            ]}),
            make.text('.')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('A postprint is the version of your work which the journal accepted for publication. It is also commonly called the "author\'s accepted manuscript" or "AAM". The postprint has been edited and peer-reviewed, but it lacks typesetting and any branding from the journal. '),
            make.element({ tag: 'a',  properties: { href: 'https://doi.org/10.31230/osf.io/8gjxy' }, children: [
               make.text('Here is an example postprint in MarXiv')
            ]}),
            make.text('.')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('The typical academic publishing workflow begins with a manuscript submitted to a journal for peer-review and publication. This manuscript is known as the '),
            make.element({ tag: 'span', properties: { className: 'emphasis'}, children: [
               make.text('preprint')
            ]}),
            make.text('. The preprint will then undergo peer-review and editing between the author, peer reviewers, and the journal editor. At this point, when the manuscript is peer-reviewed but not typeset by the publisher, the author is asked to sign a '),
            make.element({ tag: 'span', properties: { className: 'emphasis'}, children: [
               make.text('Copyright Transfer Agreement (CTA)')
            ]}),
            make.text('. The CTA then transfers the copyright held by the author on '),
            make.element({ tag: 'span', properties: { className: 'emphasis'}, children: [
               make.text('all forms of the manuscript')
            ]}),
            make.text(' (both pre- and post-peer-review) to the publisher. Before signing the CTA, we recommend authors utilize an '),
            make.element({ tag: 'span', properties: { className: 'emphasis'}, children: [
               make.text('Author Addendum')
            ]}),
            make.text(' to negotiate with the publisher that the author should keep full copyright on the peer-reviewed (but not typeset) manuscript. After the CTA is signed, the publisher will generate a rough-draft of the typeset and peer-reviewed manuscript, known as the '),
            make.element({ tag: 'span', properties: { className: 'emphasis'}, children: [
               make.text('postprint')
            ]}),
            make.text(". After typesetting edits are made, the final publisher's PDF is created, known as the "),
            make.element({ tag: 'span', properties: { className: 'emphasis'}, children: [
               make.text('Version of Record (VOR)')
            ]}),
            make.text('. The VOR is what users typically download from the publisher. Some publishers make postprints available for download as an "early view" of the publication.')
         ]})
      ]}),
      collapsible: 'maxWhatPreprint',
   },
   cta: {
      title: 'What is a Copyright Transfer Agreement?',
      id: 'ctaGuidelines',
      miniDescription: make.element({ tag: 'p', children: [
         make.text('A Copyright Transfer Agreement (CTA) is legal document which is generally used by publishers to "transfer" copyright on a manuscript from the author(s) to the publisher. CTAs may transfer some or all legal rights. In the end, this means the authors do not have full legal copyright on the manuscript which they produced — instead, the publisher is given these rights.')
      ]}),
      content: make.element({tag: 'div', children: [
         make.element({ tag: 'p', children: [
            make.text('A Copyright Transfer Agreement (CTA) is legal document which is generally used by publishers to "transfer" copyright on a manuscript from the author(s) to the publisher. CTAs may transfer some or all legal rights. In the end, this means the authors do not have full legal copyright on the manuscript which they produced — instead, the publisher is given these rights.')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('When publishing in a traditional pay-walled academic journal, the CTA will define how the author may use their manuscript. Authors may be allowed to deposit their preprint in MarXiv immediately. Or, authors may only be allowed to share their preprint after an embargo period. Every CTA is unique. We encourage all authors to consult their CTA to determine how they can legally share their work.')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('If you published in a traditional pay-walled academic journal, but paid for "gold" Open Access (usually identifiable with a Creative Commons license on the final document) then you may not have signed a CTA since you (the author) retains final copyright.')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('The '),
            make.element({ tag: 'a', properties: { href: 'http://www.sherpa.ac.uk/romeo/index.php' }, children: [
               make.text('SHERPA/RoMEO database of publisher copyright and self-archiving policies')
            ]}),
            make.text(' may help you determine which journals allow preprints to be posted publicly. Please note, however, that SHERPA/RoMEO should not be considered 100% accurate and all claims made by SHERPA/RoMEO should be verified by your CTA.')
         ]})
      ]}),
      collapsible: 'maxCTA',
   },
   publish: {
      title: '"I am about to publish my work in a traditional pay-walled journal. How can I ensure I will be able to share my work in MarXiv?"',
      id: 'publishGuidelines',
      miniDescription: make.element({ tag: 'p', children: [
         make.text('The '),
         make.element({ tag: 'a', properties: { href: 'http://scholars.sciencecommons.org/' }, children: [
            make.text("Scholar's Copyright Addendum Engine")
         ]}),
         make.text(', provided by '),
         make.element({ tag: 'a', properties: { href: 'https://creativecommons.org/about/program-areas/open-science' }, children: [
            make.text("Science Commons")
         ]}),
         make.text(', offers a template to generate an addendum to your Copyright Transfer Agreement (CTA). The addendum allows you to "retain sufficient rights to post a copy of the published version of your article (usually in pdf form) online immediately to a site that does not charge for access to the article."')
      ]}),
      content: make.element({ tag: 'div', children: [
         make.element({ tag: 'p', children: [
            make.text('The '),
            make.element({ tag: 'a', properties: { href: 'http://scholars.sciencecommons.org/' }, children: [
               make.text("Scholar's Copyright Addendum Engine")
            ]}),
            make.text(', provided by '),
            make.element({ tag: 'a', properties: { href: 'https://creativecommons.org/about/program-areas/open-science' }, children: [
               make.text("Science Commons")
            ]}),
            make.text(', offers a template to generate an addendum to your Copyright Transfer Agreement (CTA). The addendum allows you to "retain sufficient rights to post a copy of the published version of your article (usually in pdf form) online immediately to a site that does not charge for access to the article."')
         ]}),
         make.element({ tag: 'p', children: [
            make.text('MarXiv recommends that before signing a CTA, you utilize the '),
            make.element({ tag: 'a', properties: { href: 'http://scholars.sciencecommons.org/' }, children: [
               make.text("Scholar's Copyright Addendum")
            ]}),
            make.text(' to generate an "Access - Reuse" agreement. If this addendum is accepted by the publisher, it will ensure you have the legal rights to post your paper in MarXiv immediately. For more information, see '),
            make.element({ tag: 'a', properties: { href: 'https://sparcopen.org/our-work/author-rights/brochure-html/' }, children: [
               make.text("Author's Rights: Using the SPARC Author Addendum")
            ]}),
            make.text('.')
         ]})
      ]}),
      collapsible: 'maxPublish',
   },
})

/**
 * Self-archiving policies
 */
export const preprintAnytime: HastElement = make.element({ tag: 'p', children: [
   make.text('Preprints may be shared in MarXiv at any time')
]})
export const postprintAnytime: HastElement = make.element({ tag: 'p', children: [
   make.text('Postprints may be shared in MarXiv at any time')
]})
export const postprint1year: HastElement = make.element({ tag: 'p', children: [
   make.text('Postprints may be shared in MarXiv after a 1-year embargo period')
]})

export const englishSAPublishers: SelfArchivingPoliciesPublishers = {
   aaas: {
      name: "American Association for the Advancement of Science (AAAS)",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Preprints of may be shared in MarXiv at any time, but are subject to a strict press embargo before publication')
         ]}),
         coverPage: "Link to the publisher's final version",
         allowedLicenses: ['all'],
         source: 'https://www.sciencemag.org/authors/science-journals-editorial-policies'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Postprints may be shared in MarXiv after a 6-month embargo period')
         ]}),
         allowedLicenses: ['all'],
         source: 'https://www.sciencemag.org/site/feature/contribinfo/faq/index.xhtml#pmc_faq'
      }
   },
   ams: {
      name: "American Meteorological Society",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         coverPage: 'If already published, provide the full citation to the Version of Record; or if the work has been accepted, but not yet published: "This work has been accepted to <journal name>. The AMS does not guarantee that the copy provided here is an accurate copy of the final published work."',
         allowedLicenses: ['all'],
         source: 'https://www.ametsoc.org/ams/index.cfm/publications/ethical-guidelines-and-ams-policies/ams-copyright-policy/'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Postprints may not be shared in MarXiv, unless mandated by your institution, employer or funder')
         ]}),
         allowedLicenses: ['noSharing'],
         source: 'https://www.ametsoc.org/ams/index.cfm/publications/ethical-guidelines-and-ams-policies/ams-copyright-policy/'
      }
   },
   brill: {
      name: "Brill",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Preprints of journal articles and multi-authored books may be shared in MarXiv at any time')
         ]}),
         allowedLicenses: ['all'],
         source: 'https://brill.com/page/RightsPermissions/rights-and-permissions'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: postprint1year,
         allowedLicenses: ['all'],
         source: 'https://brill.com/page/RightsPermissions/rights-and-permissions'
      }
   },
   cambridge: {
      name: "Cambridge University Press",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         coverPage: '"This article has been published in a revised form in [Journal] [http://doi.org/XXX]. [License, e.g. "Available under a CC-BY license"] © copyright holder."',
         allowedLicenses: ['all'],
         source: 'https://www.cambridge.org/core/services/open-access-policies/open-access-journals/green-open-access-policy-for-journals'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Postprints may be shared in MarXiv immediately for humanities and social science journals, or after a 6-month embargo period for STM journals')
         ]}),
         coverPage: '"This article has been published in a revised form in [Journal] [http://doi.org/XXX]. This version is published under a Creative Commons ccByNcNd. No commercial re-distribution or re-use allowed. Derivative works cannot be distributed. © copyright holder.',
         allowedLicenses: ['ccByNcNd'],
         source: 'https://www.cambridge.org/core/services/open-access-policies/open-access-journals/green-open-access-policy-for-journals'
      }
   },
   cell: {
      name: "Cell Press",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Preprints may be shared in MarXiv at any time, but are subject to a press embargo until after publication')
         ]}),
         coverPage: 'Authors should link to the published version on the CSIRO Publishing website.',
         allowedLicenses: ['all'],
         source: 'https://www.cell.com/rights-sharing-embargoes'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: postprint1year,
         coverPage: 'Authors should link to the published version on the CSIRO Publishing website.',
         allowedLicenses: ['all'],
         source: 'https://www.cell.com/rights-sharing-embargoes'
      }
   },
   csiro: {
      name: "CSIRO Publishing",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         coverPage: 'Authors should link to the published version on the CSIRO Publishing website.',
         allowedLicenses: ['all'],
         source: 'http://www.publish.csiro.au/pc/forauthors/openaccess'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: postprintAnytime,
         coverPage: 'Authors should link to the published version on the CSIRO Publishing website.',
         allowedLicenses: ['all'],
         source: 'http://www.publish.csiro.au/pc/forauthors/openaccess'
      }
   },
   elsevier: {
      name: "Elsevier",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         allowedLicenses: ['all'],
         source: 'https://www.elsevier.com/about/policies/sharing'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Postprints may be shared in MarXiv (just like arXiv and RePEc) at any time')
         ]}),
         coverPage: "Authors should include a link with the publisher's DOI.",
         allowedLicenses: ['ccByNcNd'],
         source: 'https://www.elsevier.com/about/policies/sharing'
      }
   },
   gsa: {
      name: "Geological Society of America",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         allowedLicenses: ['all'],
         source: 'http://www.geosociety.org/gsa/pubs/openaccess.aspx'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: postprint1year,
         coverPage: "Authors should include a link with the publisher's DOI.",
         allowedLicenses: ['all'],
         source: 'http://www.geosociety.org/gsa/pubs/openaccess.aspx'
      }
   },
   ir: {
      name: "Inter-Research",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         allowedLicenses: ['all'],
         source: 'https://www.int-res.com/journals/open-access/'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Postprints may be shared in MarXiv after a 12-month embargo period. Additionally, the Version of Record (final PDF) may be shared after a 5-year embargo period')
         ]}),
         allowedLicenses: ['all'],
         source: 'https://www.int-res.com/journals/open-access/'
      }
   },
   nas: {
      name: "National Academy of Sciences",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         coverPage: "Once the work has been published, authors must offer a link to the publisher's version",
         allowedLicenses: ['all'],
         source: 'https://www.pnas.org/content/106/1/3.full'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: postprintAnytime,
         coverPage: "Authors must offer a link to the publisher's version",
         allowedLicenses: ['all'],
         source: 'https://www.pnas.org/content/106/1/3.full'
      }
   },
   nature: {
      name: "Nature Research",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         allowedLicenses: ['all'],
         source: 'https://www.nature.com/nature-research/editorial-policies/self-archiving-and-license-to-publish'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Postprints may be shared in MarXiv after a 6-month embargo period, but are subject to very restrictive license terms')
         ]}),
         coverPage: "Authors should cite the publication reference and DOI number on the first page of any deposited version, and provide a link from it to the URL of the published article on the journal's website.",
         allowedLicenses: ['noLicense'],
         source: 'https://www.nature.com/nature-research/editorial-policies/self-archiving-and-license-to-publish#terms-for-use'
      }
   },
   nrc: {
      name: "NRC Research Press",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         allowedLicenses: ['all'],
         source: 'https://www.nrcresearchpress.com/page/authors/information/rights'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: postprintAnytime,
         allowedLicenses: ['all'],
         source: 'https://www.nrcresearchpress.com/page/authors/information/rights'
      }
   },
   oxford: {
      name: "Oxford University Press",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Preprints may be shared in MarXiv at any time, provided "that once the article is accepted [the authors] provide a statement of acknowledgement, and that once the article has been published this acknowledgement is updated to provide details such as the volume and issue number, the DOI, and a link to the published article on the journal’s website"')
         ]}),
         coverPage: '"This article has been accepted for publication in [Journal Title] Published by Oxford University Press. The version of record is available at [DOI link]"',
         allowedLicenses: ['all'],
         source: 'https://academic.oup.com/journals/pages/access_purchase/rights_and_permissions/self_archiving_policy_b'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: postprintAnytime,
         coverPage: '"This is a pre-copyedited, author-produced version of an article accepted for publication in [insert journal title] following peer review. The version of record [insert complete citation information here] is available online at: xxxxxxx [insert URL and DOI of the article on the OUP website]."',
         allowedLicenses: ['all'],
         source: 'https://academic.oup.com/journals/pages/access_purchase/rights_and_permissions/self_archiving_policy_b'
      }
   },
   royalSociety: {
      name: "Royal Society Publishing",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Preprints may be shared in MarXiv at any time, but they are subject to media embargoes until the day of publication')
         ]}),
         allowedLicenses: ['all'],
         source: 'https://royalsociety.org/journals/ethics-policies/media-embargo/'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: postprintAnytime,
         allowedLicenses: ['ccByNc', 'ccByNcNd', 'ccByNcSa'],
         source: 'https://royalsociety.org/journals/ethics-policies/media-embargo/'
      }
   },
   sage: {
      name: "SAGE",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         coverPage: '"This is the preprint of an article published by SAGE. The final publisher\'s version is: Author(s), Contribution Title, Journal Title (Journal Volume Number and Issue Number) pp. xx-xx. Copyright © [year] (Copyright Holder). DOI: [DOI number]."',
         allowedLicenses: ['ccByNcNd', 'ccByNd'],
         source: 'https://us.sagepub.com/en-us/nam/journal-author-archiving-policies-and-re-use'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: postprintAnytime,
         coverPage: '"This is the postprint of an article published by SAGE. The final publisher\'s version is: Author(s), Contribution Title, Journal Title (Journal Volume Number and Issue Number) pp. xx-xx. Copyright © [year] (Copyright Holder). DOI: [DOI number]."',
         allowedLicenses: ['ccByNcNd', 'ccByNd'],
         source: 'https://us.sagepub.com/en-us/nam/journal-author-archiving-policies-and-re-use'
      }
   },
   springer: {
      name: "Springer Nature",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         coverPage: '"This is a pre-print of an article published in [insert journal title]. The final authenticated version is available online at: https://doi.org/[insert DOI]"',
         allowedLicenses: ['all'],
         source: 'https://www.springer.com/gp/open-access/authors-rights/self-archiving-policy/2124'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: postprint1year,
         coverPage: '"This is a post-peer-review, pre-copyedit version of an article published in [insert journal title]. The final authenticated version is available online at: http://dx.doi.org/[insert DOI]"',
         allowedLicenses: ['all'],
         source: 'https://www.springer.com/gp/open-access/authors-rights/self-archiving-policy/2124'
      }
   },
   tandf: {
      name: "Taylor & Francis",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         coverPage: '"This is an original manuscript / preprint of an article published by Taylor & Francis in [JOURNAL TITLE] on [date of publication], available online: http://www.tandfonline.com/[Article DOI]."',
         allowedLicenses: ['all'],
         source: 'https://authorservices.taylorandfrancis.com/sharing-your-work/'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Postprints may be shared after an embargo period, usually up to 18-months')
         ]}),
         coverPage: '"This is an Accepted Manuscript of an article published by Taylor & Francis in [JOURNAL TITLE] on [date of publication], available online: http://www.tandfonline.com/[Article DOI]."',
         allowedLicenses: ['all'],
         source: 'https://authorservices.taylorandfrancis.com/sharing-your-work/'
      }
   },
   tos: {
      name: "The Oceanography Society",
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         allowedLicenses: ['all'],
         source: 'https://tos.org/oceanography/permissions'
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: make.element({ tag: 'p', children: [
            make.text("Both Postprints and the Version of Record (publisher's final PDF) may be shared in MarXiv at any time")
         ]}),
         allowedLicenses: ['all'],
         source: 'https://tos.org/oceanography/permissions'
      }
   },
   wiley: {
      name: 'Wiley',
      defaultPolicyPreprint: {
         forWorkType: 'preprint',
         policy: preprintAnytime,
         allowedLicenses: ['all'],
         source: 'https://authorservices.wiley.com/author-resources/Journal-Authors/licensing/self-archiving.html',
         coverPage: "This is the pre-peer reviewed version of the following article: [FULL CITE], which has been published in final form at [Link to final article using the DOI]. This article may be used for non-commercial purposes in accordance with Wiley Terms and Conditions for Use of Self-Archived Versions.",
      },
      defaultPolicyPostprint: {
         forWorkType: 'postprint',
         policy: make.element({ tag: 'p', children: [
            make.text('Postprints may be shared after an embargo period: "12 months for scientific, technical, medical, and psychology (STM) journals and 24 months for social science and humanities (SSH) journals following publication of the final article."')
         ]}),
         allowedLicenses: ['ccByNcNd'],
         source: 'https://authorservices.wiley.com/author-resources/Journal-Authors/licensing/self-archiving.html',
         coverPage: "This is the peer reviewed version of the following article: [FULL CITE], which has been published in final form at [Link to final article using the DOI]. This article may be used for non-commercial purposes in accordance with Wiley Terms and Conditions for Use of Self-Archived Versions.",
      }
   },
}

export const makeEnglishSelfArchivingPoliciesContent = (context: MarXivDataWithoutSitemap) => (d: SelfArchivingPoliciesPublishers): SelfArchivingPoliciesText => ({
   preprints: 'Preprints',
   postprints: 'Postprints',
   source: 'source',
   // Make cover pages
   coverpageDescriptionPreprint: 'Preprints should include the following on a cover page:',
   coverpageDescriptionPostprint: 'Postprints should include the following on a cover page:',
   // Make licenses
   licenseDescriptionAnyPreprint: 'Preprints may be shared under any license.',
   licenseDescriptionAnyPostprint: 'Postprints may be shared under any license.',
   licenseDescriptionNonePreprint: 'Authors may not grant others any additional license/rights to the preprint, so the author should retain copyright.',
   licenseDescriptionNonePostprint: 'Authors may not grant others any additional license/rights to the postprint, so the author should retain copyright.',
   licenseDescriptionLimitedPreprint: 'Preprints are limited to the following license options by the publisher:',
   licenseDescriptionLimitedPostprint: 'Postprints are limited to the following license options by the publisher:',
   bookChaptersPolicies: [
      make.element({ tag: 'h2', children: [
         make.text('Default Policies for Book Chapters')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('While publisher policies are often transparent for journal articles, for books things are not as clear.')
      ]}),
      make.element({ tag: 'h3', children: [
         make.text('No archiving allowed')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Springer does not allow public sharing of book chapters. Though, you can always ask by emailing '),
         make.element({ tag: 'a', properties: { href: 'mailto:permissions.springer@spi-global.com' }, children: [
            make.text('permissions.springer@spi-global.com')
         ]}),
         make.text('.')
      ]}),
      make.element({ tag: 'h3', children: [
         make.text('Archiving allowed after embargo period')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Brill allows preprints to be shared at any time. Postprints may be shared after a 2-year embargo ('),
         make.element({ tag: 'a', properties: { href: 'http://www.brill.com/resources/authors/publishing-books-brill/self-archiving-rights' }, children:[
            make.text('source')
         ]}),
         make.text(').')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Cambridge University Press allows postprints of chapters to be shared after a 6-month embargo.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Palgrave Macmillan allows for the deposit of a single chapter after a 3-year embargo.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Routledge/Taylor & Francis allow '),
         make.element({ tag: 'span', properties: { className: 'emphasis' }, children: [
            make.text('each individual author or contributor')
         ]}),
         make.text(' to upload '),
         make.element({ tag: 'span', properties: { className: 'strong' }, children: [
            make.text('one (1) chapter')
         ]}),
         make.text(' from the "Accepted Manuscript" (the post-contract but pre-production, i.e. not copy–edited, proofread or typeset, Word Document/PDF of the chapter) after an embargo of 18-months for Humanities and Social Sciences books, or 1-year for STEM books.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Sage allows the publisher's PDF of book chapters to be shared on a case-by-case basis. The author must email "),
         make.element({ tag: 'a', properties: { href: 'mailto:permissions@sagepub.co.uk' }, children: [
            make.text('permissions@sagepub.co.uk')
         ]}),
         make.text(' for permission.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('"Wiley allows postprints of book chapters to be shared on a case-by-case basis. The author (not admin staff) must email '),
         make.element({ tag: 'a', properties: { href: 'mailto:permissions@wiley.com' }, children: [
            make.text('permissions@wiley.com')
         ]}),
         make.text(' for permission.')
      ]}),
   ],
   defaultPublisherPoliciesTitle: 'Default Policies for Journal Articles by Publisher Name',
   journalPoliciesTitle: 'Default Policies for Journal Articles by Journal Title',
   titleAndIntro: [
      make.element({ tag: 'h1', children: [
         make.text('Self-Archiving Policies of Major Publishers')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('This is a list of self-archiving policies for major ocean and climate-science journals. '),
         make.element({ tag: 'span', properties: { className: 'strong' }, children: [
            make.text('This list excludes fully Open Access journals (like Frontiers in Marine Science and PLoS ONE) as you may share Open Access publications anywhere.')
         ]})
      ]}),
      make.element({ tag: 'p', children: [
         make.text('This list was last updated September 2019. For additions and corrections, please contact Nick Wehner at '),
         make.element({ tag: 'a', properties: { href: 'mailto:nick@octogroup.org' }, children: [
            make.text('nick@octogroup.org')
         ]}),
         make.text('.')
      ]}),
      make.element({ tag: 'p', children: [
         make.element({ tag: 'span', properties: { className: 'emphasis' }, children: [
            make.text('This list is for informational purposes only! Authors should always ensure they have the proper copyright permissions before sharing work when they are no longer the copyright holder. In cases where authors have signed a Copyright Transfer Agreement (CTA), this binding, legal document dictates what authors may do with their work')
         ]}),
         make.text('. That said: it is rare for CTAs to reserve more rights for the publisher (thus taking additional rights away from the author) than what is "standard." It is much more common for CTAs to be modified to reserve more rights for the author. If your employer (university) has an Open Access policy, it is probable that any CTAs you sign must keep additional rights with you (the author) than the publisher\'s standard CTA. Usually, these policies ensure authors have the rights to share preprints and postprints immediately, rather than after a lengthy embargo period. Contact your friendly, local librarian to learn more about CTAs and your rights.')
      ]})
   ],
   journals: [
      {
         title: 'Atmosphere-Ocean',
         publisher: d.tandf
      },
      {
         title: 'Aquatic Conservation: Marine and Freshwater Ecosystems',
         publisher: d.wiley
      },
      {
         title: 'Biological Conservation',
         publisher: d.elsevier,
      },
      {
         title: 'Biology Letters',
         publisher: d.royalSociety,
      },
      {
         title: 'Canadian Journal of Fisheries and Aquatic Sciences',
         publisher: d.nrc,
      },
      {
         title: 'Climate Dynamics',
         publisher: d.springer,
      },
      {
         title: 'Climatic Change',
         publisher: d.springer,
      },
      {
         title: 'Coastal Management',
         publisher: d.tandf,
      },
      {
         title: 'Conservation Biology',
         publisher: d.wiley,
      },
      {
         title: 'Current Biology',
         publisher: d.cell,
      },
      {
         title: 'Ecological Indicators',
         publisher: d.elsevier,
      },
      {
         title: 'Ecological Modelling',
         publisher: d.elsevier,
      },
      {
         title: 'Ecosystem Services',
         publisher: d.elsevier,
      },
      {
         title: 'Environmental Pollution',
         publisher: d.elsevier,
      },
      {
         title: 'Environmental Science & Policy',
         publisher: d.elsevier,
      },
      {
         title: 'Estuarine, Coastal and Shelf Science',
         publisher: d.elsevier,
      },
      {
         title: 'Fish and Fisheries',
         publisher: d.wiley,
      },
      {
         title: 'Fisheries Research',
         publisher: d.elsevier,
      },
      {
         title: 'Geophysical Research Letters',
         publisher: d.wiley,
         overridingPreprintPolicy: {
            forWorkType: 'preprint',
            policy: make.element({ tag: 'p', children: [
               make.text('Preprints may be shared in MarXiv at any time, just be sure to inform AGU if you post before official publication')
            ]}),
            coverPage: '"This is the preprint of an article published by SAGE. The final publisher\'s version is: Author(s), Contribution Title, Journal Title (Journal Volume Number and Issue Number) pp. xx-xx. Copyright © [year] (Copyright Holder). DOI: [DOI number]."',
            allowedLicenses: ['ccByNcNd', 'ccByNd'],
            source: 'https://publications.agu.org/author-resource-center/publication-policies/dual-publication-policy/prior-publication-and-posting-primer/'
         },
      },
      {
         title: 'Global Change Biology',
         publisher: d.wiley,
      },
      {
         title: 'Global Environmental Change',
         publisher: d.elsevier,
      },
      {
         title: 'ICES Journal of Marine Science',
         publisher: d.oxford,
      },
      {
         title: 'International Journal of Climatology',
         publisher: d.wiley,
      },
      {
         title: 'Journal of Climate',
         publisher: d.ams,
      },
      {
         title: 'Journal of Environmental Economics and Management',
         publisher: d.elsevier,
      },
      {
         title: 'Journal of Environmental Management',
         publisher: d.elsevier,
      },
      {
         title: 'Journal of Experimental Marine Biology and Ecology',
         publisher: d.elsevier,
      },
      {
         title: 'Marine Ecology Progress Series (MEPS)',
         publisher: d.ir,
      },
      {
         title: 'Marine Environment Research',
         publisher: d.elsevier,
      },
      {
         title: 'Marine Policy',
         publisher: d.elsevier,
      },
      {
         title: 'Marine Pollution Bulletin',
         publisher: d.elsevier,
      },
      {
         title: 'Maritime Studies',
         publisher: d.springer,
      },
      {
         title: 'Nature',
         publisher: d.nature,
      },
      {
         title: 'Nature Climate Change',
         publisher: d.nature,
      },
      {
         title: 'Nature Communications',
         publisher: d.nature,
         overridingPreprintPolicy: {
            forWorkType: 'preprint',
            policy: make.element({ tag: 'p', children: [
               make.text('Preprints may be shared in MarXiv at any time')
            ]}),
            allowedLicenses: ['all'],
            source: 'https://us.sagepub.com/en-us/nam/journal-author-archiving-policies-and-re-use'
         }
      },
      {
         title: 'Nature Ecology and Evolution',
         publisher: d.nature,
      },
      {
         title: 'Ocean and Coastal Management',
         publisher: d.elsevier,
      },
      {
         title: 'Oceanography',
         publisher: d.tos
      },
      {
         title: 'Oryx',
         publisher: d.cambridge,
      },
      {
         title: 'Pacific Conservation Biology',
         publisher: d.csiro,
      },
      {
         title: 'Proceedings of the National Academy of Sciences of the United States of America (PNAS)',
         publisher: d.nas,
      },
      {
         title: 'Proceedings of the Royal Society B: Biological Sciences',
         publisher: d.royalSociety,
      },
      {
         title: 'Renewable Energy',
         publisher: d.elsevier,
      },
      {
         title: 'Science',
         publisher: d.aaas,
      },
      {
         title: 'Science of the Total Environment',
         publisher: d.elsevier,
      },
   ]
})

/**
 * Works
 */
export const englishStaticWorkText: StaticWorkText = {
   anonymousContrib: 'Anonymous Contributor',
   doi: 'DOI',
   pending: 'Pending acceptance',
   vor: 'Version of Record available at',
   downloads: 'Downloads',
   download: 'Download',
   openPDF: 'Open PDF in a new window',
   peerReviewStatement: 'The content of this work has not undergone formal peer review.',
   description: 'Description',
   references: "References",
   relatedMaterials: 'Related Materials',
   datasetCollection: 'This dataset is part of a collection',
   reasonTitle: 'Reason for withdrawal',
   badges: {
      preprint: 'Preprint',
      postprint: 'Postprint',
      report: 'Report',
      thesis: 'Thesis',
      dissertation: 'Dissertation',
      database: 'Database',
      dataset: 'Dataset',
      letter: 'Letter',
      workingPaper: 'Working Paper',
      peerReview: 'Peer Review',
      journalArticle: 'Journal Article',
      paper: 'Paper',
      poster: 'Poster',
      conferenceProceedings: 'Conference Proceedings',
      conferencePaper: 'Conference Paper',
      presentation: 'Presentation',
      supplementalInfo: 'Supplemental Info',
      other: 'Other',
      // For OCTO Work Types
      reportPublished: 'Published Report',
      reportUnpublished: 'Unpublished Report',
      copyrightedPaper: 'Copyrighted Paper',
      oaPaper: 'Open Access Paper',
   },
   licensesFull: {
      cc0: 'Public Domain (CC0)',
      afl: 'Academic Free License, V3 (AFL)',
      ccby: 'Creative Commons Attribution (CC BY)',
      ccbysa: 'Creative Commons Attribution-ShareAlike (CC BY-SA)',
      ccbynd: 'Creative Commons Attribution-NoDerivs (CC BY-ND)',
      ccbync: 'Creative Commons Attribution-NonCommercial (CC BY-NC)',
      ccbyncsa: 'Creative Commons Attribution-NonCommercial-ShareAlike (CC BY-NC-SA)',
      ccbyncnd: 'Creative Commons Attribution-NonCommercial-NoDerivs (CC BY-NC-ND)',
      gpl: 'GNU General Public License, V3 (GPL)',
      mit: 'MIT License (MIT)',
      ecl: 'Educational Community License, V2 (ECL)',
      none: 'No License',
   },
   licensesAbbreviated: {
      cc0: 'CC0',
      afl: 'AFL',
      ccby: 'CC BY',
      ccbysa: 'CC BY-SA',
      ccbynd: 'CC BY-ND',
      ccbync: 'CC BY-NC',
      ccbyncsa: 'CC BY-NC-SA',
      ccbyncnd: 'CC BY-NC-ND',
      gpl: 'GPL',
      mit: 'MIT',
      ecl: 'ECL',
      none: 'Copyrighted',
   },
   licensesLinks: {
      cc0: 'https://creativecommons.org/share-your-work/public-domain/cc0/',
      afl: 'https://opensource.org/licenses/AFL-3.0',
      ccby: 'https://creativecommons.org/licenses/by/4.0/',
      ccbysa: 'https://creativecommons.org/licenses/by-sa/4.0/',
      ccbynd: 'https://creativecommons.org/licenses/by-nd/4.0',
      ccbync: 'https://creativecommons.org/licenses/by-nc/4.0',
      ccbyncsa: 'https://creativecommons.org/licenses/by-nc-sa/4.0',
      ccbyncnd: 'https://creativecommons.org/licenses/by-nc-nd/4.0',
      gpl: 'https://opensource.org/licenses/GPL-3.0',
      mit: 'https://opensource.org/licenses/MIT',
      ecl: 'https://opensource.org/licenses/ECL-2.0',
   },
   relationships: {
      relatedMaterial: "This work is related material to",
      reviewOf: "This work is a review of",
      commentOn: "This work is a comment on",
      replyTo: "This work is a reply to",
      derivedFrom: "This work is derived from",
      basedOn: "This work is based on data from",
      basisFor: "This work is the data basis for",
      preprintOf: "This work is the preprint of",
      manuscriptOf: "This work is the manuscript of",
      translationOf: "This work is a translation of",
      replaces: "This work replaces",
      replacedBy: "This work is replaced by",
      sameAs: "This work is the same as",
   },
   peerReview: {
      prePublication: 'This work is a pre-publication peer review of',
      postPublication: 'This work is a post-publication peer review of',
      typeOfReview: 'Type of Review',
      recommendation: 'Recommendation of Review',
      competingInterests: 'Competing Interest Statement',
      type: {
         refereeReport: 'Referee report',
         editorReport: 'Editor report',
         authorComment: 'Author comment',
         communityComment: 'Community comment',
         aggregateReport: 'Aggregate report',
         recommendation: 'Recommendation',
      },
      recommendationType: {
         majorRevision: 'Major revision',
         minorRevision: 'Minor revision',
         reject: 'Reject',
         resubmit: 'Reject and resubmit',
         accept: 'Accept',
         acceptReservations: 'Accept with reservations',
      }
   },
   moderation: {
      pending: 'Pending Approval',
      approved: 'Approved',
      rejected: 'Rejected — Action Required',
      withdrawn: 'Withdrawn',
   },
   userDashboardTitle: 'Submitted Works',
};

const makeEnglishDynamicWorkReplacements = (context: MarXivDataWithoutSitemap) => (d: StaticWorkText) => (work: ActiveWorkData): DynamicWorkText => {
   const tombstoneNotice: string = work.withdrawnDate ? 'This work was withdrawn on ' + work.withdrawnDate : 'This work has been withdrawn';
   const copyrightHolder: string = work.copyrightHolder ? work.copyrightHolder : 'The Authors';
   let year = work.originalPublicationDate ? work.originalPublicationDate.year : '2019';
   const copyrightNoLicense: string = "This work is © " + year + ' ' + copyrightHolder + '. It has not been licensed for reuse, distribution, or modification.';
   const noLicense: HastElement = make.element({ tag: 'div', properties: { id: 'licenseString' }, children: [
      make.text(copyrightNoLicense)
   ]});
   let licenseLink = work.license ? translateLicenseLink(d)(work) : undefined;
   let licenseName = work.license ? translateLicenseName(d)(work) : '';
   const licensed: HastElement = make.element({ tag: 'div', properties: { id: 'licenseString' }, children: [
      make.text("This work is © " + year + ' ' + copyrightHolder + '. It is made available under the '),
      make.element({ tag: 'a', properties: { href: licenseLink }, children: [
         make.text(licenseName)
      ]}),
      make.text(' license.')
   ]});
   return {
      ...d,
      copyright: {
         none: noLicense,
         licensed: licensed
      },
      tombstoneNotice: make.element({ tag: 'p', children: [
         make.text(tombstoneNotice)
      ]})
   }
}

export const makeEnglishWorkText = (context: MarXivDataWithoutSitemap) => (work: ActiveWorkData): WorkText => ({
   static: englishStaticWorkText,
   dynamic: makeEnglishDynamicWorkReplacements(context)(englishStaticWorkText)(work)
})

/**
 * Share / Submit Guide
 */
export const makeEnglishSubmitGuideText = (context: MarXivDataWithoutSitemap): SubmitGuideText => ({
   // Splash page
   spashDescription: 'First we need to know about you, the submitter, so we can automatically fill-in required contributor metadata for Crossref.',
   splashLogin: 'Login to an existing MarXiv/OCTO user account',
   splashRegister: 'Tell us about yourself',
   // Common elements
   shareSomethingElse: 'Something else',
   shareTitle: 'Submit a Work to MarXiv',
   shareSubtitle: 'Ready to share your research with the world? Great! Click the option below that matches what you would like to share.',
   // Published or Not
   share1OptionUnpublished: 'Unpublished paper, manuscript, or abstract',
   share1OptionPublished: 'Paper which has already been published or accepted for publication',
   // Options 2
   share2OptionReport: 'Report',
   share2OptionThesis: 'Thesis or dissertation',
   share2OptionConference: 'Conference, conference paper, or proceedings',
   share2OptionDataset: 'Dataset or database',
   // Options 3
   share3OptionPoster: 'Poster, presentation, or supplementary information',
   share3OptionLetter: 'Letter',
   share3OptionPaper: 'Working paper',
   share3OptionReview: 'Peer review',
   share3OptionReturn: "That's all our options! Return to the beginning",
   // OA or Paywalled paper
   shareOA: 'Open Access publication, or a manuscript which has been accepted for publication in an Open Access journal',
   sharePaywalled: 'Paper or accepted manuscript which was published in a traditional pay-walled or copyrighted journal',
   // Published Paper options
   sharePublishedOptions: make.element({ tag: 'div', children: [
      make.element({ tag: 'h3', children: [
         make.text('If your paper has already been published, you should have signed a '),
         make.element({ tag: 'span', properties: { className: 'underline' }, children: [
            make.text('Copyright Transfer Agreement (CTA)')
         ]}),
         make.text('.')
      ]}),
      makeOrderedList({
         listTitle: make.element({ tag: 'p', children: [
            make.text('Your CTA will state what you can (and cannot) do with:'),
         ]}),
         listContent: [
            make.element({ tag: 'span', children: [
               make.text('Your '),
               make.element({ tag: 'span', properties: { className: 'strong underline' }, children: [
                  make.text('Preprint')
               ]}),
               make.text("  — the manuscript you originally submitted to the journal;")
            ]}),
            make.element({ tag: 'span', children: [
               make.text('Your '),
               make.element({ tag: 'span', properties: { className: 'strong underline' }, children: [
                  make.text('Postprint')
               ]}),
               make.text("  — the manuscript which contains edits from the journal's peer-review process (but not the journal's typesetting or logos);")
            ]}),
            make.element({ tag: 'span', children: [
               make.text('The '),
               make.element({ tag: 'span', properties: { className: 'strong underline' }, children: [
                  make.text('Version of Record (VoR)')
               ]}),
               make.text("  — the publisher's final, typeset, PDF.")
            ]}),
         ]
      }),
      make.element({ tag: 'p', children: [
         make.text("The CTA will normally transfer all copyrights from you (the author) to your publisher. This means you have limited legal right for what you can do with these versions of your work. Please consult your CTA for your specific legal right. "),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.selfArchivingPolicies) }, children: [
            make.text('We also maintain a list of self-archiving policies for major publishers')
         ]}),
         make.text(', which represent the "default" sharing policies for CTAs.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Most publishers (Elsevier, Springer Nature, Wiley, Taylor and Francis, etc.) will allow you to share your '),
         make.element({ tag: 'span', properties: { className: 'emphasis' }, children: [
            make.text('preprint')
         ]}),
         make.text(' and '),
         make.element({ tag: 'span', properties: { className: 'emphasis' }, children: [
            make.text('postprint')
         ]}),
         make.text(', but '),
         make.element({ tag: 'span', properties: { className: 'strong emphasis underline' }, children: [
            make.text('in nearly all circumstances it is illegal to share the Version of Record (VoR)')
         ]}),
         make.text('.'),
      ]}),
      make.element({ tag: 'h4', children: [
         make.text('This leaves you with two options for sharing your work:')
      ]}),
      make.element({ tag: 'h5', children: [
         make.text('1. Share your '),
         make.element({ tag: 'span', properties: { className: 'emphasis' }, children: [
            make.text('preprint')
         ]}),
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Nearly all publishers allow authors to share preprints immediately, provided the author inserts a note that the final version may be found on the publisher's website.")
      ]}),
      make.element({ tag: 'p', children: [
         make.text("There are usually no restrictions on licensing, so we encourage authors to license their work openly.")
      ]}),
      make.element({ tag: 'div', properties: { className: 'flex all-parent-column center' }, children: [
         makeOptionButton({
            text: 'Share your preprint now',
            link: 'uploadPreprint.html'
         })
      ]}),
      make.element({ tag: 'h5', children: [
         make.text('2. Share your '),
         make.element({ tag: 'span', properties: { className: 'emphasis' }, children: [
            make.text('postprint')
         ]}),
      ]}),
      makeBulletList({
         listTitle: make.element({ tag: 'p', children: [
            make.text("Most publishers (Springer Nature, Wiley, Taylor and Francis, etc.) allow authors to share postprints after an embargo period so the publisher can charge for exclusive access during this time:")
         ]}),
         listContent: [
            make.text("The typical embargo period is 1-2 years. Embargo periods for the social-sciences are typically longer than those for the natural-sciences."),
            make.element({ tag: 'span', children: [
               make.text('Publishers often restrict how postprints may be licensed. For example, Elsevier mandates that all postprints are licensed "CC BY-NC-ND". '),
               make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.licenses) }, children: [
                  make.text('Restrictive licenses like this prevent your work from being translated or being used in many other ways')
               ]}),
               make.text(', so we encourage authors to share preprints instead.')
            ]}),
            make.text('If your embargo period has already passed, or you have published with Elsevier:')
         ]
      }),
      make.element({ tag: 'div', properties: { className: 'flex all-parent-column center' }, children: [
         makeOptionButton({
            text: 'Share your postprint now',
            link: 'uploadPostprint.html'
         })
      ]}),
      makeBulletList({
         listTitle: make.element({ tag: 'h4', children: [
            make.text('If you still retain your copyright or you did not sign a CTA, which can happen for a number of reasons...')
         ]}),
         listContent: [
            make.text('if your paper is a work of the US, Canadian, or other national government;'),
            make.text('your university or employer has an Open Access policy that prohibits copyright transfers;'),
            make.text('you amended the standard publishing agreement, etc., then')
         ]
      }),
      make.element({ tag: 'div', properties: { className: 'flex all-parent-column center' }, children: [
         makeOptionButton({
            text: 'Share your openly- or un-copyrighted work now',
            link: 'uploadCopyrighted.html'
         })
      ]})
   ]}),
   shareReportOptions: make.element({ tag: 'div', children: [
      make.element({ tag: 'h3', children: [
         make.text('Reports can be pubished in one of two ways:'),
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Reports can be shared in MarXiv as "posted content", the same category used for preprints and other unpublished works.'),
      ]}),
      make.element({ tag: 'p', children: [
         make.text("However, organizations have the legal right to formally publish their reports, providing additional metadata for discovery and gaining control over DOI names and resolution. MarXiv charges a fee for this service to help us offset the costs of sharing other content for free."),
      ]}),
   ]}),
   shareReportUnpublished: 'Share an unpublished report',
   shareReportPublished: 'Learn more about organizational publishing options',
   // Conference options
   shareConferenceOptions: make.element({ tag: 'div', children: [
      make.element({ tag: 'h3', children: [
         make.text('Conference-related works can be published in one of two ways:'),
      ]}),
      make.element({ tag: 'p', children: [
         make.text('Anyone can submit a conference-related work unofficially as a preprint.'),
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Crossref requires published conference papers to be associated with official Proceedings. However, only a conference's organizers have the legal right to publish official Proceedings."),
      ]}),
      make.element({ tag: 'p', children: [
         make.text("Publishing official Proceedings and Conference Papers provides additional metadata for discovery and grants conference organizers control over DOI names and resolution. MarXiv charges a fee for this service to help us offset the costs of sharing other content for free."),
      ]}),
   ]}),
   shareConferenceUnpublished: 'Share an unpublished conference-related work as a preprint',
   shareConferencePublished: 'Learn more about conference publishing options',
   // Dataset and Database options
   shareDatabaseOptions: make.element({ tag: 'div', children: [
      make.element({ tag: 'h2', children: [
         make.text('Crossref requires that '),
         make.element({ tag: 'span', properties: { className: 'emphasis' }, children: [
            make.text('datasets')
         ]}),
         make.text(' are attached to a published '),
         make.element({ tag: 'span', properties: { className: 'emphasis' }, children: [
            make.text('database')
         ]}),
         make.text('.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('A '),
         make.element({ tag: 'span', properties: { className: 'emphasis' }, children: [
            make.text('database')
         ]}),
         make.text(' is the metadata (title, contributors, etc.) for a collection of '),
         make.element({ tag: 'span', properties: { className: 'emphasis' }, children: [
            make.text('datasets')
         ]}),
         make.text('.')
      ]}),
      make.element({ tag: 'p', children: [
         make.text('To share an individual dataset, you must first describe the database or collection the dataset belongs to. With the DOI of the published database, you will be able to share related datasets.')
      ]})
   ]}),
   shareDatabase: 'Describe a Database or Collection of Datasets',
   shareDataset: 'Share a Dataset',
})

/**
 * Registration form
 */
export const englishJobOptions: JobOptions = {
   researcher: "Researcher",
   conservationist: "Conservationist",
   undergrad: "Student, undergraduate",
   grad: "Student, graduate",
   student: "Student, other",
   comms: "Communications or Outreach",
   teaching: "Teaching or Education",
   naturalRes: "Natural Resource Manager",
   projectManager: "Community or Project Manager",
   policyAnalysis: "Policy Analysis",
   consultant: "Consultant",
   policy: "Policy-making",
   fisher: "Commercial Fisher or Ocean Farmer",
   regulator: "Regulator",
   recFisher: "Recreational Fisher",
   citSci: "Citizen Scientist",
   dataScience: "Data Science, Statistics, or Mathematics",
   chem: "Chemistry or Biochemistry",
   atmos: "Atmospheric Sciences, Oceanography, or Physics",
   fisheries: "Fisheries Policy or Management",
   biology: "Biology or Marine Biology",
   psychology: "Psychology or Sociology",
   political: "Political Science",
   econ: "Economics",
   lobby: "Lobbyist",
   otherSocialSci: "Other Social Science",
   otherNatSci: "Other Natural Science",
   otherInter: "Other Interdisciplinary Science",
   otherNonSci: "Other (Non-Science)",
   journalist: "Journalist or Reporter",
   charity: "Charitable Foundation Officer or Aid Official",
   military: "Military",
   extraction: "Natural Resource Extraction"
}

export const makeEnglishRegistrationFormText = (context: MarXivDataWithoutSitemap): RegistrationFormText => ({
   username: 'Username',
   emailAddress: 'Email address',
   confirmEmailAddress: 'Confirm email address',
   password: 'Password',
   confirmPassword: 'Confirm password',
   givenName: 'Given name',
   surName: 'Surname',
   organization: 'Organization',
   jobFocus: 'Job focus',
   orcid: 'ORCID',
   country: 'Country',
   timezone: 'Timezone',
   newsletter: 'Would you like to subscribe to the MarXiv newsletter, sharing the latest research added to MarXiv each month?',
   terms: make.element({ tag: 'div', properties: { className: 'disclaimer center' }, children: [
      make.element({ tag: 'div', children: [
         make.text('Please note that by creating a MarXiv/OCTO account, you agree to our '),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.privacyPolicy) }, children: [
            make.text('privacy policy')
         ]}),
         make.text(' and '),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.codeOfConduct) }, children: [
            make.text('code of conduct')
         ]}),
         make.text('.')
      ]}),
      make.element({ tag: 'div', children: [
         make.text("TL;DR: We don't sell your data to anyone, and if you engage in bad behavior we will ban you.")
      ]})
   ]}),
   // Defaults
   usernameDefault: 'natalia.narwhal',
   emailAddressDefault: 'natalia.narwhal@marxiv.org',
   passwordDefault: 'OCTO recommends using a password manager',
   givenNameDefault: 'Natalia',
   surNameDefault: 'Narwhal',
   organizationDefault: 'University of the Sea',
   orcidDefault: '0000-0002-0061-9749'
})

export const makeEnglishJoinText = (context: MarXivDataWithoutSitemap) => ({
   formTitle: 'Create a new MarXiv/OCTO account',
   description: '',
   jobOptions: englishJobOptions,
   fields: makeEnglishRegistrationFormText(context)
})

/**
 * User login form
 */
export const englishLoginForm = {
   formTitle: 'Login',
   usernameOrEmail: 'Username or Email Address',
   usernameOrEmailDefault: 'e.g. iluvmarxiv or iluvmarxiv@octogroup.org'
}

/**
 * Reset password form
 */
export const englishResetPassword = {
   formTitle: 'Reset Password',
   description: 'Forgot your password? Input your email address and we will send you a link to reset it.'
}

/**
 * User Logout
 */
export const englishLogout = {
   formTitle: 'Logout'
}

/**
 * Submission forms
 */
export const makeEnglishSubmissionForms = (context: MarXivDataWithoutSitemap): SubmissionFormText => ({
   topDisclaimer: make.element({ tag: 'div', properties: { className: 'disclaimer'}, children:[
      make.element({ tag: 'p', children: [
         make.text('All documents added to MarXiv are immediately publicly accessible. However, the MarXiv Team reviews all submissions. In the event your submission violates our '),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.submissionGuidelines) }, children: [
            make.text('Submission Guidelines')
         ]}),
         make.text(', your paper will be removed from MarXiv. Violations of the '),
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.codeOfConduct) }, children: [
            make.text('MarXiv Code of Conduct ')
         ]}),
         make.text(' may result in a permanent ban from sharing content in MarXiv. DOIs are minted after acceptance.'),
      ]}),
      make.element({ tag: 'h6', properties: { className: 'verticalBuffer2' }, children: [
         make.text('By posting a work to MarXiv, you are contributing to the scholarly record. Works cannot be removed after acceptance. You affirm you have the right to share the work publicly, and you consent for the work to be archived, shared, and displayed by OCTO in perpetuity.'),
      ]}),
   ]}),
   titles: {
      preprint: 'Submit a Preprint',
      postprint: 'Submit a Postprint',
      openAccess: 'Submit an Open Access Work',
      unCopyrighted: 'Submit an Openly- or Un-copyrighted Work',
      unpublishedReport: 'Submit an Unpublished Report',
      publishedReport: 'Submit a Report',
      thesis: 'Submit a Thesis',
      database: 'Submit a Database',
      dataset: 'Submit a Dataset',
      peerReview: 'Submit a Peer Review',
      workingPaper: 'Submit a Working Paper',
      letter: 'Submit a Letter',
      posterPresentationSI: 'Submit a Poster, Presentation, or Supplementary Information',
      publishedConference: 'Submit a Published Conference Paper',
   },
   files: {
      // generic: {
      //    or: 'or',
      //    formTitle: 'PDF File or Compressed Archive',
      //    dragAndDrop: 'Drag and Drop File Here',
      //    chooseFile: 'Choose File',
      //    miniDescription: 'Upload your work here.',
      // },
      pdf: {
         formTitle: 'PDF File',
         dragAndDrop: 'Drag and Drop PDF Here',
         choosePDF: 'Choose PDF File',
         or: 'or',
         miniDescription: 'Upload the PDF of your work here.',
      },
      uploadingFile: 'Uploading file',
      uploadingError: 'Error uploading file. Please check your internet connection and try again.',
      cancelUpload: 'Cancel Upload',
      removeFile: 'Remove File',
      // dataset: {
      //    formTitle: 'Dataset Files',
      //    dragAndDrop: 'Drag and Drop a Compressed Archive Here',
      //    chooseCompressedArchive: 'Choose Compressed Archive File',
      //    miniDescription: 'Upload your dataset here.',
      // },
      editDescription: 'You may provide a new version of your original work here. Please note that existing files are not deleted, but retained as a prior version.',
   },
   basic: {
      formTitle: 'Basic Information',
      formTitleDataset: 'Dataset Information',
      formTitleDatabase: 'Database Information',
      formMinidescription: 'Provide a title, description, and other information',
      title: {
         fieldTitle: 'Title',
         default: 'Title of your work',
         defaultDataset: 'Title of the dataset',
         defaultDatabase: 'Title of the database or collection',
      },
      publisherDOI: {
         fieldTitle: "Publisher's DOI",
         default: '10.31230/osf.io/kfvx9',
      },
      type: {
         fieldTitle: 'Type of Work',
         preprint: 'Preprint',
         workingPaper: 'Working Paper',
         letter: 'Letter',
         thesis: 'Thesis or Dissertation',
         report: 'Report',
         other: 'Other',
      },
      peerReview: {
         fieldTitle: 'Has the content of this work undergone formal peer review?',
         yes: 'Yes, this content has been formally peer-reviewed',
         no: 'No, this content has not been formally peer-reviewed',
      },
      // database: {
      //    createdDate: 'Created Date',
      //    updatedDate: 'Updated Date',
      // },
      originalPublicationDate: {
         fieldTitle: 'Original Publication Date',
         year: {
            fieldTitle: 'Year',
            default: '2019',
         },
         month: {
            fieldTitle: 'Month',
            default: '10',
         },
         day: {
            fieldTitle: 'Day',
            default: '6',
         }
      },
      description: {
         fieldTitle: 'Description',
         default: 'Abstract or summary describing your work',
         defaultDatabase: 'Describe the database or collection',
         defaultDataset: 'Describe the dataset',
      },
      optional: {
         fieldTitle: 'Optional information',
         miniDescription: 'Provide the language and/or subtitle',
         language: {
            fieldTitle: 'Language',
         },
         subtitle: {
            fieldTitle: 'Subtitle',
            default: 'Subtitle of your work. Omit colons and semicolons.',
         },
      },
   },
   contributors: {
      formTitle: 'Contributors',
      description: make.element({ tag: 'div', properties: { className: 'field' }, children: [
         make.element({ tag: 'div', properties: { className: 'description' }, children: [
            make.element({tag: 'p', children: [
               make.text('Listing contributors helps your co-authors know their work had an impact.')
            ]}),
            make.element({ tag: 'p', children: [
               make.text("You are automatically included as the first author of this work. If you should not be listed as an author, click the X next to your name below to remove yourself. You may click an author's name to edit their information, or the X next to their name to remove them.")
            ]}),
            make.element({ tag: 'p', children: [
               make.text('Please provide the email addresses for contributors who should be invited to co-administer this deposit. Doing so allows your fellow contributors to modify this deposit in the future. Contributors whom have already registered with MarXiv/OCTO will be granted immediate administrative access.')
            ]})
         ]})
      ]}),
      previewDefault: 'Added contributors will appear here.',
      contributor: {
         formTitle: 'Contributor',
         add: 'Add/Update Contributor',
         affiliation: {
            fieldTitle: 'Affiliation',
            default: 'University of the Sea'
         },
         suffix: {
            fieldTitle: 'Suffix',
            default: 'Jr',
         },
         nameStyle: {
            fieldTitle: 'Name style',
            default: 'western',
            western: 'Western',
            eastern: 'Eastern',
            islensk: 'Islensk',
            givenOnly: 'Given only',

         },
         role: {
            fieldTitle: 'Role',
            default: 'author',
            author: 'Author',
            editor: 'Editor',
            chair: 'Chair',
            reviewer: 'Reviewer',
            reviewerAst: 'Review assistant',
            statsReviewer: 'Stats reviewer',
            externalReviewer: 'External reviewer',
            reader: 'Reader',
            translator: 'Translator',
         },
         firstAuthor: 'First author',
         additionalAuthor: 'Additional author',
         order: 'Order of this contributor in the list'
      },
      organization: {
         isOrg: 'If this contributor is an organization, check this box',
         fieldTitle: 'Organization name',
         default: 'OCTO: Open Communications for The Ocean',
      },
   },
   // publisher: {
   //    formTitle: 'Publisher',
   //    description: 'While Crossref allows up to 5 sponsoring institutions, only one publisher is allowed.',
   //    descriptionDatabase: 'Who is the publisher of this database or collection of datasets?',
   //    name: {
   //       formTitle: 'Publisher name',
   //       default: 'OCTO'
   //    },
   //    location: {
   //       formTitle: 'Publisher location',
   //       default: 'Seattle, Washington, USA'
   //    },
   // },
   // citations: {
   //    formTitle: 'Citations',
   //    description: make.element({ tag: 'div', properties: { className: 'field' }, children: [
   //       make.element({ tag: 'div', properties: { className: 'description' }, children: [
   //          make.text('Describing your citations in metadata allows your cited authors to know their work had an impact. It also helps inform you if any of your citations are retracted or modified in the future.'),
   //          make.element({ tag: 'p', properties: { className: 'strong' }, children: [
   //             make.text('Providing the DOI is enough to identify each citation. You only need to provide detailed information if the citation lacks a DOI, or if you only used a part of the citation.')
   //          ]})
   //       ]})
   //    ]}),
   //    previewDefault: 'Added citations will appear here.',
   //    citation: {
   //       formTitle: 'Citation',
   //       add: 'Add citation',
   //       doi: {
   //          fieldTitle: 'DOI',
   //          default: '10.31230/osf.io/kfvx9',
   //       },
   //       isbn: {
   //          fieldTitle: 'ISBN',
   //          default: '978-3-16-148410-0',
   //       },
   //       issn: {
   //          fieldTitle: 'ISSN',
   //          default: '2049-3630',
   //       },
   //       articleTitle: {
   //          fieldTitle: 'Article title',
   //          default: 'From measuring outcomes to providing inputs: Governance, management, and local development for more effective marine protected areas',
   //       },
   //       journalTitle: {
   //          fieldTitle: 'Journal title',
   //          default: 'Frontiers in Marine Science',
   //       },
   //       surname: {
   //          fieldTitle: 'Cvitanovic',
   //          default: 'Surname of the first author',
   //       },
   //       volume: {
   //          fieldTitle: 'Volume',
   //          default: '42',
   //       },
   //       issue: {
   //          fieldTitle: 'Issue',
   //          default: '6',
   //       },
   //       page: {
   //          fieldTitle: 'First page',
   //          default: '128',
   //       },
   //       year: {
   //          fieldTitle: 'Year',
   //          default: '1986',
   //       },
   //       edition: {
   //          fieldTitle: 'Edition number',
   //          default: '3',
   //       },
   //       component: {
   //          fieldTitle: 'Component number',
   //          default: '2',
   //       },
   //       seriesTitle: {
   //          fieldTitle: 'Series title',
   //          default: 'Effects from ocean noise',
   //       },
   //       volumeTitle: {
   //          fieldTitle: 'Volume title',
   //          default: 'Mainstreaming the social sciences in conservation',
   //       },
   //       unstructured: {
   //          fieldTitle: 'Unstructured citation',
   //          default: 'If you need to cite something other than a journal article, book, or conference paper, please cite the work here.',
   //       },
   //    },
   // },
   // institutions: {
   //    formTitle: 'Institutional Information',
   //    description: make.element({ tag: 'div', properties: { className: 'field' }, children: [
   //       make.element({ tag: 'div', properties: { className: 'description' }, children: [
   //          make.text('You may provide up to 5 institutional sponsors or hosts.')
   //       ]})
   //    ]}),
   //    previewDefault: 'Added institutions will appear here.',
   //    institution: {
   //       formTitle: 'Institution',
   //       add: 'Add institution',
   //       name: {
   //          fieldTitle: 'Name',
   //          default: 'Open Communications for The Ocean',
   //       },
   //       acronym: {
   //          fieldTitle: 'Acronym',
   //          default: 'OCTO',
   //       },
   //       location: {
   //          fieldTitle: 'Location',
   //          default: 'Seattle, Washington, USA',
   //       },
   //       department: {
   //          fieldTitle: 'Department',
   //          default: 'Marine and Environmental Affairs',
   //       },
   //    },
   // },
   relationships: {
      formTitle: 'Relationships Between Works',
      description: make.element({ tag: 'div', properties: { className: 'field' }, children: [
         make.element({ tag: 'div', properties: { className: 'description' }, children: [
            make.text("You may link this submission to another work with Crossref's metadata. For each relationship, please provide the DOI of the related item and define how the current submission relates to this item.")
         ]})
      ]}),
      previewDefault: 'Added relationships will appear here.',
      relationship: {
         formTitle: 'Relationship',
         add: 'Add Relationship',
         doi: {
            fieldTitle: 'DOI of related work',
            default: '10.31230/osf.io/kfvx9',
         },
         typeOfRelationship: {
            fieldTitle: 'Type of Relationship',
            relatedMaterial: 'The current submission is related material to',
            reviewOf: 'The current submission is a review of',
            commentOn: 'The current submission is a comment on',
            replyTo: 'The current submission is a reply to',
            derivedFrom: 'The current submission is derived from',
            basedOn: 'The current submission is based on data from',
            basisFor: 'The current submission is the data basis for',
            preprintOf: 'The current submission is the preprint of',
            manuscriptOf: 'The current submission is the manuscript of',
            translationOf: 'The current submission is a translation of',
            replacedBy: 'The current submission is replaced by',
            replaces: 'The current submission replaces',
            sameAs: 'The current submission is the same as',
         },
      },
   },
   license: {
      formTitle: 'License/Copyright',
      description: make.element({ tag: 'div', properties: { className: 'field' }, children: [
         make.element({ tag: 'div', properties: { className: 'description' }, children: [
            make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.licenses), target:'_blank' }, children: [
               make.text('Learn more about license options')
            ]}),
            make.text(' (new tab).')
         ]})
      ]}),
      descriptionPostprint: make.element({ tag: 'div', properties: { className: 'field' }, children: [
         make.element({ tag: 'div', properties: { className: 'description' }, children: [
            make.text('Remember that publishers often restrict how postprints may be licensed. Examples include:'),
            makeBulletList({ listContent: [
               make.text('Elsevier and Wiley require a CC-BY-NC-ND license'),
               make.text('Cambridge University Press requires any CC option'),
               make.text('Nature Publishing Group disallows any of the CC options'),
            ]})
         ]})
      ]}),
      license: {
         formTitle: 'License',
         cc0: 'Public Domain (CC0)',
         afl: 'Academic Free License, V3 (AFL)',
         ccBy: 'Creative Commons Attribution (CC BY)',
         ccBySa: 'Creative Commons Attribution-ShareAlike (CC BY-SA)',
         ccByNd: 'Creative Commons Attribution-NoDerivs (CC BY-ND)',
         ccByNc: 'Creative Commons Attribution-NonCommercial (CC BY-NC)',
         ccByNcSa: 'Creative Commons Attribution-NonCommercial-ShareAlike (CC BY-NC-SA)',
         ccByNcNd: 'Creative Commons Attribution-NonCommercial-NoDerivs (CC BY-NC-ND)',
         gpl: 'GNU General Public License, V3 (GPL)',
         mit: 'MIT License (MIT)',
         ecl: 'Educational Community License, V2 (ECL)',
         none: 'No License',
      },
      noLicense: 'Since you are not granting others a license to this work, please enter the copyright holder below. This should not be your publisher.',
      copyrightHolder: {
         fieldTitle: 'Copyright holder',
         default: 'Crown copyright',
      },
   },
   support: {
      formTitle: 'Support MarXiv',
      paypalLink: 'https://www.paypal.me/openocto/10',
      p1: 'It costs OCTO $10 to store and host your work forever. Making a tax-deductible donation ensures MarXiv will be able to accept new works in the future.',
      button: 'Make a $10 donation to keep MarXiv running',
      p2: 'Donating to OCTO will open a new window. When you are done, please return to this form to finish your submission. We appreciate your donation.',
   },
   // peerReview: {
   //    formTitle: 'Peer Review Information',
   //    description: 'Provide basic information about your Peer Review.',
   //    prePost: {
   //       fieldTitle: 'Was this peer-review conducted before (pre-publication) or after (post-publication) formal publication?',
   //       prePublication: 'Pre-publication',
   //       postPublication: 'Post-publication',
   //    },
   //    doiOfReviewedWork: {
   //       fieldTitle: 'DOI of reviewed work',
   //       default: '10.31230/osf.io/2pwdb',
   //    },
   //    type: {
   //       fieldTitle: 'Type',
   //       refereeReport: 'Referee report',
   //       editorReport: 'Editor report',
   //       authorComment: 'Author comment',
   //       communityComment: 'Community comment',
   //       aggregateReport: 'Aggregate report',
   //       recommendation: 'Recommendation',
   //    },
   //    recommendation: {
   //       fieldTitle: 'Recommendation',
   //       majorRevision: 'Major revision',
   //       minorRevision: 'Minor revision',
   //       reject: 'Reject',
   //       resubmit: 'Reject and resubmit',
   //       accept: 'Accept',
   //       acceptReservations: 'Accept with reservations',
   //    },
   //    competingInterests: {
   //       fieldTitle: 'Competing interest statement',
   //       default: 'Please disclose any competing or conflicting interests.',
   //    },
   // },
   // databaseDOI: {
   //    formTitle: 'Database DOI',
   //    description: make.element({ tag: 'div', properties: { className: 'field' }, children: [
   //       make.element({ tag: 'div', properties: { className: 'description' }, children: [
   //          make.text('Crossref requires that datasets are attached to published databases. If you have not published your database or described your collection of datasets yet, '),
   //          make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.submit.database) }, children: [
   //             make.text('click here to publish your database first')
   //          ]}),
   //          make.text('.')
   //       ]})
   //    ]}),
   //    databaseDOI: {
   //       fieldTitle: 'Database DOI',
   //       default: '10.31230/osf.io/kfvx9',
   //    },
   // },
   // conferenceEvent: {
   //    formTitle: 'Conference Event',
   //    description: 'Provide basic information about the conference itself.',
   //    conferenceName: {
   //       fieldTitle: 'Conference name',
   //       default: 'International Marine Conservation Congress',
   //    },
   //    theme: {
   //       fieldTitle: 'Theme',
   //       default: 'Make Marine Science Matter',
   //    },
   //    acronym: {
   //       fieldTitle: 'Acronym',
   //       default: 'IMCC5',
   //    },
   //    number: {
   //       fieldTitle: 'Number',
   //       default: '5',
   //    },
   //    sponsor: {
   //       fieldTitle: 'Sponsor',
   //       default: 'Society for Conservation Biology',
   //    },
   //    location: {
   //       fieldTitle: 'Location',
   //       default: 'Kuching, Sarawak, Malaysia',
   //    },
   //    dates: {
   //       formTitle: 'Dates',
   //       startYear: {
   //          fieldTitle: 'Start year',
   //          default: '2018',
   //       },
   //       startMonth: {
   //          fieldTitle: 'Start month',
   //          default: '06',
   //       },
   //       startDay: {
   //          fieldTitle: 'Start day',
   //          default: '24',
   //       },
   //       endYear: {
   //          fieldTitle: 'End year',
   //          default: '2018',
   //       },
   //       endMonth: {
   //          fieldTitle: 'End month',
   //          default: '06',
   //       },
   //       endDay: {
   //          fieldTitle: 'End day',
   //          default: '29',
   //       },
   //    },
   // },
   // conferencePaper: {
   //    formTitle: 'Conference Paper',
   //    description: 'Provide information about your conference paper.',
   //    type: {
   //       fieldTitle: 'Are you submitting the full-text or just an abstract?',
   //       fullText: 'Full-text',
   //       abstract: 'Abstract'
   //    },
   //    firstPage: {
   //       fieldTitle: 'First page',
   //       default: '1'
   //    },
   //    lastPage: {
   //       fieldTitle: 'Last page',
   //       default: '42'
   //    }
   // },
   assignDOI: {
      formTitle: 'Assign DOI',
      description: make.element({ tag: 'div', properties: { className: 'field' }, children: [
         make.element({ tag: 'div', properties: { className: 'description' }, children: [
            make.text('MarXiv automatically generates DOI suffixes (the text after "10.12686/") for all works. If you like, you may enter a custom DOI suffix below. DOIs are not case-sensitive.')
         ]})
      ]}),
      marxivDOI: {
         fieldTitle: 'MarXiv DOI',
         default: 'MarXiv.org/works/[id]',
      },
      doiReplacement: 'The DOI for this work will be ',
      marxivPrefix: '10.12686/',
   }
})

/**
 * Edit forms
 */
export const englishEditForm = {
   titles: {
      preprint: 'Edit a Preprint',
      postprint: 'Edit a Postprint',
      openAccess: 'Edit an Open Access Work',
      unCopyrighted: 'Edit an Openly- or Un-copyrighted Work',
      unpublishedReport: 'Edit an Unpublished Report',
      publishedReport: 'Edit a Report',
      thesis: 'Edit a Thesis',
      database: 'Edit a Database',
      dataset: 'Edit a Dataset',
      peerReview: 'Edit a Peer Review',
      workingPaper: 'Edit a Working Paper',
      letter: 'Edit a Letter',
      posterPresentationSI: 'Edit a Poster, Presentation, or Supplementary Information',
      publishedConference: 'Edit a Published Conference Paper',
   },
   rejectedNote: make.element({ tag: 'p', properties: { className: 'description' }, children: [
      make.element({ tag: 'div', properties: { className: 'rejected' }, children: [
         make.text('This work was rejected from MarXiv. Please address the issues noted below before resubmitting this form:')
      ]})
   ]}),
   editNote: make.element({ tag: 'p', properties: { className: 'description' }, children: [
      make.text('Edits to approved works are updated immediately.')
   ]}),
}
 
/**
 * Moderation forms
 */
export const makeEnglishModerationForms = (context: MarXivDataWithoutSitemap): ModerateFormText => ({
   titles: {
      preprint: 'Moderate a Preprint',
      postprint: 'Moderate a Postprint',
      openAccess: 'Moderate an Open Access Work',
      unCopyrighted: 'Moderate an Openly- or Un-copyrighted Work',
      unpublishedReport: 'Moderate an Unpublished Report',
      publishedReport: 'Moderate a Report',
      thesis: 'Moderate a Thesis',
      database: 'Moderate a Database',
      dataset: 'Moderate a Dataset',
      peerReview: 'Moderate a Peer Review',
      workingPaper: 'Moderate a Working Paper',
      letter: 'Moderate a Letter',
      posterPresentationSI: 'Moderate a Poster, Presentation, or Supplementary Information',
      publishedConference: 'Moderate a Published Conference Paper',
   },
   messageToSubmitter: {
      formTitle: 'Message to Submitter',
      description: make.element({ tag: 'div', properties: { className: 'field' }, children: [
         make.element({ tag: 'div', properties: { className: 'description' }, children: [
            make.text('The following text will be included in the confirmation message sent to the user when this work is accpeted or rejected.')
         ]})
      ]}),
      message: {
         fieldTitle: 'Message',
         default: 'Thank you for sharing!',
      }
   },
   octoWorkType: {
      fieldTitle: 'OCTO Work Type',
      badgeTypeFieldTitle: 'Badge Type'
   },
   vorCost: {
      fieldTitle: 'VoR Cost',
      default: '39.95',
   },
   moderateExtras: {
      formTitle: 'Moderation Extras',
      miniDescription: 'Provide metadata for the submission',
   },
})

/**
 * User account forms
 */
export const userAccountForm = {
   title: 'MarXiv/OCTO User Account Information',
   editButton: 'Edit your user account',
   editTitle: 'Update Your MarXiv/OCTO User Account Information',
   jobFocus: 'Job focus'
} 

/**
 * Page Metadata
 */
export const englishPageMetadata: TranslatablePageMetadata = {
   homepage: {
      title: 'MarXiv: The free repository for ocean and marine-climate science',
      description: 'With MarXiv, authors and researchers may share preprints and postprints of academic journal articles to make their research freely-available with Green Open Access.'
   },
   whyMarXiv: {
      title: 'Why Should You Share Your Research in MarXiv?',
      description: 'Most peer-reviewed academic journal articles require a subscription to access the full-text for free, which makes ocean and climate-change research inaccessble to anyone outside academia, including managers and policymakers.'
   },
   team: {
      title: 'The MarXiv Team',
      description: 'MarXiv is produced by OCTO: Open Communications for The Ocean.'
   },
   ambassadors: {
      title: 'MarXiv Ambassadors',
      description: 'MarXiv Ambassadors are volunteers who help raise awareness of open science within their communities.'
   },
   workListing: {
      title: 'Works Shared in MarXiv',
      description: 'A complete listing of all the papers, reports, and other works shared in MarXiv.'
   },
   forOrgs: {
      title: 'MarXiv for Organizations',
      description: 'MarXiv offers publishing and IT services to help organizations publish reports and conference-related works.'
   },
   submit: {
      title: 'Share Your Work in MarXiv',
      description: 'Submit a paper, poster, dataset, thesis, abstract, or other scholarly work to MarXiv.'
   },
   selfArchivingPolicies: {
      title: 'Self-Archiving Policies of Major Ocean- and Climate-related Publishers',
      description: 'Most academic publishers allow authors to "self-archive" their work in repositories like MarXiv to make their work available for free with Green Open Access.'
   },
   submissionGuidelines: {
      title: 'MarXiv Submission Guidelines',
      description: 'Full guidelines on how to submit a paper to MarXiv.'
   },
   // Google Search
   googleSearch: {
      title: 'Search MarXiv',
      description: 'Search for works and content in the MarXiv repository for ocean and marine-climate science.'
   },
   // Minor pages
   codeOfConduct: {
      title: 'MarXiv Code of Conduct',
      description: 'The MarXiv Code of Conduct sets policy for the responsibilities of submitters.'
   },
   privacyPolicy: {
      title: 'MarXiv Privacy Policy',
      description: 'Full privacy policy for the MarXiv website, including references to services we use.'
   },
   celebrate: {
      title: 'Celebrate!',
      description: 'Thank you for sharing your work in MarXiv!',
      
   },
   licenses: {
      title: 'MarXiv License Options',
      description: 'Full descriptions of the licenses authors may choose from, including Creative Commons.'
   },
   typesOfWork: {
      title: 'Types of Works Accepted',
      description: 'Required and recommended metadata to include when sharing works in MarXiv.'
   },
   // User menu
   join: {
      title: 'Join MarXiv',
      description: 'Create a MarXiv account to share your work with the ocean and climate-science community.'
   },
   login: {
      title: 'Login to MarXiv',
      description: 'Login to your MarXiv user account.'
   },
   resetPassword: {
      title: 'Reset your MarXiv Password',
      description: 'Reset the password to your MarXiv user account.'
   },
   logout: {
      title: 'Logout',
      description: 'Logout of your MarXiv user account'
   },
   // User pages
   user: {
      dashboard: {
         title: 'MarXiv Dashboard',
         description: 'Lists your shared works in MarXiv.'
      },
      account: {
         title: 'Your MarXiv User Account',
         description: 'Information from your MarXiv user account.'
      },
      editAccount: {
         title: 'Update Your MarXiv User Account',
         description: 'Edit your MarXiv user account.'
      },
   },
   // General confirmation pages (no need to make specific since google won't be allowed to crawl)
   confirm: {
      newSubmission: {
         title: 'Thank you for your submission',
         description: 'Confirmation that a new work has been submitted to MarXiv.'
      },
      updatedWork: {
         title: 'Your work has been updated',
         description: 'Confirmation that a new work has been updated in MarXiv.'
      },
      resubmit: {
         title: 'Your work has been resubmitted to MarXiv',
         description: 'Confirmation that a rejected work has been re-submitted to MarXiv.'
      },
      newAccount: {
         title: 'You have created a new MarXiv user account',
         description: 'Confirmation of a new MarXiv/OCTO user account.'
      },
      verifyEmail: {
         title: 'Verify your email account',
         description: 'Confirmation of the email address used to create a MarXiv user account.'
      },
   },
   // Upload works
   upload: {
      preprint: {
         title: 'Share a Preprint',
         description: 'Upload a preprint to MarXiv.'
      },
      postprint: {
         title: 'Share a Postprint',
         description: 'Upload a postprint to MarXiv.'
      },
      openAccess: {
         title: 'Share an OA Paper',
         description: 'Upload an Open Access paper to MarXiv.'
      },
      unCopyrighted: {
         title: 'Share an Uncopyrighted Paper',
         description: 'Upload a freely- or un-copyrighted paper to MArXiv.'
      },
      unpublishedReport: {
         title: 'Share a Report',
         description: 'Upload an unpublished report to MarXiv.'
      },
      thesis: {
         title: 'Share a Thesis or Dissertation',
         description: 'Upload a thesis or dissertation to MarXiv.'
      },
      database: {
         title: 'Share a Database',
         description: 'Upload a database to MarXiv.'
      },
      dataset: {
         title: 'Share a Dataset',
         description: 'Upload a dataset to MarXiv.'
      },
      posterPresentationSI: {
         title: 'Share a Poster, Presentation, or Supplemental Work',
         description: 'Upload a conference poster or presentation, or a supplemental work, to MarXiv.'
      },
      letter: {
         title: 'Share an Academic Letter',
         description: 'Upload an academic letter to MarXiv.'
      },
      workingPaper: {
         title: 'Share a Working Paper',
         description: 'Upload a working paper to MarXiv.'
      },
      peerReview: {
         title: 'Share a Peer Review',
         description: 'Upload a peer review or comment to MarXiv.'
      },
      publishedReport: {
         title: 'Publish a Report',
         description: 'Publish a report in MarXiv.'
      },
      publishedConferencePaper: {
         title: 'Publish a Conference-related Paper',
         description: 'Publush a conference paper or conference proceedings in MarXiv.'
      },
   },
   // Edit works
   edit: {
      preprint: {
         title: 'Update a Preprint',
         description: 'Edit a preprint submitted to MarXiv.'
      },
      postprint: {
         title: 'Update a Postprint',
         description: 'Edit a postprint submitted to MarXiv.'
      },
      openAccess: {
         title: 'Update an OA Paper',
         description: 'Edit an Open Access paper submitted to MarXiv.'
      },
      unCopyrighted: {
         title: 'Update an Uncopyrighted Paper',
         description: 'Edit an uncopyrighted paper submitted to MarXiv.'
      },
      unpublishedReport: {
         title: 'Update a Report',
         description: 'Edit a report submitted to MarXiv.'
      },
      thesis: {
         title: 'Update a Thesis or Dissertation',
         description: 'Edit a thesis or dissertation submitted to MarXiv.'
      },
      database: {
         title: 'Update a Database',
         description: 'Edit a database submitted to MarXiv.'
      },
      dataset: {
         title: 'Update a Dataset',
         description: 'Edit a dataset submitted to MarXiv.'
      },
      posterPresentationSI: {
         title: 'Update a Poster, Presentation, or Supplemental Work',
         description: 'Edit a poster, presentation, or supplemental-work submitted to MarXiv.'
      },
      letter: {
         title: 'Update an Academic Letter',
         description: 'Edit an academic letter submitted to MarXiv.'
      },
      workingPaper: {
         title: 'Update a Working Paper',
         description: 'Edit a working paper submitted to MarXiv.'
      },
      peerReview: {
         title: 'Update a Peer Review',
         description: 'Edit a peer-review submitted to MarXiv.'
      },
      publishedReport: {
         title: 'Update a Published Report',
         description: 'Edit a report published in MarXiv.'
      },
      publishedConferencePaper: {
         title: 'Update a Published Conference-related Paper',
         description: 'Edit a conference paper or conference proceedings published in MarXiv.'
      },
   },
   // Moderate works
   moderate: {
      preprint: {
         title: 'Moderate a Preprint',
         description: 'Moderate a preprint submitted to MarXiv.'
      },
      postprint: {
         title: 'Moderate a Postprint',
         description: 'Moderate a postprint submitted to MarXiv.'
      },
      openAccess: {
         title: 'Moderate an OA Paper',
         description: 'Moderate an Open Access paper submitted to MarXiv.'
      },
      unCopyrighted: {
         title: 'Moderate an Uncopyrighted Paper',
         description: 'Moderate an uncopyrighted paper submitted to MarXiv.'
      },
      unpublishedReport: {
         title: 'Moderate a Report',
         description: 'Moderate a report submitted to MarXiv.'
      },
      thesis: {
         title: 'Moderate a Thesis or Dissertation',
         description: 'Moderate a thesis or dissertation submitted to MarXiv.'
      },
      database: {
         title: 'Moderate a Database',
         description: 'Moderate a database submitted to MarXiv.'
      },
      dataset: {
         title: 'Moderate a Dataset',
         description: 'Moderate a dataset submitted to MarXiv.'
      },
      posterPresentationSI: {
         title: 'Moderate a Poster, Presentation, or Supplemental Work',
         description: 'Moderate a poster, presentation, or supplemental-work submitted to MarXiv.'
      },
      letter: {
         title: 'Moderate an Academic Letter',
         description: 'Moderate an academic letter submitted to MarXiv.'
      },
      workingPaper: {
         title: 'Moderate a Working Paper',
         description: 'Moderate a working paper submitted to MarXiv.'
      },
      peerReview: {
         title: 'Moderate a Peer Review',
         description: 'Moderate a peer-review submitted to MarXiv.'
      },
      publishedReport: {
         title: 'Moderate a Published Report',
         description: 'Moderate a report published in MarXiv.'
      },
      publishedConferencePaper: {
         title: 'Moderate a Published Conference-related Paper',
         description: 'Moderate a conference paper or conference proceedings published in MarXiv.'
      },
   },
   // Error pages
   error: {
      notFound: {
         title: 'Page Not Found',
         description: 'The requested page could not be found.'
      },
      accessDenied: {
         title: 'Access Denied',
         description: 'You do not have the proper credentials to access this page.'
      },
      uhoh: {
         title: 'Uh oh',
         description: 'An unknown error has occured.'
      },
   },
   // Admin Dashboard
   admin: {
      title: 'MarXiv Administrative Dashboard',
      description: 'MarXiv Administrative Dashboard'
   },
   adminEditAccount: {
      title: 'Admin Edit User Account',
      description: 'Edit a MarXiv user account'
   },
}

/**
 * Form Messages
 */
export const formMessageText: FormMessageText = {
   errors: {
      usernameOrEmail: 'Invalid username or email address. This field is required.',
      emailAddress: 'Invalid email address. This field is required.',
      legacyDOI: 'Invalid DOI. Only the following special characters may be used: hyphen, period, underscore, semicolon, parentheses, forward-slash, plus, hashtag, and the greater- and-less-than symbols.',
      relationshipType: 'Please select a type of relationship.',
      contribsRequired: 'At least one contributor is required.',
      doi: 'Invalid DOI. Only the following special characters may be used: hyphen, period, underscore, semicolon, parentheses, and forward-slash.',
      passwordShort: 'Passwords must be at least 8 characters long.',
      passwordLong: 'Passwords must be less than 128 characters long.',
      passwordInvalid: 'Invalid password. This field is required.',
      newsletter: 'Newsletter is a required field.',
      title: 'A title is required, and must be less than 255 characters long. HTML is not allowed.',
      description: 'A description is required, and  must be less than 2550 characters long. HTML is not allowed.',
      year: 'Invalid year. It must be between 1900 and 2 years from now.',
      month: 'Invalid month. Number must be between 1 and 12.',
      day: 'Invalid day. Number must be between 1 and 31.',
      date: 'Invalid date. A year is required, but month and day are optional.',
      orcid: 'Invalid ORCID. ORCIDs are 4 sets of 4 digits separated by hyphens, for example 0000-0002-0061-9749.',
      copyright: 'Only alphamumeric characters, commas, hyphens, and spaces are allowed. Your publisher cannot be the copyright holder.',
      emailAddressNoMatch: 'The email addresses you entered do not match.',
      passwordNoMatch: 'The passwords you entered do not match.',
      generalInvlidShortText: 'The text you entered is invalid. HTML is not allowed, and input is limited to less than 255 characters.',
      surname: 'Surname is required. HTML is not allowed, and input is limited to less than 255 characters.',
      givenName: 'Given name is required. HTML is not allowed, and input is limited to less than 255 characters.',
      fileTypePDF: 'Only PDF files are allowed.',
      fileTypeTarball: 'Only compressed archive files are allowed, like .zip and .tar.gz',
      fileTypeEither: 'File must be either a PDF or a compressed archive, like .zip or .tar.gz',
      fileMissing: 'A file is required.',
      license: 'You must select a license.',
      emailInUse: 'This email address is already associated with a MarXiv/OCTO user account. Please login to your existing account.',
      usernameTaken: 'The username you entered is already in-use. Please try a different username.',
      // Moderation Form
      generalRequired: 'This field is required.',
      // File errors
      fileInvalid: 'This file is invalid or corrupted. Please remove this file and try again.',
      fileUploadError: 'Error saving file. Please remove this file and try again.',
   },
   confirmations: {
      fileSaved: 'File saved.'
   },
   loading: {
      loading: 'Loading',
      form: 'Form',
      dashboard: 'Dashboard',
      users: 'Users',
      works: 'Works',
      page: 'Page',
   },
}

/**
 * Combine all our translatables together
 */
// Remove translatables from the context, so when we pass it along, it's not self-referential
export const makeEnglishTranslatables = (context: MarXivDataWithoutSitemap) => (work: ActiveWorkData): Translatables => ({
   commonReplacements: englishCommonReplacements,
   mainMenuText: englishMainMenuText,
   userMenuText: englisherUserMenuText,
   footerText: makeEnglishFooterInformation(context),
   headerText: englishHeaderText,
   pages: {
      homepage: englishHomepageText,
      staticPages: makeEnglishStaticPagesData(context),
      workListing: englishWorkListingText,
      submissionGuidelines: makeEnglishSubmissionGuidelinesContent(context),
      selfArchivingPolicies: makeEnglishSelfArchivingPoliciesContent(context)(englishSAPublishers),
      submitGuide: makeEnglishSubmitGuideText(context)
   },
   works: makeEnglishWorkText(context)(work),
   forms: {
      join: makeEnglishJoinText(context),
      login: englishLoginForm,
      logout: englishLogout,
      resetPassword: englishResetPassword,
      submit: makeEnglishSubmissionForms(context),
      edit: englishEditForm,
      moderate: makeEnglishModerationForms(context),
      userAccount: userAccountForm,
      messages: formMessageText,
      grecaptcha: {
         terms: make.element({ tag: 'div', properties: { id: 'grecaptchaTerms' }, children: [
            make.text("MarXiv uses invisible reCAPTCHA elements to prevent spam submissions. Google offers reCAPTCHA as a free service, therefore Google's "),
            make.element({ tag: 'a', properties: { href: 'https://policies.google.com/privacy' }, children: [
               make.text('Privacy Policy')
            ]}),
            make.text(' and '),
            make.element({ tag: 'a', properties: { href: 'https://policies.google.com/terms' }, children: [
               make.text('Terms of Service')
            ]}),
            make.text(' apply.')
         ]}),
         errorMessage: 'Our servers are unable to respond. Please check that you are contected to the Internet, or wait a moment before trying again.',
         isBotMessage: 'Google reCAPTCHA has determined it is more than likely this is a fraudulent submission. If you are an actual human, try using a different browser. Sorry!',
      },
      dashboards: {
         applyFilters: 'Apply filters',
         removeFilters: 'Remove filters',
         loadMoreWorks: 'Load more works',
         loadMoreUsers: 'Load more users',
         userFilters: 'User Filters',
         workFilters: 'Work Filters',
         emailVerificationStatus: 'Email verification status',
         workID: 'Work ID',
         moderationStatus: 'Moderation status',
         postedContentType: 'Posted Content type',
         marxivDOI: 'MarXiv DOI',
         hasContributorSurname: 'Has Contributor surname',
         submittedBy: 'Submitted by',
         displayWorks: 'Display Works',
         displayUsers: 'Display Users',
         newsletter: 'Newsletter',
      },
   },
   pageMetadata: englishPageMetadata
});