/**
 * Make header compnents for all pages
 */

import { HastElement, make } from '@octogroup/hast-typescript';
import { MarXivDataWithoutSitemap } from './context';
import { Translatables } from './translatables';
import { makeLocalPath } from './common';
import { makeMainMenu, makeUserMenu, makeSubmenu, SubmenuType } from './menus';

/**
 * Import local dictionaries
 */
import { HeaderText, } from './translatables';

/**
 * MarXiv logo
 */
export const makeLogo = (context: MarXivDataWithoutSitemap): HastElement => make.element({ tag: 'div', properties: { id: 'logo'}, children: [
   make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.marxivVert), className: 'marxivLogo' }, children: [] })
]});

/**
 * Homepage header information, with the "Why" + "Submit" + "Papers" pages
 */
export const makeBelowHeaderMobile = (context: MarXivDataWithoutSitemap) => (d: HeaderText): HastElement => make.element({ tag: 'div', properties: { className: 'home mobileInfo' }, children: [
   // Icons
   make.element({ tag: 'div', properties: { className: 'iconWrapper flex all-parent-row center' }, children: [
      make.element({ tag: 'div', properties: { className: 'why flex all-child-span1'}, children: [
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.why.whyMarXiv) }, children: [
            make.element({ tag: 'img', properties: { className: 'icon', src: context.linkToAsset(context.currentPage)(context.assets.images.questionCircle) }, children: []})
         ]})
      ]}),
      make.element({ tag: 'div', properties: { className: 'flex all-child-span1' }, children: [
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.splash) }, children: [
            make.element({ tag: 'img', properties: { className: 'icon', src: context.linkToAsset(context.currentPage)(context.assets.images.arrowCircleUp) }, children: []})
         ]})
      ]}),
      make.element({ tag: 'div', properties: { className: 'flex all-child-span1' }, children: [
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.works.workListing) }, children: [
            make.element({ tag: 'img', properties: { className: 'icon', src: context.linkToAsset(context.currentPage)(context.assets.images.search) }, children: []})
         ]})
      ]})
   ]}),
   // Text
   make.element({ tag: 'div', properties: { className: 'textWrapper flex all-parent-row center' }, children: [
      make.element({ tag: 'div', properties: { className: 'why flex all-child-span1' }, children: [
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.why.whyMarXiv) }, children: [
            make.text(d.why.title)
         ]})
      ]}),
      make.element({ tag: 'div', properties: { className: 'submit flex all-child-span1' }, children: [
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.splash) }, children: [
            make.text(d.submit.title)
         ]})
      ]}),
      make.element({ tag: 'div', properties: { className: 'find flex all-child-span1' }, children: [
         make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.works.workListing) }, children: [
            make.text(d.find.title)
         ]})
      ]})
   ]})
]});

export const makeBelowHeaderDesktop = (context: MarXivDataWithoutSitemap) => (d: HeaderText): HastElement => make.element({ tag: 'div', properties: { className: 'home desktopInfo flex all-parent-row' }, children: [
   // Why
   make.element({ tag: 'div', properties: { className: 'wrapper flex all-child-span1' }, children: [
      make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.why.whyMarXiv), className: 'normalBodyLink center' }, children: [
         make.element({ tag: 'div', properties: { className: 'orangeButton button orange' }, children: [
            make.text(d.why.title)
         ]}),
         make.element({ tag: 'div', properties: { className: 'text noBreak' }, children: [
            make.text(d.why.desktopSubtitle)
         ]})
      ]})
   ]}),
   // Submit
   make.element({ tag: 'div', properties: { className: 'wrapper flex all-child-span1' }, children: [
      make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.share.splash), className: 'normalBodyLink center' }, children: [
         make.element({ tag: 'div', properties: { className: 'orangeButton button orange' }, children: [
            make.text(d.submit.title)
         ]}),
         make.element({ tag: 'div', properties: { className: 'text noBreak' }, children: [
            make.text(d.submit.desktopSubtitle)
         ]})
      ]})
   ]}),
   // Find
   make.element({ tag: 'div', properties: { className: 'wrapper flex all-child-span1' }, children: [
      make.element({ tag: 'a', properties: { href: makeLocalPath(context.requestedLanguage)(context.minimalSitemap.works.workListing), className: 'normalBodyLink center' }, children: [
         make.element({ tag: 'div', properties: { className: 'orangeButton button orange' }, children: [
            make.text(d.find.title)
         ]}),
         make.element({ tag: 'div', properties: { className: 'text noBreak' }, children: [
            make.text(d.find.desktopSubtitle)
         ]})
      ]})
   ]}),
]})

export const makeUpperHeader = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { id: 'upperHeader', className: 'menusWrapper flex all-parent-row' }, children: [
   // Main menu
   make.element({ tag: 'div', properties: { className: 'headerMainMenu flex all-child-span1 all-child-first tablet-landscape-child-second' }, children: [
      makeMainMenu(context)(d.mainMenuText)
   ]}),
   // Logo
   make.element({ tag: 'div', properties: { className: 'headerLogo flex all-child-span1 all-child-second tablet-landscape-child-first', id: 'marxivLogoWrapper' }, children: [
      makeLogo(context)
   ]}),
   // User menu
   make.element({ tag: 'div', properties: { className: 'headerUserMenu flex all-child-span1 all-child-third right', id: 'shareMenuWrapper' }, children: [
      makeUserMenu(context)(d.mainMenuText)
   ]})
]})

export const makeSubtitle = (d: HeaderText) => (submenu: SubmenuType): HastElement[] => {
   if (submenu === 'none') {
      return [
         make.element({ tag: 'div', properties: { className: 'about header flex all-child-first' }, children: [
            make.text(d.subtitle)
         ]}),
         make.element({ tag: 'div', properties: { className: 'about subheader flex all-child-second' }, children: [
            make.text(d.producedBy)
         ]}),
         make.element({ tag: 'div', properties: { className: 'about desktop flex all-child-second' }, children: [
            make.text(d.subtitleProduced)
         ]})
      ]
   } else {
      return []
   }
}

export const makeOctoLogo = (context: MarXivDataWithoutSitemap) => (submenu: SubmenuType): HastElement[] => {
   if (submenu === 'none') {
      return [
         make.element({ tag: 'img', properties: { src: context.linkToAsset(context.currentPage)(context.assets.images.octoWhite), className: 'octoIcon' }, children: []})
      ]
   }
   else {
      return []
   }
}

export const makeLowerHeaderCommon = (context: MarXivDataWithoutSitemap) => (d: HeaderText) => (submenu: SubmenuType): HastElement[] => [
   make.element({ tag: 'div', properties: { id: 'headerWaves' }, children: []}),
   make.element({ tag: 'div', properties: { id: 'lowerMenu', className: 'flex all-parent-row' }, children: [
      make.element({ tag: 'div', properties: {className: 'octoIconWrapper flex all-child-span1'}, children: makeOctoLogo(context)(submenu) }),
      make.element({ tag: 'div', properties: { className: 'aboutWrapper flex all-parent-column tablet-landscape-parent-row all-child-span5' }, children: makeSubtitle(d)(submenu) })
   ]})
]

/**
 * Headers for everywhere but the homepage.
 */
export const makeLowerHeader = (context: MarXivDataWithoutSitemap) => (d: HeaderText) => (submenu: SubmenuType): HastElement => make.element({ tag: 'div', properties: { id: 'lowerHeader'}, children: makeLowerHeaderCommon(context)(d)(submenu) })

export const makeHeader = (context: MarXivDataWithoutSitemap) => (d: Translatables) => (submenu: SubmenuType): HastElement => {
   if (submenu === 'none') {
      return make.element({ tag: 'div', properties: { id: 'header' }, children: [
         makeUpperHeader(context)(d),
         makeLowerHeader(context)(d.headerText)(submenu)
      ]})
   }
   else {
      return make.element({ tag: 'div', properties: { id: 'header' }, children: [
         makeUpperHeader(context)(d),
         makeLowerHeader(context)(d.headerText)(submenu),
         makeSubmenu(context)(d)(submenu)
      ]})
   }
}

/**
 * Homepage headers
 */
export const makeLowerHeaderHome = (context: MarXivDataWithoutSitemap) => (d: HeaderText): HastElement => make.element({ tag: 'div', properties: { id: 'lowerHeader' }, children: [
   ...makeLowerHeaderCommon(context)(d)('none'),
   makeBelowHeaderMobile(context)(d),
   makeBelowHeaderDesktop(context)(d)
]})

export const makeHeaderHome = (context: MarXivDataWithoutSitemap) => (d: Translatables): HastElement => make.element({ tag: 'div', properties: { id: 'header' }, children: [
   makeUpperHeader(context)(d),
   makeLowerHeaderHome(context)(d.headerText),
]})